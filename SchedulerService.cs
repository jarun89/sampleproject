using APSSubmission.Repository;
using APSSubmission.Service.Interface;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using APSSubmission.Models.ViewModels;
using System.Threading.Tasks;
using System.IO;
using APSSubmission.Models.Constants;
using System.Web;
using System.Xml;
using System.Linq;
using APSSubmission.Models.EntityModels;
using APSSubmission.Models.Enums;
using APSSubmission.Repository.Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;
using System.Net.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Renci.SshNet;
using System.Xml.Serialization;
using System.Data;
using System.IO.Compression;
using Cinchoo.PGP;
using System.Net.Http.Headers;
using System.Net;
using System.Threading;
using ClosedXML.Excel;
using MimeKit;
using MailKit.Net.Smtp;
using System.Globalization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using FastMember;
using System.Text.RegularExpressions;

namespace APSSubmission.Service.Service
{
    public class SchedulerService : ISchedulerService
    {
        private readonly IConfiguration _configuration;
        private readonly APSDBContext _context;
        private readonly IMapper _mapper;
        private readonly static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private readonly APSCommonContext _commonContext;
        private readonly IWebHostEnvironment _env;
        private readonly UserManager<User> _userManager;
        private static bool _isMailAlreadySent = false;
        private string _rootDirectoryPath = null;
        private string _logFilePath = null;
        private string _archiveFolderPath = null;
        private DateTime PrevDate;

        public SchedulerService(APSDBContext context, IConfiguration configuration, IMapper mapper, IWebHostEnvironment env, APSCommonContext commonContext, UserManager<User> userManager)
        {
            _configuration = configuration;
            _mapper = mapper;
            _context = context;
            _commonContext = commonContext;
            _env = env;
            _userManager = userManager;
        }

        public ResponseBase CopyResultsToSFTPFolder()
        {
            _logger.Info("Copy Results To SFTPFolder  Started");
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;

            try
            {
                List<AuthDocumentViewModel> authDocuments = null;
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = "SELECT * FROM [dbo].[CompanyDetail]";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }


                if (companyDetails != null && companyDetails.Any())
                {
                    authDocuments = new List<AuthDocumentViewModel>();
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];

                            string[] companies = _configuration.GetSection("CopyResultsToSFTPFolderCompanies").Get<string[]>();
                            var companyCode = _configuration.GetSection("OutboundCompanyCode").Get<string>();
                            companies = companies.Select(c => c.ToLower()).ToArray();
                            if (companies.Contains(companyDetail.CompanyCode.ToLower()))
                            {
                                switch (companyDetail.CompanyCode.ToLower())
                                {
                                    case "anico":
                                        {
                                            string connectionString = companyDetail.ConnectionString;
                                            string selectSqlQuery = $@"SELECT DISTINCT OrderFacility.eNoahOrderId AS ENoahOrderId, Patient.PolicyNumber AS PolicyNumber, OrderingOffice.OfficeCode AS OfficeCode, OrderFacility.DoctorId AS DoctorId, AuthDocument.Id AS Id, AuthDocument.Path As Path, AuthDocument.DocumentName As DocumentName, AuthDocument.Title AS Title, AuthDocument.DocumentType AS DoumentType
                                            ,OrderFacility.ParentId AS ParentId, OrderFacility.ParentEnoahOrderId AS ParentEnoahOrderId
                                            FROM [dbo].[OrderFacility] AS OrderFacility
                                            INNER JOIN [dbo].[Order] As O ON O.Id = OrderFacility.OrderId
                                            INNER JOIN [dbo].[Patient] AS Patient ON Patient.Id = O.PatientId
                                            INNER JOIN [dbo].[OrderOfficeInformation] AS OrderingOffice ON OrderingOffice.Id = O.OrderOfficeId
                                            INNER JOIN [dbo].[OrderStatus] As OrderStatus ON OrderStatus.FacilityMapId = OrderFacility.Id
                                            INNER JOIN [dbo].[AuthDocument] AS AuthDocument ON AuthDocument.OrderFacilityId = OrderFacility.Id AND AuthDocument.IsFromExpertTool = 1 AND AuthDocument.IsSavedExternally = 0 AND AuthDocument.IsDeleted = 0 AND AuthDocument.StatusFlag != 1
                                            WHERE OrderFacility.IsDeleted = 0";

                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                                                SqlDataReader sqlDataReader = command.ExecuteReader();

                                                while (sqlDataReader.Read())
                                                {
                                                    AuthDocumentViewModel authDocument = new AuthDocumentViewModel();

                                                    var eNoahOrderId = sqlDataReader.GetValue(0);
                                                    var policyNumber = sqlDataReader.GetValue(1);
                                                    var orderingOfficeCode = sqlDataReader.GetValue(2);
                                                    var doctorId = sqlDataReader.GetValue(3);
                                                    var id = sqlDataReader.GetValue(4);
                                                    var path = sqlDataReader.GetValue(5);
                                                    var documentName = sqlDataReader.GetValue(6);
                                                    var title = sqlDataReader.GetValue(7);
                                                    var documentType = sqlDataReader.GetValue(8);
                                                    var parentIdObject = sqlDataReader.GetValue(9);
                                                    var parentEnoahOrderId = sqlDataReader.GetValue(10);

                                                    if (doctorId != null)
                                                    {
                                                        bool hasMatchedDoctor = false;
                                                        if (companyDetail.CompanyType.ToUpper() == CompanyType.LIFE.ToString())
                                                        {
                                                            hasMatchedDoctor = _commonContext.DoctorCommon.Any(d => d.Id == Convert.ToInt32(doctorId.ToString()));
                                                        }
                                                        else
                                                        {
                                                            hasMatchedDoctor = _commonContext.CustodianCommon.Any(d => d.Id == Convert.ToInt32(doctorId.ToString()));
                                                        }
                                                        _logger.Info(companyDetail.CompanyType);
                                                        _logger.Info(CompanyType.LIFE.ToString());
                                                        _logger.Info(doctorId.ToString());
                                                        if (hasMatchedDoctor)
                                                        {
                                                            if (eNoahOrderId != null)
                                                            {
                                                                if (parentIdObject != null)
                                                                {
                                                                    bool successfullyParsed = int.TryParse(parentIdObject.ToString(), out int parentId);

                                                                    if (successfullyParsed && parentId > 0 && !string.IsNullOrEmpty(parentEnoahOrderId.ToString()) && parentEnoahOrderId.ToString().ToLower() != "null")
                                                                    {
                                                                        eNoahOrderId = parentEnoahOrderId;
                                                                    }
                                                                }
                                                                authDocument.eNoahOrderId = eNoahOrderId.ToString();
                                                            }

                                                            if (policyNumber != null)
                                                            {
                                                                string policyNumberString = policyNumber.ToString();
                                                                if (!string.IsNullOrEmpty(policyNumberString))
                                                                {
                                                                    string decryptPolicyNumber = Support.Decrypt(policyNumberString);
                                                                    authDocument.eNoahOrderId = decryptPolicyNumber;
                                                                }
                                                            }

                                                            if (orderingOfficeCode != null)
                                                            {
                                                                string officeCode = orderingOfficeCode.ToString();

                                                                if (officeCode.Length > 2)
                                                                {
                                                                    officeCode = officeCode.Substring(1);
                                                                }
                                                                authDocument.Description = officeCode;
                                                            }

                                                            if (id != null)
                                                            {
                                                                authDocument.Id = Convert.ToInt32(id.ToString());
                                                            }

                                                            if (path != null)
                                                            {
                                                                authDocument.Path = path.ToString();
                                                            }

                                                            if (documentName != null)
                                                            {
                                                                authDocument.DocumentName = documentName.ToString();
                                                            }

                                                            if (title != null)
                                                            {
                                                                authDocument.Title = title.ToString();
                                                            }

                                                            if (documentType != null)
                                                            {
                                                                authDocument.DocumentType = documentType.ToString();
                                                            }

                                                            authDocuments.Add(authDocument);
                                                        }
                                                        else
                                                        {
                                                            _logger.Error("Facility not matched");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _logger.Error("Doctor id not found");
                                                    }
                                                }
                                                connection.Close();
                                            }

                                            if (authDocuments != null && authDocuments.Any())
                                            {
                                                foreach (AuthDocumentViewModel authDocument in authDocuments)
                                                {
                                                    try
                                                    {
                                                        UploadFileRequest request = new UploadFileRequest
                                                        {
                                                            CompanyCode = authDocument.Path,
                                                            UserId = authDocument.DocumentName
                                                        };
                                                        DocDownloadResponse docDownloadResponse = GetFileStreamAsync(request).Result;

                                                        if (docDownloadResponse != null)
                                                        {
                                                            int lastIndexValue = docDownloadResponse.Name.LastIndexOf(".");
                                                            string fileExtension = docDownloadResponse.Name.Substring(lastIndexValue + 1);
                                                            string randomNumber = GetUniqueTxnId();
                                                            string formattedFileName = $"{authDocument.Description}{authDocument.eNoahOrderId}_APS_{DateTime.Now:yyyyMMdd}_{randomNumber}.{fileExtension}";

                                                            string sftpHostUrl = string.Empty;
                                                            int sftpPortNumber = 0;
                                                            string sftpUserName = string.Empty;
                                                            string sftpPassword = string.Empty;
                                                            string sftpFolderPath = string.Empty;
                                                            string sftpArchiveFolderPath = string.Empty;
                                                            //
                                                            using (var myConnection = new SqlConnection(defaultConnectionString))
                                                            {
                                                                var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='anico' AND IsInbound=0";
                                                                var oCmd = new SqlCommand(query, myConnection);
                                                                myConnection.Open();
                                                                var oReader = oCmd.ExecuteReader();
                                                                while (oReader.Read())
                                                                {
                                                                    sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                                                                    sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                                                                    sftpUserName = Convert.ToString(oReader["UserName"]);
                                                                    sftpPassword = Convert.ToString(oReader["Password"]);
                                                                    sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                                                    sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                                                }
                                                                oReader.Close();
                                                                myConnection.Close();
                                                            }
                                                            //
                                                            string remoteFilePath = sftpFolderPath + formattedFileName;
                                                            string remoteFileArchivePath = sftpArchiveFolderPath + formattedFileName;
                                                            using var client = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword);
                                                            try
                                                            {
                                                                _logger.Info($"anico - SFTP connection establishment has started {sftpHostUrl}");
                                                                client.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                                client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                                client.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                                client.Connect();
                                                                _logger.Info($"anico - SFTP connection establishment was successful {sftpHostUrl}");
                                                                client.WriteAllBytes(remoteFilePath, docDownloadResponse.Bytes);
                                                                client.WriteAllBytes(remoteFileArchivePath, docDownloadResponse.Bytes);
                                                                _logger.Info($"File write sftp to {remoteFilePath}");
                                                                _logger.Info($"File write sftp to {remoteFileArchivePath}");
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                _logger.Info($"anico - sftp File {remoteFilePath} NOT copied successfully");
                                                                _logger.Error(ex);
                                                            }
                                                            finally
                                                            {
                                                                client.Disconnect();
                                                                _logger.Info($"anico - The SFTP connection was disconnected {sftpHostUrl}");
                                                            }

                                                            string path = _configuration["OrderResultsFolderPath"];
                                                            filePath = $"{path}{companyDetail.CompanyCode}/";
                                                            string fullPath = "";

                                                            fullPath = filePath + formattedFileName;

                                                            if (Directory.Exists(filePath))
                                                            {
                                                                File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                            }
                                                            else
                                                            {
                                                                DirectoryInfo di = Directory.CreateDirectory(filePath);
                                                                File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                            }
                                                            _logger.Info($"anico - File {fullPath} copied successfully");
                                                            string updateSqlQuery = $"UPDATE [dbo].[AuthDocument] SET IsSavedExternally = 1, ModifiedDateTime = GETDATE() WHERE Id = {authDocument.Id}";
                                                            using (SqlConnection con = new SqlConnection(connectionString))
                                                            {
                                                                con.Open();
                                                                SqlCommand cmd = new SqlCommand(updateSqlQuery, con);
                                                                cmd.ExecuteNonQuery();
                                                                con.Close();
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        _logger.Error(ex);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _logger.Error("Results not found");
                                            }
                                        }
                                        break;
                                    case "koc":
                                        {
                                            string connectionString = companyDetail.ConnectionString;
                                            string selectSqlQuery = $@"SELECT DISTINCT Patient.FirstName, Patient.LastName,Patient.DateOfBirth,Patient.PolicyNumber,AuthDocument.Id, AuthDocument.Path, AuthDocument.DocumentName
                                                        FROM [dbo].[OrderFacility] AS OrderFacility
                                                        INNER JOIN [dbo].[Order] As O ON O.Id = OrderFacility.OrderId
                                                        INNER JOIN [dbo].[Patient] AS Patient ON Patient.Id = O.PatientId
                                                        INNER JOIN [dbo].[OrderOfficeInformation] AS OrderingOffice ON OrderingOffice.Id = O.OrderOfficeId
                                                        INNER JOIN [dbo].[OrderStatus] As OrderStatus ON OrderStatus.FacilityMapId = OrderFacility.Id
                                                        INNER JOIN [dbo].[AuthDocument] AS AuthDocument ON AuthDocument.OrderFacilityId = OrderFacility.Id AND AuthDocument.IsFromExpertTool = 1 AND AuthDocument.IsSavedExternally = 0 AND AuthDocument.IsDeleted = 0 AND AuthDocument.StatusFlag != 1
                                                        WHERE OrderFacility.IsDeleted = 0";
                                            var kocOutboundList = new List<KOCOutboundDetail>();
                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                                                SqlDataReader reader = command.ExecuteReader();
                                                while (reader.Read())
                                                {
                                                    var kocOutbound = new KOCOutboundDetail();
                                                    kocOutbound.Id = Convert.ToInt32(reader["Id"]);
                                                    kocOutbound.FirstName = Convert.ToString(reader["FirstName"]);
                                                    kocOutbound.LastName = Convert.ToString(reader["LastName"]);
                                                    kocOutbound.DateOfBirth = Convert.ToString(reader["DateOfBirth"]);
                                                    kocOutbound.PolicyNumber = Convert.ToString(reader["PolicyNumber"]);
                                                    kocOutbound.Path = Convert.ToString(reader["Path"]);
                                                    kocOutbound.DocumentName = Convert.ToString(reader["DocumentName"]);
                                                    kocOutboundList.Add(kocOutbound);
                                                }
                                                connection.Close();
                                            }

                                            var sftpHostUrl = "";
                                            var sftpPortNumber = 0;
                                            var sftpUserName = "";
                                            var sftpPassword = "";
                                            var sftpFolderPath = "";
                                            var sftpArchiveFolderPath = "";
                                            using (var myConnection = new SqlConnection(defaultConnectionString))
                                            {
                                                var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='KOC' AND IsInbound=0";
                                                var oCmd = new SqlCommand(query, myConnection);
                                                myConnection.Open();
                                                var oReader = oCmd.ExecuteReader();
                                                while (oReader.Read())
                                                {
                                                    sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                                                    sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                                                    sftpUserName = Convert.ToString(oReader["UserName"]);
                                                    sftpPassword = Convert.ToString(oReader["Password"]);
                                                    sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                                    sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                                }
                                                oReader.Close();
                                                myConnection.Close();
                                            }

                                            if (kocOutboundList == null || !kocOutboundList.Any())
                                            {
                                                _logger.Info("KOC - No results found for processing");
                                            }

                                            foreach (var item in kocOutboundList)
                                            {
                                                try
                                                {
                                                    var documentName = DateTime.Now.ToString("yyyyMMddhhmmssff") + "_" + item.DocumentName;
                                                    var transactionId = GetUniqueTxnId();
                                                    var kocXmlField = new AWDRIP();

                                                    var pDOB = DateTime.Now;
                                                    DateTime.TryParse(Support.Decrypt(item.DateOfBirth), out pDOB);
                                                    var patientDOB = string.IsNullOrEmpty(item.DateOfBirth) ? string.Empty : pDOB.ToString("MM/dd/yyyy");

                                                    var txnFieldList = new List<field> {
                                                    new field { name="UNIT", value="UNDWRTING" },
                                                    new field {name="WRKT", value="REQUIREMNT" },
                                                    new field { name="STAT", value="CREATED"},
                                                    new field { name="VEND", value="ENOAH" },
                                                    new field { name="TXID", value=transactionId },
                                                    new field { name="ANLN", value= string.IsNullOrEmpty(item.LastName)?"":Support.Decrypt(item.LastName) },
                                                    new field { name="ANFN", value=string.IsNullOrEmpty(item.FirstName)?"":Support.Decrypt(item.FirstName)},
                                                    new field { name="ANDB", value=patientDOB },
                                                    //new field { name="ANDB", value=string.IsNullOrEmpty(item.DateOfBirth)?"":(DateTime.ParseExact(Support.Decrypt(item.DateOfBirth), "yyyy-MM-dd hh:mm:ss.fff", new CultureInfo("en-US"), DateTimeStyles.None).Date).ToString("MM/dd/yyyy") },
                                                    new field {  name="POLN", value=string.IsNullOrEmpty(item.PolicyNumber)?"":Support.Decrypt(item.PolicyNumber)},
                                                    new field {  name="CYCD", value="01"}
                                                };
                                                    kocXmlField.transaction.kVPs.AddRange(txnFieldList);

                                                    var srcFieldList = new List<field> {
                                                    new field { name="UNIT", value="UNDWRTING" },
                                                    new field { name="OBJT", value="MAIL" },
                                                    new field { name="IDTP", value="11" },
                                                    new field { name="DOCT", value="APS" },
                                                    new field { name="SPD2", value="" },
                                                    new field { name="^SPT", value="N" }
                                                };
                                                    kocXmlField.transaction.source.kVPs.AddRange(srcFieldList);
                                                    kocXmlField.transaction.source.path.name = _configuration.GetSection("KOCXmlPath").Get<string>() + documentName;
                                                    var xmlStr = "";
                                                    var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                                                    var settings = new XmlWriterSettings();
                                                    settings.Indent = true;
                                                    settings.OmitXmlDeclaration = true;

                                                    using (var stream = new StringWriter())
                                                    using (var stringwriter = XmlWriter.Create(stream, settings))
                                                    {
                                                        var serializer = new XmlSerializer(kocXmlField.GetType());
                                                        serializer.Serialize(stringwriter, kocXmlField, emptyNamespaces);
                                                        xmlStr = stream.ToString();
                                                    }
                                                    xmlStr = xmlStr.Replace("<kVPs>", "").Replace("</kVPs>", "");

                                                    var request = new UploadFileRequest
                                                    {
                                                        CompanyCode = item.Path,
                                                        UserId = item.DocumentName
                                                    };
                                                    var docDownloadResponse = GetFileStreamAsync(request).Result;

                                                    if (docDownloadResponse != null)
                                                    {
                                                        using (var sftpclient = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                                                        {
                                                            try
                                                            {
                                                                _logger.Info($"KOC - SFTP connection establishment has started {sftpHostUrl}");
                                                                sftpclient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                                sftpclient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                                sftpclient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                                sftpclient.Connect();
                                                                _logger.Info($"KOC - Results - Successful connection to {sftpHostUrl}");
                                                                sftpclient.WriteAllText(sftpFolderPath + "eNoah_" + transactionId + ".xml", xmlStr);
                                                                sftpclient.WriteAllBytes(sftpFolderPath + documentName, docDownloadResponse.Bytes);
                                                                _logger.Info("KOC - XML file copied to " + sftpFolderPath + " eNoah." + transactionId + ".xml");
                                                                _logger.Info("KOC - Result copied to " + sftpFolderPath + documentName);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                _logger.Info($"KOC - sftp File {sftpFolderPath + documentName} NOT copied successfully");
                                                                _logger.Error(ex);
                                                            }
                                                            finally
                                                            {
                                                                sftpclient.Disconnect();
                                                                _logger.Info($"KOC - The SFTP connection was disconnected {sftpHostUrl}");
                                                            }
                                                        }

                                                        string path = _configuration["OrderResultsFolderPath"];
                                                        filePath = $"{path}{companyDetail.CompanyCode}/";
                                                        string fullPath = "";

                                                        fullPath = filePath + documentName;

                                                        if (Directory.Exists(filePath))
                                                        {
                                                            File.WriteAllText(filePath + "eNoah_" + transactionId + ".xml", xmlStr);
                                                            File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                        }
                                                        else
                                                        {
                                                            DirectoryInfo di = Directory.CreateDirectory(filePath);
                                                            File.WriteAllText(filePath + "eNoah." + transactionId + ".xml", xmlStr);
                                                            File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                        }
                                                        _logger.Info($"KOC - File {fullPath} copied successfully");
                                                        string updateSqlQuery = $"UPDATE [dbo].[AuthDocument] SET IsSavedExternally = 1, ModifiedDateTime = GETDATE() WHERE Id = {item.Id}";
                                                        using (SqlConnection con = new SqlConnection(connectionString))
                                                        {
                                                            con.Open();
                                                            SqlCommand cmd = new SqlCommand(updateSqlQuery, con);
                                                            cmd.ExecuteNonQuery();
                                                            con.Close();
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    _logger.Error(ex);
                                                }
                                            }
                                            if (kocOutboundList != null && kocOutboundList.Any())
                                            {
                                                using (var sftpclient = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                                                {
                                                    try
                                                    {
                                                        _logger.Info($"KOC - SFTP connection establishment has started {sftpHostUrl}");
                                                        sftpclient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                        sftpclient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                        sftpclient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                        sftpclient.Connect();
                                                        _logger.Info($"KOC - Trigger file - Successful connection to {sftpHostUrl}");
                                                        sftpclient.WriteAllText(sftpFolderPath + "ENOAHREADY.TRG", "");
                                                        _logger.Info("KOC - Trigger file copied to " + sftpFolderPath + "ENOAHREADY.TRG");
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        _logger.Error(ex);
                                                        _logger.Info($"KOC - sftp File {sftpFolderPath} ENOAHREADY.TRG NOT copied successfully");
                                                    }
                                                    finally
                                                    {
                                                        sftpclient.Disconnect();
                                                        _logger.Info($"KOC - The SFTP connection was disconnected {sftpHostUrl}");
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (carrier.ToLower() == "uhone" || carrier.ToLower() == "uhoneuw")
                            {
                                DeleteUHFiles(filePath);
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }
            _logger.Info("Copy Results To SFTPFolder Completed");
            return response;
        }

        public ResponseBase CopyResultsToSFTPFolder_UHONE()
        {
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;

            try
            {
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                string fName = string.Empty;
                int DocumentCounter = 0;
                int TotalDocumentCounter = 0;
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = "SELECT* FROM[dbo].[CompanyDetail]";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }


                if (companyDetails != null && companyDetails.Any())
                {
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];

                            string[] companies = _configuration.GetSection("CopyResultsToSFTPFolderCompanies").Get<string[]>();
                            var companyCode = _configuration.GetSection("OutboundCompanyCode").Get<string>();
                            companies = companies.Select(c => c.ToLower()).ToArray();
                            if (companies.Contains(companyDetail.CompanyCode.ToLower()))
                            {
                                switch (companyDetail.CompanyCode.ToLower())
                                {

                                    case "uhoneuw":
                                    case "uhone":
                                        {
                                            carrier = companyDetail.CompanyCode;
                                            using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                                            {
                                                myConnection.Open();
                                                string oString = "select DocCounterNo, TotalDocumentCounter from ResultsCounter Where CreatedDate = CONVERT(Date, GETDATE())";
                                                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                                var oReader = oCmd.ExecuteReader();
                                                while (oReader.Read())
                                                {
                                                    TotalDocumentCounter = Convert.ToInt32(oReader["TotalDocumentCounter"]) + 1;
                                                    DocumentCounter = Convert.ToInt32(oReader["DocCounterNo"]) + 1;
                                                }
                                                myConnection.Close();
                                            }
                                            if (DocumentCounter == 0)
                                            {
                                                DocumentCounter = 1;
                                            }
                                            if (TotalDocumentCounter == 0)
                                            {
                                                TotalDocumentCounter = 1;
                                            }
                                            string connectionString = companyDetail.ConnectionString;
                                            string selectSqlQuery = "EXEC [GetUhoneOrderDetails] @Action='GET', @AuthDocumentId=0";
                                            var uhOneOutboundList = new List<UHONEOutboundDetail>();

                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                                                SqlDataAdapter sda = new SqlDataAdapter(command);
                                                DataSet ds = new DataSet();
                                                sda.Fill(ds);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    for (int a = 0; a < ds.Tables[0].Rows.Count; a++)
                                                    {
                                                        string eNoahOrderId = "";
                                                        var parentIdObject = ds.Tables[0].Rows[a]["ParentId"];
                                                        var parentEnoahOrderIdObject = ds.Tables[0].Rows[a]["ParentEnoahOrderId"];

                                                        if (parentIdObject != null)
                                                        {
                                                            bool successfullyParsed = int.TryParse(parentIdObject.ToString(), out int parentId);

                                                            if (successfullyParsed && parentId > 0 && !string.IsNullOrEmpty(parentEnoahOrderIdObject.ToString()) && parentEnoahOrderIdObject.ToString().ToLower() != "null")
                                                            {
                                                                eNoahOrderId = Convert.ToString(parentEnoahOrderIdObject);
                                                            }
                                                            else
                                                            {
                                                                eNoahOrderId = Convert.ToString(ds.Tables[0].Rows[a]["eNoahOrderId"]);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            eNoahOrderId = Convert.ToString(ds.Tables[0].Rows[a]["eNoahOrderId"]);
                                                        }

                                                        var UhOneOutbound = new UHONEOutboundDetail();
                                                        UhOneOutbound.Id = Convert.ToInt32(ds.Tables[0].Rows[a]["Id"]);
                                                        UhOneOutbound.Carrier = Convert.ToString(ds.Tables[0].Rows[a]["Carrier"]);
                                                        UhOneOutbound.Department = Convert.ToString(ds.Tables[0].Rows[a]["Department"]);
                                                        UhOneOutbound.DocumentCounter = Convert.ToInt32(ds.Tables[0].Rows[a]["num_row"]);
                                                        UhOneOutbound.ReceivedDate = Convert.ToString(ds.Tables[0].Rows[a]["ReceivedDate"]);
                                                        UhOneOutbound.SubscriberID = Support.Decrypt(Convert.ToString(ds.Tables[0].Rows[a]["SubscriberID"]));
                                                        UhOneOutbound.MemberSuffix = Support.Decrypt(Convert.ToString(ds.Tables[0].Rows[a]["MemberSuffix"]));
                                                        UhOneOutbound.OrderNumber = eNoahOrderId;
                                                        UhOneOutbound.PageCount = Convert.ToString(ds.Tables[0].Rows[a]["PageCount"]);
                                                        UhOneOutbound.PDFFilename = Convert.ToString(ds.Tables[0].Rows[a]["DocumentName"]);
                                                        UhOneOutbound.System = Convert.ToString(ds.Tables[0].Rows[a]["System"]);
                                                        UhOneOutbound.TotalDocumentCount = Convert.ToString(ds.Tables[0].Columns.Count);
                                                        UhOneOutbound.Path = Convert.ToString(ds.Tables[0].Rows[a]["Path"]);
                                                        uhOneOutboundList.Add(UhOneOutbound);
                                                        fName = Convert.ToString(ds.Tables[0].Rows[a]["FileName"]);
                                                        DocumentCounter = DocumentCounter + 1;
                                                    }
                                                    connection.Close();

                                                    var sftpHostUrl = "";
                                                    var sftpPortNumber = 0;
                                                    var sftpUserName = "";
                                                    var sftpPassword = "";
                                                    var sftpFolderPath = "";
                                                    var sftpArchiveFolderPath = "";
                                                    using (var myConnection = new SqlConnection(defaultConnectionString))
                                                    {
                                                        var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='UHONE' AND IsInbound=0";
                                                        var oCmd = new SqlCommand(query, myConnection);
                                                        myConnection.Open();
                                                        var oReader = oCmd.ExecuteReader();
                                                        while (oReader.Read())
                                                        {
                                                            sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                                                            sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                                                            sftpUserName = Convert.ToString(oReader["UserName"]);
                                                            sftpPassword = Convert.ToString(oReader["Password"]);
                                                            sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                                            sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                                        }
                                                        oReader.Close();
                                                        myConnection.Close();
                                                    }


                                                    var uhoneXmlField = new UHONEXmlFields();
                                                    uhoneXmlField.ExportDateTime = DateTime.Now.ToString("yyyyMMddhhmmss");
                                                    string path = _configuration["ZipOrderResultsFolderPath"];
                                                    filePath = $"{path}{companyDetail.CompanyCode}//";
                                                    int failedCount = 0;
                                                    foreach (var item in uhOneOutboundList)
                                                    {
                                                        try
                                                        {
                                                            var documentName = item.OrderNumber + "_" + item.Id + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";
                                                            var request = new UploadFileRequest
                                                            {
                                                                CompanyCode = item.Path,
                                                                UserId = item.PDFFilename

                                                            };
                                                            var docDownloadResponse = GetFileStreamAsync(request).Result;
                                                            if (docDownloadResponse != null)
                                                            {
                                                                File.WriteAllBytes(filePath + documentName, docDownloadResponse.Bytes);

                                                                var document = new Document
                                                                {
                                                                    Carrier = item.Carrier,
                                                                    Department = item.Department,
                                                                    DocumentCounter = item.DocumentCounter - failedCount,
                                                                    PageCount = item.PageCount,
                                                                    AgentName = "",
                                                                    ReceivedDate = item.ReceivedDate,
                                                                    SubscriberID = item.SubscriberID,
                                                                    MemberSuffix = item.MemberSuffix,
                                                                    OrderNumber = item.OrderNumber,
                                                                    PDFFilename = documentName,
                                                                    System = item.System

                                                                };
                                                                uhoneXmlField.Documents.Add(document);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            failedCount++;
                                                            _logger.Info(companyDetail.CompanyCode.ToLower() + " Image packing failed");
                                                            _logger.Info(ex);
                                                        }
                                                    }
                                                    uhoneXmlField.TotalDocumentCount = uhOneOutboundList.Count - failedCount;
                                                    //EncryptStringToPGPfile(xmlStr.ToString(), fileName, publicKey.ToString(), filePath, companyDetail.CompanyCode);
                                                    var xmlStr = "";
                                                    var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                                                    var settings = new XmlWriterSettings();
                                                    settings.Indent = true;
                                                    settings.OmitXmlDeclaration = true;

                                                    using (var stream = new StringWriter())
                                                    using (var stringwriter = XmlWriter.Create(stream, settings))
                                                    {
                                                        var serializer = new XmlSerializer(uhoneXmlField.GetType());
                                                        serializer.Serialize(stringwriter, uhoneXmlField, emptyNamespaces);
                                                        xmlStr = stream.ToString();
                                                    }
                                                    xmlStr = xmlStr.Replace("<Documents>", "").Replace("</Documents>", "");
                                                    xmlStr = xmlStr.Replace("<UHONEXmlFields>", "<Export>").Replace("</UHONEXmlFields>", "</Export>");

                                                    var fileName = fName + DateTime.Now.ToString("yyyyMMddhhmmss");
                                                    var zipfileName = "UHONE_ENOAH_MEDREC_" + DateTime.Now.ToString("yyyyMMdd") + "_" + TotalDocumentCounter;
                                                    if (Directory.Exists(filePath))
                                                    {
                                                        File.WriteAllText(filePath + fileName + ".xml", xmlStr);
                                                    }
                                                    else
                                                    {
                                                        DirectoryInfo d1 = Directory.CreateDirectory(filePath);
                                                        File.WriteAllText(filePath + fileName + ".xml", xmlStr);
                                                    }


                                                    string orderpath = _configuration["OrderResultsFolderPath"];
                                                    string orderfilePath = $"{orderpath}{companyDetail.CompanyCode}/";

                                                    if (Directory.Exists(orderfilePath))
                                                    {
                                                        if (File.Exists(orderfilePath + zipfileName + ".zip"))
                                                        {
                                                            File.Delete(orderfilePath + zipfileName + ".zip");
                                                        }
                                                        ZipFile.CreateFromDirectory(filePath, orderfilePath + zipfileName + ".zip", CompressionLevel.Optimal, true);
                                                        _logger.Info(companyDetail.CompanyCode.ToLower() + $" zip file created {fileName}");
                                                    }
                                                    else
                                                    {
                                                        DirectoryInfo di = Directory.CreateDirectory(orderfilePath);
                                                        ZipFile.CreateFromDirectory(filePath, orderfilePath + zipfileName + ".zip", CompressionLevel.Optimal, true);
                                                        _logger.Info(companyDetail.CompanyCode.ToLower() + $" zip file created {fileName}");
                                                    }

                                                    DirectoryInfo dd = new DirectoryInfo(filePath);
                                                    foreach (FileInfo file in dd.GetFiles())
                                                    {
                                                        file.Delete();
                                                    }

                                                    using (var client = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                                                    {
                                                        try
                                                        {
                                                            client.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                            client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                            client.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                            client.Connect();
                                                            _logger.Info(companyDetail.CompanyCode.ToLower() + $" - Results - Successful connection to {sftpHostUrl}");

                                                            client.ChangeDirectory(sftpFolderPath);
                                                            _logger.Info(companyDetail.CompanyCode.ToLower() + $" - Results - Changed directory to {sftpFolderPath}");

                                                            using (var fileStream = new FileStream(orderfilePath + zipfileName + ".zip", FileMode.Open))
                                                            {
                                                                Console.WriteLine("Uploading {0} ({1:N0} bytes)", orderfilePath + zipfileName + ".zip", fileStream.Length);
                                                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                                                client.UploadFile(fileStream, Path.GetFileName(orderfilePath + zipfileName + ".zip"));
                                                                _logger.Info(companyDetail.CompanyCode.ToLower() + " - Zip file copied to " + sftpFolderPath + "/" + zipfileName + ".zip");
                                                                fileStream.Dispose();
                                                            }
                                                            foreach (var item in uhOneOutboundList)
                                                            {
                                                                string updateSqlQuery = "EXEC [GetUhoneOrderDetails] @Action='UPDATE', @AuthDocumentId=" + item.Id + "";
                                                                using (SqlConnection con = new SqlConnection(connectionString))
                                                                {
                                                                    con.Open();
                                                                    SqlCommand cmd = new SqlCommand(updateSqlQuery, con);
                                                                    cmd.ExecuteNonQuery();
                                                                    con.Close();
                                                                }
                                                            }
                                                            string updateCountQuery = "UPDATE [dbo].[ResultsCounter] SET DocCounterNo =" + (DocumentCounter - 1) + ", TotalDocumentCounter =" + TotalDocumentCounter + ", CreatedDate = convert(date, getdate())  Where ID = 1";
                                                            using (SqlConnection con = new SqlConnection(defaultConnectionString))
                                                            {
                                                                con.Open();
                                                                SqlCommand cmd = new SqlCommand(updateCountQuery, con);
                                                                cmd.ExecuteNonQuery();
                                                                con.Close();
                                                            }
                                                        }

                                                        catch (Exception ex)
                                                        {
                                                            _logger.Error(ex);
                                                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                                            helper.SendUhoneStatusFailedToSentMail("SFTP", ex.Message, companyDetail.CompanyCode);
                                                        }
                                                        finally
                                                        {
                                                            client.Disconnect();
                                                        }
                                                    }
                                                }
                                                else { _logger.Info(companyDetail.CompanyCode.ToLower() + " - No results found for processing"); }
                                            }

                                        }
                                        break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            DeleteUHFiles(filePath);
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }
            return response;
        }

        public ResponseBase CopyResultsToSFTPFolder_NavyMutual()
        {
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;

            try
            {
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                string fName = string.Empty;
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = "SELECT* FROM[dbo].[CompanyDetail]";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }


                if (companyDetails != null && companyDetails.Any())
                {
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];

                            string[] companies = _configuration.GetSection("CopyResultsToSFTPFolderCompanies").Get<string[]>();
                            var companyCode = _configuration.GetSection("OutboundCompanyCode").Get<string>();
                            companies = companies.Select(c => c.ToLower()).ToArray();
                            if (companies.Contains(companyDetail.CompanyCode.ToLower()))
                            {
                                switch (companyDetail.CompanyCode.ToLower())
                                {

                                    case "navy":
                                        {
                                            string connectionString = companyDetail.ConnectionString;
                                            string selectSqlQuery = "EXEC [GetNavyOrderDetails] @Action='GET', @AuthDocumentId=0";
                                            var navyOutboundList = new List<NavyOutBoundDetails>();

                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                                                SqlDataAdapter sda = new SqlDataAdapter(command);
                                                DataSet ds = new DataSet();
                                                sda.Fill(ds);
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    for (int a = 0; a < ds.Tables[0].Rows.Count; a++)
                                                    {
                                                        string eNoahOrderId = "";
                                                        var parentIdObject = ds.Tables[0].Rows[a]["ParentId"];
                                                        var parentEnoahOrderIdObject = ds.Tables[0].Rows[a]["ParentEnoahOrderId"];

                                                        if (parentIdObject != null)
                                                        {
                                                            bool successfullyParsed = int.TryParse(parentIdObject.ToString(), out int parentId);

                                                            if (successfullyParsed && parentId > 0 && !string.IsNullOrEmpty(parentEnoahOrderIdObject.ToString()) && parentEnoahOrderIdObject.ToString().ToLower() != "null")
                                                            {
                                                                eNoahOrderId = Convert.ToString(parentEnoahOrderIdObject);
                                                            }
                                                            else
                                                            {
                                                                eNoahOrderId = Convert.ToString(ds.Tables[0].Rows[a]["eNoahOrderId"]);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            eNoahOrderId = Convert.ToString(ds.Tables[0].Rows[a]["eNoahOrderId"]);
                                                        }

                                                        var NavyOutbound = new NavyOutBoundDetails();
                                                        NavyOutbound.DateTimeStamp = Convert.ToString(ds.Tables[0].Rows[a]["DateTimeStamp"]);
                                                        NavyOutbound.PolicyNumber = Support.Decrypt(Convert.ToString(ds.Tables[0].Rows[a]["CP_CONT"]));
                                                        NavyOutbound.OtherField1 = Support.Decrypt(Convert.ToString(ds.Tables[0].Rows[a]["CR_MEMBER_TAX_ID"]));
                                                        NavyOutbound.FileName = Convert.ToString(ds.Tables[0].Rows[a]["FileName"]);
                                                        NavyOutbound.DocType = Convert.ToString(ds.Tables[0].Rows[a]["DocType"]);
                                                        NavyOutbound.SubType = Convert.ToString(ds.Tables[0].Rows[a]["SubType"]);
                                                        NavyOutbound.Author = Convert.ToString(ds.Tables[0].Rows[a]["Author"]);
                                                        NavyOutbound.eNoahOrderId = eNoahOrderId;
                                                        NavyOutbound.PdfFileName = Convert.ToString(ds.Tables[0].Rows[a]["DocumentName"]);
                                                        NavyOutbound.Path = Convert.ToString(ds.Tables[0].Rows[a]["Path"]);
                                                        NavyOutbound.Id = Convert.ToInt32(ds.Tables[0].Rows[a]["Id"]);

                                                        navyOutboundList.Add(NavyOutbound);
                                                    }
                                                    connection.Close();

                                                    var sftpHostUrl = "";
                                                    var sftpPortNumber = 0;
                                                    var sftpUserName = "";
                                                    var sftpPassword = "";
                                                    var sftpFolderPath = "";
                                                    var sftpArchiveFolderPath = "";
                                                    using (var myConnection = new SqlConnection(defaultConnectionString))
                                                    {
                                                        var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='navy' AND IsInbound = 0 AND Type ='External'";
                                                        var oCmd = new SqlCommand(query, myConnection);
                                                        myConnection.Open();
                                                        var oReader = oCmd.ExecuteReader();
                                                        while (oReader.Read())
                                                        {
                                                            sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                                                            sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                                                            sftpUserName = Convert.ToString(oReader["UserName"]);
                                                            sftpPassword = Convert.ToString(oReader["Password"]);
                                                            sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                                            sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                                        }
                                                        oReader.Close();
                                                        myConnection.Close();
                                                    }

                                                    var xmlStr = "";
                                                    var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
                                                    var settings = new XmlWriterSettings();
                                                    settings.Indent = true;
                                                    settings.OmitXmlDeclaration = true;
                                                    string path = _configuration["OrderResultsFolderPath"];
                                                    filePath = $"{path}{companyDetail.CompanyCode}//";
                                                    var navyXmlField = new NavyXmlFields();
                                                    foreach (var item in navyOutboundList)
                                                    {
                                                        var documentName = item.eNoahOrderId + "_" + DateTime.Now.ToString("yyyyMMddhhmmss");
                                                        var policydetail = new PolicyDetails
                                                        {
                                                            DateTimeStamp = item.DateTimeStamp,
                                                            FileName = documentName + ".pdf",
                                                            DocType = item.DocType,
                                                            SubType = item.SubType,
                                                            Author = item.Author,
                                                            CR_MEMBER_TAX_ID = item.OtherField1,
                                                            CP_CONT = item.PolicyNumber,

                                                        };
                                                        navyXmlField.PolicyDetails = policydetail;


                                                        using (var stream = new StringWriter())
                                                        using (var stringwriter = XmlWriter.Create(stream, settings))
                                                        {
                                                            var serializer = new XmlSerializer(navyXmlField.GetType());
                                                            serializer.Serialize(stringwriter, navyXmlField, emptyNamespaces);
                                                            xmlStr = stream.ToString();
                                                        }
                                                        xmlStr = xmlStr.Replace("<NavyXmlFields>", "<MessageDetails xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">").Replace("</NavyXmlFields>", "</MessageDetails>");

                                                        try
                                                        {
                                                            var request = new UploadFileRequest
                                                            {
                                                                CompanyCode = item.Path,
                                                                UserId = item.PdfFileName

                                                            };
                                                            var docDownloadResponse = GetFileStreamAsync(request).Result;

                                                            if (docDownloadResponse != null)
                                                            {
                                                                var fileName = documentName;

                                                                if (Directory.Exists(filePath))
                                                                {
                                                                    File.WriteAllText(filePath + fileName + ".xml", xmlStr);
                                                                    File.WriteAllBytes(filePath + documentName + ".pdf", docDownloadResponse.Bytes);
                                                                }
                                                                else
                                                                {
                                                                    DirectoryInfo d1 = Directory.CreateDirectory(filePath);
                                                                    File.WriteAllText(filePath + fileName + ".xml", xmlStr);
                                                                    File.WriteAllBytes(filePath + documentName + ".pdf", docDownloadResponse.Bytes);
                                                                }

                                                                using (var client = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                                                                {
                                                                    try
                                                                    {
                                                                        client.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                                        client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                                        client.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                                        client.Connect();
                                                                        _logger.Info($"Navy - Results - Successful connection to {sftpHostUrl}");
                                                                        client.WriteAllText(sftpFolderPath + documentName + ".xml", xmlStr);
                                                                        client.WriteAllBytes(sftpFolderPath + documentName + ".pdf", docDownloadResponse.Bytes);
                                                                        _logger.Info("Navy - XML file copied to " + sftpFolderPath + documentName + ".xml");
                                                                        _logger.Info("Navy - Result copied to " + sftpFolderPath + documentName + ".pdf");
                                                                        client.WriteAllText(sftpArchiveFolderPath + documentName + ".xml", xmlStr);
                                                                        client.WriteAllBytes(sftpArchiveFolderPath + documentName + ".pdf", docDownloadResponse.Bytes);
                                                                        _logger.Info("Navy - XML file copied to " + sftpArchiveFolderPath + documentName + ".xml");
                                                                        _logger.Info("Navy - Result copied to " + sftpArchiveFolderPath + documentName + ".pdf");


                                                                    }

                                                                    catch (Exception ex)
                                                                    {
                                                                        _logger.Error(ex);
                                                                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                                                        helper.SendUhoneStatusFailedToSentMail("SFTP", ex.Message, companyDetail.CompanyCode);
                                                                    }
                                                                    finally
                                                                    {
                                                                        client.Disconnect();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            _logger.Info(companyDetail.CompanyCode.ToLower() + "Image packing failed");
                                                            throw (ex);
                                                        }

                                                    }


                                                    foreach (var item in navyOutboundList)
                                                    {
                                                        string updateSqlQuery = "EXEC [GetNavyOrderDetails] @Action='UPDATE', @AuthDocumentId=" + item.Id + "";
                                                        using (SqlConnection con = new SqlConnection(connectionString))
                                                        {
                                                            con.Open();
                                                            SqlCommand cmd = new SqlCommand(updateSqlQuery, con);
                                                            cmd.ExecuteNonQuery();
                                                            con.Close();
                                                        }
                                                    }

                                                }
                                                else { _logger.Info(companyDetail.CompanyCode.ToLower() + " - No results found for processing"); }
                                            }

                                        }
                                        break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }

            return response;
        }

        public ResponseBase CopyResultsToSFTPFolder_Primerica()
        {
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;
            var companyCode = _configuration.GetSection("PrimericaCompanycode").Get<string>();

            try
            {
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = $"SELECT* FROM[dbo].[CompanyDetail] WHERE Companycode = '{companyCode}'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }

                if (companyDetails != null && companyDetails.Any())
                {
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];
                            if (companyDetail.CompanyCode.ToLower() == companyCode.ToLower())
                            {
                                var sftpHostUrl = "";
                                var sftpPortNumber = 0;
                                var sftpUserName = "";
                                var sftpPassword = "";
                                var sftpFolderPath = "";
                                var sftpArchiveFolderPath = "";

                                using (var myConnection = new SqlConnection(defaultConnectionString))
                                {
                                    var query = $"SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='{companyDetail.CompanyCode.ToLower()}' AND IsInbound=0";
                                    var oCmd = new SqlCommand(query, myConnection);
                                    myConnection.Open();
                                    var oReader = oCmd.ExecuteReader();
                                    while (oReader.Read())
                                    {
                                        sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                                        sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                                        sftpUserName = Convert.ToString(oReader["UserName"]);
                                        sftpPassword = Convert.ToString(oReader["Password"]);
                                        sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                        sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                    }
                                    oReader.Close();
                                    myConnection.Close();
                                }
                                string orderStatusAndResultsPath = _configuration["OrderStatusAndResultsPath"];
                                string newFilePath = $"{orderStatusAndResultsPath}{companyDetail.CompanyCode}/";
                                string[] files = Directory.GetFiles(newFilePath);
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);

                                if (files != null && files.Any())
                                {
                                    using (var sftpclient = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                                    {
                                        try
                                        {
                                            sftpclient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                            sftpclient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                            sftpclient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                            sftpclient.Connect();
                                            _logger.Info($"Primerica - Successful connection to {sftpHostUrl}");
                                            foreach (string file in files)
                                            {
                                                try
                                                {
                                                    File.SetAttributes(file, FileAttributes.Normal);
                                                    string fileName = Path.GetFileName(file);
                                                    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                                                    {
                                                        sftpclient.BufferSize = 4 * 1024; // bypass Payload error large files
                                                        sftpclient.UploadFile(fileStream, fileName);
                                                        fileStream.Dispose();
                                                    }

                                                    FileInfo fi = new FileInfo(file);
                                                    fi.Delete();
                                                    _isMailAlreadySent = false;

                                                    _logger.Info($"Primerica - File {fileName} copied successfully");
                                                }
                                                catch (Exception ex)
                                                {
                                                    string fileName = Path.GetFileName(file);
                                                    _logger.Info($"Primerica - File {fileName} NOT copied successfully");
                                                    _logger.Error(ex);
                                                    if (!_isMailAlreadySent)
                                                    {
                                                        _isMailAlreadySent = true;
                                                        helper.Send1122StatusAndResultsFailedToSentMail(ex.Message, companyDetail.CompanyCode, "1122", fileName);
                                                    }
                                                }
                                            }

                                            _logger.Info("Primerica - files copied successfully");
                                        }
                                        catch (Exception ex)
                                        {
                                            _logger.Error(ex);
                                            if (!_isMailAlreadySent)
                                            {
                                                _isMailAlreadySent = true;
                                                helper.Send1122StatusAndResultsFailedToSentMail(ex.Message, companyDetail.CompanyCode, "1122");
                                            }
                                        }
                                        finally
                                        {
                                            sftpclient.Disconnect();
                                        }
                                    }
                                }
                                else
                                {
                                    _logger.Info(companyDetail.CompanyCode.ToLower() + " - No files found for processing");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }

            return response;
        }

        public ResponseBase CopyResultsToSFTPFolder_Protective()
        {
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;
            var companyCode = _configuration.GetSection("ProtectiveCompanycode").Get<string>();

            try
            {
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = $"SELECT* FROM[dbo].[CompanyDetail] WHERE Companycode = '{companyCode}'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }

                if (companyDetails != null && companyDetails.Any())
                {
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];
                            if (companyDetail.CompanyCode.ToLower() == companyCode.ToLower())
                            {
                                var sftpHostUrl = "";
                                var sftpPortNumber = 0;
                                var sftpUserName = "";
                                var sftpPassword = "";
                                var sftpFolderPath = "";
                                var sftpArchiveFolderPath = "";

                                using (var myConnection = new SqlConnection(defaultConnectionString))
                                {
                                    var query = $"SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='{companyDetail.CompanyCode.ToLower()}' AND IsInbound=0 and Type is null"; //
                                    var oCmd = new SqlCommand(query, myConnection);
                                    myConnection.Open();
                                    var oReader = oCmd.ExecuteReader();
                                    while (oReader.Read())
                                    {
                                        sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                                        sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                                        sftpUserName = Convert.ToString(oReader["UserName"]);
                                        sftpPassword = Convert.ToString(oReader["Password"]);
                                        sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                        sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                    }
                                    oReader.Close();
                                    myConnection.Close();
                                }
                                string orderpath = _configuration["Acord1122FilePath"];
                                string orderfilePath = $"{orderpath}{companyDetail.CompanyCode}/";
                                string orderpatharchives = $"{orderpath}{companyDetail.CompanyCode}{"//Archives"}/";
                                string fileName = "ProtectiveStatus" + DateTime.Now.ToString("yyyyMMdd_hh_mm_ss") + ".txt";

                                string[] files = Directory.GetFiles(orderfilePath);
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);

                                using (var sftpclient = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                                {
                                    try
                                    {
                                        sftpclient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                        sftpclient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                        sftpclient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                        sftpclient.Connect();
                                        _logger.Info($"Protective - Successful connection to {sftpHostUrl}");
                                        try
                                        {
                                            if (File.Exists(orderfilePath + "ProtectiveStatus.txt"))
                                            {
                                                byte[] bytes = File.ReadAllBytes(orderfilePath + "ProtectiveStatus.txt");
                                                File.WriteAllBytes(orderpatharchives + fileName, bytes);
                                                sftpclient.WriteAllBytes(sftpFolderPath + fileName, bytes);
                                                sftpclient.WriteAllBytes(sftpArchiveFolderPath + fileName, bytes);
                                                _logger.Info($"Protective - File uploaded to {sftpFolderPath}");

                                                FileInfo fi = new FileInfo(orderfilePath + "ProtectiveStatus.txt");
                                                fi.Delete();
                                                _logger.Info("Protective - files copied successfully");
                                                _isMailAlreadySent = false;
                                            }
                                            else
                                            {
                                                _logger.Info("Protective - No files found");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            _logger.Error(ex);
                                            if (!_isMailAlreadySent)
                                            {
                                                helper.Send1122StatusAndResultsFailedToSentMail(ex.Message, companyDetail.CompanyCode, "Custom", "");
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        _logger.Error(ex);
                                        if (!_isMailAlreadySent)
                                        {
                                            _isMailAlreadySent = true;
                                            helper.Send1122StatusAndResultsFailedToSentMail(ex.Message, companyDetail.CompanyCode, "Custom");
                                        }
                                    }
                                    finally
                                    {
                                        sftpclient.Disconnect();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }

            return response;
        }

        public async Task<ResponseBase> ProcessKOCOrdersAsync()
        {
            ResponseBase responses = new ResponseBase();
            _rootDirectoryPath = _configuration["WSKOC_FolderPath"];
            _archiveFolderPath = _configuration["WSKOC_ArchiveFolderPath"];
            _logFilePath = _rootDirectoryPath + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            WriteLog("Order Processing Started - " + DateTime.Now.ToString(), _logFilePath);
            string[] directories = Directory.GetDirectories(_rootDirectoryPath);

            try
            {
                //_timer.Enabled = false;

                var sftpHostUrl = "";
                var sftpPortNumber = 0;
                var sftpUserName = "";
                var sftpPassword = "";
                var sftpFolderPath = "";
                var sftpArchiveFolderPath = "";
                //string remoteFilePath = sftpFolderPath + formattedFileName;
                //string remoteFileArchivePath = sftpArchiveFolderPath + formattedFileName;

                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (var myConnection = new SqlConnection(defaultConnectionString))
                {
                    WriteLog("Company configuration lookup started - " + DateTime.Now.ToString(), _logFilePath);
                    var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='KOC' AND IsInbound=1";
                    var oCmd = new SqlCommand(query, myConnection);
                    myConnection.Open();
                    var oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                        sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                        sftpUserName = Convert.ToString(oReader["UserName"]);
                        sftpPassword = Convert.ToString(oReader["Password"]);
                        sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                        sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                    }
                    oReader.Close();
                    myConnection.Close();
                    WriteLog("Company configuration lookup ended - " + DateTime.Now.ToString(), _logFilePath);
                }


                var directory = _rootDirectoryPath;
                using (var sftpclient = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                {
                    WriteLog("SFTP connection establishment started - " + DateTime.Now.ToString(), _logFilePath);
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        sftpclient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                        sftpclient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                        sftpclient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                        sftpclient.Connect();
                        WriteLog("SFTP connection establishment successfull - " + DateTime.Now.ToString(), _logFilePath);
                        var sftpfiles = sftpclient.ListDirectory(sftpFolderPath).ToList();
                        var files = sftpfiles.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.ToLower().EndsWith(".txt")).Select(x => x.FullName).ToList();

                        if (files != null)
                        {
                            WriteLog($"Files picked up - {string.Join(";", files)}" + DateTime.Now.ToString(), _logFilePath);

                            foreach (var file in files)
                            {
                                try
                                {
                                    if (!file.Contains("Response_Log"))
                                    {
                                        WriteLog(file, _logFilePath);
                                        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                                        WriteLog("Http Request Started - " + DateTime.Now.ToString(), _logFilePath);

                                        WriteRunLog(Path.GetFileName(file));

                                        using (var client = new HttpClient())
                                        {
                                            client.Timeout = TimeSpan.FromMinutes(10); // Timeout value is 10 minutes
                                            HttpResponseMessage response = null;
                                            MultipartFormDataContent content = new MultipartFormDataContent();
                                            //ByteArrayContent fileContent = new ByteArrayContent(System.IO.File.ReadAllBytes(file));
                                            ByteArrayContent fileContent = new ByteArrayContent(sftpclient.ReadAllBytes(file));
                                            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                                            content.Add(fileContent, "file", Path.GetFileName(file));

                                            client.BaseAddress = new Uri(_configuration["URL"]);
                                            client.DefaultRequestHeaders.Accept.Clear();
                                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                            client.DefaultRequestHeaders.Add("Companycode", "KOC");

                                            WriteLog("Http Request Processing - " + DateTime.Now.ToString(), _logFilePath);
                                            var sftpFileContent = sftpclient.ReadAllBytes(file);
                                            sftpclient.WriteAllBytes(sftpArchiveFolderPath + Path.GetFileName(file), sftpFileContent);
                                            WriteLog("File copied to archive folder - " + DateTime.Now.ToString(), _logFilePath);
                                            sftpclient.DeleteFile(file);
                                            WriteLog("File deleted - " + DateTime.Now.ToString(), _logFilePath);

                                            response = await client.PostAsync("services/api/ProcessKofCOrders", content);
                                            WriteLog("Http Request Ended - " + DateTime.Now.ToString(), _logFilePath);

                                            if (response != null && response.IsSuccessStatusCode)
                                            {
                                                WriteLog("Processing Response Started - " + DateTime.Now.ToString(), _logFilePath);
                                                WriteLog("Request Message Information - " + DateTime.Now.ToString() + " - " + response.RequestMessage, _logFilePath);
                                                string responseJsonString = await response.Content.ReadAsStringAsync();
                                                WriteLog("Response data - " + DateTime.Now.ToString() + " - " + responseJsonString, _logFilePath);

                                                ImportOrdersResponse importOrdersResponse = JsonConvert.DeserializeObject<ImportOrdersResponse>(responseJsonString);

                                                if (importOrdersResponse != null)
                                                {
                                                    if (Directory.Exists(directory))
                                                    {
                                                        string responseLogFilePath = directory + "\\Response_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                        WriteLog("Successfull response at - " + DateTime.Now.ToString(), responseLogFilePath);
                                                        WriteLog("------------------------------------------------", responseLogFilePath);
                                                        WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, responseLogFilePath);
                                                        WriteLog("Successful Orders - " + importOrdersResponse.SuccessfulOrders, responseLogFilePath);
                                                        WriteLog("Failed Orders - " + importOrdersResponse.FailedOrders, responseLogFilePath);

                                                        if (!Directory.Exists(directory + "\\SucessOrders"))
                                                        {
                                                            Directory.CreateDirectory(directory + "\\SucessOrders");
                                                        }

                                                        if (!Directory.Exists(directory + "\\FailedOrders"))
                                                        {
                                                            Directory.CreateDirectory(directory + "\\FailedOrders");
                                                        }

                                                        string successLogFilePath = directory + "\\SucessOrders\\Sucess_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                        WriteLog("Processed at - " + DateTime.Now.ToString(), successLogFilePath);
                                                        WriteLog("-------------------------------------", successLogFilePath);
                                                        WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, successLogFilePath);
                                                        WriteLog("Successful Orders - " + importOrdersResponse.SuccessfulOrders, successLogFilePath);
                                                        WriteLog("\n", successLogFilePath);

                                                        if (importOrdersResponse.SuccessfulOrders > 0 && importOrdersResponse.SuccessfulOrderNumbers != null && importOrdersResponse.SuccessfulOrderNumbers.Count > 0)
                                                        {
                                                            WriteLog("Order Numbers", successLogFilePath);
                                                            WriteLog("-------------", successLogFilePath);
                                                            foreach (string successfulOrderNumber in importOrdersResponse.SuccessfulOrderNumbers)
                                                            {
                                                                WriteLog(successfulOrderNumber, successLogFilePath);
                                                            }
                                                        }

                                                        string failedLogFilePath = directory + "\\FailedOrders\\Failed_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                        WriteLog("Processed at - " + DateTime.Now.ToString(), failedLogFilePath);
                                                        WriteLog("-------------------------------------", failedLogFilePath);
                                                        WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, failedLogFilePath);
                                                        WriteLog("Failed Orders - " + importOrdersResponse.FailedOrders, failedLogFilePath);
                                                        WriteLog("\n", failedLogFilePath);

                                                        if (importOrdersResponse.FailedOrders > 0 && importOrdersResponse.FailedOrdersResponse != null && importOrdersResponse.FailedOrdersResponse.Count > 0)
                                                        {
                                                            WriteLog("Failed Reasons", failedLogFilePath);
                                                            WriteLog("---------------", failedLogFilePath);
                                                            WriteLog("\n", failedLogFilePath);

                                                            foreach (FailedOrdersResponse failedOrder in importOrdersResponse.FailedOrdersResponse)
                                                            {
                                                                List<string> validationMessages = failedOrder.ValidationMessages ?? new List<string>();
                                                                string base64XmlDocument = failedOrder.Base64XmlDocument;

                                                                Guid newGuid = Guid.NewGuid();
                                                                string newGuidString = newGuid.ToString();

                                                                string fileName = newGuidString + ".txt";

                                                                WriteLog("File Name - " + fileName, failedLogFilePath);

                                                                foreach (string validationMessage in validationMessages)
                                                                {
                                                                    WriteLog(validationMessage, failedLogFilePath);
                                                                }

                                                                WriteLog("\n", failedLogFilePath);

                                                                // byte[] docBytes = Convert.FromBase64String(base64XmlDocument); //TODO: Need to check whether do we need to add mime type

                                                                File.WriteAllText(directory + "\\FailedOrders\\" + fileName, base64XmlDocument);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    WriteLog("Deserialized response object is null", _logFilePath);
                                                }

                                                WriteLog("Processing Response Ended - " + DateTime.Now.ToString(), _logFilePath);
                                            }
                                            else
                                            {
                                                WriteLog("Server Error - " + response.ReasonPhrase + DateTime.Now.ToString(), _logFilePath);
                                            }
                                        }
                                    }
                                }
                                catch (FileNotFoundException ex)
                                {
                                    WriteLog("Exception - " + DateTime.Now.ToString() + ": FILE NOT FOUND - " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            WriteLog($"Files not found - " + DateTime.Now.ToString(), _logFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message + "\n" + ex.InnerException, _logFilePath);
                    }
                    finally
                    {
                        sftpclient.Disconnect();
                    }
                    WriteLog("SFTP connection establishment ended - " + DateTime.Now.ToString(), _logFilePath);
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message + "\n" + ex.InnerException, _logFilePath);
            }
            return responses;
        }

        public async Task<ResponseBase> ProcessOrders()
        {
            ResponseBase responses = new ResponseBase();
            PrevDate = DateTime.Now;
            try
            {
                _rootDirectoryPath = _configuration["FolderPath"];
                _logFilePath = _rootDirectoryPath + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                _archiveFolderPath = _configuration["ArchiveFolderPath"];
                WriteLog("Order Processing Started - " + DateTime.Now.ToString(), _logFilePath);
                string[] directories = Directory.GetDirectories(_rootDirectoryPath);
                foreach (string directory in directories)
                {
                    string[] files = Directory.GetFiles(directory, "*.txt");
                    foreach (string file in files)
                    {
                        try
                        {
                            if (!file.Contains("Response_Log"))
                            {
                                //FileInfo fileInfo = new FileInfo(s);
                                WriteLog(file, _logFilePath);
                                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                                WriteLog("Http Request Started - " + DateTime.Now.ToString(), _logFilePath);

                                string resultString = File.ReadAllText(file);
                                resultString = resultString.Replace("\r\n", String.Empty);
                                string[] splitted = resultString.Split(new string[] { "</TXLife>" }, StringSplitOptions.None);
                                string newFile = splitted[0];

                                if (newFile.Contains(" & "))
                                {
                                    newFile = newFile.Replace(" & ", " &amp; ");
                                }

                                newFile += "</TXLife>";
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(newFile);

                                XmlNodeList companyCodeXmlNodeList = doc.GetElementsByTagName("Policy");
                                string companyCode = companyCodeXmlNodeList != null && companyCodeXmlNodeList.Count > 0 ? companyCodeXmlNodeList[0].ChildNodes[2].ChildNodes[0].InnerText : "";
                                if (!CheckRunFile(Path.GetFileName(file)))
                                {
                                    WriteRunLog(Path.GetFileName(file));
                                    using (var client = new HttpClient())
                                    {
                                        client.Timeout = System.Threading.Timeout.InfiniteTimeSpan; //Wait until the response is returned from server.
                                        HttpResponseMessage response = null;
                                        MultipartFormDataContent content = new MultipartFormDataContent();
                                        ByteArrayContent fileContent = new ByteArrayContent(System.IO.File.ReadAllBytes(file));
                                        fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                                        content.Add(fileContent, "file", Path.GetFileName(file));

                                        client.BaseAddress = new Uri(_configuration["URL"]);
                                        client.DefaultRequestHeaders.Accept.Clear();
                                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                        if (!string.IsNullOrEmpty(companyCode))
                                        {
                                            client.DefaultRequestHeaders.Add("Companycode", companyCode);
                                        }


                                        WriteLog("Http Request Processing - " + DateTime.Now.ToString(), _logFilePath);
                                        if (File.Exists(Path.Combine(directory, file)))
                                        {
                                            File.Copy(file, Path.Combine(_archiveFolderPath, Path.GetFileName(file)));
                                            WriteLog("File copied to archive folder - " + DateTime.Now.ToString(), _logFilePath);
                                            File.Delete(file);
                                            WriteLog("File deleted - " + DateTime.Now.ToString(), _logFilePath);
                                        }
                                        response = await client.PostAsync("api/login/ImportOrders", content);
                                        WriteLog("Http Request Ended - " + DateTime.Now.ToString(), _logFilePath);

                                        if (response != null && response.IsSuccessStatusCode)
                                        {
                                            WriteLog("Processing Response Started - " + DateTime.Now.ToString(), _logFilePath);
                                            WriteLog("Request Message Information - " + DateTime.Now.ToString() + " - " + response.RequestMessage, _logFilePath);
                                            string responseJsonString = await response.Content.ReadAsStringAsync();
                                            WriteLog("Response data - " + DateTime.Now.ToString() + " - " + responseJsonString, _logFilePath);

                                            ImportOrdersResponse importOrdersResponse = JsonConvert.DeserializeObject<ImportOrdersResponse>(responseJsonString);

                                            if (importOrdersResponse != null)
                                            {
                                                if (Directory.Exists(directory))
                                                {
                                                    string responseLogFilePath = directory + "\\Response_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                    WriteLog("Successfull response at - " + DateTime.Now.ToString(), responseLogFilePath);
                                                    WriteLog("------------------------------------------------", responseLogFilePath);
                                                    WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, responseLogFilePath);
                                                    WriteLog("Successful Orders - " + importOrdersResponse.SuccessfulOrders, responseLogFilePath);
                                                    WriteLog("Failed Orders - " + importOrdersResponse.FailedOrders, responseLogFilePath);

                                                    if (!Directory.Exists(directory + "\\SucessOrders"))
                                                    {
                                                        Directory.CreateDirectory(directory + "\\SucessOrders");
                                                    }

                                                    if (!Directory.Exists(directory + "\\FailedOrders"))
                                                    {
                                                        Directory.CreateDirectory(directory + "\\FailedOrders");
                                                    }

                                                    string successLogFilePath = directory + "\\SucessOrders\\Sucess_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                    WriteLog("Processed at - " + DateTime.Now.ToString(), successLogFilePath);
                                                    WriteLog("-------------------------------------", successLogFilePath);
                                                    WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, successLogFilePath);
                                                    WriteLog("Successful Orders - " + importOrdersResponse.SuccessfulOrders, successLogFilePath);
                                                    WriteLog("\n", successLogFilePath);

                                                    if (importOrdersResponse.SuccessfulOrders > 0 && importOrdersResponse.SuccessfulOrderNumbers != null && importOrdersResponse.SuccessfulOrderNumbers.Count > 0)
                                                    {
                                                        WriteLog("Order Numbers", successLogFilePath);
                                                        WriteLog("-------------", successLogFilePath);
                                                        foreach (string successfulOrderNumber in importOrdersResponse.SuccessfulOrderNumbers)
                                                        {
                                                            WriteLog(successfulOrderNumber, successLogFilePath);
                                                        }
                                                    }

                                                    string failedLogFilePath = directory + "\\FailedOrders\\Failed_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                    WriteLog("Processed at - " + DateTime.Now.ToString(), failedLogFilePath);
                                                    WriteLog("-------------------------------------", failedLogFilePath);
                                                    WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, failedLogFilePath);
                                                    WriteLog("Failed Orders - " + importOrdersResponse.FailedOrders, failedLogFilePath);
                                                    WriteLog("\n", failedLogFilePath);

                                                    if (importOrdersResponse.FailedOrders > 0 && importOrdersResponse.FailedOrdersResponse != null && importOrdersResponse.FailedOrdersResponse.Count > 0)
                                                    {
                                                        WriteLog("Failed Reasons", failedLogFilePath);
                                                        WriteLog("---------------", failedLogFilePath);
                                                        WriteLog("\n", failedLogFilePath);

                                                        foreach (FailedOrdersResponse failedOrder in importOrdersResponse.FailedOrdersResponse)
                                                        {
                                                            List<string> validationMessages = failedOrder.ValidationMessages ?? new List<string>();
                                                            string base64XmlDocument = failedOrder.Base64XmlDocument;

                                                            Guid newGuid = Guid.NewGuid();
                                                            string newGuidString = newGuid.ToString();

                                                            string fileName = newGuidString + ".txt";

                                                            WriteLog("File Name - " + fileName, failedLogFilePath);

                                                            foreach (string validationMessage in validationMessages)
                                                            {
                                                                WriteLog(validationMessage, failedLogFilePath);
                                                            }

                                                            WriteLog("\n", failedLogFilePath);

                                                            byte[] docBytes = Convert.FromBase64String(base64XmlDocument); //TODO: Need to check whether do we need to add mime type

                                                            File.WriteAllBytes(directory + "\\FailedOrders\\" + fileName, docBytes);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                WriteLog("Deserialized response object is null", _logFilePath);
                                            }

                                            WriteLog("Processing Response Ended - " + DateTime.Now.ToString(), _logFilePath);
                                        }
                                        else
                                        {
                                            WriteLog("Server Error - " + response.ReasonPhrase + DateTime.Now.ToString(), _logFilePath);
                                        }
                                    }
                                }
                                else
                                {
                                    WriteLog("File already processed:" + file, _logFilePath);
                                    if (File.Exists(Path.Combine(directory, file)))
                                    {
                                        File.Delete(Path.Combine(directory, file));
                                        WriteLog("File deleted - " + DateTime.Now.ToString(), _logFilePath);
                                    }
                                }
                            }
                        }
                        catch (FileNotFoundException ex)
                        {
                            WriteLog("Exception - " + DateTime.Now.ToString() + ": FILE NOT FOUND - " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                            continue;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                WriteLog("Exception - " + DateTime.Now.ToString() + ": " + (ex != null && ex.InnerException != null ? ex.InnerException.Message : (ex != null ? ex.Message : "UNKNOW ERROR")), _logFilePath);
            }
            return responses;
        }

        public void DailyOrderReport()
        {
            _rootDirectoryPath = _configuration["DailyReportPath"];
            _logFilePath = _rootDirectoryPath + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            ResponseBase responses = new ResponseBase();
            PrevDate = DateTime.Today.AddDays(-1);
            var startOfMonth = new DateTime(PrevDate.Year, PrevDate.Month, 1);
            var startOfYear = new DateTime(PrevDate.Year, 1, 1);
            DateTime currDate = DateTime.Now;
            string oString = string.Empty;
            if (_configuration["SendEmail"] == "1")
            {
                try
                {
                    List<CompanyDetails> companies = new List<CompanyDetails>();
                    List<OrderCount> counts = new List<OrderCount>();
                    try
                    {
                        //var connectionString = connectionStr;
                        var connectionString = _configuration.GetConnectionString("DefaultConnection");
                        using (SqlConnection myConnection = new SqlConnection(connectionString))
                        {
                            oString = "SELECT * FROM [dbo].[CompanyDetail] WHERE CompanyCode NOT IN ('Default','VerifyIns','VerifyLegal') ";
                            SqlCommand oCmd = new SqlCommand(oString, myConnection);
                            myConnection.Open();
                            SqlDataReader oReader = oCmd.ExecuteReader();
                            while (oReader.Read())
                            {
                                CompanyDetails company = new CompanyDetails()
                                {
                                    Id = Convert.ToInt32(oReader["Id"]),
                                    CompanyCode = oReader["CompanyCode"].ToString(),
                                    CompanyName = oReader["CompanyName"].ToString(),
                                    WSUsers = oReader["WSUsers"].ToString(),
                                    ConnectionString = oReader["ConnectionString"].ToString(),
                                    CompanyType = oReader["CompanyType"].ToString(),
                                };
                                companies.Add(company);
                            }
                            oReader.Close();
                            myConnection.Close();
                        }
                        foreach (var comp in companies)
                        {
                            try
                            {
                                DataTable dt = new DataTable();
                                using (SqlConnection accConnection = new SqlConnection(comp.ConnectionString))
                                {
                                    oString = "select  distinct isnull(Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber),'''') as AccountNo " +

                                                         "from OrderOfficeInformation oof " +
                                                         " inner join CompanyMaster on CompanyId = oof.companyId " +
                                                         "where oof.IsDeleted=0 ";

                                    SqlCommand oCmds = new SqlCommand(oString, accConnection);
                                    accConnection.Open();
                                    SqlDataAdapter sdas = new SqlDataAdapter(oCmds);
                                    sdas.Fill(dt);
                                }
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        string account = Convert.ToString(dt.Rows[i][0]);

                                        using (SqlConnection myConnection = new SqlConnection(comp.ConnectionString))
                                        {                         
                                            MTDCount mtdCount = new MTDCount();                                                                             
                                            if (comp.CompanyType.ToLower() == "life")
                                            {

                                                oString = "select CompanyCode, CAST(OrderFacility.CreatedOnDateTime AS DATE) as Date,count(*) as DailyCount , " +
                                                               "SUM(COUNT(*)) OVER() as MTD_Count " +
                                                               "from OrderFacility inner join [order] on orderid=[order].id " +
                                                               "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                               "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                               "inner join " + _configuration["DoctorCommonDB"] + " D On D.id = OrderFacility.DoctorId " +
                                                               "where eNoahOrderId is not null and OrderOfficeId is not null and D.IsActive=1 and OrderFacility.IsDeleted=0 " +
                                                               "and CAST(OrderFacility.CreatedOnDateTime AS DATE) BETWEEN '" + startOfMonth.ToString("yyyy-MM-dd") + "'" +
                                                               " AND '" + PrevDate.ToString("yyyy-MM-dd") + "' And isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + "' " +
                                                               "group by CAST(OrderFacility.CreatedOnDateTime AS DATE) , " +
                                                               "companycode order by CAST(OrderFacility.CreatedOnDateTime AS DATE) desc " +


                                                               "SELECT AccountNo,Sum(YTD_B2B_Orders) as YTD_B2B_Orders FROM (SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*) as [YTD_B2B_Orders] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                               _configuration["DoctorCommonDB"] + " D On D.id = O.DoctorId " + "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                               "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                               " where O.IsB2BOrder = 1 and CAST(O.CreatedOnDateTime AS DATE) between '" +
                                                               startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                               "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber) AS a group by AccountNo " +

                                                               "SELECT AccountNo,Sum(YTD_Orders_by_Users) as YTD_Orders_by_Users FROM (SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*)  as [YTD_Orders_by_Users] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                               _configuration["DoctorCommonDB"] + " D On D.id = O.DoctorId " +
                                                               "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                               "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                               "where O.IsB2BOrder = 0 and CAST(O.CreatedOnDateTime AS DATE) between '"
                                                               + startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                               "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber) AS a group by AccountNo ";
                                            }
                                            if (comp.CompanyType.ToLower() == "legal")
                                            {
                                                oString = "select  CompanyCode,CAST(OrderFacility.CreatedOnDateTime AS DATE) as Date,count(*) as DailyCount , " +
                                                               "SUM(COUNT(*)) OVER() as MTD_Count " +

                                                               "from OrderFacility inner join [order] on orderid=[order].id " +
                                                               "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                               "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                               "inner join " + _configuration["CustodianCommonDB"] + " D On D.id = OrderFacility.DoctorId " +
                                                               "where eNoahOrderId is not null and OrderOfficeId is not null and D.IsActive=1 and OrderFacility.IsDeleted=0 " +
                                                               "and CAST(OrderFacility.CreatedOnDateTime AS DATE) BETWEEN '" + startOfMonth.ToString("yyyy-MM-dd") + "'" +
                                                               " AND '" + PrevDate.ToString("yyyy-MM-dd") + "' And isnull(Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''')= '" + account + "'" +
                                                               " group by CAST(OrderFacility.CreatedOnDateTime AS DATE) , " +
                                                               "companycode order by CAST(OrderFacility.CreatedOnDateTime AS DATE) desc " +

                                                               "SELECT AccountNo,Sum(YTD_B2B_Orders) as YTD_B2B_Orders FROM (SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*) as [YTD_B2B_Orders] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                               _configuration["CustodianCommonDB"] + " D On D.id = O.DoctorId " +
                                                               "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                               "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                               "where O.IsB2BOrder = 1 and CAST(O.CreatedOnDateTime AS DATE) between '" + startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null " +
                                                               "and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                               "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber) AS a group by AccountNo " +


                                                               "SELECT AccountNo,Sum(YTD_Orders_by_Users) as YTD_Orders_by_Users FROM (SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*) as [YTD_Orders_by_Users] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                               _configuration["CustodianCommonDB"] + " D On D.id = O.DoctorId  " +
                                                               "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                               "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                               "where O.IsB2BOrder = 0 and CAST(O.CreatedOnDateTime AS DATE) between '" + startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null " +
                                                               "and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                               "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber) AS a group by AccountNo ";
                                            }


                                            SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                            myConnection.Open();
                                            SqlDataAdapter sda = new SqlDataAdapter(oCmd);
                                            DataSet ds = new DataSet();
                                            sda.Fill(ds);

                                            OrderCount OC = new OrderCount();
                                            OC.MTDCount = mtdCount.MTD_Count;
                                            var dailycounts = new List<DailyCount>();

                                            for (int a = 0; a < ds.Tables[0].Rows.Count; a++)
                                            {
                                                OC.CompanyCode = Convert.ToString(ds.Tables[0].Rows[a]["CompanyCode"]);
                                                OC.MTDCount = Convert.ToInt32(ds.Tables[0].Rows[a]["MTD_Count"]);
                                                DailyCount dc = new DailyCount();
                                                dc.Date = Convert.ToDateTime(ds.Tables[0].Rows[a]["Date"]);
                                                dc.Count = Convert.ToInt32(ds.Tables[0].Rows[a]["DailyCount"]);
                                                dailycounts.Add(dc);
                                            }

                                            for (int a = 0; a < ds.Tables[1].Rows.Count; a++)
                                            {
                                                OC.TotalCount = Convert.ToInt32(ds.Tables[1].Rows[a]["YTD_B2B_Orders"]);
                                                OC.AccountNo = Convert.ToString(ds.Tables[1].Rows[a]["AccountNo"]);
                                            }
                                            for (int a = 0; a < ds.Tables[2].Rows.Count; a++)
                                            {
                                                OC.AccountNo = Convert.ToString(ds.Tables[2].Rows[a]["AccountNo"]);
                                                OC.ToolCount = Convert.ToInt32(ds.Tables[2].Rows[a]["YTD_Orders_by_Users"]); 
                                            }

                                            if (ds.Tables[1].Rows.Count <= 0 && ds.Tables[2].Rows.Count <= 0)
                                            {
                                                OC.AccountNo = account;
                                                OC.ToolCount = 0;
                                            }
                                            if (OC.CompanyCode == null || OC.CompanyCode.Length == 0)
                                            {
                                                OC.CompanyCode = comp.CompanyCode;
                                            }
                                            OC.DailyCounts = GetDailyCounts(dailycounts, PrevDate);
                                            counts.Add(OC);

                                            myConnection.Close();
                                        }
                                    }
                                }
                                else
                                {
                                    using (SqlConnection myConnection = new SqlConnection(comp.ConnectionString))
                                    {
                                        string account = Convert.ToString(dt);                                   
                                        MTDCount mtdCount = new MTDCount();                                      
                                        if (comp.CompanyType.ToLower() == "life")
                                        {

                                            oString = "select CompanyCode, CAST(OrderFacility.CreatedOnDateTime AS DATE) as Date,count(*) as DailyCount , " +
                                                           "SUM(COUNT(*)) OVER() as MTD_Count " +
                                                           "from OrderFacility inner join [order] on orderid=[order].id " +
                                                           "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                           "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                           "inner join " + _configuration["DoctorCommonDB"] + " D On D.id = OrderFacility.DoctorId " +
                                                           "where eNoahOrderId is not null and OrderOfficeId is not null and D.IsActive=1 and OrderFacility.IsDeleted=0 " +
                                                           "and CAST(OrderFacility.CreatedOnDateTime AS DATE) BETWEEN '" + startOfMonth.ToString("yyyy-MM-dd") + "'" +
                                                           " AND '" + PrevDate.ToString("yyyy-MM-dd") + "' And isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + "' " +
                                                           "group by CAST(OrderFacility.CreatedOnDateTime AS DATE) , " +
                                                           "companycode order by CAST(OrderFacility.CreatedOnDateTime AS DATE) desc " +


                                                           "SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*) as [YTD_B2B_Orders] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                           _configuration["DoctorCommonDB"] + " D On D.id = O.DoctorId " + "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                           "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                           " where O.IsB2BOrder = 1 and CAST(O.CreatedOnDateTime AS DATE) between '" +
                                                           startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                           "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber " +

                                                           "SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*)  as [YTD_Orders_by_Users] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                           _configuration["DoctorCommonDB"] + " D On D.id = O.DoctorId " +
                                                           "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                           "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                           "where O.IsB2BOrder = 0 and CAST(O.CreatedOnDateTime AS DATE) between '"
                                                           + startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                           "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber ";
                                        }
                                        if (comp.CompanyType.ToLower() == "legal")
                                        {
                                            oString = "select  CompanyCode,CAST(OrderFacility.CreatedOnDateTime AS DATE) as Date,count(*) as DailyCount , " +
                                                           "SUM(COUNT(*)) OVER() as MTD_Count " +

                                                           "from OrderFacility inner join [order] on orderid=[order].id " +
                                                           "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                           "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                           "inner join " + _configuration["CustodianCommonDB"] + " D On D.id = OrderFacility.DoctorId " +
                                                           "where eNoahOrderId is not null and OrderOfficeId is not null and D.IsActive=1 and OrderFacility.IsDeleted=0 " +
                                                           "and CAST(OrderFacility.CreatedOnDateTime AS DATE) BETWEEN '" + startOfMonth.ToString("yyyy-MM-dd") + "'" +
                                                           " AND '" + PrevDate.ToString("yyyy-MM-dd") + "' And isnull(Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''')= '" + account + "'" +
                                                           " group by CAST(OrderFacility.CreatedOnDateTime AS DATE) , " +
                                                           "companycode order by CAST(OrderFacility.CreatedOnDateTime AS DATE) desc " +

                                                           "SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*) as [YTD_B2B_Orders] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                           _configuration["CustodianCommonDB"] + " D On D.id = O.DoctorId " +
                                                           "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                           "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                           "where O.IsB2BOrder = 1 and CAST(O.CreatedOnDateTime AS DATE) between '" + startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null " +
                                                           "and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                           "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber " +


                                                           "SELECT Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end ,CompanyMaster.CropNumber) as AccountNo, count(*) as [YTD_Orders_by_Users] from OrderFacility O join [order]  on orderid =[order].id join " +
                                                           _configuration["CustodianCommonDB"] + " D On D.id = O.DoctorId  " +
                                                           "inner join CompanyMaster on CompanyId=CompanyMaster.id " +
                                                           "left join OrderOfficeInformation oof on [order].OrderOfficeId = oof.Id " +
                                                           "where O.IsB2BOrder = 0 and CAST(O.CreatedOnDateTime AS DATE) between '" + startOfYear.ToString("yyyy-MM-dd") + "' AND '" + PrevDate.ToString("yyyy-MM-dd") + "' and eNoahOrderId is not null and O.IsDeleted = 0 and OrderOfficeId is not null " +
                                                           "and D.IsActive = 1 and  isnull (Coalesce(case when oof.OfficeAccountNumber = '' then null else oof.OfficeAccountNumber end, CompanyMaster.CropNumber),'''') = '" + account + " ' " +
                                                           "group by oof.OfficeAccountNumber,CompanyMaster.CropNumber  ";
                                        }
                                        

                                     SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                        myConnection.Open();
                                        SqlDataAdapter sda = new SqlDataAdapter(oCmd);
                                        DataSet ds = new DataSet();
                                        sda.Fill(ds);

                                        OrderCount OC = new OrderCount();
                                        OC.MTDCount = mtdCount.MTD_Count;
                                        var dailycounts = new List<DailyCount>();

                                        for (int a = 0; a < ds.Tables[0].Rows.Count; a++)
                                        {
                                            OC.CompanyCode = Convert.ToString(ds.Tables[0].Rows[a]["CompanyCode"]);
                                            OC.MTDCount = Convert.ToInt32(ds.Tables[0].Rows[a]["MTD_Count"]);
                                            DailyCount dc = new DailyCount();
                                            dc.Date = Convert.ToDateTime(ds.Tables[0].Rows[a]["Date"]);
                                            dc.Count = Convert.ToInt32(ds.Tables[0].Rows[a]["DailyCount"]);
                                            dailycounts.Add(dc);
                                        }

                                        for (int a = 0; a < ds.Tables[1].Rows.Count; a++)
                                        {
                                            OC.TotalCount = Convert.ToInt32(ds.Tables[1].Rows[a]["YTD_B2B_Orders"]); 
                                            OC.AccountNo = Convert.ToString(ds.Tables[1].Rows[a]["AccountNo"]);
                                        }
                                        for (int a = 0; a < ds.Tables[2].Rows.Count; a++)
                                        {
                                            OC.AccountNo = Convert.ToString(ds.Tables[2].Rows[a]["AccountNo"]);
                                            OC.ToolCount = Convert.ToInt32(ds.Tables[2].Rows[a]["YTD_Orders_by_Users"]);
                                        }

                                        if (ds.Tables[1].Rows.Count <= 0 && ds.Tables[2].Rows.Count <= 0)
                                        {
                                            OC.AccountNo = account;
                                            OC.ToolCount = 0;
                                        }

                                        if (OC.CompanyCode == null || OC.CompanyCode.Length == 0)
                                        {
                                            OC.CompanyCode = comp.CompanyCode;
                                        }
                                        OC.DailyCounts = GetDailyCounts(dailycounts, PrevDate);
                                        counts.Add(OC);

                                        myConnection.Close();
                                    }

                                }                            
                            }
                            catch (Exception ex)
                            {
                                WriteLog("Exception - " + DateTime.Now.ToString() + ": mail error " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        WriteLog("Exception - " + DateTime.Now.ToString() + ": mail error " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                    }
                    var excelFilePath = string.Concat(_env.ContentRootPath,
                       Path.DirectorySeparatorChar.ToString(),
                       "EMailTemplates",
                       Path.DirectorySeparatorChar.ToString(),
                       "DailyReport.xlsx");
                    XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                    IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");

                    int NumberOfLastCol = Worksheet.LastColumnUsed().ColumnNumber();
                    int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                    int toDate = PrevDate.Date.Day;
                    int negDateDiff = -System.Math.Abs(toDate);
                    DateTime today = PrevDate.AddDays(negDateDiff);
                    List<int> DayTotal = new List<int>() { };
                    for (int i = 0; i < toDate; i++)
                    {
                        today = today.AddDays(1);
                        var rngDates = Worksheet.Range(IndexToColumn(NumberOfLastCol + (i + 1)) + "1");
                        rngDates.Style.DateFormat.Format = "MMM-dd";
                        rngDates.Style.Font.Bold = true;
                        Worksheet.Cell(IndexToColumn(NumberOfLastCol + (i + 1)) + "1").Value = today.Date;
                        DayTotal.Add(0);
                    }
                    var notinserted = counts.Where(a => !a.Inserted).OrderBy(a => a.CompanyCode).ToList();
                    //List<int> DayTotal = new List<int>() { 0, 0, 0, 0, 0, 0, 0 };

                    if (notinserted.Count != 0)
                    {
                        int prevCell = NumberOfLastRow + 1;
                        for (int j = 0; j < notinserted.Count; j++)
                        {
                            var rngCC = Worksheet.Range("A" + prevCell.ToString());
                            Worksheet.Cell("A" + prevCell.ToString()).Value = notinserted[j].CompanyCode;
                            Worksheet.Cell("F" + prevCell.ToString()).Value = notinserted[j].MTDCount;

                            if (double.TryParse(notinserted[j].CompanyCode, out double b))
                            {
                                rngCC.SetDataType(XLDataType.Number);
                                Worksheet.Cell("A" + prevCell.ToString()).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                Worksheet.Cell("A" + prevCell.ToString()).Style.NumberFormat.Format = "00000000000000000000".Substring(0, notinserted[j].CompanyCode.Length);
                            }
                            else
                                rngCC.SetDataType(XLDataType.Text);

                            Worksheet.Cell("B" + prevCell.ToString()).Value = "'" + Convert.ToString(notinserted[j].AccountNo);
                            Worksheet.Cell("C" + prevCell.ToString()).Value = notinserted[j].ToolCount;
                            Worksheet.Cell("D" + prevCell.ToString()).Value = notinserted[j].TotalCount;
                            Worksheet.Cell("E" + prevCell.ToString()).Value = notinserted[j].TotalCount + notinserted[j].ToolCount;
                            int row = 7;
                            foreach (var col in notinserted[j].DailyCounts)
                            {
                                Worksheet.Cell(IndexToColumn(row) + prevCell.ToString()).Value = col.Count;
                                DayTotal[row - 7] = DayTotal[row - 7] + col.Count;
                                row++;
                            }
                            prevCell++;
                        }
                    }
                    Worksheet.Cell("A" + (notinserted.Count + 2)).Value = "Total";
                    Worksheet.Cell("B" + (notinserted.Count + 2)).Value = "";
                    Worksheet.Cell("C" + (notinserted.Count + 2)).Value = notinserted.Sum(a => a.ToolCount);
                    Worksheet.Cell("D" + (notinserted.Count + 2)).Value = notinserted.Sum(a => a.TotalCount);
                    Worksheet.Cell("E" + (notinserted.Count + 2)).Value = notinserted.Sum(a => a.ToolCount + a.TotalCount);
                    Worksheet.Cell("F" + (notinserted.Count + 2)).Value = notinserted.Sum(a => a.MTDCount);

                    NumberOfLastCol = Worksheet.LastColumnUsed().ColumnNumber();
                    for (int i = 7; i < NumberOfLastCol + 1; i++)
                        Worksheet.Cell(IndexToColumn(i) + (notinserted.Count + 2)).Value = DayTotal[i - 7];
                    string File = _configuration["DailyReportPath"] + "DailyReport_" + PrevDate.ToString("yyyyMMddhhmmss") + ".xlsx";
                    Worksheet.Columns().AdjustToContents();
                    Worksheet.Rows().AdjustToContents();
                    Workbook.SaveAs(File);
                    //SendMail("Venkat", "bvenkatarao@enoahisolution.com", "Daily Orders Count", File);
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendMailDailyReport("Daily Orders Count", File);
                }
                catch (Exception ex)
                {
                    WriteLog("Exception - " + DateTime.Now.ToString() + ": mail error " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                }
            }
        }
        public void DeCouplingCommentsAPI()
        {
            int TotalSuccess = 0;
            int TotalFailure = 0;
            try
            {
                List<PostCommentsRequest> postcommentsrequestlist = new List<PostCommentsRequest>();
                var commentresponse = APIHelper.GetComments(_configuration["DecouplingapiBaseUrl"]).Result;

                if (commentresponse != null)
                {
                    _logger.Info("DeCoupling Comments API started");
                    var finalresult = commentresponse.results;
                    if (finalresult != null && finalresult.Any())
                    {
                        _logger.Info(string.Format("Total Comments Received:{0}", finalresult.Count));
                        DecouplingRequests requestresponseall = new DecouplingRequests();
                        requestresponseall.Request = JsonConvert.SerializeObject(finalresult);
                        requestresponseall.CreatedOnDateTime = DateTime.Now;
                        requestresponseall.TotalRecord = finalresult.Count;
                        requestresponseall.IsProcessed = true;
                        requestresponseall.ProcessType = "decouplingcommentsapi";
                        _context.DecouplingRequests.Add(requestresponseall);
                        _context.SaveChanges();

                        foreach (var re in finalresult)
                        {
                            try
                            {
                                var request = JsonConvert.DeserializeObject<UpdateOrderStatusRequest>(re.request_json);

                                string defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                                CompanyDetails company = null;

                                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                                {
                                    string oString = $"SELECT * FROM [dbo].[CompanyDetail] WHERE CompanyCode = '{request.companyCode}'";
                                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                    myConnection.Open();
                                    SqlDataReader oReader = oCmd.ExecuteReader();
                                    while (oReader.Read())
                                    {
                                        company = new CompanyDetails()
                                        {
                                            Id = Convert.ToInt32(oReader["Id"]),
                                            CompanyCode = oReader["CompanyCode"].ToString(),
                                            CompanyName = oReader["CompanyName"].ToString(),
                                            CompanyType = oReader["CompanyType"].ToString(),
                                            ConnectionString = oReader["ConnectionString"].ToString(),
                                        };
                                    }
                                    oReader.Close();
                                    myConnection.Close();
                                }
                                if (company != null)
                                {
                                    try
                                    {
                                        var optionsBuilder = new DbContextOptionsBuilder<APSDBContext>();
                                        optionsBuilder.UseSqlServer(company.ConnectionString);

                                        using (APSDBContext dbContext = new APSDBContext(optionsBuilder.Options))
                                        {
                                            var userManagerCND = new UserManager<User>(new UserStore<User, Role, APSDBContext>(dbContext), null, new PasswordHasher<User>(), null, null, null, null, null, null);
                                            RequestResponseLog requestresponse = new RequestResponseLog();

                                            OrderService orderService = new OrderService(dbContext, _configuration, _mapper, _env, _commonContext);
                                            try
                                            {
                                                requestresponse.Request = JsonConvert.SerializeObject(request);
                                                requestresponse.RequestOn = DateTime.Now;
                                                dbContext.RequestResponseLogs.Add(requestresponse);
                                                dbContext.SaveChanges();

                                                var updateorderstatusresponse = new UpdateOrderStatusResponse();
                                                updateorderstatusresponse = orderService.UpdateOrderStatus(request);

                                                if (updateorderstatusresponse != null)
                                                {
                                                    if (updateorderstatusresponse.StatusCode == StatusCodes.SuccessStatusCode)
                                                    {
                                                        TotalSuccess = TotalSuccess + 1;
                                                    }
                                                    else
                                                    {
                                                        TotalFailure = TotalFailure + 1;
                                                    }
                                                }
                                                else
                                                {
                                                    TotalFailure = TotalFailure + 1;
                                                }

                                                requestresponse.Response = JsonConvert.SerializeObject(updateorderstatusresponse);
                                                requestresponse.ResponseOn = DateTime.Now;
                                                dbContext.RequestResponseLogs.Update(requestresponse);
                                                dbContext.SaveChanges();

                                                PostCommentsRequest postcommentsrequest = new PostCommentsRequest();
                                                postcommentsrequest.ExpertLogId = Convert.ToString(request.expertLogId);
                                                postcommentsrequest.Companycode = request.companyCode;
                                                //postcommentsrequest.EopResponse = new ResponseBase() { StatusCode = Models.Constants.StatusCodes.SuccessStatusCode, StatusMessage = Constants.Success };
                                                postcommentsrequest.EopResponse = new ResponseBase() { StatusCode = updateorderstatusresponse.StatusCode, StatusMessage = updateorderstatusresponse.StatusMessage };
                                                postcommentsrequestlist.Add(postcommentsrequest);
                                            }
                                            catch (Exception ex)
                                            {
                                                TotalFailure = TotalFailure + 1;
                                                _logger.Error(ex);
                                                PostCommentsRequest postcommentsrequest = new PostCommentsRequest();
                                                postcommentsrequest.ExpertLogId = Convert.ToString(request.expertLogId);
                                                postcommentsrequest.Companycode = request.companyCode;
                                                postcommentsrequest.EopResponse = new ResponseBase() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message };
                                                postcommentsrequestlist.Add(postcommentsrequest);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        TotalFailure = TotalFailure + 1;
                                        _logger.Error(ex);
                                        PostCommentsRequest postcommentsrequest = new PostCommentsRequest();
                                        postcommentsrequest.ExpertLogId = Convert.ToString(request.expertLogId);
                                        postcommentsrequest.Companycode = request.companyCode;
                                        postcommentsrequest.EopResponse = new ResponseBase() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message };
                                        postcommentsrequestlist.Add(postcommentsrequest);
                                    }
                                }
                                else
                                {
                                    TotalFailure = TotalFailure + 1;
                                    PostCommentsRequest postcommentsrequest = new PostCommentsRequest();
                                    postcommentsrequest.ExpertLogId = Convert.ToString(request.expertLogId);
                                    postcommentsrequest.Companycode = request.companyCode;
                                    postcommentsrequest.EopResponse = new ResponseBase() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = Constants.CarrierInfoNotFound };
                                    postcommentsrequestlist.Add(postcommentsrequest);
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.Error(ex);
                            }
                        }

                        try
                        {
                            if (postcommentsrequestlist != null && postcommentsrequestlist.Any())
                            {
                                _logger.Info("Comments are Pushed and ready for send Response to eXpert");
                                var requestresponse = new RequestResponseLog()
                                {
                                    Request = JsonConvert.SerializeObject(postcommentsrequestlist),
                                    RequestOn = DateTime.Now
                                };
                                _context.RequestResponseLogs.Add(requestresponse);
                                _context.SaveChanges();

                                var postcommentsresponse = APIHelper.PostComments(postcommentsrequestlist, _configuration["DecouplingapiBaseUrl"]).Result;

                                requestresponse.Response = JsonConvert.SerializeObject(postcommentsresponse);
                                requestresponse.ResponseOn = DateTime.Now;
                                _context.RequestResponseLogs.Update(requestresponse);
                                _context.SaveChanges();

                                _logger.Info(string.Format("Total Comments Pushed:{0}", postcommentsrequestlist.Count));

                                requestresponseall.Response = JsonConvert.SerializeObject(postcommentsrequestlist);
                                requestresponseall.ModifiedDateTime = DateTime.Now;
                                requestresponseall.TotalSuccess = TotalSuccess;
                                requestresponseall.TotalFailed = TotalFailure;
                                requestresponseall.IsProcessed = false;
                                _context.DecouplingRequests.Update(requestresponseall);
                                _context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }

                }
                else
                {
                    _logger.Info("DeCoupling Comments API: No Record found");
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        public void DeCouplingUploadDocumentAPI()
        {
            int TotalSuccess = 0;
            int TotalFailure = 0;
            try
            {
                List<DocumentUploadRequest> postdocumentuploadrequestlist = new List<DocumentUploadRequest>();
                var uploadresponse = APIHelper.GetUploadDocuments(_configuration["DecouplingapiBaseUrl"]).Result;

                if (uploadresponse != null)
                {
                    _logger.Info("DeCoupling UploadDocument API Started");
                    var finalresult = uploadresponse.results;
                    if (finalresult != null && finalresult.Any())
                    {
                        _logger.Info(string.Format("Total Document Received:{0}", finalresult.Count));

                        DecouplingRequests requestresponseall = new DecouplingRequests();
                        requestresponseall.Request = JsonConvert.SerializeObject(finalresult);
                        requestresponseall.CreatedOnDateTime = DateTime.Now;
                        requestresponseall.TotalRecord = finalresult.Count;
                        requestresponseall.IsProcessed = true;
                        requestresponseall.ProcessType = "decouplinguploaddocumentapi";
                        _context.DecouplingRequests.Add(requestresponseall);
                        _context.SaveChanges();

                        foreach (var re in finalresult)
                        {
                            try
                            {
                                var request = JsonConvert.DeserializeObject<UploadDocumentRequest>(re.request_json);

                                string defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                                CompanyDetails company = null;

                                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                                {
                                    string oString = $"SELECT * FROM [dbo].[CompanyDetail] WHERE CompanyCode = '{request.companyCode}'";
                                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                    myConnection.Open();
                                    SqlDataReader oReader = oCmd.ExecuteReader();
                                    while (oReader.Read())
                                    {
                                        company = new CompanyDetails()
                                        {
                                            Id = Convert.ToInt32(oReader["Id"]),
                                            CompanyCode = oReader["CompanyCode"].ToString(),
                                            CompanyName = oReader["CompanyName"].ToString(),
                                            CompanyType = oReader["CompanyType"].ToString(),
                                            ConnectionString = oReader["ConnectionString"].ToString(),
                                        };
                                    }
                                    oReader.Close();
                                    myConnection.Close();
                                }
                                if (company != null)
                                {
                                    try
                                    {
                                        var optionsBuilder = new DbContextOptionsBuilder<APSDBContext>();
                                        optionsBuilder.UseSqlServer(company.ConnectionString);

                                        using (APSDBContext dbContext = new APSDBContext(optionsBuilder.Options))
                                        {
                                            var userManagerCND = new UserManager<User>(new UserStore<User, Role, APSDBContext>(dbContext), null, new PasswordHasher<User>(), null, null, null, null, null, null);
                                            RequestResponseLog requestresponse = new RequestResponseLog();

                                            OrderService orderService = new OrderService(dbContext, _configuration, _mapper, _env, _commonContext);
                                            try
                                            {
                                                requestresponse.Request = JsonConvert.SerializeObject(request);
                                                requestresponse.RequestOn = DateTime.Now;
                                                dbContext.RequestResponseLogs.Add(requestresponse);
                                                dbContext.SaveChanges();

                                                var uploaddocumentresponse = new UploadDocumentResponse();
                                                uploaddocumentresponse = orderService.UploadDocument(request);

                                                if (uploaddocumentresponse != null)
                                                {
                                                    if (uploaddocumentresponse.StatusCode == StatusCodes.SuccessStatusCode)
                                                    {
                                                        TotalSuccess = TotalSuccess + 1;
                                                    }
                                                    else
                                                    {
                                                        TotalFailure = TotalFailure + 1;
                                                    }
                                                }
                                                else
                                                {
                                                    TotalFailure = TotalFailure + 1;
                                                }

                                                requestresponse.Response = JsonConvert.SerializeObject(uploaddocumentresponse);
                                                requestresponse.ResponseOn = DateTime.Now;
                                                dbContext.RequestResponseLogs.Update(requestresponse);
                                                dbContext.SaveChanges();

                                                DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                                documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                                documentuploadrequest.ExpertDocumentId = re.documentid;
                                                //documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.SuccessStatusCode, StatusMessage = Constants.Success, documentId = re.documentid };
                                                documentuploadrequest.EopResponse = new Response() { StatusCode = uploaddocumentresponse.StatusCode, StatusMessage = uploaddocumentresponse.StatusMessage, documentId = re.documentid };
                                                postdocumentuploadrequestlist.Add(documentuploadrequest);
                                            }
                                            catch (Exception ex)
                                            {
                                                TotalFailure = TotalFailure + 1;
                                                _logger.Error(ex);
                                                DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                                documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                                documentuploadrequest.ExpertDocumentId = re.documentid;
                                                documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message, documentId = re.documentid };
                                                postdocumentuploadrequestlist.Add(documentuploadrequest);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        TotalFailure = TotalFailure + 1;
                                        _logger.Error(ex);
                                        DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                        documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                        documentuploadrequest.ExpertDocumentId = re.documentid;
                                        documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message, documentId = re.documentid };
                                        postdocumentuploadrequestlist.Add(documentuploadrequest);
                                    }
                                }
                                else
                                {
                                    TotalFailure = TotalFailure + 1;
                                    DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                    documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                    documentuploadrequest.ExpertDocumentId = re.documentid;
                                    documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = Constants.CarrierInfoNotFound, documentId = re.documentid };
                                    postdocumentuploadrequestlist.Add(documentuploadrequest);
                                }
                            }

                            catch (Exception ex)
                            {
                                TotalFailure = TotalFailure + 1;
                                _logger.Error(ex);
                                DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                documentuploadrequest.Companycode = Convert.ToString(re.company_code);
                                documentuploadrequest.ExpertDocumentId = re.documentid;
                                documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message, documentId = re.documentid };
                                postdocumentuploadrequestlist.Add(documentuploadrequest);
                            }
                        }
                        try
                        {
                            if (postdocumentuploadrequestlist != null && postdocumentuploadrequestlist.Any())
                            {
                                _logger.Info("documents are Pushed and ready for send Response to eXpert");
                                var requestresponse = new RequestResponseLog()
                                {
                                    Request = JsonConvert.SerializeObject(postdocumentuploadrequestlist),
                                    RequestOn = DateTime.Now
                                };
                                _context.RequestResponseLogs.Add(requestresponse);
                                _context.SaveChanges();

                                var postcommentsresponse = APIHelper.PostDocumentUpload(postdocumentuploadrequestlist, _configuration["DecouplingapiBaseUrl"]).Result;

                                requestresponse.Response = JsonConvert.SerializeObject(postcommentsresponse);
                                requestresponse.ResponseOn = DateTime.Now;
                                _context.RequestResponseLogs.Update(requestresponse);
                                _context.SaveChanges();

                                _logger.Info(string.Format("Total document Pushed:{0}", postdocumentuploadrequestlist.Count));

                                requestresponseall.Response = JsonConvert.SerializeObject(postdocumentuploadrequestlist);
                                requestresponseall.ModifiedDateTime = DateTime.Now;
                                requestresponseall.TotalSuccess = TotalSuccess;
                                requestresponseall.TotalFailed = TotalFailure;
                                requestresponseall.IsProcessed = false;
                                _context.DecouplingRequests.Update(requestresponseall);
                                _context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }

                    }
                }
                else
                {
                    _logger.Info("DeCoupling UploadDocument API: No Record found");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void KOCDailySummaryReport()
        {
            _rootDirectoryPath = _configuration["FolderPath"];
            _logFilePath = _rootDirectoryPath + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='KOC'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }

                var connectionString = companyDetail.ConnectionString;
                var selectSqlQuery = $@"SELECT DISTINCT OrderFacility.eNoahOrderId,AuthDocument.PageCount,AuthDocument.CreatedOnDateTime,AuthDocument.ModifiedDateTime,Patient.PolicyNumber,Patient.FirstName, Patient.LastName,AuthDocument.DocumentType,AuthDocument.Path,AuthDocument.DocumentName
											FROM [dbo].[OrderFacility] AS OrderFacility
                                            INNER JOIN [dbo].[Order] As O ON O.Id = OrderFacility.OrderId
                                            INNER JOIN [dbo].[Patient] AS Patient ON Patient.Id = O.PatientId
                                            INNER JOIN [dbo].[OrderOfficeInformation] AS OrderingOffice ON OrderingOffice.Id = O.OrderOfficeId
                                            INNER JOIN [dbo].[OrderStatus] As OrderStatus ON OrderStatus.FacilityMapId = OrderFacility.Id
                                            INNER JOIN [dbo].[AuthDocument] AS AuthDocument ON AuthDocument.OrderFacilityId = OrderFacility.Id AND AuthDocument.IsSavedExternally = 1 AND AuthDocument.IsDeleted = 0
                                            WHERE OrderFacility.IsDeleted = 0 AND CONVERT(DATE,AuthDocument.ModifiedDateTime,110)=CONVERT(DATE, DATEADD(DAY,-1,GETDATE()),110)";

                var headerString = "eNoah_CaseNumber,PageCount,DateScanned,DateExported,PolicyNumber,FirstName,LastName,ImageType";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        headerString += "\r\n" + Convert.ToString(reader["eNoahOrderId"]) + "," + Convert.ToString(reader["PageCount"]) + "," + Convert.ToString(reader["CreatedOnDateTime"]) + "," + Convert.ToString(reader["ModifiedDateTime"]) + "," + Support.Decrypt(Convert.ToString(reader["PolicyNumber"])) + "," + Support.Decrypt(Convert.ToString(reader["FirstName"])) + "," + Support.Decrypt(Convert.ToString(reader["LastName"])) + "," + Convert.ToString(reader["DocumentType"]);
                    }
                    connection.Close();
                }

                string sftpHostUrl = string.Empty;
                int sftpPortNumber = 0;
                string sftpUserName = string.Empty;
                string sftpPassword = string.Empty;
                string sftpFolderPath = string.Empty;
                string sftpArchiveFolderPath = string.Empty;
                //
                using (var myConnection = new SqlConnection(defaulltConnectionString))
                {
                    var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='KOC' AND IsInbound=0";
                    var oCmd = new SqlCommand(query, myConnection);
                    myConnection.Open();
                    var oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                        sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                        sftpUserName = Convert.ToString(oReader["UserName"]);
                        sftpPassword = Convert.ToString(oReader["Password"]);
                        sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                        sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                //
                var kocDailySummaryReportPath = _configuration["KOCDailySummaryReportPath"];
                var KOCDailySummaryReportServerPath = _configuration["KOCDailySummaryReportServerPath"];
                using (var client = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                {
                    try
                    {
                        client.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                        client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                        client.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                        client.Connect();
                        client.WriteAllText(kocDailySummaryReportPath + "ENOAH_" + DateTime.Now.ToString("yyyyMMdd") + "_DailySummary.txt", headerString);
                        File.WriteAllText(KOCDailySummaryReportServerPath + "ENOAH_" + DateTime.Now.ToString("yyyyMMdd") + "_DailySummary.txt", headerString);
                    }
                    catch (Exception ex)
                    {
                        WriteLog("Exception - " + DateTime.Now.ToString() + ": KOC - Daily Report - SFTP - " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                    }
                    finally
                    {
                        client.Disconnect();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog("Exception - " + DateTime.Now.ToString() + ": KOC - Daily Report - " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
            }
        }
        public void CheckCommentsAvailability()
        {
            try
            {
                _logger.Info("CheckCommentsAvailability API started");
                AvailabilityCommentsList resultDetailsResponse = new AvailabilityCommentsList();
                List<ResultCount> FailedCommentsCount = new List<ResultCount>();
                DataTable CommentResponse = new DataTable();
                CommentResponse.Columns.Add("Carrier Name", typeof(System.String));
                CommentResponse.Columns.Add("Case Number", typeof(System.String));
                CommentResponse.Columns.Add("Comment Log Id", typeof(System.String));
                CommentResponse.Columns.Add("Comments", typeof(System.String));
                CommentResponse.Columns.Add("Order Stage", typeof(System.String));
                CommentResponse.Columns.Add("Event Type", typeof(System.String));
                CommentResponse.Columns.Add("Comments Created Date", typeof(System.String));
                resultDetailsResponse = APIHelper.GetCommentsAvailablity(_configuration["apiBaseUrl"]).Result;
                if (resultDetailsResponse != null && resultDetailsResponse.result.Count > 0)
                {
                    _logger.Info(string.Format("CheckCommentsAvailability Total record received :{0}", resultDetailsResponse.result.Count));
                    RequestResponseLog requestresponseall = new RequestResponseLog();
                    requestresponseall.Request = JsonConvert.SerializeObject(resultDetailsResponse.result);
                    requestresponseall.RequestOn = DateTime.Now;
                    _context.RequestResponseLogs.Add(requestresponseall);
                    _context.SaveChanges();
                    List<AvailabilityCommentsList> commentslists = new List<AvailabilityCommentsList>();
                    DataSet ds = new DataSet();
                    try
                    {
                        List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                        var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                        using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                        {
                            string oString = "SELECT CompanyCode,CompanyType,ConnectionString FROM [dbo].[CompanyDetail]";
                            SqlCommand oCmd = new SqlCommand(oString, myConnection);
                            myConnection.Open();
                            SqlDataReader oReader = oCmd.ExecuteReader();
                            while (oReader.Read())
                            {
                                CompanyDetails company = new CompanyDetails()
                                {
                                    CompanyCode = oReader["CompanyCode"].ToString(),
                                    CompanyType = oReader["CompanyType"].ToString(),
                                    ConnectionString = oReader["ConnectionString"].ToString(),
                                };
                                companyDetails.Add(company);
                            }
                            oReader.Close();
                            myConnection.Close();
                        }
                        if (companyDetails != null && companyDetails.Any())
                        {
                            for (int i = 0; i < companyDetails.Count; i++)
                            {
                                try
                                {
                                    CompanyDetails companyDetail = companyDetails[i];
                                    var results = resultDetailsResponse.result.Where(a => a.CompanyCode.ToLower() == companyDetail.CompanyCode.ToLower()).ToList();
                                    DataTable dtOrdersList = new DataTable();
                                    if (results.Count > 0)
                                    {
                                        using (var reader = ObjectReader.Create(results))
                                        {
                                            dtOrdersList.Load(reader);
                                        }
                                        using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                                        {
                                            var exec = _context.Database.GetDbConnection();
                                            string connectionString = exec.ConnectionString.ToString();
                                            SqlConnection sqlCon = new SqlConnection(connectionString);
                                            sqlCon.Open();
                                            SqlCommand sqlCmd = new SqlCommand("sp_GetOrderingOfficeInformation", sqlCon);
                                            sqlCmd.CommandType = CommandType.StoredProcedure;
                                            sqlCmd.Parameters.AddWithValue("@CommentsList", dtOrdersList);
                                            SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                                            sqlDa.Fill(ds);
                                            sqlCon.Close();
                                            myConnection.Close();
                                            if (ds != null && ds.Tables.Count > 0)
                                            {
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    ResultCount resultcount = new ResultCount();
                                                    resultcount.CompanyCode = companyDetail.CompanyCode.ToLower();
                                                    resultcount.MissingCounts = ds.Tables[0].Rows.Count;
                                                    FailedCommentsCount.Add(resultcount);
                                                    foreach (DataRow dr in ds.Tables[0].Rows)
                                                    {
                                                        CommentResponse.Rows.Add(
                                                        Convert.ToString(dr["CompanyCode"]),
                                                        Convert.ToString(dr["eNoahOrderId"]),
                                                        Convert.ToString(dr["eXpertLogId"]),
                                                        Convert.ToString(dr["Comments"]),
                                                        Convert.ToString(dr["OrderStage"]),
                                                        Convert.ToString(dr["EventType"]),
                                                        Convert.ToString(dr["CommentsCreatedDate"]));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Error(ex);
                                }
                            }
                        }
                        if (CommentResponse != null && CommentResponse.Rows.Count > 0)
                        {
                            _logger.Info("Check Comments Availability API Ready for mail sending");
                            string File = _configuration["CommentsReconReportPath"] + "Comments_Recon_Report_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                            Export(CommentResponse, File, "CommentsList");
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            DataRow dr = CommentResponse.Rows[0];
                            helper.SendMailCommentsReconReport(File, true, false, FailedCommentsCount, Convert.ToString(dr["Comments Created Date"]));
                            requestresponseall.Response = JsonConvert.SerializeObject(CommentResponse);
                            requestresponseall.ResponseOn = DateTime.Now;
                            _context.RequestResponseLogs.Update(requestresponseall);
                            _context.SaveChanges();
                        }
                        else
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendMailCommentsReconReport("", false, false, FailedCommentsCount, "");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            _logger.Info("CheckCommentsAvailability API Stopped");
        }
        public bool Export(DataTable dt, string file, string sheetName)
        {
            if (!Directory.Exists(Path.GetDirectoryName(file)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(file));
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, sheetName);
                wb.SaveAs(file);
            }
            return true;
        }
        private bool CheckRunFile(string FileName)
        {
            return File.ReadAllLines(_configuration["RunLog"]).Contains(FileName);
        }
        public class MTDCount
        {
            public string CompanyCode { get; set; }
            public int MTD_Count { get; set; }
        }
        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        private List<DailyCount> GetDailyCounts(List<DailyCount> data, DateTime todate)
        {
            List<DailyCount> result = new List<DailyCount>();
            int toDate = todate.Date.Day;
            int negDateDiff = -System.Math.Abs(toDate);
            DateTime date = todate.AddDays(negDateDiff);
            for (int i = 0; i < toDate; i++)
            {
                date = date.AddDays(1);
                var count = data.Where(a => a.Date.Date == date.Date).FirstOrDefault();
                if (count != null)
                    result.Add(new DailyCount() { Date = date, Count = count.Count });
                else
                    result.Add(new DailyCount() { Date = date, Count = 0 });
            }
            return result;
        }
        private List<DailyCount> GetDailyCountswithtool(List<DailyCount> data, List<DailyCount> tooldata, DateTime todate)
        {
            List<DailyCount> result = new List<DailyCount>();
            int toDate = todate.Date.Day;
            int negDateDiff = -System.Math.Abs(toDate);
            DateTime date = todate.AddDays(negDateDiff);
            for (int i = 0; i < toDate; i++)
            {
                DailyCount dc = new DailyCount();
                date = date.AddDays(1);
                var count = data.Where(a => a.Date.Date == date.Date).FirstOrDefault();
                var toolcount = tooldata.Where(a => a.Date.Date == date.Date).FirstOrDefault();
                if (count != null && toolcount != null)
                {
                    dc.Count = toolcount.Count + count.Count;
                    dc.Date = date;
                }
                else if (count != null && toolcount == null)
                {
                    dc.Count = count.Count;
                    dc.Date = date;
                }
                else if (count == null && toolcount != null)
                {
                    dc.Count = toolcount.Count;
                    dc.Date = date;
                }
                else
                {
                    dc.Count = 0;
                    dc.Date = date;
                }
                result.Add(dc);
            }
            return result;
        }
        private string IndexToColumn(int index)
        {
            int ColumnBase = 26;
            int DigitMax = 7;
            string Digits = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (index <= 0)
                throw new IndexOutOfRangeException("index must be a positive number");

            if (index <= ColumnBase)
                return Digits[index - 1].ToString();

            var sb = new StringBuilder().Append(' ', DigitMax);
            var current = index;
            var offset = DigitMax;
            while (current > 0)
            {
                sb[--offset] = Digits[--current % ColumnBase];
                current /= ColumnBase;
            }
            return sb.ToString(offset, DigitMax - offset);
        }
        public void DeCouplingUpdateSummarizationDocumentAPI()
        {
            int TotalSuccess = 0;
            int TotalFailure = 0;
            try
            {
                List<DocumentUploadRequest> postdocumentuploadrequestlist = new List<DocumentUploadRequest>();
                var uploadresponse = APIHelper.GetUpdateSummarizationDocument(_configuration["DecouplingapiBaseUrl"]).Result;

                if (uploadresponse != null)
                {
                    _logger.Info("DeCoupling Update Summarization Document API Started");
                    var finalresult = uploadresponse.results;
                    if (finalresult != null && finalresult.Any())
                    {
                        _logger.Info(string.Format("Total Summarization Document Received:{0}", finalresult.Count));

                        DecouplingRequests requestresponseall = new DecouplingRequests();
                        requestresponseall.Request = JsonConvert.SerializeObject(finalresult);
                        requestresponseall.CreatedOnDateTime = DateTime.Now;
                        requestresponseall.TotalRecord = finalresult.Count;
                        requestresponseall.IsProcessed = true;
                        requestresponseall.ProcessType = "decouplingupdatesummarizationdocument";
                        _context.DecouplingRequests.Add(requestresponseall);
                        _context.SaveChanges();

                        foreach (var re in finalresult)
                        {
                            try
                            {
                                var request = JsonConvert.DeserializeObject<UpdateSummarizationDocumentRequest>(re.request_json);

                                string defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                                CompanyDetails company = null;

                                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                                {
                                    string oString = $"SELECT * FROM [dbo].[CompanyDetail] WHERE CompanyCode = '{request.companyCode}'";
                                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                    myConnection.Open();
                                    SqlDataReader oReader = oCmd.ExecuteReader();
                                    while (oReader.Read())
                                    {
                                        company = new CompanyDetails()
                                        {
                                            Id = Convert.ToInt32(oReader["Id"]),
                                            CompanyCode = oReader["CompanyCode"].ToString(),
                                            CompanyName = oReader["CompanyName"].ToString(),
                                            CompanyType = oReader["CompanyType"].ToString(),
                                            ConnectionString = oReader["ConnectionString"].ToString(),
                                        };
                                    }
                                    oReader.Close();
                                    myConnection.Close();
                                }
                                if (company != null)
                                {
                                    try
                                    {
                                        var optionsBuilder = new DbContextOptionsBuilder<APSDBContext>();
                                        optionsBuilder.UseSqlServer(company.ConnectionString);

                                        using (APSDBContext dbContext = new APSDBContext(optionsBuilder.Options))
                                        {
                                            var userManagerCND = new UserManager<User>(new UserStore<User, Role, APSDBContext>(dbContext), null, new PasswordHasher<User>(), null, null, null, null, null, null);
                                            RequestResponseLog requestresponse = new RequestResponseLog();

                                            OrderService orderService = new OrderService(dbContext, _configuration, _mapper, _env, _commonContext);
                                            try
                                            {
                                                requestresponse.Request = JsonConvert.SerializeObject(request);
                                                requestresponse.RequestOn = DateTime.Now;
                                                dbContext.RequestResponseLogs.Add(requestresponse);
                                                dbContext.SaveChanges();

                                                var uploaddocumentresponse = new UploadDocumentResponse();
                                                uploaddocumentresponse = orderService.UpdateSummarizationDocument(request);

                                                if (uploaddocumentresponse != null)
                                                {
                                                    if (uploaddocumentresponse.StatusCode == StatusCodes.SuccessStatusCode)
                                                    {
                                                        TotalSuccess = TotalSuccess + 1;
                                                    }
                                                    else
                                                    {
                                                        TotalFailure = TotalFailure + 1;
                                                    }
                                                }
                                                else
                                                {
                                                    TotalFailure = TotalFailure + 1;
                                                }

                                                requestresponse.Response = JsonConvert.SerializeObject(uploaddocumentresponse);
                                                requestresponse.ResponseOn = DateTime.Now;
                                                dbContext.RequestResponseLogs.Update(requestresponse);
                                                dbContext.SaveChanges();

                                                DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                                documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                                documentuploadrequest.ExpertDocumentId = re.documentid;
                                                //documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.SuccessStatusCode, StatusMessage = Constants.Success, documentId = re.documentid };
                                                documentuploadrequest.EopResponse = new Response() { StatusCode = uploaddocumentresponse.StatusCode, StatusMessage = uploaddocumentresponse.StatusMessage, documentId = re.documentid };
                                                postdocumentuploadrequestlist.Add(documentuploadrequest);
                                            }
                                            catch (Exception ex)
                                            {
                                                TotalFailure = TotalFailure + 1;
                                                _logger.Error(ex);
                                                DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                                documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                                documentuploadrequest.ExpertDocumentId = re.documentid;
                                                documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message, documentId = re.documentid };
                                                postdocumentuploadrequestlist.Add(documentuploadrequest);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        TotalFailure = TotalFailure + 1;
                                        _logger.Error(ex);
                                        DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                        documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                        documentuploadrequest.ExpertDocumentId = re.documentid;
                                        documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message, documentId = re.documentid };
                                        postdocumentuploadrequestlist.Add(documentuploadrequest);
                                    }
                                }
                                else
                                {
                                    TotalFailure = TotalFailure + 1;
                                    DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                    documentuploadrequest.Companycode = Convert.ToString(request.companyCode);
                                    documentuploadrequest.ExpertDocumentId = re.documentid;
                                    documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = Constants.CarrierInfoNotFound, documentId = re.documentid };
                                    postdocumentuploadrequestlist.Add(documentuploadrequest);
                                }
                            }

                            catch (Exception ex)
                            {
                                TotalFailure = TotalFailure + 1;
                                _logger.Error(ex);
                                DocumentUploadRequest documentuploadrequest = new DocumentUploadRequest();
                                documentuploadrequest.Companycode = Convert.ToString(re.company_code);
                                documentuploadrequest.ExpertDocumentId = re.documentid;
                                documentuploadrequest.EopResponse = new Response() { StatusCode = Models.Constants.StatusCodes.FailedException, StatusMessage = ex.Message, documentId = re.documentid };
                                postdocumentuploadrequestlist.Add(documentuploadrequest);
                            }
                        }
                        try
                        {
                            if (postdocumentuploadrequestlist != null && postdocumentuploadrequestlist.Any())
                            {
                                _logger.Info("Update summarization documents are Pushed and ready for send Response to eXpert");
                                var requestresponse = new RequestResponseLog()
                                {
                                    Request = JsonConvert.SerializeObject(postdocumentuploadrequestlist),
                                    RequestOn = DateTime.Now
                                };
                                _context.RequestResponseLogs.Add(requestresponse);
                                _context.SaveChanges();

                                var postcommentsresponse = APIHelper.PostUpdateSummarizationDocument(postdocumentuploadrequestlist, _configuration["DecouplingapiBaseUrl"]).Result;

                                requestresponse.Response = JsonConvert.SerializeObject(postcommentsresponse);
                                requestresponse.ResponseOn = DateTime.Now;
                                _context.RequestResponseLogs.Update(requestresponse);
                                _context.SaveChanges();

                                _logger.Info(string.Format("Total Summarization document Pushed:{0}", postdocumentuploadrequestlist.Count));

                                requestresponseall.Response = JsonConvert.SerializeObject(postdocumentuploadrequestlist);
                                requestresponseall.ModifiedDateTime = DateTime.Now;
                                requestresponseall.TotalSuccess = TotalSuccess;
                                requestresponseall.TotalFailed = TotalFailure;
                                requestresponseall.IsProcessed = false;
                                _context.DecouplingRequests.Update(requestresponseall);
                                _context.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }

                    }
                }
                else
                {
                    _logger.Info("DeCoupling Summarization Document API: No Record found");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public void ResultsPreviousDayReport()
        {
            try
            {
                _logger.Info("Results Previous Day Report API Started");
                DataTable ds = new DataTable();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    using (SqlCommand sqlCmd = new SqlCommand("SP_Get_Results_Previous_Day_Report", myConnection))
                    {
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        using (var da = new SqlDataAdapter(sqlCmd))
                        {
                            myConnection.Open();
                            sqlCmd.CommandType = CommandType.StoredProcedure;
                            sqlCmd.CommandTimeout = 0;
                            SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                            sqlDa.Fill(ds);
                            myConnection.Close();
                        }
                        if (ds != null && ds.Rows.Count > 0)
                        {
                            DataColumnCollection columns = ds.Columns;
                            if (columns.Contains("ErrorMessage"))
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendResultsPreviousDayReport("", "Error");
                                _logger.Error(Convert.ToString(ds.Rows[0]["ErrorMessage"]));
                            }
                            else
                            {
                                if (ds.Rows.Count > 0)
                                {
                                    _logger.Info("Results Previous Day Report: Records Received,Ready to Export on Local Path.");
                                    string File = _configuration["ResultsReportPath"] + "Results_Previous_Day_Report_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                    for (int i = 0; i < ds.Rows.Count; i++)
                                    {
                                        ds.Rows[i]["FirstName"] = Support.Decrypt(Convert.ToString(ds.Rows[i]["FirstName"]));
                                        ds.Rows[i]["LastName"] = Support.Decrypt(Convert.ToString(ds.Rows[i]["LastName"]));
                                        ds.Rows[i]["PolicyNumber"] = Support.Decrypt(Convert.ToString(ds.Rows[i]["PolicyNumber"]));
                                    }
                                    ds.AcceptChanges();
                                    if (Export(ds, File, "Results_Previous_Day_Report"))
                                    {
                                        _logger.Info("Results Previous Day Report:Ready to send Mail");
                                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                        helper.SendResultsPreviousDayReport(File, "Complete");
                                        _logger.Info("Results Previous Day Report:The email was successfully delivered");
                                    }
                                }
                                else
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendResultsPreviousDayReport("", "NoRecords");
                                    _logger.Info("Results Previous Day Report:No Records found");
                                }
                            }
                        }
                        else
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendResultsPreviousDayReport("", "NoRecords");
                            _logger.Info("Results Previous Day Report:No Records found");
                        }
                    }
                }
                _logger.Info("Results Previous Day Report API Stopped");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                helper.SendResultsPreviousDayReport("", "Error");
                _logger.Error("Results Previous Day Report:Error occured");
            }
        }
        public ResponseBase CopyResultsToSFTPFolder_ANCIOClaims()
        {
            _logger.Info("Copy Results To SFTPFolder ANCIOClaims Started");
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;
            try
            {
                List<AuthDocumentViewModel> authDocuments = null;
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                List<CompanyConfiguration> companyCofig = new List<CompanyConfiguration>();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = "SELECT * FROM [dbo].[CompanyDetail] where companycode='anicoclaims'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                if (companyDetails != null && companyDetails.Any())
                {
                    authDocuments = new List<AuthDocumentViewModel>();
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];
                            string[] companies = _configuration.GetSection("CopyResultsToSFTPFolderCompanies").Get<string[]>();
                            companies = companies.Select(c => c.ToLower()).ToArray();
                            if (companies.Contains(companyDetail.CompanyCode.ToLower()))
                            {
                                switch (companyDetail.CompanyCode.ToLower())
                                {
                                    case "anicoclaims":
                                        {
                                            string connectionString = companyDetail.ConnectionString;
                                            string selectSqlQuery = $@"SELECT DISTINCT OrderFacility.eNoahOrderId AS ENoahOrderId, Patient.PolicyNumber AS PolicyNumber, OrderingOffice.OfficeCode AS OfficeCode, OrderFacility.DoctorId AS DoctorId, AuthDocument.Id AS Id, AuthDocument.Path As Path, AuthDocument.DocumentName As DocumentName, AuthDocument.Title AS Title, AuthDocument.DocumentType AS DoumentType
                                            ,OrderFacility.ParentId AS ParentId, OrderFacility.ParentEnoahOrderId AS ParentEnoahOrderId,OrderingOffice.OfficeAccountNumber as OfficeAccountNumber
                                            FROM [dbo].[OrderFacility] AS OrderFacility
                                            INNER JOIN [dbo].[Order] As O ON O.Id = OrderFacility.OrderId
                                            INNER JOIN [dbo].[Patient] AS Patient ON Patient.Id = O.PatientId
                                            INNER JOIN [dbo].[OrderOfficeInformation] AS OrderingOffice ON OrderingOffice.Id = O.OrderOfficeId
                                            INNER JOIN [dbo].[OrderStatus] As OrderStatus ON OrderStatus.FacilityMapId = OrderFacility.Id
                                            INNER JOIN [dbo].[AuthDocument] AS AuthDocument ON AuthDocument.OrderFacilityId = OrderFacility.Id AND AuthDocument.IsFromExpertTool = 1 AND AuthDocument.IsSavedExternally = 0 AND AuthDocument.IsDeleted = 0 AND AuthDocument.StatusFlag != 1
                                            WHERE OrderFacility.IsDeleted = 0";
                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                                                SqlDataReader sqlDataReader = command.ExecuteReader();

                                                while (sqlDataReader.Read())
                                                {
                                                    AuthDocumentViewModel authDocument = new AuthDocumentViewModel();
                                                    var eNoahOrderId = sqlDataReader.GetValue(0);
                                                    var policyNumber = sqlDataReader.GetValue(1);
                                                    var orderingOfficeCode = sqlDataReader.GetValue(2);
                                                    var doctorId = sqlDataReader.GetValue(3);
                                                    var id = sqlDataReader.GetValue(4);
                                                    var path = sqlDataReader.GetValue(5);
                                                    var documentName = sqlDataReader.GetValue(6);
                                                    var title = sqlDataReader.GetValue(7);
                                                    var documentType = sqlDataReader.GetValue(8);
                                                    var parentIdObject = sqlDataReader.GetValue(9);
                                                    var parentEnoahOrderId = sqlDataReader.GetValue(10);
                                                    var OfficeAccountNumber = sqlDataReader.GetValue(11);

                                                    if (doctorId != null)
                                                    {
                                                        bool hasMatchedDoctor = false;
                                                        if (companyDetail.CompanyType.ToUpper() == CompanyType.LIFE.ToString())
                                                        {
                                                            hasMatchedDoctor = _commonContext.DoctorCommon.Any(d => d.Id == Convert.ToInt32(doctorId.ToString()));
                                                        }
                                                        else
                                                        {
                                                            hasMatchedDoctor = _commonContext.CustodianCommon.Any(d => d.Id == Convert.ToInt32(doctorId.ToString()));
                                                        }
                                                        _logger.Info(companyDetail.CompanyType);
                                                        _logger.Info(CompanyType.LIFE.ToString());
                                                        _logger.Info(doctorId.ToString());
                                                        if (hasMatchedDoctor)
                                                        {
                                                            if (eNoahOrderId != null)
                                                            {
                                                                if (parentIdObject != null)
                                                                {
                                                                    bool successfullyParsed = int.TryParse(parentIdObject.ToString(), out int parentId);

                                                                    if (successfullyParsed && parentId > 0 && !string.IsNullOrEmpty(parentEnoahOrderId.ToString()) && parentEnoahOrderId.ToString().ToLower() != "null")
                                                                    {
                                                                        eNoahOrderId = parentEnoahOrderId;
                                                                    }
                                                                }
                                                                authDocument.eNoahOrderId = eNoahOrderId.ToString();
                                                            }

                                                            if (policyNumber != null)
                                                            {
                                                                string policyNumberString = policyNumber.ToString();
                                                                string decryptPolicyNumber = Support.Decrypt(policyNumberString);
                                                                if (!string.IsNullOrEmpty(decryptPolicyNumber))
                                                                {
                                                                    authDocument.eNoahOrderId = decryptPolicyNumber;
                                                                }
                                                            }

                                                            if (orderingOfficeCode != null)
                                                            {
                                                                string officeCode = orderingOfficeCode.ToString();

                                                                if (officeCode.Length > 2)
                                                                {
                                                                    officeCode = officeCode.Substring(1);
                                                                }
                                                                authDocument.Description = officeCode;
                                                            }
                                                            if (id != null)
                                                            {
                                                                authDocument.Id = Convert.ToInt32(id.ToString());
                                                            }

                                                            if (path != null)
                                                            {
                                                                authDocument.Path = path.ToString();
                                                            }

                                                            if (documentName != null)
                                                            {
                                                                authDocument.DocumentName = documentName.ToString();
                                                            }

                                                            if (title != null)
                                                            {
                                                                authDocument.Title = title.ToString();
                                                            }

                                                            if (documentType != null)
                                                            {
                                                                authDocument.DocumentType = documentType.ToString();
                                                            }
                                                            authDocument.OfficeAccountNumber = Convert.ToString(OfficeAccountNumber);
                                                            authDocuments.Add(authDocument);
                                                        }
                                                        else
                                                        {
                                                            _logger.Error("Facility not matched");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _logger.Error("Doctor id not found");
                                                    }
                                                }
                                                connection.Close();
                                            }

                                            if (authDocuments != null && authDocuments.Any())
                                            {
                                                string sftpHostUrl = string.Empty;
                                                int sftpPortNumber = 0;
                                                string sftpUserName = string.Empty;
                                                string sftpPassword = string.Empty;
                                                string sftpFolderPath = string.Empty;
                                                string sftpArchiveFolderPath = string.Empty;
                                                string Type = string.Empty;
                                                using (var myConnection = new SqlConnection(defaultConnectionString))
                                                {
                                                    var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath,[Type] FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='anicoclaims' AND IsInbound=0";
                                                    var oCmd = new SqlCommand(query, myConnection);
                                                    myConnection.Open();
                                                    var oReader = oCmd.ExecuteReader();
                                                    while (oReader.Read())
                                                    {
                                                        CompanyConfiguration compconfig = new CompanyConfiguration()
                                                        {
                                                            HostUrl = Convert.ToString(oReader["HostUrl"]),
                                                            PortNumber = Convert.ToInt32(oReader["PortNumber"]),
                                                            UserName = Convert.ToString(oReader["UserName"]),
                                                            Password = Convert.ToString(oReader["Password"]),
                                                            FolderPath = Convert.ToString(oReader["FolderPath"]),
                                                            ArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]),
                                                            Type = Convert.ToString(oReader["Type"])
                                                        };
                                                        companyCofig.Add(compconfig);
                                                    }
                                                    oReader.Close();
                                                    myConnection.Close();
                                                }

                                                foreach (AuthDocumentViewModel authDocument in authDocuments)
                                                {
                                                    bool IsUpload = true;
                                                    try
                                                    {
                                                        UploadFileRequest request = new UploadFileRequest
                                                        {
                                                            CompanyCode = authDocument.Path,
                                                            UserId = authDocument.DocumentName
                                                        };
                                                        DocDownloadResponse docDownloadResponse = GetFileStreamAsync(request).Result;

                                                        if (docDownloadResponse != null)
                                                        {
                                                            int lastIndexValue = docDownloadResponse.Name.LastIndexOf(".");
                                                            string fileExtension = docDownloadResponse.Name.Substring(lastIndexValue + 1);
                                                            string randomNumber = GetUniqueTxnId();
                                                            string formattedFileName = string.Empty;
                                                            if (authDocument.OfficeAccountNumber == "USC00154")
                                                            {
                                                                formattedFileName = $"{authDocument.eNoahOrderId}_MEDREC_{DateTime.Now:yyyyMMdd}_{randomNumber}.{fileExtension}";
                                                            }
                                                            else if (authDocument.OfficeAccountNumber == "USC00155")
                                                            {
                                                                formattedFileName = $"{authDocument.eNoahOrderId}_MR_{DateTime.Now:yyyyMMdd}_{randomNumber}.{fileExtension}";
                                                            }

                                                            var configdata = companyCofig.Where(a => a.Type == authDocument.OfficeAccountNumber).FirstOrDefault();
                                                            if (configdata != null)
                                                            {
                                                                sftpHostUrl = configdata.HostUrl;
                                                                sftpPortNumber = configdata.PortNumber;
                                                                sftpUserName = configdata.UserName;
                                                                sftpPassword = configdata.Password;
                                                                sftpFolderPath = configdata.FolderPath;
                                                                sftpArchiveFolderPath = configdata.ArchiveFolderPath;

                                                                string remoteFilePath = sftpFolderPath + formattedFileName;
                                                                string remoteFileArchivePath = sftpArchiveFolderPath + formattedFileName;
                                                                using var client = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword);
                                                                try
                                                                {
                                                                    _logger.Info($"ANCIOClaims - SFTP connection establishment has started {sftpHostUrl}");
                                                                    client.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                                    client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                                    client.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                                    client.Connect();
                                                                    _logger.Info($"ANCIOClaims - SFTP connection establishment was successful {sftpHostUrl}");
                                                                    client.WriteAllBytes(remoteFilePath, docDownloadResponse.Bytes);
                                                                    client.WriteAllBytes(remoteFileArchivePath, docDownloadResponse.Bytes);
                                                                    _logger.Info($"ANCIOClaims - File write sftp to {remoteFilePath}");
                                                                    _logger.Info($"ANCIOClaims - File write sftp to {remoteFileArchivePath}");
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    _logger.Info($"ANCIOClaims - sftp File {remoteFilePath} NOT copied successfully");
                                                                    _logger.Error(ex);
                                                                    IsUpload = false;
                                                                }
                                                                finally
                                                                {
                                                                    client.Disconnect();
                                                                    _logger.Info($"ANCIOClaims - The SFTP connection was disconnected {sftpHostUrl}");
                                                                }
                                                                if (IsUpload)
                                                                {
                                                                    string path = _configuration["OrderResultsFolderPath"];
                                                                    filePath = $"{path}{companyDetail.CompanyCode}/";
                                                                    string fullPath = "";

                                                                    fullPath = filePath + formattedFileName;

                                                                    if (Directory.Exists(filePath))
                                                                    {
                                                                        File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                                    }
                                                                    else
                                                                    {
                                                                        DirectoryInfo di = Directory.CreateDirectory(filePath);
                                                                        File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                                    }
                                                                    _logger.Info($"ANCIOClaims - File {fullPath} copied successfully");
                                                                    string updateSqlQuery = $"UPDATE [dbo].[AuthDocument] SET IsSavedExternally = 1, ModifiedDateTime = GETDATE() WHERE Id = {authDocument.Id}";
                                                                    using (SqlConnection con = new SqlConnection(connectionString))
                                                                    {
                                                                        con.Open();
                                                                        SqlCommand cmd = new SqlCommand(updateSqlQuery, con);
                                                                        cmd.ExecuteNonQuery();
                                                                        con.Close();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        _logger.Error(ex);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _logger.Error("Results not found");
                                            }
                                        }
                                        break;
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }
            _logger.Info("Copy Results To SFTPFolder ANCIOClaims Completed");
            return response;
        }
        public ResponseBase CopyResultsToSFTPFolder_ANICOHealth()
        {
            _logger.Info("Copy Results To SFTPFolder ANICOHealth Started");
            ResponseBase response = new ResponseBase();
            string filePath = string.Empty;
            string carrier = string.Empty;
            try
            {
                List<AuthDocumentViewModel> authDocuments = null;
                List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                List<CompanyConfiguration> companyCofig = new List<CompanyConfiguration>();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                {
                    string oString = "SELECT * FROM [dbo].[CompanyDetail] where companycode='anicohealth'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        CompanyDetails company = new CompanyDetails()
                        {
                            Id = Convert.ToInt32(oReader["Id"]),
                            CompanyCode = oReader["CompanyCode"].ToString(),
                            CompanyName = oReader["CompanyName"].ToString(),
                            CompanyType = oReader["CompanyType"].ToString(),
                            ConnectionString = oReader["ConnectionString"].ToString(),
                        };
                        companyDetails.Add(company);
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                if (companyDetails != null && companyDetails.Any())
                {
                    authDocuments = new List<AuthDocumentViewModel>();
                    for (int i = 0; i < companyDetails.Count; i++)
                    {
                        try
                        {
                            CompanyDetails companyDetail = companyDetails[i];
                            string[] companies = _configuration.GetSection("CopyResultsToSFTPFolderCompanies").Get<string[]>();
                            companies = companies.Select(c => c.ToLower()).ToArray();
                            if (companies.Contains(companyDetail.CompanyCode.ToLower()))
                            {
                                switch (companyDetail.CompanyCode.ToLower())
                                {
                                    case "anicohealth":
                                        {
                                            string connectionString = companyDetail.ConnectionString;
                                            string selectSqlQuery = $@"SELECT DISTINCT OrderFacility.eNoahOrderId AS ENoahOrderId, Patient.PolicyNumber AS PolicyNumber, OrderingOffice.OfficeCode AS OfficeCode, OrderFacility.DoctorId AS DoctorId, AuthDocument.Id AS Id, AuthDocument.Path As Path, AuthDocument.DocumentName As DocumentName, AuthDocument.Title AS Title, AuthDocument.DocumentType AS DoumentType
                                            ,OrderFacility.ParentId AS ParentId, OrderFacility.ParentEnoahOrderId AS ParentEnoahOrderId,OrderingOffice.OfficeAccountNumber as OfficeAccountNumber
                                            FROM [dbo].[OrderFacility] AS OrderFacility
                                            INNER JOIN [dbo].[Order] As O ON O.Id = OrderFacility.OrderId
                                            INNER JOIN [dbo].[Patient] AS Patient ON Patient.Id = O.PatientId
                                            INNER JOIN [dbo].[OrderOfficeInformation] AS OrderingOffice ON OrderingOffice.Id = O.OrderOfficeId
                                            INNER JOIN [dbo].[OrderStatus] As OrderStatus ON OrderStatus.FacilityMapId = OrderFacility.Id
                                            INNER JOIN [dbo].[AuthDocument] AS AuthDocument ON AuthDocument.OrderFacilityId = OrderFacility.Id AND AuthDocument.IsFromExpertTool = 1 AND AuthDocument.IsSavedExternally = 0 AND AuthDocument.IsDeleted = 0 AND AuthDocument.StatusFlag != 1
                                            WHERE OrderFacility.IsDeleted = 0 AND OrderingOffice.OfficeAccountNumber = 'USC00212'";
                                            using (SqlConnection connection = new SqlConnection(connectionString))
                                            {
                                                connection.Open();
                                                SqlCommand command = new SqlCommand(selectSqlQuery, connection);
                                                SqlDataReader sqlDataReader = command.ExecuteReader();

                                                while (sqlDataReader.Read())
                                                {
                                                    AuthDocumentViewModel authDocument = new AuthDocumentViewModel();
                                                    var eNoahOrderId = sqlDataReader.GetValue(0);
                                                    var policyNumber = sqlDataReader.GetValue(1);
                                                    var orderingOfficeCode = sqlDataReader.GetValue(2);
                                                    var doctorId = sqlDataReader.GetValue(3);
                                                    var id = sqlDataReader.GetValue(4);
                                                    var path = sqlDataReader.GetValue(5);
                                                    var documentName = sqlDataReader.GetValue(6);
                                                    var title = sqlDataReader.GetValue(7);
                                                    var documentType = sqlDataReader.GetValue(8);
                                                    var parentIdObject = sqlDataReader.GetValue(9);
                                                    var parentEnoahOrderId = sqlDataReader.GetValue(10);
                                                    var OfficeAccountNumber = sqlDataReader.GetValue(11);

                                                    if (doctorId != null)
                                                    {
                                                        bool hasMatchedDoctor = false;
                                                        if (companyDetail.CompanyType.ToUpper() == CompanyType.LIFE.ToString())
                                                        {
                                                            hasMatchedDoctor = _commonContext.DoctorCommon.Any(d => d.Id == Convert.ToInt32(doctorId.ToString()));
                                                        }
                                                        else
                                                        {
                                                            hasMatchedDoctor = _commonContext.CustodianCommon.Any(d => d.Id == Convert.ToInt32(doctorId.ToString()));
                                                        }
                                                        _logger.Info(companyDetail.CompanyType);
                                                        _logger.Info(CompanyType.LIFE.ToString());
                                                        _logger.Info(doctorId.ToString());
                                                        if (hasMatchedDoctor)
                                                        {
                                                            if (eNoahOrderId != null)
                                                            {
                                                                if (parentIdObject != null)
                                                                {
                                                                    bool successfullyParsed = int.TryParse(parentIdObject.ToString(), out int parentId);

                                                                    if (successfullyParsed && parentId > 0 && !string.IsNullOrEmpty(parentEnoahOrderId.ToString()) && parentEnoahOrderId.ToString().ToLower() != "null")
                                                                    {
                                                                        eNoahOrderId = parentEnoahOrderId;
                                                                    }
                                                                }
                                                                authDocument.eNoahOrderId = eNoahOrderId.ToString();
                                                            }

                                                            if (policyNumber != null)
                                                            {
                                                                string policyNumberString = policyNumber.ToString();
                                                                string decryptPolicyNumber = Support.Decrypt(policyNumberString);
                                                                if (!string.IsNullOrEmpty(decryptPolicyNumber))
                                                                {
                                                                    authDocument.eNoahOrderId = decryptPolicyNumber;
                                                                }
                                                            }

                                                            if (orderingOfficeCode != null)
                                                            {
                                                                string officeCode = orderingOfficeCode.ToString();

                                                                if (officeCode.Length > 2)
                                                                {
                                                                    officeCode = officeCode.Substring(1);
                                                                }
                                                                authDocument.Description = officeCode;
                                                            }
                                                            if (id != null)
                                                            {
                                                                authDocument.Id = Convert.ToInt32(id.ToString());
                                                            }

                                                            if (path != null)
                                                            {
                                                                authDocument.Path = path.ToString();
                                                            }

                                                            if (documentName != null)
                                                            {
                                                                authDocument.DocumentName = documentName.ToString();
                                                            }

                                                            if (title != null)
                                                            {
                                                                authDocument.Title = title.ToString();
                                                            }

                                                            if (documentType != null)
                                                            {
                                                                authDocument.DocumentType = documentType.ToString();
                                                            }
                                                            authDocument.OfficeAccountNumber = Convert.ToString(OfficeAccountNumber);
                                                            authDocuments.Add(authDocument);
                                                        }
                                                        else
                                                        {
                                                            _logger.Error("Facility not matched");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _logger.Error("Doctor id not found");
                                                    }
                                                }
                                                connection.Close();
                                            }

                                            if (authDocuments != null && authDocuments.Any())
                                            {
                                                string sftpHostUrl = string.Empty;
                                                int sftpPortNumber = 0;
                                                string sftpUserName = string.Empty;
                                                string sftpPassword = string.Empty;
                                                string sftpFolderPath = string.Empty;
                                                string sftpArchiveFolderPath = string.Empty;
                                                string Type = string.Empty;
                                                using (var myConnection = new SqlConnection(defaultConnectionString))
                                                {
                                                    var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath,[Type] FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='anicohealth' AND IsInbound=0";
                                                    var oCmd = new SqlCommand(query, myConnection);
                                                    myConnection.Open();
                                                    var oReader = oCmd.ExecuteReader();
                                                    while (oReader.Read())
                                                    {
                                                        CompanyConfiguration compconfig = new CompanyConfiguration()
                                                        {
                                                            HostUrl = Convert.ToString(oReader["HostUrl"]),
                                                            PortNumber = Convert.ToInt32(oReader["PortNumber"]),
                                                            UserName = Convert.ToString(oReader["UserName"]),
                                                            Password = Convert.ToString(oReader["Password"]),
                                                            FolderPath = Convert.ToString(oReader["FolderPath"]),
                                                            ArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]),
                                                            Type = Convert.ToString(oReader["Type"])
                                                        };
                                                        companyCofig.Add(compconfig);
                                                    }
                                                    oReader.Close();
                                                    myConnection.Close();
                                                }

                                                foreach (AuthDocumentViewModel authDocument in authDocuments)
                                                {
                                                    bool IsUpload = true;
                                                    try
                                                    {
                                                        UploadFileRequest request = new UploadFileRequest
                                                        {
                                                            CompanyCode = authDocument.Path,
                                                            UserId = authDocument.DocumentName
                                                        };
                                                        DocDownloadResponse docDownloadResponse = GetFileStreamAsync(request).Result;

                                                        if (docDownloadResponse != null)
                                                        {
                                                            int lastIndexValue = docDownloadResponse.Name.LastIndexOf(".");
                                                            string fileExtension = docDownloadResponse.Name.Substring(lastIndexValue + 1);
                                                            string randomNumber = GetUniqueTxnId();
                                                            string formattedFileName = string.Empty;
                                                            if (authDocument.OfficeAccountNumber == "USC00212")
                                                            {
                                                                formattedFileName = $"{authDocument.eNoahOrderId}_MRC_{DateTime.Now:yyyyMMdd}_{randomNumber}.{fileExtension}";
                                                            }

                                                            var configdata = companyCofig.Where(a => a.Type == authDocument.OfficeAccountNumber).FirstOrDefault();
                                                            if (configdata != null)
                                                            {
                                                                sftpHostUrl = configdata.HostUrl;
                                                                sftpPortNumber = configdata.PortNumber;
                                                                sftpUserName = configdata.UserName;
                                                                sftpPassword = configdata.Password;
                                                                sftpFolderPath = configdata.FolderPath;
                                                                sftpArchiveFolderPath = configdata.ArchiveFolderPath;

                                                                string remoteFilePath = sftpFolderPath + formattedFileName;
                                                                string remoteFileArchivePath = sftpArchiveFolderPath + formattedFileName;
                                                                using var client = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword);
                                                                try
                                                                {
                                                                    _logger.Info($"ANICOHealth - SFTP connection establishment has started {sftpHostUrl}");
                                                                    client.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                                                    client.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                                                    client.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                                                    client.Connect();
                                                                    _logger.Info($"ANICOHealth - SFTP connection establishment was successful {sftpHostUrl}");
                                                                    client.WriteAllBytes(remoteFilePath, docDownloadResponse.Bytes);
                                                                    client.WriteAllBytes(remoteFileArchivePath, docDownloadResponse.Bytes);
                                                                    _logger.Info($"ANICOHealth - File write sftp to {remoteFilePath}");
                                                                    _logger.Info($"ANICOHealth - File write sftp to {remoteFileArchivePath}");
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    _logger.Info($"ANICOHealth - sftp File {remoteFilePath} NOT copied successfully");
                                                                    _logger.Error(ex);
                                                                    IsUpload = false;
                                                                }
                                                                finally
                                                                {
                                                                    client.Disconnect();
                                                                    _logger.Info($"ANICOHealth - The SFTP connection was disconnected {sftpHostUrl}");
                                                                }
                                                                if (IsUpload)
                                                                {
                                                                    string path = _configuration["OrderResultsFolderPath"];
                                                                    filePath = $"{path}{companyDetail.CompanyCode}/";
                                                                    string fullPath = "";

                                                                    fullPath = filePath + formattedFileName;

                                                                    if (Directory.Exists(filePath))
                                                                    {
                                                                        File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                                    }
                                                                    else
                                                                    {
                                                                        DirectoryInfo di = Directory.CreateDirectory(filePath);
                                                                        File.WriteAllBytes(fullPath, docDownloadResponse.Bytes);
                                                                    }
                                                                    _logger.Info($"ANICOHealth - File {fullPath} copied successfully");
                                                                    string updateSqlQuery = $"UPDATE [dbo].[AuthDocument] SET IsSavedExternally = 1, ModifiedDateTime = GETDATE() WHERE Id = {authDocument.Id}";
                                                                    using (SqlConnection con = new SqlConnection(connectionString))
                                                                    {
                                                                        con.Open();
                                                                        SqlCommand cmd = new SqlCommand(updateSqlQuery, con);
                                                                        cmd.ExecuteNonQuery();
                                                                        con.Close();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        _logger.Error(ex);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                _logger.Error("Results not found");
                                            }
                                        }
                                        break;
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }
                else
                {
                    throw new Exception("CopyResultsToSFTPFolder - Company details is not found");
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }
            _logger.Info("Copy Results To SFTPFolder ANICOHealth Completed");
            return response;
        }
        public class OrderCount
        {
            public string CompanyCode { get; set; }
            public string AccountNo { get; set; }
            public int Count { get; set; }
            public int TotalCount { get; set; }
            public int ToolCount { get; set; }
            public int MTDCount { get; set; }
            public string Date { get; set; }
            public bool Inserted { get; set; }
            public List<DailyCount> DailyCounts { get; set; }
        }
        public class DailyCount
        {
            public DateTime Date { get; set; }
            public int Count { get; set; }
        }

        public class DailyOrderCount
        {
            public string CompanyCode { get; set; }
            public int YTD_User { get; set; }
            public int YTD_B2B { get; set; }
            public int YTD_Count { get; set; }
            public int MTD_Count { get; set; }
            public bool Inserted { get; set; }
            public List<DailyCount> DailyCounts { get; set; }
        }
        private void WriteLog(string message, string logFilePath)
        {
            File.AppendAllText(logFilePath, message + Environment.NewLine);
        }
        private void WriteRunLog(string message)
        {
            File.AppendAllText(_configuration["RunLog"], message + Environment.NewLine);
        }

        public class ImportOrdersResponse
        {
            public int TotalOrders { get; set; }
            public int SuccessfulOrders { get; set; }
            public int FailedOrders { get; set; }
            public List<string> SuccessfulOrderNumbers { get; set; }
            public List<FailedOrdersResponse> FailedOrdersResponse { get; set; }
            public int StatusCode { get; set; }
            public string StatusMessage { get; set; }

            public ImportOrdersResponse()
            {
                SuccessfulOrderNumbers = new List<string>();
                FailedOrdersResponse = new List<FailedOrdersResponse>();
            }
        }
        public class FailedOrdersResponse
        {
            public string Base64XmlDocument { get; set; }
            public List<string> ValidationMessages { get; set; }
            public FailedOrdersResponse()
            {
                ValidationMessages = new List<string>();
            }
        }
        private void DeleteUHFiles(string filepath)
        {
            DirectoryInfo dd = new DirectoryInfo(filepath);
            foreach (FileInfo file in dd.GetFiles())
            {
                file.Delete();
            }
        }

        public class CompanyDetails
        {
            public int Id { get; set; }
            public string CompanyCode { get; set; }
            public string CompanyName { get; set; }
            public string ConnectionString { get; set; }
            public string CompanyType { get; set; }
            public string WSUsers { get; set; }
        }
        public class CompanyConfiguration
        {
            public string HostUrl { get; set; }
            public int PortNumber { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string FolderPath { get; set; }
            public string ArchiveFolderPath { get; set; }
            public string Type { get; set; }
        }

        private string GetUniqueTxnId()
        {
            var random = new Random();
            var randomNumber = random.Next(0, 1000000).ToString("D6");
            return randomNumber;
        }

        public async Task<DocDownloadResponse> GetFileStreamAsync(UploadFileRequest request)
        {
            DocDownloadResponse response = new DocDownloadResponse();
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = Timeout.InfiniteTimeSpan;
                    using (var result = await client.GetAsync(GetImageDownloadUrlfromPath(request)))
                    {
                        if (result.IsSuccessStatusCode)
                        {
                            response.Bytes = await result.Content.ReadAsByteArrayAsync();
                        }
                        else
                        {
                            throw new Exception(result.ReasonPhrase);
                        }
                    }
                }
                response.Mime = request.UserId.Split('.')[request.UserId.Split('.').Length - 1];
                response.Name = request.UserId;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
            return response;
        }

        public string GetImageDownloadUrlfromPath(UploadFileRequest request)
        {
            try
            {
                String URL = Support.Decrypt(request.CompanyCode);
                int lastDotPosition = URL.LastIndexOf('.');
                string filename = lastDotPosition < 0 ? URL : URL.Substring(0, lastDotPosition);
                string fileExtension = lastDotPosition < 0 ? "" : URL.Substring(lastDotPosition + 1);
                var storageAccount = CloudStorageAccount.Parse(_configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlockBlob blob = new CloudBlockBlob(new Uri(URL), blobClient);
                blob.Properties.ContentType = Support.GetDocType(fileExtension);
                blob.SetPropertiesAsync();
                var sas = GetBlockBlobSasUriAsync(blob);
                return blob.Uri.AbsoluteUri + sas;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return string.Empty;
            }
        }

        #region  public string GetContainerSasUri(CloudBlobContainer container, string storedPolicyName = null)
        public string GetBlockBlobSasUriAsync(CloudBlockBlob container, string storedPolicyName = null)
        {
            string sasContainerToken = "";
            try
            {
                // If no stored policy is specified, create a new access policy and define its constraints.

                // Note that the SharedAccessBlobPolicy class is used both to define the parameters of an ad-hoc SAS, and 
                // to construct a shared access policy that is saved to the container's shared access policies. 
                SharedAccessBlobPolicy adHocPolicy = new SharedAccessBlobPolicy()
                {
                    // Set start time to five minutes before now to avoid clock skew.
                    SharedAccessStartTime = DateTime.UtcNow.AddMinutes(-10),
                    SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(Convert.ToInt32(_configuration["AzureBlobSasURLExpireMinutes"])),
                    //Permissions = SharedAccessBlobPermissions.Write
                    Permissions = SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.List |
                    SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Create | SharedAccessBlobPermissions.Delete
                };

                //Generate the shared access signature on the container, setting the constraints directly on the signature.
                //IPAddressOrRange ipAddressOrRange = new IPAddressOrRange("192.168.0.103");
                //sasContainerToken = container.GetSharedAccessSignature(adHocPolicy, null, null, null, ipAddressOrRange);
                sasContainerToken = container.GetSharedAccessSignature(adHocPolicy);
                return sasContainerToken;

            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return sasContainerToken;
            }

        }
        #endregion

        public OrderFacilityResponse CheckEopOrdersAvaliablityInExpert()
        {
            _logger.Info("CheckEopOrdersAvaliablityInExpert API has Started.");
            _rootDirectoryPath = _configuration["DailyReportPath"];
            _logFilePath = _rootDirectoryPath + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            List<OrderFacilityDetails> orderFacilityDetails = new List<OrderFacilityDetails>();
            OrderFacilityResponse orderFacilityResponse = new OrderFacilityResponse();

            try
            {
                //List<CompanyDetails> companyDetails = new List<CompanyDetails>();
                //var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                //using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                //{
                //    string oString = "SELECT * FROM [dbo].[CompanyDetail]";
                //    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                //    myConnection.Open();
                //    SqlDataReader oReader = oCmd.ExecuteReader();
                //    while (oReader.Read())
                //    {
                //        CompanyDetails company = new CompanyDetails()
                //        {
                //            Id = Convert.ToInt32(oReader["Id"]),
                //            CompanyType = oReader["CompanyType"].ToString(),
                //            CompanyCode = oReader["CompanyCode"].ToString(),
                //            ConnectionString = oReader["ConnectionString"].ToString(),
                //        };
                //        companyDetails.Add(company);
                //    }
                //    oReader.Close();
                //    myConnection.Close();
                //}

                //if (companyDetails != null && companyDetails.Any())
                //{
                //    for (int i = 0; i < companyDetails.Count; i++)
                //    {
                //        try
                //        {
                //            CompanyDetails companyDetail = companyDetails[i];
                //            string connectionString = companyDetail.ConnectionString;
                //            string oString = $@"SELECT DISTINCT OrderFacility.eNoahOrderId AS ENoahOrderId,OrderFacility.CreatedOnDateTime AS CreatedDate,OrderFacility.Status AS Status
                //                             FROM [dbo].[OrderFacility] AS OrderFacility WHERE Status IN (1,9,23) AND IsActive =1 AND IsDeleted =0 AND CONVERT(DATE,OrderFacility.CreatedOnDateTime,110)<=CONVERT(DATE, DATEADD(DAY,-1,GETDATE()),110)";
                //            using (SqlConnection connection = new SqlConnection(connectionString))
                //            {
                //                connection.Open();
                //                SqlCommand command = new SqlCommand(oString, connection);
                //                SqlDataReader sqlDataReader = command.ExecuteReader();

                //                while (sqlDataReader.Read())
                //                {
                //                    OrderFacilityDetails orderFacilityDetail = new OrderFacilityDetails();

                //                    orderFacilityDetail.eNoahOrderId = sqlDataReader.GetValue(0).ToString();
                //                    orderFacilityDetail.Companytype = companyDetail.CompanyType.ToLower() == "life" ? "0" : "1";
                //                    orderFacilityDetail.Companycode = companyDetail.CompanyCode;
                //                    orderFacilityDetail.CreatedOnDateTime = sqlDataReader.GetValue(1).ToString();
                //                    orderFacilityDetail.Status = sqlDataReader.GetValue(2).ToString();

                //                    orderFacilityDetails.Add(orderFacilityDetail);
                //                }
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            _logger.Error(ex.ToString());
                //        }
                //    }
                //}

                var connectionString = _configuration.GetConnectionString("DefaultConnection");
                DataSet dsOrderFacilityDetals = new DataSet();
                SqlConnection sqlCon = new SqlConnection(connectionString);
                try
                {
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SP_GetOrderFacilityDetails", sqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                    sqlDa.Fill(dsOrderFacilityDetals);
                    sqlCon.Close();
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsOrderFacilityDetals.Tables.Count; i++)
                        {
                            try
                            {
                                if (dsOrderFacilityDetals.Tables[i].Rows.Count > 0)
                                {
                                    if (dsOrderFacilityDetals.Tables[i].Rows[0]["Result"].ToString() == "SUCCESS")
                                    {
                                        for (int j = 0; j < dsOrderFacilityDetals.Tables[i].Rows.Count; j++)
                                        {
                                            OrderFacilityDetails orderFacilityDetail = new OrderFacilityDetails();
                                            orderFacilityDetail.eNoahOrderId = dsOrderFacilityDetals.Tables[i].Rows[j]["eNoahOrderId"].ToString();
                                            orderFacilityDetail.Companytype = dsOrderFacilityDetals.Tables[i].Rows[j]["CompanyType"].ToString();
                                            orderFacilityDetail.Companycode = dsOrderFacilityDetals.Tables[i].Rows[j]["CompanyCode"].ToString();
                                            orderFacilityDetail.CreatedOnDateTime = dsOrderFacilityDetals.Tables[i].Rows[j]["CreatedOnDateTime"].ToString();
                                            orderFacilityDetail.Status = dsOrderFacilityDetals.Tables[i].Rows[j]["Status"].ToString();
                                            orderFacilityDetails.Add(orderFacilityDetail);
                                        }
                                    }
                                    else
                                    {
                                        _logger.Error(dsOrderFacilityDetals.Tables[i].Rows[0]["ErrorMessage"].ToString());
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _logger.Error(ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }

                if (orderFacilityDetails != null && orderFacilityDetails.Any())
                {
                    var requestResponse = new RequestResponseLog()
                    {
                        Request = JsonConvert.SerializeObject(orderFacilityDetails),
                        RequestOn = DateTime.Now
                    };
                    _context.RequestResponseLogs.Add(requestResponse);
                    _context.SaveChanges();

                    orderFacilityResponse = APIHelper.SendOrderFacilityDetails(orderFacilityDetails, _configuration["apiBaseUrl"]).Result;

                    requestResponse.Response = JsonConvert.SerializeObject(orderFacilityResponse);
                    requestResponse.ResponseOn = DateTime.Now;

                    _context.RequestResponseLogs.Update(requestResponse);
                    _context.SaveChanges();
                }

                if (orderFacilityDetails != null && orderFacilityDetails.Any() && orderFacilityResponse != null && orderFacilityResponse.result != null && orderFacilityResponse.result.Any())
                {
                    var excelFilePath = string.Concat(_env.ContentRootPath,
                                   Path.DirectorySeparatorChar.ToString(),
                                   "EMailTemplates",
                                   Path.DirectorySeparatorChar.ToString(),
                                   "DailyReconReport.xlsx");
                    XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                    IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                    int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                    string File = _configuration["ReconReportPath"] + "Order_Status_Recon_Report_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";

                    int prevCell = NumberOfLastRow + 1;
                    int count = 1;
                    for (int j = 0; j < orderFacilityDetails.Count; j++)
                    {
                        var Fac = orderFacilityResponse.result.Where(r => r.aps_order_no == orderFacilityDetails[j].eNoahOrderId && r.company_code.ToLower() == orderFacilityDetails[j].Companycode.ToLower() && r.is_available == "0").FirstOrDefault();
                        if (Fac != null)
                        {
                            Worksheet.Cell("A" + prevCell.ToString()).Value = count;
                            Worksheet.Cell("B" + prevCell.ToString()).Value = orderFacilityDetails[j].Companycode;
                            Worksheet.Cell("C" + prevCell.ToString()).Value = orderFacilityDetails[j].eNoahOrderId;
                            Worksheet.Cell("D" + prevCell.ToString()).Value = orderFacilityDetails[j].CreatedOnDateTime;
                            //Worksheet.Cell("D" + prevCell.ToString()).Value = (DateTime.ParseExact((orderFacilityDetails[j].CreatedOnDateTime), new string[] { "yyyy-MM-dd", "MM-dd-yyyy", "MM/dd/yyyy","dd/MM/yyyy", "dd-MM-yyyy" }, new CultureInfo("en-US"), DateTimeStyles.None)).ToString();
                            Worksheet.Cell("E" + prevCell.ToString()).Value = orderFacilityDetails[j].Status == "1" ? "NEW" : "PENDING";
                            Worksheet.Cell("F" + prevCell.ToString()).Value = orderFacilityResponse.result.Where(r => r.aps_order_no == orderFacilityDetails[j].eNoahOrderId && r.company_code.ToLower() == orderFacilityDetails[j].Companycode.ToLower()).Select(r => r.is_available).FirstOrDefault() == "1" ? "YES" : "NO";
                            prevCell++;
                            count++;
                        }
                    }

                    Worksheet.Columns().AdjustToContents();
                    Worksheet.Rows().AdjustToContents();
                    Workbook.SaveAs(File);

                    if (_configuration["ReconReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendMailReconReport("Recon report for order status new and pending in EOP", File, (orderFacilityResponse != null && orderFacilityResponse.result != null && orderFacilityResponse.result.Any(a => a.is_available == "0")), false);
                    }
                    try
                    {
                        var NewUnsyncedOrders = 0;
                        for (int k = 0; k < orderFacilityDetails.Count; k++)
                        {
                            var Fac = orderFacilityResponse.result.Where(r => r.aps_order_no == orderFacilityDetails[k].eNoahOrderId && r.company_code.ToLower() == orderFacilityDetails[k].Companycode.ToLower() && r.is_available == "0").FirstOrDefault();

                            DateTime datetime = DateTime.Now;
                            string dt1 = datetime.ToString();
                            var cultureInfo = new CultureInfo("en-US");
                            dt1 = Convert.ToDateTime(orderFacilityDetails[k].CreatedOnDateTime.ToString()).ToString("MM/dd/yyyy hh:mm:ss");
                            var dateTime1 = DateTime.Parse(dt1, cultureInfo, DateTimeStyles.NoCurrentDateDefault);
                            var IsOrderInOrderAvailabilityTbl = _context.OrderAvailabilityInExpert.FirstOrDefault(oa => oa.eNoahOrderId == orderFacilityDetails[k].eNoahOrderId && oa.CompanyCode.ToLower() == orderFacilityDetails[k].Companycode.ToLower() && oa.OrderCreatedDateTime == dateTime1);
                            if (Fac != null)
                            {
                                if (IsOrderInOrderAvailabilityTbl == null)
                                {
                                    var orderAvailabilityInExpert = new OrderAvailabilityInExpert();
                                    orderAvailabilityInExpert.CompanyCode = orderFacilityDetails[k].Companycode;
                                    orderAvailabilityInExpert.eNoahOrderId = orderFacilityDetails[k].eNoahOrderId;
                                    orderAvailabilityInExpert.OrderCreatedDateTime = dateTime1;
                                    orderAvailabilityInExpert.OrderStatus = orderFacilityDetails[k].Status == "1" ? "NEW" : "PENDING";
                                    orderAvailabilityInExpert.AvailabilityInExpert = orderFacilityResponse.result.Where(r => r.aps_order_no == orderFacilityDetails[k].eNoahOrderId && r.company_code.ToLower() == orderFacilityDetails[k].Companycode.ToLower()).Select(r => r.is_available).FirstOrDefault() == "1";
                                    orderAvailabilityInExpert.ResponseStatus = "UNPROCESSED";
                                    orderAvailabilityInExpert.CreatedOnDateTime = DateTime.Now;
                                    _context.OrderAvailabilityInExpert.Add(orderAvailabilityInExpert);
                                    _context.SaveChanges();
                                    NewUnsyncedOrders = NewUnsyncedOrders + 1;
                                }
                            }
                        }
                        _logger.Info($"New Unsynced Orders: {NewUnsyncedOrders}");
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);

                    }
                }
                else
                {
                    if (_configuration["ReconReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendMailReconReport("Recon report for order status new and pending in EOP", "", false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            _logger.Info("CheckEopOrdersAvaliablityInExpert API has Ended.");
            return orderFacilityResponse;
        }

        #region NYL

        public void NylDailyCompletedReport()
        {
            DataTable dsOrderFacilityDetals = new DataTable();

            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                try
                {
                    var connectionString = companyDetail.ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("Sp_NylDailyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@ReportFor", "Complete");
                        sqlCmd.Parameters.AddWithValue("@OrderOffice", "All");
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(dsOrderFacilityDetals);
                        connection.Close();
                    }
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Rows.Count > 0)
                    {
                        try
                        {
                            if (dsOrderFacilityDetals.Rows[0]["Result"].ToString() == "SUCCESS")
                            {
                                var excelFilePath = string.Concat(_env.ContentRootPath,
                                  Path.DirectorySeparatorChar.ToString(),
                                  "EMailTemplates",
                                  Path.DirectorySeparatorChar.ToString(),
                                  "NYLCompletedReport.xlsx");
                                XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                                IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                                int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                                string File = _configuration["NYLpath"] + "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                string CSVFilePath = _configuration["NYLpath"] + "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                string SFTPFileName = "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                int prevCell = NumberOfLastRow + 1;
                                int count = 1;
                                StringBuilder sb = new StringBuilder();
                                var header = "Case number,Last name,First name,DOB,Policy number,Go code,Order date,Closed date,Facility / Doctor name,Page count";
                                sb.AppendLine(header.ToString());
                                for (int j = 0; j < dsOrderFacilityDetals.Rows.Count; j++)
                                {
                                    List <string> builder= new List<string>();
                                    Worksheet.Cell("A" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    Worksheet.Cell("B" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    Worksheet.Cell("C" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()))
                                    {
                                        var DOB = DateTime.ParseExact(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()), new string[] { "yyyy-MM-dd hh:mm:ss.fff", "yyyy-MM-dd hh:mm:ss.fffffff" }, new CultureInfo("en-US"), DateTimeStyles.None).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                        builder.Add(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("E" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    Worksheet.Cell("F" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"].ToString()))
                                    {
                                        var OCD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("G" + prevCell.ToString()).SetValue(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                        builder.Add(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("G" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderClosedDate"].ToString()))
                                    {
                                        var CLD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderClosedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("H" + prevCell.ToString()).SetValue(CLD.Length > 2 ? CLD[0].ToString() + "/" + CLD[1].ToString() + "/" + CLD[2].ToString() : "");
                                        builder.Add(CLD.Length > 2 ? CLD[0].ToString() + "/" + CLD[1].ToString() + "/" + CLD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("H" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("I" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    Worksheet.Cell("J" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["PageCount"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["PageCount"].ToString());

                                    prevCell++;
                                    count++;
                                    //sb.AppendLine(string.Join(",", builder));
                                    sb.AppendLine(string.Join(",", builder.Select(v => $"\"{v}\"")));
                                }
                                Worksheet.Columns().AdjustToContents();
                                Worksheet.Rows().AdjustToContents();
                                Workbook.SaveAs(File);
                                System.IO.File.WriteAllText(CSVFilePath, sb.ToString());
                                if(_configuration["NYLReportSFTP"] == "1")
                                {
                                    SFTPforNYLReport("NYL Completed Case Report", SFTPFileName, sb);
                                }
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendNYLDailyReport("NYL Completed Case Report", File, "Complete");
                                }
                            }
                            else
                            {
                                _logger.Error(dsOrderFacilityDetals.Rows[0]["ErrorMessage"].ToString());
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendFailureMailToInternalUser("NYL Completed Case Report", "NYL");
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (_configuration["NYLReportSendMail"] == "1")
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendFailureMailToInternalUser("NYL Completed Case Report", "NYL");
                            }
                        }

                    }
                    else
                    {
                        if (_configuration["NYLReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLDailyReport("NYL Completed Case Report", "", "NoRecordsComplete");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendFailureMailToInternalUser("NYL Completed Case Report", "NYL");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (_configuration["NYLReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendFailureMailToInternalUser("NYL Completed Case Report", "NYL");
                }
            }

        }

        public void NylAllOpenReport()
        {
            DataTable dsOrderFacilityDetals = new DataTable();

            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                try
                {
                    var connectionString = companyDetail.ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("Sp_NylDailyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@ReportFor", "Open");
                        sqlCmd.Parameters.AddWithValue("@OrderOffice", "All");
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(dsOrderFacilityDetals);
                        connection.Close();
                    }
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Rows.Count > 0)
                    {
                        try
                        {
                            if (dsOrderFacilityDetals.Rows[0]["Result"].ToString() == "SUCCESS")
                            {
                                var excelFilePath = string.Concat(_env.ContentRootPath,
                                      Path.DirectorySeparatorChar.ToString(),
                                      "EMailTemplates",
                                      Path.DirectorySeparatorChar.ToString(),
                                      "NYLOpenReport.xlsx");
                                XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                                IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                                int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                                string File = _configuration["NYLpath"] + "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                string CSVFilePath = _configuration["NYLpath"] + "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                string SFTPFileName = "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                int prevCell = NumberOfLastRow + 1;
                                int count = 1;
                                StringBuilder sb = new StringBuilder();
                                var header = "Last name,First name,Go code,DOB,Order date,Last Status date,Facility /  Doctor  name,Policy number,Agent Name,Agent Code,Case number";
                                sb.AppendLine(header.ToString());
                                for (int j = 0; j < dsOrderFacilityDetals.Rows.Count; j++)
                                {
                                    string[] array = new string[] { };
                                    List<string> builder = new List<string>();
                                    Worksheet.Cell("A" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    Worksheet.Cell("B" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    Worksheet.Cell("C" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()))
                                    {
                                        var DOB = DateTime.ParseExact(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()), new string[] { "yyyy-MM-dd hh:mm:ss.fff", "yyyy-MM-dd hh:mm:ss.fffffff" }, new CultureInfo("en-US"), DateTimeStyles.None).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                        builder.Add(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"].ToString()))
                                    {
                                        var OCD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("E" + prevCell.ToString()).SetValue(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                        builder.Add(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("E" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["LastStatusDate"].ToString()))
                                    {
                                        var LSD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["LastStatusDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("F" + prevCell.ToString()).SetValue(LSD.Length > 2 ? LSD[0].ToString() + "/" + LSD[1].ToString() + "/" + LSD[2].ToString() : "");
                                        builder.Add(LSD.Length > 2 ? LSD[0].ToString() + "/" + LSD[1].ToString() + "/" + LSD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("F" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("G" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    Worksheet.Cell("H" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    Worksheet.Cell("I" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["AgentName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["AgentName"].ToString());
                                    Worksheet.Cell("J" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["AgentCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["AgentCode"].ToString());
                                    Worksheet.Cell("K" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());


                                    prevCell++;
                                    count++;
                                    //sb.AppendLine(string.Join(",", builder));
                                    sb.AppendLine(string.Join(",", builder.Select(v => $"\"{v}\"")));
                                }
                                Worksheet.Columns().AdjustToContents();
                                Worksheet.Rows().AdjustToContents();
                                Workbook.SaveAs(File);
                                System.IO.File.WriteAllText(CSVFilePath, sb.ToString());
                                if (_configuration["NYLReportSFTP"] == "1")
                                {
                                    SFTPforNYLReport("NYL Outstanding Case Report", SFTPFileName,sb);
                                }
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendNYLDailyReport("NYL Outstanding Case Report", File, "Open");
                                }
                            }
                            else
                            {
                                _logger.Error(dsOrderFacilityDetals.Rows[0]["ErrorMessage"].ToString());
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendFailureMailToInternalUser("NYL Outstanding Case Report", "NYL");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (_configuration["NYLReportSendMail"] == "1")
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendFailureMailToInternalUser("NYL Outstanding Case Report", "NYL");
                            }
                        }
                    }
                    else
                    {
                        if (_configuration["NYLReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLDailyReport("NYL Outstanding Case Report", "", "NoRecordsOpen");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendFailureMailToInternalUser("NYL Outstanding Case Report", "NYL");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (_configuration["NYLReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendFailureMailToInternalUser("NYL Outstanding Case Report", "NYL");
                }
            }

        }

        //Sarath PE-577 20042022

        public void NylDailyDisabilityClaimsCompletedReport()
        {
            DataTable dsOrderFacilityDetals = new DataTable();

            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                try
                {
                    var connectionString = companyDetail.ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("Sp_NylDailyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@ReportFor", "Complete");
                        sqlCmd.Parameters.AddWithValue("@OrderOffice", "Disability Claims");
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(dsOrderFacilityDetals);
                        connection.Close();
                    }
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Rows.Count > 0)
                    {
                        try
                        {
                            if (dsOrderFacilityDetals.Rows[0]["Result"].ToString() == "SUCCESS")
                            {
                                var excelFilePath = string.Concat(_env.ContentRootPath,
                                  Path.DirectorySeparatorChar.ToString(),
                                  "EMailTemplates",
                                  Path.DirectorySeparatorChar.ToString(),
                                  "NYLCompletedReport.xlsx");
                                XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                                IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                                int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                                string File = _configuration["NYLpath"] + "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                string CSVFilePath = _configuration["NYLpath"] + "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                string SFTPFileName = "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                int prevCell = NumberOfLastRow + 1;
                                int count = 1;
                                StringBuilder sb = new StringBuilder();
                                var header = "Case number,Last name,First name,DOB,Policy number,Go code,Order date,Closed date,Facility / Doctor name,Page count";
                                sb.AppendLine(header.ToString());
                                for (int j = 0; j < dsOrderFacilityDetals.Rows.Count; j++)
                                {
                                    List<string> builder = new List<string>();
                                    Worksheet.Cell("A" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    Worksheet.Cell("B" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    Worksheet.Cell("C" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()))
                                    {
                                        var DOB = DateTime.ParseExact(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()), new string[] { "yyyy-MM-dd hh:mm:ss.fff", "yyyy-MM-dd hh:mm:ss.fffffff" }, new CultureInfo("en-US"), DateTimeStyles.None).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                        builder.Add(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");

                                    }
                                    else
                                    {
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("E" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    Worksheet.Cell("F" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"].ToString()))
                                    {
                                        var OCD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("G" + prevCell.ToString()).SetValue(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                        builder.Add(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("G" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderClosedDate"].ToString()))
                                    {
                                        var CLD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderClosedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("H" + prevCell.ToString()).SetValue(CLD.Length > 2 ? CLD[0].ToString() + "/" + CLD[1].ToString() + "/" + CLD[2].ToString() : "");
                                        builder.Add(CLD.Length > 2 ? CLD[0].ToString() + "/" + CLD[1].ToString() + "/" + CLD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("H" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("I" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    Worksheet.Cell("J" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["PageCount"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["PageCount"].ToString());

                                    //

                                    prevCell++;
                                    count++;
                                    //sb.AppendLine(string.Join(",",builder));
                                    sb.AppendLine(string.Join(",", builder.Select(v => $"\"{v}\"")));
                                }
                                Worksheet.Columns().AdjustToContents();
                                Worksheet.Rows().AdjustToContents();
                                Workbook.SaveAs(File);
                                System.IO.File.WriteAllText(CSVFilePath, sb.ToString());
                                if (_configuration["NYLReportSFTP"] == "1")
                                {
                                    SFTPforNYLReport("NYL Disability Claims Completed Case Report", SFTPFileName, sb);
                                }
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendNYLDailyReport("NYL Disability Claims Completed Case Report", File, "Complete");
                                }
                            }
                            else
                            {
                                _logger.Error(dsOrderFacilityDetals.Rows[0]["ErrorMessage"].ToString());
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendFailureMailToInternalUser("NYL Disability Claims Completed Case Report", "NYL");
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (_configuration["NYLReportSendMail"] == "1")
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendFailureMailToInternalUser("NYL Disability Claims Completed Case Report", "NYL");
                            }
                        }

                    }
                    else
                    {
                        if (_configuration["NYLReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLDailyReport("NYL Disability Claims Completed Case Report", "", "NoRecordsComplete");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendFailureMailToInternalUser("NYL Disability Claims Completed Case Report", "NYL");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (_configuration["NYLReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendFailureMailToInternalUser("NYL Disability Claims Completed Case Report", "NYL");
                }
            }

        }
        public void NylDailyDisabilityUnderwritingCompletedReport()
        {
            DataTable dsOrderFacilityDetals = new DataTable();

            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                try
                {
                    var connectionString = companyDetail.ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("Sp_NylDailyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@ReportFor", "Complete");
                        sqlCmd.Parameters.AddWithValue("@OrderOffice", "Disability Underwriting");
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(dsOrderFacilityDetals);
                        connection.Close();
                    }
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Rows.Count > 0)
                    {
                        try
                        {
                            if (dsOrderFacilityDetals.Rows[0]["Result"].ToString() == "SUCCESS")
                            {
                                var excelFilePath = string.Concat(_env.ContentRootPath,
                                  Path.DirectorySeparatorChar.ToString(),
                                  "EMailTemplates",
                                  Path.DirectorySeparatorChar.ToString(),
                                  "NYLCompletedReport.xlsx");
                                XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                                IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                                int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                                string File = _configuration["NYLpath"] + "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                string CSVFilePath = _configuration["NYLpath"] + "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                string SFTPFileName = "CompletedCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                int prevCell = NumberOfLastRow + 1;
                                int count = 1;
                                StringBuilder sb = new StringBuilder();
                                var header = "Case number,Last name,First name,DOB,Policy number,Go code,Order date,Closed date,Facility / Doctor name,Page count";
                                sb.AppendLine(header.ToString());
                                for (int j = 0; j < dsOrderFacilityDetals.Rows.Count; j++)
                                {
                                    List<string> builder = new List<string>();
                                    Worksheet.Cell("A" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    Worksheet.Cell("B" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    Worksheet.Cell("C" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()))
                                    {
                                        var DOB = DateTime.ParseExact(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()), new string[] { "yyyy-MM-dd hh:mm:ss.fff", "yyyy-MM-dd hh:mm:ss.fffffff" }, new CultureInfo("en-US"), DateTimeStyles.None).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                        builder.Add(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("E" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    Worksheet.Cell("F" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"].ToString()))
                                    {
                                        var OCD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("G" + prevCell.ToString()).SetValue(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                        builder.Add(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("G" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderClosedDate"].ToString()))
                                    {
                                        var CLD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderClosedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("H" + prevCell.ToString()).SetValue(CLD.Length > 2 ? CLD[0].ToString() + "/" + CLD[1].ToString() + "/" + CLD[2].ToString() : "");
                                        builder.Add(CLD.Length > 2 ? CLD[0].ToString() + "/" + CLD[1].ToString() + "/" + CLD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("H" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("I" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    Worksheet.Cell("J" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["PageCount"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["PageCount"].ToString());

                                    prevCell++;
                                    count++;
                                    //sb.AppendLine(string.Join(",", builder));
                                    sb.AppendLine(string.Join(",", builder.Select(v => $"\"{v}\"")));
                                }
                                Worksheet.Columns().AdjustToContents();
                                Worksheet.Rows().AdjustToContents();
                                Workbook.SaveAs(File);
                                System.IO.File.WriteAllText(CSVFilePath, sb.ToString());
                                if (_configuration["NYLReportSFTP"] == "1")
                                {
                                    SFTPforNYLReport("NYL Disability Underwriting Completed Case Report", SFTPFileName, sb);
                                }
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendNYLDailyReport("NYL Disability Underwriting Completed Case Report", File, "Complete");
                                }
                            }
                            else
                            {
                                _logger.Error(dsOrderFacilityDetals.Rows[0]["ErrorMessage"].ToString());
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendFailureMailToInternalUser("NYL Disability Underwriting Completed Case Report", "NYL");
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (_configuration["NYLReportSendMail"] == "1")
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendFailureMailToInternalUser("NYL Disability Underwriting Completed Case Report", "NYL");
                            }
                        }

                    }
                    else
                    {
                        if (_configuration["NYLReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLDailyReport("NYL Disability Underwriting Completed Case Report", "", "NoRecordsComplete");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendFailureMailToInternalUser("NYL Disability Underwriting Completed Case Report", "NYL");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (_configuration["NYLReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendFailureMailToInternalUser("NYL Disability Underwriting Completed Case Report", "NYL");
                }
            }

        }

        public void NylDisabilityClaimsOpenReport()
        {
            DataTable dsOrderFacilityDetals = new DataTable();

            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                try
                {
                    var connectionString = companyDetail.ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("Sp_NylDailyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@ReportFor", "Open");
                        sqlCmd.Parameters.AddWithValue("@OrderOffice", "Disability Claims");
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(dsOrderFacilityDetals);
                        connection.Close();
                    }
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Rows.Count > 0)
                    {
                        try
                        {
                            if (dsOrderFacilityDetals.Rows[0]["Result"].ToString() == "SUCCESS")
                            {
                                var excelFilePath = string.Concat(_env.ContentRootPath,
                                      Path.DirectorySeparatorChar.ToString(),
                                      "EMailTemplates",
                                      Path.DirectorySeparatorChar.ToString(),
                                      "NYLOpenReport.xlsx");
                                XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                                IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                                int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                                string File = _configuration["NYLpath"] + "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                string CSVFilePath = _configuration["NYLpath"] + "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                string SFTPFileName = "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                int prevCell = NumberOfLastRow + 1;
                                int count = 1;
                                StringBuilder sb = new StringBuilder();
                                var header = "Last name,First name,Go code,DOB,Order date,Last Status date,Facility /  Doctor  name,Policy number,Agent Name,Agent Code,Case number";
                                sb.AppendLine(header.ToString());
                                for (int j = 0; j < dsOrderFacilityDetals.Rows.Count; j++)
                                {
                                    List<string> builder = new List<string>();
                                    Worksheet.Cell("A" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    Worksheet.Cell("B" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    Worksheet.Cell("C" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()))
                                    {
                                        var DOB = DateTime.ParseExact(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()), new string[] { "yyyy-MM-dd hh:mm:ss.fff", "yyyy-MM-dd hh:mm:ss.fffffff" }, new CultureInfo("en-US"), DateTimeStyles.None).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                        builder.Add(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"].ToString()))
                                    {
                                        var OCD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("E" + prevCell.ToString()).SetValue(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                        builder.Add(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("E" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["LastStatusDate"].ToString()))
                                    {
                                        var LSD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["LastStatusDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("F" + prevCell.ToString()).SetValue(LSD.Length > 2 ? LSD[0].ToString() + "/" + LSD[1].ToString() + "/" + LSD[2].ToString() : "");
                                        builder.Add(LSD.Length > 2 ? LSD[0].ToString() + "/" + LSD[1].ToString() + "/" + LSD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("F" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("G" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    Worksheet.Cell("H" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    Worksheet.Cell("I" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["AgentName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["AgentName"].ToString());
                                    Worksheet.Cell("J" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["AgentCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["AgentCode"].ToString());
                                    Worksheet.Cell("K" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());

                                    prevCell++;
                                    count++;
                                    //sb.AppendLine(string.Join(",", builder));
                                    sb.AppendLine(string.Join(",", builder.Select(v => $"\"{v}\"")));
                                }
                                Worksheet.Columns().AdjustToContents();
                                Worksheet.Rows().AdjustToContents();
                                Workbook.SaveAs(File);
                                System.IO.File.WriteAllText(CSVFilePath, sb.ToString());
                                if (_configuration["NYLReportSFTP"] == "1")
                                {
                                    SFTPforNYLReport("NYL Disability Claims Outstanding Case Report", SFTPFileName, sb);
                                }
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendNYLDailyReport("NYL Disability Claims Outstanding Case Report", File, "Open");
                                }
                            }
                            else
                            {
                                _logger.Error(dsOrderFacilityDetals.Rows[0]["ErrorMessage"].ToString());
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendFailureMailToInternalUser("NYL Disability Claims Outstanding Case Report", "NYL");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (_configuration["NYLReportSendMail"] == "1")
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendFailureMailToInternalUser("NYL Disability Claims Outstanding Case Report", "NYL");
                            }
                        }
                    }
                    else
                    {
                        if (_configuration["NYLReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLDailyReport("NYL Disability Claims Outstanding Case Report", "", "NoRecordsOpen");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendFailureMailToInternalUser("NYL Disability Claims Outstanding Case Report", "NYL");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (_configuration["NYLReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendFailureMailToInternalUser("NYL Disability Claims Outstanding Case Report", "NYL");
                }
            }

        }

        public void NylDisabilityUnderwritingOpenReport()
        {
            DataTable dsOrderFacilityDetals = new DataTable();

            try
            {
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }
                try
                {
                    var connectionString = companyDetail.ConnectionString;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("Sp_NylDailyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Parameters.AddWithValue("@ReportFor", "Open");
                        sqlCmd.Parameters.AddWithValue("@OrderOffice", "Disability Underwriting");
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(dsOrderFacilityDetals);
                        connection.Close();
                    }
                    if (dsOrderFacilityDetals != null && dsOrderFacilityDetals.Rows.Count > 0)
                    {
                        try
                        {
                            if (dsOrderFacilityDetals.Rows[0]["Result"].ToString() == "SUCCESS")
                            {
                                var excelFilePath = string.Concat(_env.ContentRootPath,
                                      Path.DirectorySeparatorChar.ToString(),
                                      "EMailTemplates",
                                      Path.DirectorySeparatorChar.ToString(),
                                      "NYLOpenReport.xlsx");
                                XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                                IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                                int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                                string File = _configuration["NYLpath"] + "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".xlsx";
                                string CSVFilePath = _configuration["NYLpath"] + "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                string SFTPFileName = "OutstandingCaseReport_" + DateTime.Now.AddDays(-1).ToString("yyyyMMddhhmmss") + ".csv";
                                int prevCell = NumberOfLastRow + 1;
                                int count = 1;
                                StringBuilder sb = new StringBuilder();
                                var header = "Last name,First name,Go code,DOB,Order date,Last Status date,Facility /  Doctor  name,Policy number,Agent Name,Agent Code,Case number";
                                sb.AppendLine(header.ToString());
                                for (int j = 0; j < dsOrderFacilityDetals.Rows.Count; j++)
                                {
                                    List<string> builder = new List<string>();
                                    Worksheet.Cell("A" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["LastName"].ToString()));
                                    Worksheet.Cell("B" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["FirstName"].ToString()));
                                    Worksheet.Cell("C" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["OfficeCode"].ToString());
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()))
                                    {
                                        var DOB = DateTime.ParseExact(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["DateOfBirth"].ToString()), new string[] { "yyyy-MM-dd hh:mm:ss.fff", "yyyy-MM-dd hh:mm:ss.fffffff" }, new CultureInfo("en-US"), DateTimeStyles.None).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                        builder.Add(DOB.Length > 2 ? DOB[0].ToString() + "/" + DOB[1].ToString() + "/" + DOB[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("D" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"].ToString()))
                                    {
                                        var OCD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["OrderCreatedDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("E" + prevCell.ToString()).SetValue(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                        builder.Add(OCD.Length > 2 ? OCD[0].ToString() + "/" + OCD[1].ToString() + "/" + OCD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("E" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    if (!string.IsNullOrEmpty(dsOrderFacilityDetals.Rows[j]["LastStatusDate"].ToString()))
                                    {
                                        var LSD = Convert.ToDateTime(dsOrderFacilityDetals.Rows[j]["LastStatusDate"]).ToString("MM-dd-yyyy").Split("-");
                                        Worksheet.Cell("F" + prevCell.ToString()).SetValue(LSD.Length > 2 ? LSD[0].ToString() + "/" + LSD[1].ToString() + "/" + LSD[2].ToString() : "");
                                        builder.Add(LSD.Length > 2 ? LSD[0].ToString() + "/" + LSD[1].ToString() + "/" + LSD[2].ToString() : "");
                                    }
                                    else
                                    {
                                        Worksheet.Cell("F" + prevCell.ToString()).SetValue("");
                                        builder.Add("");
                                    }
                                    Worksheet.Cell("G" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["FacilityName"].ToString());
                                    Worksheet.Cell("H" + prevCell.ToString()).SetValue(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    builder.Add(Support.Decrypt(dsOrderFacilityDetals.Rows[j]["PolicyNumber"].ToString()));
                                    Worksheet.Cell("I" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["AgentName"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["AgentName"].ToString());
                                    Worksheet.Cell("J" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["AgentCode"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["AgentCode"].ToString());
                                    Worksheet.Cell("K" + prevCell.ToString()).SetValue(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    builder.Add(dsOrderFacilityDetals.Rows[j]["eNoahOrderId"].ToString());
                                    prevCell++;
                                    count++;
                                    //sb.AppendLine(string.Join(",", builder));
                                    sb.AppendLine(string.Join(",", builder.Select(v => $"\"{v}\"")));
                                }
                                Worksheet.Columns().AdjustToContents();
                                Worksheet.Rows().AdjustToContents();
                                Workbook.SaveAs(File);
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendNYLDailyReport("NYL Disability Underwriting Outstanding Case Report", File, "Open");
                                }
                                System.IO.File.WriteAllText(CSVFilePath, sb.ToString());
                                if (_configuration["NYLReportSFTP"] == "1")
                                {
                                    SFTPforNYLReport("NYL Disability Underwriting Outstanding Case Report", SFTPFileName, sb);
                                }
                            }
                            else
                            {
                                _logger.Error(dsOrderFacilityDetals.Rows[0]["ErrorMessage"].ToString());
                                if (_configuration["NYLReportSendMail"] == "1")
                                {
                                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                    helper.SendFailureMailToInternalUser("NYL Disability Underwriting Outstanding Case Report", "NYL");
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            if (_configuration["NYLReportSendMail"] == "1")
                            {
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                helper.SendFailureMailToInternalUser("NYL Disability Underwriting Outstanding Case Report", "NYL");
                            }
                        }
                    }
                    else
                    {
                        if (_configuration["NYLReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLDailyReport("NYL Disability Underwriting Outstanding Case Report", "", "NoRecordsOpen");
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendFailureMailToInternalUser("NYL Disability Underwriting Outstanding Case Report", "NYL");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                if (_configuration["NYLReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendFailureMailToInternalUser("NYL Disability Underwriting Outstanding Case Report", "NYL");
                }
            }

        }

        public void UserAuditReportMonthOfLastWeek(string forDay)
        {
            FunUserAuditReport(forDay);
        }

        public void UserAuditReportMonthOfFirstday(string forDay)
        {
            FunUserAuditReport(forDay);
        }

        public void FunUserAuditReport(string forDay)
        {
            DataSet dsUserAuditDetials = new DataSet();
            var connectionString = _configuration.GetConnectionString("DefaultConnection");

            try
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SP_UserAuditReport", sqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandTimeout = 300;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                    sqlDa.Fill(dsUserAuditDetials);
                    sqlCon.Close();
                }

                if (dsUserAuditDetials != null && dsUserAuditDetials.Tables.Count == 2)
                {
                    if (dsUserAuditDetials.Tables[0].Rows.Count > 1 || dsUserAuditDetials.Tables[1].Rows.Count > 1)
                    {
                        string File = _configuration["UserAuditPath"] + "UserAuditReport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                        XLWorkbook Workbook = new XLWorkbook();
                        Workbook.AddWorksheet(dsUserAuditDetials.Tables[0], "Enoah User");
                        Workbook.AddWorksheet(dsUserAuditDetials.Tables[1], "All User");
                        Workbook.SaveAs(File);
                        if (_configuration["AuditReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendAuditReport("User Audit Report for EOP", File, true, false, forDay);
                        }
                    }
                    else
                    {
                        if (_configuration["AuditReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendAuditReport("User Audit Report for EOP", "", false, false, forDay);
                        }
                    }
                }
                else
                {
                    if (_configuration["AuditReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendAuditReport("User Audit Report for EOP", "", false, false, forDay);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                helper.SendAuditReport("User Audit Report for EOP", "", false, true, forDay);
            }
        }

        #endregion

        public async Task<ResponseBase> ProcessGWLOrdersAsync()
        {
            ResponseBase responses = new ResponseBase();
            _rootDirectoryPath = _configuration["WSGWL_FolderPath"];
            _archiveFolderPath = _configuration["WSGWL_ArchiveFolderPath"];
            _logFilePath = _rootDirectoryPath + "Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            WriteLog("Order Processing Started - " + DateTime.Now.ToString(), _logFilePath);
            string[] directories = Directory.GetDirectories(_rootDirectoryPath);

            try
            {
                //_timer.Enabled = false;

                var sftpHostUrl = "";
                var sftpPortNumber = 0;
                var sftpUserName = "";
                var sftpPassword = "";
                var sftpFolderPath = "";
                var sftpArchiveFolderPath = "";
                //string remoteFilePath = sftpFolderPath + formattedFileName;
                //string remoteFileArchivePath = sftpArchiveFolderPath + formattedFileName;

                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (var myConnection = new SqlConnection(defaultConnectionString))
                {
                    WriteLog("Company configuration lookup started - " + DateTime.Now.ToString(), _logFilePath);
                    var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='GWL' AND IsInbound=1";
                    var oCmd = new SqlCommand(query, myConnection);
                    myConnection.Open();
                    var oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        sftpHostUrl = Convert.ToString(oReader["HostUrl"]);
                        sftpPortNumber = Convert.ToInt32(oReader["PortNumber"]);
                        sftpUserName = Convert.ToString(oReader["UserName"]);
                        sftpPassword = Convert.ToString(oReader["Password"]);
                        sftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                        sftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                    }
                    oReader.Close();
                    myConnection.Close();
                    WriteLog("Company configuration lookup ended - " + DateTime.Now.ToString(), _logFilePath);
                }


                var directory = _rootDirectoryPath;
                using (var sftpclient = new SftpClient(sftpHostUrl, sftpPortNumber, sftpUserName, sftpPassword))
                {
                    WriteLog("SFTP connection establishment started - " + DateTime.Now.ToString(), _logFilePath);
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        sftpclient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                        sftpclient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                        sftpclient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                        sftpclient.Connect();
                        WriteLog("SFTP connection establishment successfull - " + DateTime.Now.ToString(), _logFilePath);
                        var sftpfiles = sftpclient.ListDirectory(sftpFolderPath).ToList();
                        var files = sftpfiles.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.ToLower().EndsWith(".txt")).Select(x => x.FullName).ToList();
                        var authFiles = sftpfiles.Where(x => !string.IsNullOrEmpty(x.Name) && (x.Name.ToLower().EndsWith(".tif") || x.Name.ToLower().EndsWith(".tiff") || x.Name.ToLower().EndsWith(".pdf") || x.Name.ToLower().EndsWith(".jpg") || x.Name.ToLower().EndsWith(".jpeg"))).Select(x => x.FullName).ToList();

                        if (files != null)
                        {
                            WriteLog($"Text Files picked up - {string.Join(";", files)} " + DateTime.Now.ToString(), _logFilePath);
                            WriteLog($"Auth Files picked up - {string.Join(";", authFiles)} " + DateTime.Now.ToString(), _logFilePath);
                            foreach (var file in files)
                            {
                                string matchedAuthFilePerOrder = null;
                                try
                                {
                                    if (!file.Contains("Response_Log"))
                                    {
                                        WriteLog(file, _logFilePath);
                                        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                                        WriteLog("Http Request Started - " + DateTime.Now.ToString(), _logFilePath);

                                        WriteRunLog(Path.GetFileName(file));

                                        string line = sftpclient.ReadLines(file).FirstOrDefault();

                                        if (!string.IsNullOrWhiteSpace(line))
                                        {
                                            int.TryParse(_configuration.GetSection("GWL121InboundFieldCount").Get<string>(), out int gwl121InboundFieldCount);
                                            string[] splittedRequestString = line.Split('|');
                                            if (splittedRequestString != null && splittedRequestString.Any() && splittedRequestString.Length == gwl121InboundFieldCount)
                                            {
                                                string authFileName = splittedRequestString[28].Trim();
                                                if (!string.IsNullOrWhiteSpace(authFileName))
                                                {
                                                    authFileName = authFileName.RemoveLastCharacterFromFileName(_configuration["GWLAuthFileNameLastCharacter"]);
                                                    matchedAuthFilePerOrder = authFiles.Where(t => Path.GetFileName(t).ToLower() == authFileName.ToLower()).FirstOrDefault();
                                                }
                                            }
                                        }

                                        using (var client = new HttpClient())
                                        {
                                            client.Timeout = TimeSpan.FromMinutes(10); // Timeout value is 10 minutes
                                            HttpResponseMessage response = null;
                                            MultipartFormDataContent content = new MultipartFormDataContent();
                                            //ByteArrayContent fileContent = new ByteArrayContent(System.IO.File.ReadAllBytes(file));
                                            ByteArrayContent fileContent = new ByteArrayContent(sftpclient.ReadAllBytes(file));
                                            fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                                            content.Add(fileContent, "file", Path.GetFileName(file));

                                            if (!string.IsNullOrEmpty(matchedAuthFilePerOrder))
                                            {
                                                ByteArrayContent tiffFileContent = new ByteArrayContent(sftpclient.ReadAllBytes(matchedAuthFilePerOrder));
                                                fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("multipart/form-data");
                                                content.Add(tiffFileContent, "authFile", Path.GetFileName(matchedAuthFilePerOrder));
                                            }

                                            client.BaseAddress = new Uri(_configuration["URL"]);
                                            client.DefaultRequestHeaders.Accept.Clear();
                                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                            client.DefaultRequestHeaders.Add("Companycode", "GWL");

                                            WriteLog("Http Request Processing - " + DateTime.Now.ToString(), _logFilePath);
                                            var sftpFileContent = sftpclient.ReadAllBytes(file);
                                            sftpclient.WriteAllBytes(sftpArchiveFolderPath + Path.GetFileName(file), sftpFileContent);

                                            if (!string.IsNullOrEmpty(matchedAuthFilePerOrder))
                                            {
                                                var sftpAuthFileContent = sftpclient.ReadAllBytes(matchedAuthFilePerOrder);
                                                sftpclient.WriteAllBytes(sftpArchiveFolderPath + Path.GetFileName(matchedAuthFilePerOrder), sftpAuthFileContent);
                                            }
                                            WriteLog("File copied to archive folder - " + DateTime.Now.ToString(), _logFilePath);
                                            sftpclient.DeleteFile(file);

                                            if (!string.IsNullOrEmpty(matchedAuthFilePerOrder))
                                            {
                                                sftpclient.DeleteFile(matchedAuthFilePerOrder);
                                            }
                                            WriteLog("File deleted - " + DateTime.Now.ToString(), _logFilePath);

                                            response = await client.PostAsync("services/api/ProcessGWLOrders", content);
                                            WriteLog("Http Request Ended - " + DateTime.Now.ToString(), _logFilePath);

                                            if (response != null && response.IsSuccessStatusCode)
                                            {
                                                WriteLog("Processing Response Started - " + DateTime.Now.ToString(), _logFilePath);
                                                WriteLog("Request Message Information - " + DateTime.Now.ToString() + " - " + response.RequestMessage, _logFilePath);
                                                string responseJsonString = await response.Content.ReadAsStringAsync();
                                                WriteLog("Response data - " + DateTime.Now.ToString() + " - " + responseJsonString, _logFilePath);

                                                ImportOrdersResponse importOrdersResponse = JsonConvert.DeserializeObject<ImportOrdersResponse>(responseJsonString);

                                                if (importOrdersResponse != null)
                                                {
                                                    if (Directory.Exists(directory))
                                                    {
                                                        string responseLogFilePath = directory + "\\Response_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                        WriteLog("Successfull response at - " + DateTime.Now.ToString(), responseLogFilePath);
                                                        WriteLog("------------------------------------------------", responseLogFilePath);
                                                        WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, responseLogFilePath);
                                                        WriteLog("Successful Orders - " + importOrdersResponse.SuccessfulOrders, responseLogFilePath);
                                                        WriteLog("Failed Orders - " + importOrdersResponse.FailedOrders, responseLogFilePath);

                                                        if (!Directory.Exists(directory + "\\SucessOrders"))
                                                        {
                                                            Directory.CreateDirectory(directory + "\\SucessOrders");
                                                        }

                                                        if (!Directory.Exists(directory + "\\FailedOrders"))
                                                        {
                                                            Directory.CreateDirectory(directory + "\\FailedOrders");
                                                        }

                                                        string successLogFilePath = directory + "\\SucessOrders\\Sucess_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                        WriteLog("Processed at - " + DateTime.Now.ToString(), successLogFilePath);
                                                        WriteLog("-------------------------------------", successLogFilePath);
                                                        WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, successLogFilePath);
                                                        WriteLog("Successful Orders - " + importOrdersResponse.SuccessfulOrders, successLogFilePath);
                                                        WriteLog("\n", successLogFilePath);

                                                        if (importOrdersResponse.SuccessfulOrders > 0 && importOrdersResponse.SuccessfulOrderNumbers != null && importOrdersResponse.SuccessfulOrderNumbers.Count > 0)
                                                        {
                                                            WriteLog("Order Numbers", successLogFilePath);
                                                            WriteLog("-------------", successLogFilePath);
                                                            foreach (string successfulOrderNumber in importOrdersResponse.SuccessfulOrderNumbers)
                                                            {
                                                                WriteLog(successfulOrderNumber, successLogFilePath);
                                                            }
                                                        }

                                                        string failedLogFilePath = directory + "\\FailedOrders\\Failed_Log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                                                        WriteLog("Processed at - " + DateTime.Now.ToString(), failedLogFilePath);
                                                        WriteLog("-------------------------------------", failedLogFilePath);
                                                        WriteLog("Total Orders - " + importOrdersResponse.TotalOrders, failedLogFilePath);
                                                        WriteLog("Failed Orders - " + importOrdersResponse.FailedOrders, failedLogFilePath);
                                                        WriteLog("\n", failedLogFilePath);

                                                        if (importOrdersResponse.FailedOrders > 0 && importOrdersResponse.FailedOrdersResponse != null && importOrdersResponse.FailedOrdersResponse.Count > 0)
                                                        {
                                                            WriteLog("Failed Reasons", failedLogFilePath);
                                                            WriteLog("---------------", failedLogFilePath);
                                                            WriteLog("\n", failedLogFilePath);

                                                            foreach (FailedOrdersResponse failedOrder in importOrdersResponse.FailedOrdersResponse)
                                                            {
                                                                List<string> validationMessages = failedOrder.ValidationMessages ?? new List<string>();
                                                                string base64XmlDocument = failedOrder.Base64XmlDocument;

                                                                Guid newGuid = Guid.NewGuid();
                                                                string newGuidString = newGuid.ToString();

                                                                string fileName = newGuidString + ".txt";

                                                                WriteLog("File Name - " + fileName, failedLogFilePath);

                                                                foreach (string validationMessage in validationMessages)
                                                                {
                                                                    WriteLog(validationMessage, failedLogFilePath);
                                                                }

                                                                WriteLog("\n", failedLogFilePath);

                                                                // byte[] docBytes = Convert.FromBase64String(base64XmlDocument); //TODO: Need to check whether do we need to add mime type

                                                                File.WriteAllText(directory + "\\FailedOrders\\" + fileName, base64XmlDocument);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    WriteLog("Deserialized response object is null", _logFilePath);
                                                }

                                                WriteLog("Processing Response Ended - " + DateTime.Now.ToString(), _logFilePath);
                                            }
                                            else
                                            {
                                                WriteLog("Server Error - " + response.ReasonPhrase + DateTime.Now.ToString(), _logFilePath);
                                            }
                                        }
                                    }
                                }
                                catch (FileNotFoundException ex)
                                {
                                    WriteLog("Exception - " + DateTime.Now.ToString() + ": FILE NOT FOUND - " + (ex != null ? ex.Message : "UNKNOW ERROR"), _logFilePath);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            WriteLog($"Files not found - " + DateTime.Now.ToString(), _logFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.Message + "\n" + ex.InnerException, _logFilePath);
                    }
                    finally
                    {
                        sftpclient.Disconnect();
                    }
                    WriteLog("SFTP connection establishment ended - " + DateTime.Now.ToString(), _logFilePath);
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message + "\n" + ex.InnerException, _logFilePath);
            }
            return responses;
        }
        public void DeleteB2BTransactionBackupFiles()
        {
            _logger.Info("DeleteB2BTransactionBackupFiles Job - Started");
            try
            {
                string[] logFolderPaths = _configuration.GetSection("LogFolderPaths").Get<string[]>();
                if (logFolderPaths != null && logFolderPaths.Length > 0)
                {
                    foreach (var logFolderPath in logFolderPaths.ToList())
                    {
                        if (Directory.Exists(logFolderPath))
                        {
                            #region Check root directory

                            List<FileInfo> rootFolderFiles = (from f in new DirectoryInfo(logFolderPath).GetFiles()
                                                              where f.CreationTime.Date <= DateTime.Now.Subtract(TimeSpan.FromDays(int.Parse(_configuration["B2BTransactionPurgeDays"]))).Date
                                                              select f).ToList();

                            int rootFolderFilesCount = 0;
                            if (rootFolderFiles != null && rootFolderFiles.Any())
                            {
                                rootFolderFilesCount = rootFolderFiles.Count;
                                rootFolderFiles.ForEach(f => f.Delete());
                            }

                            string insertSqlQuery = @$"
                            IF NOT EXISTS (SELECT c.name FROM sys.tables t INNER JOIN sys.columns c ON t.object_id=c.object_id and t.name='LogPurgeHistory')
				            BEGIN			
					            CREATE TABLE [dbo].[LogPurgeHistory](
					            [Id] [int] IDENTITY(1,1) NOT NULL,
					            [CreatedBy] [nvarchar](max) NULL,
					            [CreatedOnDateTime] [datetime2](7) NOT NULL DEFAULT CURRENT_TIMESTAMP,
					            [PurgeCount] [int] NOT NULL DEFAULT 0,
					            [TableName] [nvarchar](max) NULL,
					            CONSTRAINT [PK_LogPurgeHistory] PRIMARY KEY CLUSTERED 
					            (
						            [Id] ASC
					            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
					            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
				            END

                            INSERT INTO [dbo].[LogPurgeHistory] (CreatedBy, PurgeCount, TableName) VALUES('HangFireJob', {rootFolderFilesCount}, '{logFolderPath}')
                            ";

                            var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                            using (SqlConnection con = new SqlConnection(defaultConnectionString))
                            {
                                con.Open();
                                SqlCommand cmd = new SqlCommand(insertSqlQuery, con);
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }

                            #endregion

                            #region Check all sub-directories

                            string[] dirs = Directory.GetDirectories(logFolderPath, "*", SearchOption.AllDirectories);

                            foreach (string dir in dirs)
                            {
                                List<FileInfo> subFolderFiles = (from f in new DirectoryInfo(dir).GetFiles()
                                                                 where f.CreationTime.Date <= DateTime.Now.Subtract(TimeSpan.FromDays(int.Parse(_configuration["B2BTransactionPurgeDays"]))).Date
                                                                 select f).ToList();

                                int subFolderFilesCount = 0;
                                if (subFolderFiles != null && subFolderFiles.Any())
                                {
                                    subFolderFilesCount = subFolderFiles.Count;
                                    subFolderFiles.ForEach(f => f.Delete());
                                }

                                insertSqlQuery = @$"
                                IF NOT EXISTS (SELECT c.name FROM sys.tables t INNER JOIN sys.columns c ON t.object_id=c.object_id and t.name='LogPurgeHistory')
				                BEGIN			
					                CREATE TABLE [dbo].[LogPurgeHistory](
					                [Id] [int] IDENTITY(1,1) NOT NULL,
					                [CreatedBy] [nvarchar](max) NULL,
					                [CreatedOnDateTime] [datetime2](7) NOT NULL DEFAULT CURRENT_TIMESTAMP,
					                [PurgeCount] [int] NOT NULL DEFAULT 0,
					                [TableName] [nvarchar](max) NULL,
					                CONSTRAINT [PK_LogPurgeHistory] PRIMARY KEY CLUSTERED 
					                (
						                [Id] ASC
					                )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
					                ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
				                END

                                INSERT INTO [dbo].[LogPurgeHistory] (CreatedBy, PurgeCount, TableName) VALUES('HangFireJob', {subFolderFilesCount}, '{dir}')
                                ";

                                using (SqlConnection con = new SqlConnection(defaultConnectionString))
                                {
                                    con.Open();
                                    SqlCommand cmd = new SqlCommand(insertSqlQuery, con);
                                    cmd.ExecuteNonQuery();
                                    con.Close();
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            throw new Exception("Log Folder not found.");
                        }
                    }
                }
                else
                {
                    throw new Exception("LogFolderPaths not found.");
                }
            }
            catch (Exception ex)
            {
                _logger.Info("DeleteB2BTransactionBackupFiles Job - Failure");
                _logger.Error(ex);
            }
            _logger.Info("DeleteB2BTransactionBackupFiles Job - Completed");
        }

        public void CancellationEmailsReportForPreviousMonth(string forDay)
        {
            CancellationEmailsReport(forDay);
        }
        public void CancellationEmailsReportForPreviousDay(string forDay)
        {
            CancellationEmailsReport(forDay);
        }
        public void CancellationEmailsReport(string forDay)
        {
            try
            {
                _logger.Info("Cancellation Emails Report API Started");
                DataTable dt = new DataTable();
                var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection conn = new SqlConnection(defaultConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("usp_CancellationEmailsReport", conn);
                    cmd.Parameters.AddWithValue("@Flag", forDay.ToString());

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    da.Fill(dt);
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataColumnCollection columns = dt.Columns;
                    if (columns.Contains("ErrorMessage"))
                    {
                        _logger.Error(Convert.ToString(dt.Rows[0]["ErrorMessage"]));
                    }
                    else
                    {
                        _logger.Info("Cancellation Emails Report: Records Received, Ready to Export on Local Path.");
                        string file = "";
                        if (forDay == "PreviousDay")
                        {
                            file = _configuration["PreviousDayCancellationReport"] + "PreviousDayCancellationNotification_" + DateTime.Now.ToString("MMddyyyy") + ".xlsx";
                        }
                        else
                        {
                            file = _configuration["PreviousMonthCancellationReport"] + "PreviousMonthCancellationNotification_" + DateTime.Now.ToString("MMddyyyy") + ".xlsx";
                        }
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dt.Rows[i]["PatientFirstName"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["PatientFirstName"]));
                            dt.Rows[i]["PatientLastName"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["PatientLastName"]));
                            dt.Rows[i]["PolicyNumber"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["PolicyNumber"]));
                            dt.Rows[i]["OrderNo"] = !string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["OrderNo"])) ? Convert.ToString(dt.Rows[i]["OrderNo"]) : "";
                        }
                        dt.AcceptChanges();
                        if (Directory.Exists(Path.GetDirectoryName(file)))
                        {
                            try
                            {
                                if (File.Exists(file))
                                {
                                    File.Delete(file);
                                }
                            }
                            catch (System.IO.IOException ex)
                            {
                                _logger.Error(ex);
                            }
                        }
                        if (Export(dt, file, "CancellationEmailsReport"))
                        {
                            _logger.Info("Cancellation Emails Report: File Generated Successfully");
                        }
                    }
                }
                else
                {
                    _logger.Info("Cancellation Emails Report: No Records found");
                }
                _logger.Info("Cancellation Emails Report API Stopped");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                _logger.Error("Cancellation Emails Report: Error occured");
            }
        }

        public void PasswordExpiryMail(string eMail)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[8] {
                                                     new DataColumn("UserName"),
                                                     new DataColumn("Emailid"),
                                                     new DataColumn("ExpiryCount"),
                                                     new DataColumn("CompCode"),
                                                     new DataColumn("UserId"),
                                                     new DataColumn("RememberPassCount"),
                                                     new DataColumn("FullName"),
                                                     new DataColumn("Token")
                                                     });
            var connectionString = _configuration.GetConnectionString("DefaultConnection");

            try
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {
                    sqlCon.Open();
                    SqlCommand sqlCmd = new SqlCommand("SP_SendPasswordExpiryMail", sqlCon);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandTimeout = 300;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                    sqlDa.Fill(ds);
                    sqlCon.Close();
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        try
                        {
                            if (ds.Tables[i].Rows.Count > 0)
                            {
                                if (ds.Tables[i].Rows[0]["Result"].ToString() == "SUCCESS")
                                {
                                    var company = _context.CompanyDetails.Where(a => a.CompanyCode == ds.Tables[i].Rows[0]["CarrierName"].ToString()).FirstOrDefault();
                                    var optionsBuilder = new DbContextOptionsBuilder<APSDBContext>();
                                    optionsBuilder.UseSqlServer(company.ConnectionString);

                                    for (int j = 0; j < ds.Tables[i].Rows.Count; j++)
                                    {
                                        DataRow row = dt.NewRow();
                                        var username = ds.Tables[i].Rows[j]["UserName"].ToString();
                                        row["UserName"] = ds.Tables[i].Rows[j]["UserName"].ToString();
                                        row["Emailid"] = ds.Tables[i].Rows[j]["Email"].ToString();
                                        row["ExpiryCount"] = ds.Tables[i].Rows[j]["ExpiryCount"].ToString();
                                        row["CompCode"] = ds.Tables[i].Rows[j]["CarrierName"].ToString();
                                        row["UserId"] = ds.Tables[i].Rows[j]["UserId"].ToString();
                                        row["RememberPassCount"] = ds.Tables[i].Rows[j]["RememberPassCount"].ToString();
                                        row["FullName"] = ds.Tables[i].Rows[j]["FullName"].ToString();
                                        string token = "";
                                        using (APSDBContext dbContext = new APSDBContext(optionsBuilder.Options))
                                        {

                                            var userManagerCND = new UserManager<User>(new UserStore<User, Role, APSDBContext>(dbContext), null, new PasswordHasher<User>(), null, null, null, null, null, null);
                                            User user = userManagerCND.FindByNameAsync(ds.Tables[i].Rows[j]["UserName"].ToString()).Result;
                                            token = _userManager.GeneratePasswordResetTokenAsync(user).Result;
                                            var mailexpiryhours = dbContext.CompanyMasters.Select(x => x.EmailExpiryHours).FirstOrDefault().ToString();
                                            var emailtokenhistory = new EmailTokenHistory();
                                            emailtokenhistory.Token = token;
                                            emailtokenhistory.UserId = user.Id;
                                            emailtokenhistory.TokenExpiryDate = DateTime.Now.AddHours(Convert.ToInt32(mailexpiryhours));
                                            emailtokenhistory.CreatedOnDateTime = DateTime.Now;
                                            emailtokenhistory.IsPasswordToken = true;
                                            dbContext.EmailTokenHistory.Add(emailtokenhistory);
                                            user.PassTokenAccessFailedCount = 0;
                                            dbContext.Users.Update(user);
                                            dbContext.SaveChanges();
                                        }
                                        row["Token"] = token;
                                        dt.Rows.Add(row);
                                    }
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                }
                if (dt != null || dt.Rows.Count > 0)
                {
                    if (_configuration["IsEmailSent"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendPasswordExpiryMail("eNoah Online Portal Support", dt);
                    }
                }
            }

            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }

        public bool ExportNYLReport(DataTable dt, string file, string sheetName)
        {
            try
            {
                string directoryPath = Path.GetDirectoryName(file);

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                using (XLWorkbook wb = new XLWorkbook())
                {
                    IXLWorksheet worksheet = wb.Worksheets.Add(dt, sheetName);

                    // Auto-align columns
                    worksheet.Columns().AdjustToContents();

                    // Add borders to the available data
                    var dataRange = worksheet.Range(worksheet.Cell(1, 1).Address, worksheet.Cell(dt.Rows.Count + 1, dt.Columns.Count).Address);
                    dataRange.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    dataRange.Style.Border.InsideBorder = XLBorderStyleValues.Thin;

                    wb.SaveAs(file);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        public void NylWeeklyActionRequiredReport()
        {
            _logger.Info("Weekly Action Required Report API Started");
            DataTable dt = new DataTable();
            var companyDetail = new CompanyDetails();
            var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
            {
                string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='NYL'";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                myConnection.Open();
                SqlDataReader oReader = oCmd.ExecuteReader();
                while (oReader.Read())
                {
                    companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                    companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                }
                oReader.Close();
                myConnection.Close();
            }
            try
            {
                var connectionString = companyDetail.ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCmd = new SqlCommand("Sp_NylWeeklyActionRequiredReport", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                    sqlDa.Fill(dt);
                    connection.Close();
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataColumnCollection columns = dt.Columns;
                    if (columns.Contains("ErrorMessage"))
                    {
                        _logger.Error(Convert.ToString(dt.Rows[0]["ErrorMessage"]));
                        if (_configuration["NYLWeeklyActionRequiredReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            helper.SendNYLWeeklyActionRequiredReport("", "Error");
                        }
                    }
                    else
                    {
                        _logger.Info("Weekly Action Required Report: Records Received, Ready to Export on Local Path.");
                        string filePath = _configuration["NYLpath"] + "NYLWeeklyActionRequiredReport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            int enumValue = int.Parse(dt.Rows[i]["Stage"].ToString());
                            dt.Rows[i]["Stage"] = Support.GetDisplayNameFromStatusId<Status>(enumValue);
                            dt.Rows[i]["PolicyNumber"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["PolicyNumber"]));
                            dt.Rows[i]["PatientFirstName"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["PatientFirstName"]));
                            dt.Rows[i]["PatientLastName"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["PatientLastName"]));
                        }
                        dt.AcceptChanges();

                        if (ExportNYLReport(dt, filePath, "NYLWeeklyActionRequiredReport"))
                        {
                            _logger.Info("Weekly Action Required Report: Exported to Local Path.");
                            if (_configuration["NYLWeeklyActionRequiredReportSendMail"] == "1")
                            {
                                _logger.Info("Weekly Action Required Report: Ready to send Mail");
                                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                                if (helper.SendNYLWeeklyActionRequiredReport(filePath, "Complete"))
                                {
                                    _logger.Info("Weekly Action Required Report: The email was successfully delivered");
                                }
                                else
                                {
                                    _logger.Info("Weekly Action Required Report: Error occured while sending the email");
                                }
                            }
                        }
                        else
                        {
                            _logger.Info("Weekly Action Required Report: Error occured while exporting the excel file to Local Path.");
                        }
                    }
                }
                else
                {
                    _logger.Info("Weekly Action Required Report: No Records found");
                    if (_configuration["NYLWeeklyActionRequiredReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendNYLWeeklyActionRequiredReport("", "NoRecords");
                    }
                }

                _logger.Info("Weekly Action Required Report API Stopped");
            }
            catch (Exception ex)
            {
                _logger.Error("Weekly Action Required Report: Error occured");
                _logger.Error(ex);
                if (_configuration["NYLWeeklyActionRequiredReportSendMail"] == "1")
                {
                    EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                    helper.SendNYLWeeklyActionRequiredReport("", "Error");
                }
            }
        }

        private bool ExportRiversourceResultReconReport(DataTable dt, string file, string sheetName)
        {
            try
            {
                string directoryPath = Path.GetDirectoryName(file);

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                StringBuilder sb = new StringBuilder();

                IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in dt.Rows)
                {
                    IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                    sb.AppendLine(string.Join(",", fields));
                }

                File.WriteAllText(file, sb.ToString());

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return false;
            }
        }

        public void RiversourceResultReconReport()
        {
            EmailHelper helper = new EmailHelper(_context, _configuration, _env);

            try
            {
                DataTable dt = new DataTable();
                var companyDetail = new CompanyDetails();
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
                {
                    string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='Riversource'";
                    SqlCommand oCmd = new SqlCommand(oString, myConnection);
                    myConnection.Open();
                    SqlDataReader oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                        companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                    }
                    oReader.Close();
                    myConnection.Close();
                }

                var connectionString = companyDetail.ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand sqlCmd = new SqlCommand("Sp_RiversourceResultReconReport", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                    sqlDa.Fill(dt);
                    connection.Close();
                }

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataColumnCollection columns = dt.Columns;
                    if (columns.Contains("ErrorMessage"))
                    {
                        _logger.Error(Convert.ToString(dt.Rows[0]["ErrorMessage"]));
                        if (_configuration["RiversourceResultReconReportSendMail"] == "1")
                        {
                            helper.SendRiversourceResultReconReport("Error");
                        }
                    }
                    else
                    {
                        string fileName = $"eNoah_Recon_{DateTime.Now.ToString("yyyyMMdd")}.csv";
                        string filePath = $"{_configuration["RiversourceResultReconPath"]}{fileName}";
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dt.Rows[i]["AccountID"] = Support.Decrypt(Convert.ToString(dt.Rows[i]["AccountID"]));
                        }
                        dt.AcceptChanges();

                        if (ExportRiversourceResultReconReport(dt, filePath, "RiversourceResultReconReport"))
                        {
                            string riversourceSftpUserName = "";
                            string riversourceSftpPassword = "";
                            string riversourceSftpUrl = "";
                            int riversourceSftpPortNumber = 0;
                            string riversourceSftpFolderPath = "";
                            string riversourceSftpArchiveFolderPath = "";
                            using (var myConnection = new SqlConnection(defaulltConnectionString))
                            {
                                var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath,ApiUrl FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='riversource' AND IsInbound=0 AND Type='RECON'";
                                var oCmd = new SqlCommand(query, myConnection);
                                myConnection.Open();
                                var oReader = oCmd.ExecuteReader();
                                while (oReader.Read())
                                {
                                    riversourceSftpUserName = Convert.ToString(oReader["UserName"]);
                                    riversourceSftpPassword = Convert.ToString(oReader["Password"]);
                                    riversourceSftpUrl = Convert.ToString(oReader["HostUrl"]);
                                    riversourceSftpPortNumber = Convert.ToInt32((oReader["PortNumber"]));
                                    riversourceSftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                                    riversourceSftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                                }
                                oReader.Close();
                                myConnection.Close();
                            }

                            StringBuilder sb = new StringBuilder();

                            IEnumerable<string> columnNames = dt.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
                            sb.AppendLine(string.Join(",", columnNames));

                            foreach (DataRow row in dt.Rows)
                            {
                                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                                sb.AppendLine(string.Join(",", fields));
                            }

                            using var sftpClient = new SftpClient(riversourceSftpUrl, riversourceSftpPortNumber, riversourceSftpUserName, riversourceSftpPassword);
                            try
                            {
                                sftpClient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                                sftpClient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                                sftpClient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                                sftpClient.Connect();
                                _logger.Info($"Riversource - Successful connection to {riversourceSftpUrl}");
                                sftpClient.WriteAllText(riversourceSftpFolderPath + fileName, sb.ToString());
                                _logger.Info($"Riversource - Result Recon file uploaded to {riversourceSftpFolderPath}{fileName}");
                            }
                            catch (Exception ex)
                            {
                                _logger.Error(ex);
                                if (_configuration["RiversourceResultReconReportSendMail"] == "1")
                                {
                                    helper.SendRiversourceResultReconReport("Error");
                                }
                            }
                            finally
                            {
                                sftpClient.Disconnect();
                            }
                        }
                        else
                        {
                            _logger.Info("Riversource Result Recon Report: Error occured while exporting the csv file to Local Path.");
                            if (_configuration["RiversourceResultReconReportSendMail"] == "1")
                            {
                                helper.SendRiversourceResultReconReport("Error");
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.Error("Riversource Result Recon Report: Error occured");
                _logger.Error(ex);

                if (_configuration["RiversourceResultReconReportSendMail"] == "1")
                {
                    helper.SendRiversourceResultReconReport("Error");
                }
            }
        }
        public void SFTPforNYLReport(string Subject,string FileName,StringBuilder sb)
        {
            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
            try
            {
                var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
                string NYLSftpUserName = "";
                string NYLSftpPassword = "";
                string NYLSftpUrl = "";
                int NYLSftpPortNumber = 0;
                string NYLSftpFolderPath = "";
                string NYLSftpArchiveFolderPath = "";
                using (var myConnection = new SqlConnection(defaulltConnectionString))
                {
                    var query = "SELECT HostUrl,PortNumber,UserName,Password,FolderPath,ArchiveFolderPath,ApiUrl FROM [dbo].[CompanyConfiguration] WHERE CompanyCode='nyl' AND Type='SFTP'";
                    var oCmd = new SqlCommand(query, myConnection);
                    myConnection.Open();
                    var oReader = oCmd.ExecuteReader();
                    while (oReader.Read())
                    {
                        NYLSftpUserName = Convert.ToString(oReader["UserName"]);
                        NYLSftpPassword = Convert.ToString(oReader["Password"]);
                        NYLSftpUrl = Convert.ToString(oReader["HostUrl"]);
                        NYLSftpPortNumber = Convert.ToInt32((oReader["PortNumber"]));
                        NYLSftpFolderPath = Convert.ToString(oReader["FolderPath"]);
                        NYLSftpArchiveFolderPath = Convert.ToString(oReader["ArchiveFolderPath"]);
                    }
                    oReader.Close();
                    myConnection.Close();
                }

                using var sftpClient = new SftpClient(NYLSftpUrl, NYLSftpPortNumber, NYLSftpUserName, NYLSftpPassword);
                try
                {
                    sftpClient.KeepAliveInterval = TimeSpan.FromSeconds(Convert.ToInt32(_configuration["KeepAliveInterval"]));
                    sftpClient.ConnectionInfo.Timeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["ConnectionInfoTimeOut"]));
                    sftpClient.OperationTimeout = TimeSpan.FromMinutes(Convert.ToInt32(_configuration["OperationTimeout"]));
                    sftpClient.Connect();
                    _logger.Info($"NYL Daily report - Successful connection to {NYLSftpUrl}");
                    sftpClient.WriteAllText(NYLSftpFolderPath + FileName, sb.ToString());
                    _logger.Info($"NYL Daily Report - NYL report file uploaded to {NYLSftpFolderPath}{FileName}");
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    if (_configuration["NYLReportSFTPSendMail"] == "1")
                    {
                        helper.SFTPSendNYLDailyReport(Subject, "Error");
                    }
                }
                finally
                {
                    sftpClient.Disconnect();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("NYL Report: Error occured");
                _logger.Error(ex);

                if (_configuration["NYLReportSFTPSendMail"] == "1")
                {
                    helper.SFTPSendNYLDailyReport(Subject, "Error");
                }
            }
        }

        public async Task<ResponseBase> ResendOrdersToExpert()
        {
            _logger.Info("ResendOrdersToExpert API has Started.");
            ResponseBase response = new ResponseBase();
            int TotalOrders = 0;
            int TotalSuccess = 0;
            int TotalFailure = 0;
            int DuplicateOrders = 0;
            string MailFlag = "success";
            string File = _configuration["ReconReportPath"] + "Order_Status_Recon_ResponseReport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
            CompanyDetails company = null;
            List<CompanyDetails> companyDetails = new List<CompanyDetails>();
            var defaultConnectionString = _configuration.GetConnectionString("DefaultConnection");
            RequestResponseLog requestresponse = new RequestResponseLog();
            try
            {
                //var UnAvailableOrdersInExpert = await _context.OrderAvailabilityInExpert.Where(order => !order.AvailabilityInExpert).OrderBy(order => order.Id).ToListAsync();
                var UnAvailableOrdersInExpert = await _context.OrderAvailabilityInExpert.Where(order => !order.AvailabilityInExpert && !new[] { "SUCCESS", "FAILURE", "DUPLICATE" }.Contains(order.ResponseStatus)).OrderBy(order => order.Id).ToListAsync();
                TotalOrders = UnAvailableOrdersInExpert.Count;
                _logger.Info(string.Format("Total Unavailable Orders In Expert: {0}", UnAvailableOrdersInExpert.Count));

                if (UnAvailableOrdersInExpert != null && UnAvailableOrdersInExpert.Count > 0)
                {
                    foreach (var order in UnAvailableOrdersInExpert)
                    {
                        try
                        {
                            var docslist = new List<AuthDocs>();
                            DataSet dataSet = new DataSet();
                            using (SqlConnection myConnection = new SqlConnection(defaultConnectionString))
                            {
                                using (SqlCommand sqlCmd = new SqlCommand("[sp_GetOrderDetailsByOrderId]", myConnection))
                                {
                                    sqlCmd.CommandType = CommandType.StoredProcedure;
                                    sqlCmd.Parameters.Add("@eNoahorderId", SqlDbType.VarChar, 10).Value = order.eNoahOrderId;
                                    sqlCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar, 50).Value = order.CompanyCode;
                                    using (var da = new SqlDataAdapter(sqlCmd))
                                    {
                                        myConnection.Open();
                                        sqlCmd.CommandType = CommandType.StoredProcedure;
                                        da.Fill(dataSet);
                                        myConnection.Close();
                                    }
                                }
                                string oString = "SELECT * FROM [dbo].[CompanyDetail] where CompanyCode = '" + order.CompanyCode + "'";
                                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                                myConnection.Open();
                                SqlDataReader oReader = oCmd.ExecuteReader();
                                while (oReader.Read())
                                {
                                    company = new CompanyDetails()
                                    {
                                        Id = Convert.ToInt32(oReader["Id"]),
                                        CompanyCode = oReader["CompanyCode"].ToString(),
                                        CompanyName = oReader["CompanyName"].ToString(),
                                        CompanyType = oReader["CompanyType"].ToString(),
                                        ConnectionString = oReader["ConnectionString"].ToString(),
                                    };
                                }
                                oReader.Close();
                                myConnection.Close();
                            }
                            if (company != null)
                            {
                                var orderToUpdate = _context.OrderAvailabilityInExpert.FirstOrDefault(oa => oa.eNoahOrderId == order.eNoahOrderId && oa.Id == order.Id);
                                try
                                {
                                    var optionsBuilder = new DbContextOptionsBuilder<APSDBContext>();
                                    optionsBuilder.UseSqlServer(company.ConnectionString);

                                    using (APSDBContext dbContext = new APSDBContext(optionsBuilder.Options))
                                    {
                                        OrderService orderService = new OrderService(dbContext, _configuration, _mapper, _env, _commonContext);
                                        if (dataSet != null && dataSet.Tables.Count > 0)
                                        {
                                            DataTable CompanyMasterTbl = dataSet.Tables[0];
                                            DataTable OrderDetailsTbl = dataSet.Tables[1];
                                            var CropNumber = "";
                                            var CompanyType = 0;
                                            var UserId = "";
                                            var DoctorId = 0;
                                            List<int> AgentIdList = new List<int>();
                                            List<int> DocIdList = new List<int>();

                                            PatientRequest patient_info = new PatientRequest();

                                            if (CompanyMasterTbl.Rows.Count > 0 && CompanyMasterTbl != null)
                                            {
                                                foreach (DataRow dr in CompanyMasterTbl.Rows)
                                                {
                                                    var summarization = Convert.ToString(dr["Summarization"]).Replace(" ", string.Empty) == "" ? false : (Convert.ToBoolean(dr["Summarization"]));
                                                    patient_info.CompanyType = Convert.ToInt32(dr["CompanyType"]);
                                                    patient_info.Orderhandledby = Convert.ToInt32(dr["OrderHandledBy"]);
                                                    patient_info.IsApsSummarization = summarization ? 1 : 0;
                                                    patient_info.RetentionPerioddays = Convert.ToString(dr["RetentionPeriodDays"]);
                                                    patient_info.OfflineArchivePurgedays = Convert.ToString(dr["OfflineArchivePurgeDays"]);
                                                    patient_info.ProductionPurgedays = Convert.ToString(dr["ProductionPurgeDays"]);
                                                    patient_info.Carriername = Convert.ToString(dr["CompanyName"]);
                                                    patient_info.Companycode = Convert.ToString(dr["CompanyCode"]);
                                                    CropNumber = Convert.ToString(dr["CropNumber"]);
                                                    CompanyType = Convert.ToInt32(dr["CompanyType"]);
                                                }
                                            }

                                            var rowCount = 1;
                                            if (OrderDetailsTbl.Rows.Count > 0 && OrderDetailsTbl != null)
                                            {
                                                foreach (DataRow dr in OrderDetailsTbl.Rows)
                                                {
                                                    if (rowCount == 1)
                                                    {
                                                        //PatientInfo
                                                        patient_info.PatientFN = Support.Decrypt(Convert.ToString(dr["PatientFN"]));
                                                        patient_info.PatientLN = Support.Decrypt(Convert.ToString(dr["PatientLN"]));
                                                        patient_info.SSN = Convert.ToString(dr["PatientSSN"]);
                                                        var patientState = Convert.ToString(dr["PatientStateId"]).Trim();
                                                        if (!string.IsNullOrEmpty(patientState))
                                                        {
                                                            patient_info.State = _context.States.Where(a => a.Id == (Convert.ToInt32(dr["PatientStateId"]))).Select(a => a.Abbreviation).FirstOrDefault();
                                                        }
                                                        else
                                                        {
                                                            patient_info.State = null;
                                                        }
                                                        patient_info.City = Support.Decrypt(Convert.ToString(dr["PatientCity"]));
                                                        patient_info.Address = Support.Decrypt(Convert.ToString(dr["PatientAddress"]));
                                                        patient_info.DOB = Convert.ToString(dr["PatientDOB"]);
                                                        patient_info.Zip = Support.Decrypt(Convert.ToString(dr["PatientZipCode"]));
                                                        patient_info.PreferredContact = Support.Decrypt(Convert.ToString(dr["PatientPreferredContact"]));
                                                        patient_info.OtherName = Support.Decrypt(Convert.ToString(dr["PatientOtherName"]));
                                                        patient_info.Email = Support.Decrypt(Convert.ToString(dr["PatientEmailAddress"]));
                                                        var patientGender = Convert.ToString(dr["PatientGender"]).Trim();
                                                        if (!string.IsNullOrEmpty(patientGender))
                                                        {
                                                            patient_info.Gender = _context.Gender.Where(g => g.Id == Convert.ToInt32(dr["PatientGender"])).Select(a => a.Name).FirstOrDefault();
                                                        }
                                                        else
                                                        {
                                                            patient_info.Gender = null;
                                                        }
                                                        patient_info.Phone1 = Support.Decrypt(Convert.ToString(dr["PatientPhone1"]));
                                                        patient_info.Phone2 = Support.Decrypt(Convert.ToString(dr["PatientPhone2"]));
                                                        patient_info.Phone3 = Support.Decrypt(Convert.ToString(dr["PatientPhone3"]));
                                                        patient_info.Fax = Support.Decrypt(Convert.ToString(dr["PatientFax"]));
                                                        patient_info.Ext1 = Support.Decrypt(Convert.ToString(dr["PatientExtension1"]));
                                                        patient_info.Ext2 = Support.Decrypt(Convert.ToString(dr["PatientExtension2"]));
                                                        patient_info.Ext3 = Support.Decrypt(Convert.ToString(dr["PatientExtension3"]));
                                                        var phone1Type = Convert.ToString(dr["PatientPhone1Type"]);
                                                        switch (phone1Type)
                                                        {
                                                            case "Select":
                                                                patient_info.Phone1Type = "";
                                                                break;
                                                            case "1":
                                                                patient_info.Phone1Type = "Work";
                                                                break;
                                                            case "2":
                                                                patient_info.Phone1Type = "Home";
                                                                break;
                                                            case "3":
                                                                patient_info.Phone1Type = "Mobile";
                                                                break;
                                                            default:
                                                                patient_info.Phone1Type = phone1Type;
                                                                break;
                                                        }
                                                        var phone2Type = Convert.ToString(dr["PatientPhone2Type"]);
                                                        switch (phone2Type)
                                                        {
                                                            case "Select":
                                                                patient_info.Phone2Type = "";
                                                                break;
                                                            case "1":
                                                                patient_info.Phone2Type = "Work";
                                                                break;
                                                            case "2":
                                                                patient_info.Phone2Type = "Home";
                                                                break;
                                                            case "3":
                                                                patient_info.Phone2Type = "Mobile";
                                                                break;
                                                            default:
                                                                patient_info.Phone2Type = phone2Type;
                                                                break;
                                                        }
                                                        var phone3Type = Convert.ToString(dr["PatientPhone3Type"]);
                                                        switch (phone3Type)
                                                        {
                                                            case "Select":
                                                                patient_info.Phone3Type = "";
                                                                break;
                                                            case "1":
                                                                patient_info.Phone3Type = "Work";
                                                                break;
                                                            case "2":
                                                                patient_info.Phone3Type = "Home";
                                                                break;
                                                            case "3":
                                                                patient_info.Phone3Type = "Mobile";
                                                                break;
                                                            default:
                                                                patient_info.Phone3Type = phone3Type;
                                                                break;
                                                        }
                                                        patient_info.Title = Support.Decrypt(Convert.ToString(dr["PatientTitle"]));
                                                        patient_info.Policyno = Support.Decrypt(Convert.ToString(dr["PatientPolicyNumber"]));
                                                        var policyAmt = Support.Decrypt(Convert.ToString(dr["PatientPolicyAmt"])).Trim();
                                                        if (!string.IsNullOrEmpty(policyAmt))
                                                        {
                                                            patient_info.PolicyAmount = policyAmt == "" ? 0 : Convert.ToInt32(Support.Decrypt(Convert.ToString(dr["PatientPolicyAmt"])));
                                                        }
                                                        else
                                                        {
                                                            patient_info.PolicyAmount = 0;
                                                        }
                                                        patient_info.UserField1 = Support.Decrypt(Convert.ToString(dr["PatientOtherField1"]));
                                                        patient_info.UserField2 = Support.Decrypt(Convert.ToString(dr["PatientOtherField2"]));
                                                        patient_info.UserField3 = Support.Decrypt(Convert.ToString(dr["PatientOtherField3"]));
                                                        patient_info.UserField4 = Support.Decrypt(Convert.ToString(dr["PatientOtherField4"]));

                                                        patient_info.EopOrderingOfficeId = Convert.ToString(dr["OfficeId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["OfficeId"]);

                                                        //OrderingOfficeInfo                                                    
                                                        var requestOfficeAccountNum = Convert.ToString(dr["OfficeAccountNumber"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["OfficeAccountNumber"]);
                                                        patient_info.Orderingoffice = Convert.ToString(dr["OfficeName"]);
                                                        patient_info.OfficeAccountNumber = Convert.ToString(dr["OfficeAccountNumber"]);
                                                        patient_info.OfficeMail = Convert.ToString(dr["OfficeEmail"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["OfficeEmail"]);
                                                        patient_info.OfficePhone = Convert.ToString(dr["OfficePhone"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["OfficePhone"]);
                                                        patient_info.OfficeCode = Convert.ToString(dr["OfficeCode"]);

                                                        //UserInfo
                                                        UserId = Convert.ToString(dr["UserId"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["UserId"]);
                                                        patient_info.EopRequestorId = UserId;
                                                        patient_info.OrderRequestorName = Convert.ToString(dr["UserFN"]) + " " + Convert.ToString(dr["UserLN"]);
                                                        patient_info.OrderRequestorEmail = Convert.ToString(dr["UserEmail"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["UserEmail"]);
                                                        patient_info.OrderRequestorPhone = Convert.ToString(dr["UserPhone"]);

                                                        //AgencytInfo
                                                        patient_info.AgencyName = Convert.ToString(dr["OfficeName"]);
                                                        patient_info.AgencyNo = Convert.ToString(dr["OfficeCode"]);
                                                        patient_info.OrderedBy = Convert.ToString(dr["UserFN"]) + " " + Convert.ToString(dr["UserLN"]);

                                                        if (!string.IsNullOrEmpty(requestOfficeAccountNum))
                                                        {
                                                            patient_info.AccountNo = requestOfficeAccountNum;
                                                        }
                                                        else
                                                        {
                                                            patient_info.AccountNo = CropNumber;
                                                        }
                                                        var isReopen = Convert.ToInt32(dr["IsReopen"]);
                                                        patient_info.IsReopen = Convert.ToInt32(dr["IsReopen"]);
                                                        if (isReopen == 1)
                                                        {
                                                            var parentOrderId = dbContext.OrderFacilityMappings.Where(o => o.eNoahOrderId == order.eNoahOrderId).Select(o => o.ParentEnoahOrderId).FirstOrDefault();
                                                            var reOpenOrderId = dbContext.ReopenOrders.Where(a => a.CombinedOrderId == order.eNoahOrderId).OrderByDescending(x => x.Id).Select(a => a.ReopenOrderId).FirstOrDefault();
                                                            patient_info.OrderNo = parentOrderId;
                                                            patient_info.SubOrderChar = reOpenOrderId;
                                                        }
                                                        else
                                                        {
                                                            patient_info.OrderNo = order.eNoahOrderId;
                                                            patient_info.SubOrderChar = "";
                                                        }
                                                        patient_info.ReopenOrderNumber = order.eNoahOrderId;

                                                        if ((Support.Decrypt(Convert.ToString(dr["PatientDOI"])) != null) && (Support.Decrypt(Convert.ToString(dr["PatientDOI"])) != ""))
                                                        {
                                                            patient_info.DateOfIncident = Convert.ToDateTime(Support.Decrypt(Convert.ToString(dr["PatientDOI"])));
                                                        }
                                                        else
                                                        {
                                                            patient_info.DateOfIncident = null;
                                                        }

                                                        //AgentInfo                                                   
                                                        var AgentId = Convert.ToString(dr["AgentId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["AgentId"]);
                                                        if (AgentId > 0)
                                                        {
                                                            AgentInfo agentInfo = new AgentInfo();
                                                            agentInfo.AgentFN = Convert.ToString(dr["AgentFN"]);
                                                            agentInfo.AgentLN = Convert.ToString(dr["AgentLN"]);
                                                            agentInfo.AgentId = Convert.ToString(dr["AgentNumber"]);
                                                            agentInfo.AgentPhone = Convert.ToString(dr["AgentPhone"]);
                                                            agentInfo.AgentExt = Convert.ToString(dr["AgentExt"]);
                                                            agentInfo.AgentCell = Convert.ToString(dr["AgentCell"]);
                                                            agentInfo.AgentFax = Convert.ToString(dr["AgentFax"]);
                                                            agentInfo.AgentEmail = Convert.ToString(dr["AgentEmail"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["AgentEmail"]);
                                                            agentInfo.ExpertAgentId = "";
                                                            agentInfo.AgentNo = Convert.ToString(dr["AgentNumber"]);
                                                            agentInfo.APSAgentNo = Convert.ToString(dr["AgentId"]);
                                                            agentInfo.AgentType = Convert.ToString(dr["AgentType"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["AgentType"]);
                                                            //agentInfo.AgentIsNew = (AgentId == 0) ? true : false;
                                                            agentInfo.AgentIsNew = Convert.ToBoolean(dr["IsNewAgent"]);
                                                            AgentIdList.Add(AgentId);
                                                            patient_info.Agent.Add(agentInfo);
                                                        }

                                                        //CarrierManagerInfo
                                                        var CMId = Convert.ToString(dr["CMId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["CMId"]);
                                                        if (CMId > 0)
                                                        {
                                                            var carrierManagerInfo = new CarrierManagerInfo();
                                                            carrierManagerInfo.FirstName = Convert.ToString(dr["CMFN"]);
                                                            carrierManagerInfo.LastName = Convert.ToString(dr["CMLN"]);
                                                            carrierManagerInfo.EmailAddress = Convert.ToString(dr["CMEmailAddress"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["CMEmailAddress"]);
                                                            carrierManagerInfo.WorkPhone = Convert.ToString(dr["CMWorkPhone"]);
                                                            carrierManagerInfo.Ext = Convert.ToString(dr["CMExt"]);
                                                            carrierManagerInfo.CellPhone = Convert.ToString(dr["CMCellPhone"]);
                                                            carrierManagerInfo.Fax = Convert.ToString(dr["CMFax"]);
                                                            carrierManagerInfo.Id = Convert.ToString(dr["CMId"]);
                                                            //carrierManagerInfo.IsNew = (CMId == 0) ? true : false;
                                                            carrierManagerInfo.IsNew = Convert.ToBoolean(dr["IsNewCarrierManager"]);
                                                            patient_info.CarrierManager = carrierManagerInfo;
                                                        }

                                                        var drState = Convert.ToString(dr["DoctorState"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["DoctorState"]));
                                                        var rushReq = Convert.ToString(dr["OFC_RushRequest"]).Replace(" ", string.Empty) == "" ? false : (Convert.ToBoolean(dr["OFC_RushRequest"]));

                                                        //Doctor/FacilityInfo
                                                        Facility facility = new Facility();
                                                        facility.DrFirstName = Convert.ToString(dr["DoctorFN"]);
                                                        facility.DrLastName = Convert.ToString(dr["DoctorLN"]);
                                                        facility.FacilityName = Convert.ToString(dr["DoctorFacilityName"]);
                                                        facility.DrEmailAddress = Convert.ToString(dr["DoctorEmailAddress"])?.Replace(" ", string.Empty);
                                                        facility.DrAddress = Convert.ToString(dr["DoctorAddress"]);
                                                        facility.DrCity = Convert.ToString(dr["DoctorCity"]);
                                                        facility.DrStateId = _context.States.Where(a => a.Id == drState).Select(a => a.StateCode).FirstOrDefault();
                                                        facility.DrCountry = "23";
                                                        facility.DrRegion = "3";
                                                        facility.DrZipCode = Convert.ToString(dr["Doctorzip"]);
                                                        facility.DrFax = Convert.ToString(dr["DoctorFax"]);
                                                        facility.DrPhone = Convert.ToString(dr["DoctorPhone"]);
                                                        facility.RushRequest = rushReq;
                                                        facility.SpecialInstruction = Convert.ToString(dr["OFCSpecialInstruction"]);

                                                        DoctorId = Convert.ToString(dr["DrId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["DrId"]);
                                                        if (CompanyType == 0)
                                                        {
                                                            facility.FacilityId = _commonContext.DoctorCommon.Where(a => a.Id == DoctorId).Select(a => a.EMapId).FirstOrDefault();
                                                        }
                                                        else
                                                        {
                                                            facility.FacilityId = _commonContext.CustodianCommon.Where(a => a.Id == DoctorId).Select(a => a.EMapId).FirstOrDefault();
                                                        }
                                                        if (facility.FacilityId == 0)
                                                            facility.IsNewFacility = true;
                                                        else
                                                            facility.IsNewFacility = false;
                                                        facility.FacilityId = DoctorId;
                                                        patient_info.Facility = facility;
                                                        var missingFacilityDetails = Convert.ToString(dr["OFCIsMissingFacilityDetails"]).Replace(" ", string.Empty) == "" ? false : (Convert.ToBoolean(dr["OFCIsMissingFacilityDetails"]));
                                                        patient_info.MissingFacility = missingFacilityDetails;
                                                        patient_info.CustomerProvidedFacility = Convert.ToString(dr["OFCCustomerProvidedFacility"]);
                                                        var serviceType = Convert.ToString(dr["OFCDateOfServiceType"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["OFCDateOfServiceType"]));
                                                        if (serviceType == 0)
                                                        {
                                                            patient_info.ServiceType = null;
                                                        }
                                                        else
                                                        {
                                                            patient_info.ServiceType = serviceType;
                                                        }
                                                        var customStartDate = Convert.ToString(dr["OFCStartDate"]);
                                                        if (!string.IsNullOrEmpty(customStartDate))
                                                        {
                                                            if (Convert.ToDateTime(dr["OFCStartDate"]) != null && AESEncrytDecry.DecryptStringAES(Convert.ToString(dr["OFCStartDate"])) != "")
                                                            {
                                                                patient_info.CustomFromDate = Convert.ToDateTime(dr["OFCStartDate"]);

                                                            }
                                                        }
                                                        else
                                                        {
                                                            patient_info.CustomFromDate = null;
                                                        }
                                                        var customEndDate = Convert.ToString(dr["OFCEndDate"]);
                                                        if (!string.IsNullOrEmpty(customEndDate))
                                                        {
                                                            if (Convert.ToDateTime(dr["OFCEndDate"]) != null && AESEncrytDecry.DecryptStringAES(Convert.ToString(dr["OFCEndDate"])) != "")
                                                            {
                                                                patient_info.CustomToDate = Convert.ToDateTime(dr["OFCEndDate"]);

                                                            }
                                                        }
                                                        else
                                                        {
                                                            patient_info.CustomToDate = null;
                                                        }

                                                        patient_info.RequestType = new List<LegalRequestType>();
                                                        var requestType = Convert.ToString(dr["FR_RequestType"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["FR_RequestType"]));
                                                        var specialInstructionType = Convert.ToString(dr["FR_SpecialInstructionType"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["FR_SpecialInstructionType"]));
                                                        var rushRequest = Convert.ToString(dr["FR_RushRequest"]).Replace(" ", string.Empty) == "" ? false : (Convert.ToBoolean(dr["FR_RushRequest"]));
                                                        var certifiedRequest = Convert.ToString(dr["FR_CertifiedRequest"]).Replace(" ", string.Empty) == "" ? false : (Convert.ToBoolean(dr["FR_CertifiedRequest"]));
                                                        var affidavitRequest = Convert.ToString(dr["FR_AffidavitRequest"]).Replace(" ", string.Empty) == "" ? false : (Convert.ToBoolean(dr["FR_AffidavitRequest"]));

                                                        LegalRequestType legalRequestType = new LegalRequestType()
                                                        {
                                                            RequestTitle = requestType,
                                                            RushRequest = rushRequest,
                                                            CertifiedRequest = certifiedRequest,
                                                            AffidavitRequest = affidavitRequest,
                                                            SpecialInstructionType = specialInstructionType,
                                                            Comments = Convert.ToString(dr["FR_SpecialInstruction"])
                                                        };
                                                        patient_info.RequestType.Add(legalRequestType);
                                                        var orderFacilityId = Convert.ToString(dr["OrderFacilityId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["OrderFacilityId"]));
                                                        //AuthDocInfo
                                                        AuthDocs authdocInfo = new AuthDocs();
                                                        var authDocId = Convert.ToString(dr["DocumentId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["DocumentId"]));
                                                        if (authDocId > 0)
                                                        {
                                                            var azurePath = Support.Decrypt(Convert.ToString(dr["AzurePath"])).Trim();
                                                            if (!string.IsNullOrEmpty(azurePath))
                                                            {
                                                                authdocInfo.azure_path = azurePath.Substring(_configuration["Blob"].Length);
                                                            }
                                                            else
                                                            {
                                                                authdocInfo.azure_path = null;
                                                            }
                                                            authdocInfo.FileName = Regex.Replace(Convert.ToString(dr["FileName"]), "[^a-zA-Z0-9_.-]+", "", RegexOptions.Compiled);
                                                            authdocInfo.MimeType = Convert.ToString(dr["DocumentType"]);
                                                            authdocInfo.id = authDocId;
                                                            authdocInfo.RecordTitle = "2";
                                                            authdocInfo.RecordDesc = "Record Upload";
                                                            authdocInfo.RecordRemark = "Auth";
                                                            docslist.Add(authdocInfo);
                                                            DocIdList.Add(authDocId);
                                                        }
                                                        patient_info.AuthDocs = docslist;
                                                        rowCount++;
                                                    }
                                                    else
                                                    {
                                                        //AdjusterInfo
                                                        var AgentId = Convert.ToString(dr["AgentId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["AgentId"]);
                                                        if (AgentId > 0)
                                                        {
                                                            if (!AgentIdList.Contains(AgentId))
                                                            {
                                                                AgentInfo adjusterInfo = new AgentInfo();
                                                                adjusterInfo.AgentFN = Convert.ToString(dr["AgentFN"]);
                                                                adjusterInfo.AgentLN = Convert.ToString(dr["AgentLN"]);
                                                                adjusterInfo.AgentId = Convert.ToString(dr["AgentNumber"]);
                                                                adjusterInfo.AgentPhone = Convert.ToString(dr["AgentPhone"]);
                                                                adjusterInfo.AgentExt = Convert.ToString(dr["AgentExt"]);
                                                                adjusterInfo.AgentCell = Convert.ToString(dr["AgentCell"]);
                                                                adjusterInfo.AgentFax = Convert.ToString(dr["AgentFax"]);
                                                                adjusterInfo.AgentEmail = Convert.ToString(dr["AgentEmail"]).Replace(" ", string.Empty) == "" ? "" : Convert.ToString(dr["AgentEmail"]);
                                                                adjusterInfo.ExpertAgentId = "";
                                                                adjusterInfo.AgentNo = Convert.ToString(dr["AgentNumber"]);
                                                                adjusterInfo.APSAgentNo = Convert.ToString(dr["AgentId"]);
                                                                adjusterInfo.AgentType = Convert.ToString(dr["AgentType"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(dr["AgentType"]);
                                                                //adjusterInfo.AgentIsNew = (AgentId == 0) ? true : false;
                                                                adjusterInfo.AgentIsNew = Convert.ToBoolean(dr["IsNewAgent"]);
                                                                patient_info.Agent.Add(adjusterInfo);
                                                                AgentIdList.Add(AgentId);
                                                            }
                                                        }
                                                        //AuthDocInfo
                                                        var authDocId = Convert.ToString(dr["DocumentId"]).Replace(" ", string.Empty) == "" ? 0 : Convert.ToInt32(Convert.ToString(dr["DocumentId"]));
                                                        if (authDocId > 0)
                                                        {
                                                            if (!DocIdList.Contains(authDocId))
                                                            {
                                                                AuthDocs authdocInfo = new AuthDocs();
                                                                var azurePath = Support.Decrypt(Convert.ToString(dr["AzurePath"])).Trim();
                                                                if (!string.IsNullOrEmpty(azurePath))
                                                                {
                                                                    authdocInfo.azure_path = azurePath.Substring(_configuration["Blob"].Length);
                                                                }
                                                                else
                                                                {
                                                                    authdocInfo.azure_path = null;
                                                                }
                                                                authdocInfo.FileName = Regex.Replace(Convert.ToString(dr["FileName"]), "[^a-zA-Z0-9_.-]+", "", RegexOptions.Compiled);
                                                                authdocInfo.MimeType = Convert.ToString(dr["DocumentType"]);
                                                                authdocInfo.id = authDocId;
                                                                authdocInfo.RecordTitle = "2";
                                                                authdocInfo.RecordDesc = "Record Upload";
                                                                authdocInfo.RecordRemark = "Auth";
                                                                docslist.Add(authdocInfo);
                                                                patient_info.AuthDocs = docslist;
                                                                DocIdList.Add(authDocId);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            var requestResponse = new RequestResponseLog()
                                            {
                                                Request = JsonConvert.SerializeObject(patient_info),
                                                RequestOn = DateTime.Now
                                            };
                                            _context.RequestResponseLogs.Add(requestResponse);
                                            _context.SaveChanges();
                                            var apiresponse = APIHelper.CreatePatient(patient_info, _configuration["apiBaseUrl"]).Result;
                                            _logger.Info($"eXpert Order Creation Process: {order.CompanyCode}_{order.eNoahOrderId}_{_configuration["apiBaseUrl"]}");
                                            requestResponse.Response = JsonConvert.SerializeObject(apiresponse);
                                            requestResponse.ResponseOn = DateTime.Now;
                                            _context.RequestResponseLogs.Update(requestResponse);
                                            if (orderToUpdate != null)
                                            {
                                                orderToUpdate.ExpertResponse = JsonConvert.SerializeObject(apiresponse);
                                                orderToUpdate.ModifiedOnDateTime = DateTime.Now;
                                                _context.OrderAvailabilityInExpert.Update(orderToUpdate);
                                            }
                                            _context.SaveChanges();
                                            if (apiresponse != null)
                                            {
                                                if (apiresponse.message.ToLower() == "success")
                                                {
                                                    TotalSuccess = TotalSuccess + 1;
                                                    if (orderToUpdate != null)
                                                    {
                                                        orderToUpdate.AvailabilityInExpert = true;
                                                        orderToUpdate.ResponseStatus = "SUCCESS";
                                                        orderToUpdate.ModifiedOnDateTime = DateTime.Now;
                                                        _context.OrderAvailabilityInExpert.Update(orderToUpdate);
                                                        _context.SaveChanges();
                                                        _logger.Info($"Order Response: {order.CompanyCode}_{order.eNoahOrderId}_SUCCESS");
                                                    }
                                                    if (patient_info.Facility.IsNewFacility && apiresponse.result != null && !string.IsNullOrEmpty(apiresponse.result.expert_facility_id))
                                                    {
                                                        if (CompanyType == 0)
                                                        {
                                                            var doc = _commonContext.DoctorCommon.Where(a => a.Id == DoctorId).FirstOrDefault();
                                                            doc.EMapId = Convert.ToInt32(apiresponse.result.expert_facility_id);
                                                            _commonContext.SaveChanges();
                                                        }
                                                        else
                                                        {
                                                            var doc = _commonContext.CustodianCommon.Where(a => a.Id == DoctorId).FirstOrDefault();
                                                            doc.EMapId = Convert.ToInt32(apiresponse.result.expert_facility_id);
                                                            _commonContext.SaveChanges();
                                                        }
                                                    }
                                                    if (apiresponse != null && apiresponse.result != null && !string.IsNullOrEmpty(apiresponse.result.aps_order_no))
                                                    {
                                                        var enoahorder = dbContext.OrderFacilityMappings.Where(a => a.eNoahOrderId == apiresponse.result.aps_order_no).FirstOrDefault();
                                                        if (enoahorder != null)
                                                        {
                                                            if (!string.IsNullOrEmpty(apiresponse.result.lead_id))
                                                            {
                                                                enoahorder.ExpertLeadId = Convert.ToInt32(apiresponse.result.lead_id);
                                                                dbContext.OrderFacilityMappings.Update(enoahorder);
                                                                dbContext.SaveChanges();
                                                            }
                                                            var orderAgentToUpdate = dbContext.OrderAgentMappings.Where(a => a.OrderId == enoahorder.OrderId).ToList();
                                                            if (orderAgentToUpdate != null && orderAgentToUpdate.Count > 0)
                                                            {
                                                                foreach (var OA in orderAgentToUpdate)
                                                                {
                                                                    OA.IsNewAgent = false;
                                                                    dbContext.OrderAgentMappings.Update(OA);
                                                                    dbContext.SaveChanges();
                                                                }
                                                            }
                                                            var orderCarrierManagerToUpdate = dbContext.OrderCarrierManagerMappings.Where(a => a.OrderId == enoahorder.OrderId).ToList();
                                                            if (orderCarrierManagerToUpdate != null && orderCarrierManagerToUpdate.Count > 0)
                                                            {
                                                                foreach (var OCM in orderCarrierManagerToUpdate)
                                                                {
                                                                    OCM.IsNewCarrierManager = false;
                                                                    dbContext.OrderCarrierManagerMappings.Update(OCM);
                                                                    dbContext.SaveChanges();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (apiresponse.message.ToLower() == "duplicate order reference no")
                                                {
                                                    DuplicateOrders = DuplicateOrders + 1;
                                                    if (orderToUpdate != null)
                                                    {
                                                        orderToUpdate.AvailabilityInExpert = true;
                                                        orderToUpdate.ResponseStatus = "DUPLICATE";
                                                        orderToUpdate.ModifiedOnDateTime = DateTime.Now;
                                                        _context.OrderAvailabilityInExpert.Update(orderToUpdate);
                                                        _context.SaveChanges();
                                                        _logger.Info($"Order Response: {order.CompanyCode}_{order.eNoahOrderId}_DUPLICATE");
                                                    }
                                                }
                                                else
                                                {
                                                    TotalFailure = TotalFailure + 1;
                                                    orderToUpdate.ResponseStatus = "FAILURE";
                                                    orderToUpdate.ModifiedOnDateTime = DateTime.Now;
                                                    _context.OrderAvailabilityInExpert.Update(orderToUpdate);
                                                    _context.SaveChanges();
                                                    _logger.Info($"Order Response: {order.CompanyCode}_{order.eNoahOrderId}_FAILURE");
                                                }
                                            }
                                            else
                                            {
                                                TotalFailure = TotalFailure + 1;
                                                orderToUpdate.ResponseStatus = "NULLRESPONSE";
                                                orderToUpdate.AvailabilityInExpert = false;
                                                orderToUpdate.ModifiedOnDateTime = DateTime.Now;
                                                _context.OrderAvailabilityInExpert.Update(orderToUpdate);
                                                _context.SaveChanges();
                                                _logger.Info($"Order Response: {order.CompanyCode}_{order.eNoahOrderId}_EXPERT_APIRESPONSE_IS_NULL");
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    TotalFailure = TotalFailure + 1;
                                    orderToUpdate.ResponseStatus = "FAILURE";
                                    orderToUpdate.ModifiedOnDateTime = DateTime.Now;
                                    orderToUpdate.ExpertResponse = ex.ToString();
                                    _context.OrderAvailabilityInExpert.Update(orderToUpdate);
                                    _context.SaveChanges();
                                    _logger.Error($"Hit an exception - Order details: {order.CompanyCode}_{order.eNoahOrderId}");
                                    _logger.Error(ex);
                                }
                            }
                            else
                            {
                                _logger.Error("Company is Null. Could not make dbContext.");
                            }

                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                            _logger.Error($"Hit an exception - While processing Order: {order.CompanyCode}_{order.eNoahOrderId}");
                            response.StatusCode = Models.Constants.StatusCodes.FailedException;
                            response.StatusMessage = Constants.Failure;
                        }
                    }
                    _logger.Info(string.Format("Resent Orders - Total Success: {0}", TotalSuccess));
                    _logger.Info(string.Format("Resent Orders - Total Failure: {0}", TotalFailure));
                    _logger.Info(string.Format("Resent Orders - Total Duplicates: {0}", DuplicateOrders));
                }
                else
                {
                    MailFlag = "norecords";
                    _logger.Info("No Eligible orders found in the table 'OrderAvailabilityInExpert' to send to eXpert.");
                }
                if (MailFlag != "norecords")
                {
                    _rootDirectoryPath = _configuration["DailyReportPath"];
                    var excelFilePath = string.Concat(_env.ContentRootPath,
                     Path.DirectorySeparatorChar.ToString(),
                     "EMailTemplates",
                     Path.DirectorySeparatorChar.ToString(),
                     "DailyReconResponseReport.xlsx");
                    XLWorkbook Workbook = new XLWorkbook(excelFilePath);
                    IXLWorksheet Worksheet = Workbook.Worksheet("Sheet1");
                    int NumberOfLastRow = Worksheet.LastRowUsed().RowNumber();
                    int prevCell = NumberOfLastRow + 1;
                    int count = 1;
                    var processedOrdersList = await _context.OrderAvailabilityInExpert.Where(order => order.ModifiedOnDateTime.Date == DateTime.Today).OrderBy(order => order.Id).ToListAsync();
                    if (processedOrdersList != null && processedOrdersList.Any())
                    {
                        try
                        {
                            foreach (var orders in processedOrdersList)
                            {
                                Worksheet.Cell("A" + prevCell.ToString()).Value = count;
                                Worksheet.Cell("B" + prevCell.ToString()).Value = orders.CompanyCode;
                                Worksheet.Cell("C" + prevCell.ToString()).Value = orders.eNoahOrderId;
                                Worksheet.Cell("D" + prevCell.ToString()).Value = orders.OrderCreatedDateTime;
                                //Worksheet.Cell("D" + prevCell.ToString()).Value = (DateTime.ParseExact((orderFacilityDetails[j].CreatedOnDateTime), new string[] { "yyyy-MM-dd", "MM-dd-yyyy", "MM/dd/yyyy","dd/MM/yyyy", "dd-MM-yyyy" }, new CultureInfo("en-US"), DateTimeStyles.None)).ToString();
                                Worksheet.Cell("E" + prevCell.ToString()).Value = orders.OrderStatus;
                                Worksheet.Cell("F" + prevCell.ToString()).Value = orders.AvailabilityInExpert.ToString().ToLower() == "false" ? "NO" : "YES";
                                //Worksheet.Cell("F" + prevCell.ToString()).Value = orders.AvailabilityInExpert.ToString() == "0" ? "NO" : "YES";
                                Worksheet.Cell("G" + prevCell.ToString()).Value = orders.ResponseStatus;
                                prevCell++;
                                count++;
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex.ToString());
                        }
                        Worksheet.Columns().AdjustToContents();
                        Worksheet.Rows().AdjustToContents();
                        Workbook.SaveAs(File);
                    }
                }
                response.StatusCode = Models.Constants.StatusCodes.SuccessStatusCode;
                response.StatusMessage = Constants.Success;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                MailFlag = "error";
                response.StatusCode = Models.Constants.StatusCodes.FailedException;
                response.StatusMessage = Constants.Failure;
            }
            if (_configuration["ReconReportSendMail"] == "1" && MailFlag != "norecords")
            {
                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                helper.SendResentOrdersToExpertReport("Report of Resent orders to eXpert", TotalOrders, TotalSuccess, TotalFailure, DuplicateOrders, MailFlag, File);
            }
            _logger.Info("ResendOrdersToExpert API has Ended.");
            return response;
        }

        public void DIGroupWeeklyPendingFirstStatusReport()
        {
            _logger.Info("DI Group First Status Weekly Report: Job started");
            DataSet distatusreport = new DataSet();
            var companyDetail = new CompanyDetails();
            var defaulltConnectionString = _configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection myConnection = new SqlConnection(defaulltConnectionString))
            {
                string oString = "SELECT CompanyCode,ConnectionString FROM [dbo].[CompanyDetail] WHERE CompanyCode='DIGROUP'";
                SqlCommand oCmd = new SqlCommand(oString, myConnection);
                myConnection.Open();
                SqlDataReader oReader = oCmd.ExecuteReader();
                while (oReader.Read())
                {
                    companyDetail.CompanyCode = oReader["CompanyCode"].ToString();
                    companyDetail.ConnectionString = oReader["ConnectionString"].ToString();
                }
                oReader.Close();
                myConnection.Close();
            }
            try
            {
                var connectionString = companyDetail.ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                        connection.Open();
                        SqlCommand sqlCmd = new SqlCommand("sp_DIGroupFirstStatusWeeklyReport", connection);
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.CommandTimeout = 300;
                        SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCmd);
                        sqlDa.Fill(distatusreport);
                        connection.Close();
                }

                if (distatusreport.Tables[0].Rows.Count > 0)
                {                    
                        string File = _configuration["DIGroupReport"] + "DIGroupReport_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xlsx";
                        XLWorkbook Workbook = new XLWorkbook();
                        Workbook.AddWorksheet(distatusreport.Tables[0], "Sheet1");
                        Workbook.SaveAs(File);
                       _logger.Info($"DI Group First Status Weekly Report: Stored Excel file as {File}");
                    if (_configuration["DIGroupReportSendMail"] == "1")
                        {
                            EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                            bool MailSentStatusWithAttachment = helper.SendDIGroupReport("DI Group First Status Weekly Report", File,"hasRecords");
                        if (MailSentStatusWithAttachment)
                        {
                            _logger.Info("DI Group First Status Weekly Report: The email was successfully delivered with attachment file");
                        }
                    }
                }

                else
                {
                    if (_configuration["DIGroupReportSendMail"] == "1")
                    {
                        EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                        helper.SendDIGroupReport("DI Group Weekly Pending First Status Report", "", "NoRecords");
                        _logger.Info("DI Group Weekly Pending First Status Report: No records found Mail delivered Successfully.");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("DI Group First Status Weekly Report: Unexpected Error Occured.");
                _logger.Error(ex);
                EmailHelper helper = new EmailHelper(_context, _configuration, _env);
                helper.SendDIGroupReport("DI Group First Status Weekly Report", "", "ErrorOccured");
            }
            finally
            {
                _logger.Info("DI Group First Status Weekly Report: Job completed");
            }
        }

        public void ProcessingAcord1122OrderStatus()
        {
            ResponseBase response = new ResponseBase();
            try
            {
                string[] companies = _configuration.GetSection("GenerateAcord1122FormatCompanies").Get<string[]>();
                string[] company = _configuration.GetSection("GenerateAcord1122ForB2BOrderCompanies").Get<string[]>();
                string[] combinedCompanies = companies.Select(c => c.ToLower()).Concat(company.Select(c => c.ToLower())).ToArray();
                if (combinedCompanies.Length > 0)
                {
                    foreach (var comp in combinedCompanies)
                    {
                        var companyDetails = _context.CompanyDetails.Where(a => a.CompanyCode == comp).FirstOrDefault();
                        var optionsBuilder = new DbContextOptionsBuilder<APSDBContext>();
                        optionsBuilder.UseSqlServer(companyDetails.ConnectionString);
                        using (APSDBContext dbContext = new APSDBContext(optionsBuilder.Options))
                        {
                            dbContext.connectionString = companyDetails.ConnectionString;
                            var companyCode = dbContext.CompanyMasters.Select(i => i.CompanyCode).FirstOrDefault();
                            var orderStatus = dbContext.OrderStatus.Where(x => !x.IsStatusSent && !x.IsDeleted).ToList();
                            if (orderStatus.Count > 0)
                            {
                                foreach (var status in orderStatus)
                                {
                                    OrderFacilityMapping orderFacility = dbContext.OrderFacilityMappings.Where(a => a.Id == status.FacilityMapId).FirstOrDefault();
                                    string eventType = string.Empty;
                                    if (status.EventType != 0)
                                    {
                                        status.EventType = 202;
                                        APIHelper apihelper = new APIHelper(_configuration);
                                        EventTypes rs = apihelper.GetEventType(status.EventType.ToString(), "PassEventID");
                                        if (rs == null)
                                        {
                                            eventType = Constants.EventTypeNotFound;
                                            _logger.Info(companyCode + " : Event Type Not Found for eNoahOrderId - " + orderFacility.eNoahOrderId);
                                        }
                                        else
                                        {
                                            eventType = rs.StatusEventValue;
                                        }
                                    }
                                    if (eventType != Constants.EventTypeNotFound && eventType != Constants.Archive)
                                    {
                                        if (orderFacility != null || orderStatus != null)
                                        {
                                            CompanyMaster companyMaster = dbContext.CompanyMasters.FirstOrDefault();
                                            OrderFacilityMapping parentOrderFacilityMapping = null;
                                            if (orderFacility.ParentId.HasValue)
                                            {
                                                parentOrderFacilityMapping = dbContext.OrderFacilityMappings.Where(o => o.Id == orderFacility.ParentId).FirstOrDefault();
                                            }
                                            if (companies.Contains(companyMaster.CompanyCode.ToLower()) ||
                                                (companyMaster.CompanyCode.ToLower() == "amica" &&
                                                (dbContext.InboundOrderDetails.Any(i => i.OrderID == orderFacility.eNoahOrderId) || dbContext.ReopenInboundOrderDetails.Any(i => i.OrderID == orderFacility.eNoahOrderId)))
                                                || (orderFacility.IsB2BOrder && company.Contains(companyCode.ToLower())) || (parentOrderFacilityMapping != null && parentOrderFacilityMapping.IsB2BOrder) || (companyCode.ToLower() == "riversource" && _configuration["RiversourceResultsDeliveryEnabled"].ToString() == "1"))
                                            {
                                                FileService fileService = new FileService(dbContext, _configuration, _mapper, _env, _commonContext, _userManager);
                                                ResponseBase acord1122Result = fileService.GenerateAcord1122FormatXml(orderFacility.Id, companyMaster.CompanyType, companyMaster.CompanyCode.ToLower()).Result;

                                                if (acord1122Result == null || acord1122Result.StatusCode != StatusCodes.SuccessStatusCode)
                                                {
                                                    string statusType = "1122";
                                                    string eNoahOrderId = orderFacility.eNoahOrderId;

                                                    if (companyCode.ToLower() == "protective")
                                                    {
                                                        statusType = "Custom";
                                                    }
                                                    if (orderFacility.ParentId.HasValue && orderFacility.ParentId.Value > 0 && !string.IsNullOrEmpty(orderFacility.ParentEnoahOrderId))
                                                    {
                                                        eNoahOrderId = orderFacility.ParentEnoahOrderId;
                                                    }
                                                    EmailHelper helper = new EmailHelper(dbContext, _configuration, _env);
                                                    helper.Send1122StatusFailedToSentMail(eNoahOrderId, status.Status.ToString(), DateTime.Now, acord1122Result.StatusMessage, companyMaster.CompanyCode.ToLower(), statusType);
                                                }
                                            }
                                            response.StatusCode = StatusCodes.SuccessStatusCode;
                                            response.StatusMessage = Constants.Success;
                                        }
                                        else
                                        {
                                            response.StatusCode = StatusCodes.FailedException;
                                            response.StatusMessage = Constants.OrderNotFound;
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                response.StatusCode = StatusCodes.ExceptionStatusCode;
                response.StatusMessage = ex.Message;
            }
        }
    }
}
