<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
// error_reporting(E_ALL);
// ini_set('display_errors',1);
class Welcome extends CRM_Controller {
	
	public $cfg;
	public $userdata;
	private $order_age;
	public $CI = NULL;
	function __construct() 
	{
		parent::__construct();
		$this->load->library('Document_upload_trigger');
		$this->load->library('Post_trigger_api');
		$this->load->library('Esubmission_generate_token');
		$this->load->library('CQRCodeOne');
		$this->load->library('XLSXWriter');
		$this->load->library('fpdf182/FPDF');
		$this->load->library('fpdi/FPDI');
		$this->CI = & get_instance();
		$this->Login_model->check_login();
		$this->userdata = $this->session->userdata('logged_in_user');
		$this->load->model('Welcome_model');
		$this->load->model('Customer_model');
		$this->load->model('Custodian_model');
		$this->load->model('Regionsettings_model');
		$this->load->model('Email_template_model');
		$this->load->model('Welcome_view_validate_model');
		$this->load->model('Company_list_model');
        $this->load->model('Timezone_model');
		$this->load->model('Close_codes_model');
		$this->load->model('Summarization_model');
		$this->load->model('Payment_cancel_model');
		$this->load->model('Manage_lead_stage_model');
		$this->load->model('Disposition_model');
		$this->load->model('Lor_model');
		$this->load->library('Fileencryption');
		$this->load->library('sha2lib');
		$this->load->library('S3_upload');
		$this->load->library('S3');
		$this->load->helper('text');
		$this->load->helper('custom_log_helper');
		$this->email->set_newline("\r\n");
		$this->load->library('Pdf');
		//$this->load->library('My_Log');
		$this->load->helper('us_holidays_helper');
		$this->holiday = getUSHolidays();
		$this->load->library('Ajax_pagination'); 
        $this->load->library('Pagination');
		$this->load->helper('lead_stage_helper');
		$this->stg = getLeadStage();
		$this->stg_name = getLeadStageName();
		$this->evtTyp_name = getEventTypeName();
		$this->stg_queue_name = getQueueLeadStage();
		$this->stages = @implode('","', $this->stg);
        $this->load->model('Azure_model');
		$this->load->model('Azure_sas_model');
	}
	
    /*
	 * Redirect user to quotation list
	 */
	public function index() {
		redirect('welcome/quotation');
    }
	
	/*
	 * List all the Leads based on levels
	 * @access public
	 */
	
	 public function quotation($type = 'draft', $tab='') {
	// echo "TEst";die;
			$page_label = 'Order List' ;
			$proposal_expected_from_date = date('Y-m-d');
			$proposal_expected_to_date = date('Y-m-d');
			$proposal_expected_from_date1 = NULL;
			$proposal_expected_to_date1 = NULL;
			$data['lead_stage'] = $this->stg_name;
	
			$data['event_type'] = $this->evtTyp_name;
	
			// echo"<pre>";print_r($data['lead_stage']);die;
	
			//$data['customers'] = $this->Welcome_model->get_customers();
			$data['lead_owner'] = $this->Welcome_model->get_users();
			$data['regions'] = $this->Regionsettings_model->region_list();
	
			// $data['carrier_name'] = $this->Welcome_model->get_carrier_name();
			$cType = explode(',',$this->userdata['company_type']);
			$data['carrier_name'] = $this->Company_list_model->getCompany_list($cType);
					$data['stateList'] = $this->Timezone_model->getState_list();
	
			$data['queue_lead_stage'] = $this->stg_queue_name;
			if($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29){
				$lead_assign = null;
			}else{
				$lead_assign = $this->userdata['userid'];
			}
			//$company_type = $this->userdata['company_type'];
			// $data['timezone']=$this->Timezone_model->timezone_list();
			$data['timezone']=$this->Timezone_model->timezone_Activelist();
			$data['queue_lead_stage_values'] = $this->Welcome_model->get_queue_lead_stage($proposal_expected_from_date1,$proposal_expected_to_date1,$lead_assign,'');
			 // stage bucket count not matching start
			 if(is_countable($data['queue_lead_stage_values']) &&  sizeof($data['queue_lead_stage_values']) > 0)
			 {
			 $queue_ind=array_keys($data['queue_lead_stage_values']);
			 $rem_queue_ind=$this->Welcome_model->get_not_queue_stages($queue_ind);
			 }
			 if(is_countable($rem_queue_ind) &&  sizeof($rem_queue_ind) > 0)
			 {
				  $data['queue_lead_stage_values']=$data['queue_lead_stage_values'] + $rem_queue_ind;
			 }
			 // stage bucket count not matching start end
			foreach($data['queue_lead_stage_values'] as $k=> $val){
				if($k==$this->config->item('crm')['check_payment_requested_stage']){
					$rst = $this->Welcome_model->get_queue_event_type($k,$this->config->item('crm')['check_payment_requested_disposition'],$lead_assign);
					if($rst[0]['order_disposition_id'] != NULL){
						$data['queue_lead_stage_values'][$k] = array($val[0]+$rst[0]['orderCount']);
					}
				}
				// else{
				// 	$rst = $this->Welcome_model->get_queue_event_type($this->config->item('crm')['check_payment_requested_stage'],$this->config->item('crm')['check_payment_requested_disposition'],$lead_assign);
				// 	if($rst[0]['order_disposition_id'] != NULL){
				// 		$data['queue_lead_stage_values'][$k] = array($val[0]+$rst[0]['orderCount']);
				// 	}
				// }
				if($k==$this->config->item('crm')['check_payment_mailroom_stage']){
					$rst = $this->Welcome_model->get_queue_event_type($k,$this->config->item('crm')['check_payment_created_disposition'],$lead_assign);
					if($rst[0]['order_disposition_id'] != NULL){
						$data['queue_lead_stage_values'][$k] = array($val[0]+$rst[0]['orderCount']);
					}
				}
			}
			$data['sort']='';
			// print_r($data['queue_lead_stage_values']); die;
			// echo $this->db->last_query();exit;
			$this->load->view('leads/quotation_view', $data);
			
		}

	public function get_queue_lead_stage_values($proposal_expected_from_date='null',$proposal_expected_to_date='null'){
		$filt = real_escape_array($this->input->post());
		if (count($filt)>0) {
			if($this->session->userdata['logged_in_user']['role_id'] == 1 || $this->session->userdata['logged_in_user']['role_id'] == 29){
				$lead_assign = null;
			}else{
				$lead_assign = $this->session->userdata['logged_in_user']['userid'];
			}
			$user_id = $this->session->userdata['logged_in_user']['userid'];
			$proposal_expected_from_date_new 	  = ($filt['proposal_expected_start_date'])?  date('Y-m-d',strtotime($filt['proposal_expected_start_date'])) : NULL;
			$proposal_expected_to_date_new 	  = ($filt['proposal_expected_to_date'])? date('Y-m-d',strtotime($filt['proposal_expected_to_date'])) : NULL;
			$company_type = $filt['company_type'];
			$data = $this->Welcome_model->get_queue_lead_stage($proposal_expected_from_date_new,$proposal_expected_to_date_new,$lead_assign,$company_type);
            // stage bucket count not matching start
            if(is_countable($data) &&  sizeof($data) > 0)
            {
            $queue_ind_1=array_keys($data);
            $rem_queue_ind_1=$this->Welcome_model->get_not_queue_stages($queue_ind_1);
            }
            if(is_countable($rem_queue_ind_1) &&  sizeof($rem_queue_ind_1) > 0)
            {
	        $data=$data + $rem_queue_ind_1;
            }
            // stage bucket count not matching start end
			foreach($data as $k=> $val){
				if($k==$this->config->item('crm')['check_payment_requested_stage']){
					if($proposal_expected_from_date_new !='' && $proposal_expected_to_date_new!=''){
					   $rst = $this->Welcome_model->get_queue_event_type_count($k,$this->config->item('crm')['check_payment_requested_disposition'],$lead_assign,$company_type,$proposal_expected_from_date_new,$proposal_expected_to_date_new);
					}else{
					$rst = $this->Welcome_model->get_queue_event_type($k,$this->config->item('crm')['check_payment_requested_disposition'],$lead_assign,$company_type);
					}
					if($rst[0]['order_disposition_id'] != NULL){
						$data[$k] = array($val[0]+$rst[0]['orderCount']);
					}
				}
				if($k==$this->config->item('crm')['check_payment_mailroom_stage']){
					if($proposal_expected_from_date_new !='' && $proposal_expected_to_date_new!=''){
						$rst = $this->Welcome_model->get_queue_event_type_count($k,$this->config->item('crm')['check_payment_created_disposition'],$lead_assign,$company_type,$proposal_expected_from_date_new,$proposal_expected_to_date_new);
					}else{
					$rst = $this->Welcome_model->get_queue_event_type($k,$this->config->item('crm')['check_payment_created_disposition'],$lead_assign,$company_type);
					}
					if($rst[0]['order_disposition_id'] != NULL){
						$data[$k] = array($val[0]+$rst[0]['orderCount']);
					}
				}
			}

			echo json_encode($data);
		}
	}
	
	/*
	 * List all the Leads based on levels with advanced search filter.
	 */
	public function advance_filter_search($sort='null',$text='null',$limit='null',$stage='null', $customer='null', $customerph='null', $timezone='null', $from_date='null', $to_date='null', $leadassignee='null', $regionname='null',$countryname='null', $statename='null', $locname='null', $lead_status='null', $lead_indi='null', $keyword='null', $queue_management_group='null', $queue_management_status='null',$proposal_expected_from_date='null',$proposal_expected_to_date='null',$searchtype='default',$order_handle_by='null',$limit_page = 'null') 
	{
		log_message('error','Order DashBoard Advance Search Filter '.$this->session->userdata['logged_in_user']['userid'].' at '.date('Y-m-d H:i:s'));
		$filt = real_escape_array($this->input->post());
		// echo "<pre>"; print_r($filt); exit;
		if (count($filt)>0) {

			if(isset($filt['sort']))
				$sort  = $filt['sort'];
			else
				$sort  = '';

			if(isset($filt['text']))
				$text = $filt['text'];
			else
				$text  = '';

			if(isset($filt['limit']))
				$limit = $filt['limit'];
			else
				$limit = '';
			$stage 		  			= $filt['stage'];
			$customer 	  			= $filt['customer'];
			$customerph   			= $filt['customerph'];
			$timezone     			= $filt['timezone'];
			$from_date    			= $filt['start_date'];
			$to_date      			= $filt['to_date'];
			$leadassignee 			= $filt['leadassignee'];
			$regionname   			= $filt['regionname'];
			$countryname  			= $filt['countryname'];
			$statename 	  			= $filt['statename'];
			$locname 	  			= $filt['locname'];
			$lead_status  			= $filt['lead_status'];
			$lead_indi 	  			= $filt['lead_indi'];
			$keyword 	  		    = $filt['keyword'];
			if(isset($filt['queue_group']))
				$queue_management_group = $filt['queue_group'];
			else
				$queue_management_group = '';
			
			if(isset($filt['queue_status']))
				$queue_management_status = $filt['queue_status'];
			else
				$queue_management_status = '';
			$event_type 			= $filt['event_type'];
			$carrier_name 			= $filt['carrier_name'];

			if(isset($filt['search_category']))
				$search_category = $filt['search_category'];
			else
				$search_category = '';

			$limit_page				= $filt['page'];
			if($queue_management_group != '' && $queue_management_group != 'null'){
				$queue_management_group = explode('_',$queue_management_group);
				if($queue_management_group[0]==$this->config->item('crm')['check_payment_requested_stage'] || $queue_management_group[0]==$this->config->item('crm')['check_payment_mailroom_stage']){
					$event_type = $queue_management_group[1];
					$queue_management_group = $queue_management_group[0];
				}
			}
            if($carrier_name == 'Please Choose'){
				$carrier_name=NULL;
			}            
            if(empty($filt['proposal_expected_start_date'])){
				$filt['proposal_expected_start_date']=NULL;
			}

			$order_handle_by 		= array();
			if(isset($filt['order_handle_by']) && $filt['order_handle_by'] != 'null' && $filt['order_handle_by'] != NULL)
			{
				$order_handle_by 		= $filt['order_handle_by'];
			}
			
			if(empty($filt['proposal_expected_start_date'])){
				$filt['proposal_expected_start_date']=NULL;
			}

			if(empty($filt['proposal_expected_to_date'])){
				$filt['proposal_expected_to_date']=NULL;
			}
			$proposal_expected_from_date = (isset($filt['proposal_expected_start_date']))? (($filt['proposal_expected_start_date']!='null')? date('Y-m-d',strtotime($filt['proposal_expected_start_date'])) : $filt['proposal_expected_start_date']) : NULL;
			if($proposal_expected_from_date == 'null'){
				$proposal_expected_from_date='';
			}

			$proposal_expected_to_date = (isset($filt['proposal_expected_to_date']))? (($filt['proposal_expected_to_date']!='null')? date('Y-m-d',strtotime($filt['proposal_expected_to_date'])) : $filt['proposal_expected_to_date']) : NULL;
			if($proposal_expected_to_date == 'null'){
				$proposal_expected_to_date='';
			}

			$company_type = (isset($filt['company_type']))? $filt['company_type']: NULL;
			$filter_changes = (isset($filt['filter_changes']))? $filt['filter_changes']: NULL;
			$excel_arr 	  = array();
			foreach ($filt as $key => $val) {
				$excel_arr[$key] = $val;
			}
			
			$excel_arr['proposal_expected_start_date'] = (isset($filt['proposal_expected_start_date']))? (($filt['proposal_expected_start_date']!='null' && $filt['proposal_expected_start_date']!=NULL)? date('Y-m-d',strtotime($filt['proposal_expected_start_date'])) : $filt['proposal_expected_start_date']) : NULL;
			$excel_arr['proposal_expected_to_date'] = (isset($filt['proposal_expected_to_date']))? (($filt['proposal_expected_to_date']!='null' && $filt['proposal_expected_to_date']!=NULL)? date('Y-m-d',strtotime($filt['proposal_expected_to_date'])) : $filt['proposal_expected_to_date']) : NULL;
			
			$excel_arr = array_diff($excel_arr,array('null'));
			$excel_arr = array_diff($excel_arr,array('undefined'));
			$res_arr = array_filter($excel_arr, function($value) {
				return ($value !== null && $value !== ''); 
			});
			$this->session->set_userdata(array("excel_download"=>$res_arr));

		} else {
			$this->session->unset_userdata(array("excel_download"=>''));
		}
		// echo "<pre>"; print_r($this->session->userdata);
		if($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29){
			$lead_assign = null;
		}else{
			$lead_assign = $this->userdata['userid'];
		}
		$config['total_rows'] = $data['total_rows'] = count($this->Welcome_model->get_filter_results($sort,$text,'',$stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status, $lead_indi, $keyword, $queue_management_group,$queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$lead_assign,$event_type,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category));  
		$default = array('lead_id', 'desc');
		if (!$this->session->userdata('order_sort')) {
			$this->session->set_userdata('order_sort', $default);
		}
		$current = $this->session->userdata('order_sort'); 
		$config['base_url'] = $this->config->item('base_url') . 'welcome/quotation/';
        if($limit!=''){
		$config['per_page'] = $limit;
		}
		else{
		$config['per_page'] = 10;
		}
        // $config['per_page'] = 10;
		
        $config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['reuse_query_string'] = TRUE;
		$config['query_string_segment'] = 'page';

        $choice = $config['total_rows'] / $config['per_page'];
        $config['num_links'] = 2;
		$config['uri_segment'] = 3;
		$config['full_tag_open'] = "<ul class='pagination'>";
	    $config['full_tag_close'] = '</ul>';
	    $config['num_tag_open'] = '<a href="#">';
	    $config['num_tag_close'] = '</a>';
	    $config['cur_tag_open'] = '<a href="#" class="active">';
	    $config['cur_tag_close'] = '</a>';
	    $config['prev_tag_open'] = '<a href="#">';
	    $config['prev_tag_close'] = '</a>';
	    $config['first_tag_open'] = '<a href="#">';
	    $config['first_tag_close'] = '</a>';
	    $config['last_tag_open'] = '<a href="#">';
	    $config['last_tag_close'] = '</a>';
	    $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
	    $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
	    $config['next_tag_open'] = '<a href="#">';
	    $config['next_tag_close'] = '</a>';

		$data['page'] = ($limit_page != '') ? $limit_page : 0;
		$data['current_sort'] = $current;
		$this->pagination->cur_page = $data['page'];

		$this->pagination->initialize($config);
		$filter_results = $this->Welcome_model->get_filter_results($sort,$text,$limit,$stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status, $lead_indi, $keyword, $queue_management_group,$queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$lead_assign,$event_type,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category,$config['per_page'], $data['page'],'pagination');
		$data['links'] = $this->pagination->create_links();      
		
                foreach($filter_results as $ky => $val){
                    $order_age= $this->Welcome_model->get_three_tat($val['lead_id']);
                    if(isset($order_age[0]['completed_age']))
                            $filter_results[$ky]['real_tat'] = $order_age[0]['completed_age'];
                    if($val['is_retention_done'] == 1) # retension data read only 
                    {
                        $tet=$val['archive_done_at'];
                        $test_time=date("Y-m-d H:i:s", strtotime("{$tet} +".$this->config->item('crm')['PHI_duration']." days"));
                        $ctime=date("Y-m-d H:i:s");				
                        if($ctime>$test_time && $this->config->item('crm')['enable_PHI_info_hiding']==1){
                        $phi_fields = $this->config->item('crm')['retension_order_info_fields'];
                        foreach($phi_fields as $field => $value)
                        {
                                $filter_results[$ky][$field] = $value;
                        }
                        }
                    }
		}
		// echo "<pre>"; print_r($filter_results); exit;
		// echo $this->age;
		// echo $this->db->last_query(); exit;
		$data['filter_results'] = $filter_results;
		$data['sort']         =$sort;
		$data['text']         =$text;
		$data['stage'] 		  = $stage;
		$data['customer']	  = $customer;
		$data['customerph']   = $customerph;
		$data['timezone']     = $timezone;
		$data['start_date']    = $from_date;
		$data['to_date']      = $to_date;
		$data['leadassignee'] = $leadassignee;
		$data['regionname']   = $regionname;
		$data['countryname']  = $countryname;
		$data['statename'] 	  = $statename;
		$data['locname'] 	  = $locname;
		$data['lead_status']  = $lead_status;
		$data['lead_indi']    = $lead_indi;
		$data['keyword'] 	  = trim($keyword);
		// print_r($filter_results);exit;
		$this->load->view('leads/advance_filter_view', $data);
	}

	//function to calculate the number of US holidays from a given date
	public function get_holiday_count($startdate,$enddate){
		//total holiday list from helper function
		$holiday = $this->holiday;
		
			$new = array();
			//['leave_date'] = ['holiday_name']
			foreach($holiday as $array) {
				$new[$array['leave_date']] = $array['holiday'];
			}
			//list of holidays from the given date
			foreach ($new as $item=>$value){
				if ($item >= $startdate && $item <= $enddate){
					$newarray[] = $item;
				}
			}
			return count($newarray);
	}

	
	// This is temporary functin for dashboard
	
	
	/*
	 * Display the Lead
	 * @access public
	 * @param int $id - Job Id
	 */
	 
	 public function real_ts($id){
		 $getLeadDet = $this->Welcome_model-> get_stage_log($id);
	 }
	 
	 public function update_session(){
		if($this->config->item('crm')['enable_time_extend_popup']==1)
		   {
			   if($this->input->post('update_flag')==0){
				   $update_flag=0;
					$now = date("Y-m-d H:i:s");
					$update_time= date("Y-m-d H:i:s", strtotime("{$now} +2 minutes"));
					$start_view =time();
					// $start_view=new Date().getTime();
					$query=$this->Welcome_view_validate_model->update_access_log($this->input->post('user_id'),$this->input->post('lead_id'),$update_time,$update_flag);
					$this->session->unset_userdata('start_view');
					$this->session->set_userdata('Start_view',$start_view);
			   }elseif($this->input->post('update_flag')==1){
					$update_flag=1;	
					$update_time = date("Y-m-d H:i:s");
					$query=$this->Welcome_view_validate_model->update_access_log($this->input->post('user_id'),$this->input->post('lead_id'),$update_time,$update_flag);				
			   }
		   }
		return true;
	 }
	 public function reset_user_access(){
		$this->session->unset_userdata('start_view');
		$query=$this->Welcome_view_validate_model->reset_user_access($this->input->post('user_id'),$this->input->post('lead_id'));
		return true;
	 }
	 public function ffpreavent(){
		$uri=$this->uri->segment(4);
					if($uri!='drafts'){
 			$_SESSION['login_errors']= array("This order already is in process on another tab.");
			$this->session->set_flashdata('login_errors', array("This order already is in process on another tab."));
					redirect('welcome/quotation');
					} 
	 }
	public function view_quote($id = 0, $quote_section = '')
	{	
        $this->load->helper('text');
		$this->load->helper('fix_text');			
		$session_role = $this->session->userdata('logged_in');		
		$usid = $this->session->userdata('logged_in_user');		
		$getLeadDet = $this->Welcome_model->get_lead_detail($id);
                // archived data display "Redact" text 12/10/2022
		if(isset($getLeadDet[0]) && $getLeadDet[0]['is_archive_process'] == 1 && $getLeadDet[0]['is_retention_done'] == 1)
		{ 
			$phi_fields = $this->config->item('crm')['retension_order_info_fields'];
			foreach($phi_fields as $field => $value)
			{
				$getLeadDet[0][$field] = $value;
			}
		}
		$order_access = $this->Welcome_view_validate_model->get_order_detail($id);
		$get_order_access = $this->Welcome_view_validate_model->get_order_access($order_access['lead_id']);

		if(isset($get_order_access['access_datetime']))
			$db_date = $get_order_access['access_datetime'];
		else
			$db_date = '';

		// $add_time = strtotime(date("Y-m-d H:i:s", strtotime("{$db_date} +1 minutes")));	
		// $now = strtotime(date("Y-m-d H:i:s"));	
		$now = date("Y-m-d H:i:s");
		if($this->config->item('crm')['enable_time_extend_popup']==1){
			$add_time= date("Y-m-d H:i:s", strtotime("{$db_date} +30 seconds"));
		}else{
		$add_time= date("Y-m-d H:i:s", strtotime("{$db_date} +10 minutes"));
		}
		
		if(empty($get_order_access)){
			$ins=array();
			$ins['order_id']  = (trim($order_access['lead_id']) != '' && $order_access['lead_id'] != 'NULL' ? $order_access['lead_id'] : $id);			
			$ins['user_id'] = $usid['userid'];
			$ins['access_datetime']  = date("Y-m-d H:i:s");	
			if($usid['userid'] == $order_access['lead_assign']){
				$ins['access_check']  = 0;
			}else{
				$ins['access_check']  = 1;	
			}
			
			$this->db->insert($this->cfg['dbpref'] . 'order_access_log', $ins);		
		}		
		else if(!empty($get_order_access) && $now > $add_time ){			
			$this->db->where('order_id', $order_access['lead_id']);	
			if($usid['userid'] == $order_access['lead_assign']){
				if($this->config->item('crm')['enable_time_extend_popup']==1){
				$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>2,'no_of_extends'=>0));}else{
				$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>2));}
			}else if ($usid['userid'] != $order_access['lead_assign']){
				
				if($this->config->item('crm')['enable_time_extend_popup']==1){
					$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>3,'no_of_extends'=>0));
				}else{
				$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>3));
				}
			}else{					
				echo "Update error";
			}				
		}
		elseif($get_order_access['user_id']==$usid['userid']){
			if($this->config->item('crm')['enable_time_extend_popup']==1){

				// $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
				$is_page_refreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) &&($_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0' ||  $_SERVER['HTTP_CACHE_CONTROL'] == 'no-cache'); 
				 $brw = $_SERVER['HTTP_USER_AGENT'];
				if(strpos($brw,'Firefox') !== false){
					if($is_page_refreshed == ''){
						$is_page_refreshed = 'max-age=0';
					}
					// echo "<script>
					
					// // registerOpenTab FUNCTION

					// const registerOpenTab = () => {
					//   let tabsOpen = 1;
					//   while (localStorage.getItem('openTab' + tabsOpen) !== null) {
					//     tabsOpen++;
					//   }
					//   localStorage.setItem('openTab' + tabsOpen, 'open');
					//   if (localStorage.getItem('openTab2') !== null) {
					//       //window.alert('This application is already running in ' + (tabsOpen - 1) + ' other browser tab(s).');
					// 	  document.location.href='welcome/ffpreavent';
					//   }
					// }

					// // unregisterOpenTab FUNCTION

					// const unregisterOpenTab = () => {
					//   let tabsOpen = 1;
					//   while (localStorage.getItem('openTab' + tabsOpen) !== null) {
  
					//     tabsOpen++;
					//   }
					//   localStorage.removeItem('openTab' + (tabsOpen - 1));
					// }

					// // EVENT LISTENERS
					// window.addEventListener('load', registerOpenTab);
					// window.addEventListener('beforeunload', unregisterOpenTab);
					// </script>";

				}
				// if($is_page_refreshed ) {
				// 	echo "refresh";
				// }else{
				// 	echo "new-tab-or-browser";
				// }

				if($is_page_refreshed) {
					echo "<script>
							var reloadtime = Math.trunc(Date.now()/1000);
							localStorage.setItem('js_session_time',reloadtime);
							</script>";
					$this->db->where('order_id', $order_access['lead_id']);	
					if($usid['userid'] == $order_access['lead_assign']){
						$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>2,'no_of_extends'=>0));
					}else if ($usid['userid'] != $order_access['lead_assign']){
						
						$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>3,'no_of_extends'=>0));
					}else{					
						echo "Update error";
					}
				} else {					

					// echo "<pre>"; print_r($_SERVER['HTTP_REFERER']); exit;

					// $last_url = $_SERVER['HTTP_REFERER'];
					$last_url = $_SERVER['REDIRECT_URL'];					
					// echo "<pre>"; print_r($_SERVER); exit;
					// $split_url = parse_url($last_url);
					$split_url = explode('/', $last_url);
					// print_r($split_url);exit;
					array_pop($split_url);
					// print_r($split_url);exit;
					// $last_value = $split_url[5];
					$last_value = end($split_url);
					// echo "<pre>"; print_r($last_value); exit;
					if(in_array('view_quote', $split_url)){
					// if($last_value=="prepare_request_data" || $last_value=="edit_quote"){
						// echo "same"; exit;
						echo "<script>
							var reloadtime = Math.trunc(Date.now()/1000);
							localStorage.setItem('js_session_time',reloadtime);
							</script>";
						$this->db->where('order_id', $order_access['lead_id']);	
						if($usid['userid'] == $order_access['lead_assign']){
							$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>2,'no_of_extends'=>0));
						}else if ($usid['userid'] != $order_access['lead_assign']){
							
							$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>3,'no_of_extends'=>0));
						}else{					
							echo "Update error";
						}

					}else{
						// echo "duplicate"; exit;
					$uri=$this->uri->segment(4);
						if($uri!='drafts'){
							$_SESSION['login_errors']= array("This order already is in process on another tab.");
							$this->session->set_flashdata('login_errors', array("This order already is in process on another tab."));
						}
						redirect('welcome/quotation');
					
				}
			
			}				
			}				

		}

		$lead_agent_id = $getLeadDet[0]['agent_id'];
		$attorney_id = $getLeadDet[0]['attorney_id'];
        $adjuster_id = $getLeadDet[0]['adjuster_id'];
		$carr_mang_id = $getLeadDet[0]['carrier_manager_id'];
		if(!empty($getLeadDet)){
			$this->access_order($getLeadDet);
			
            $data['quote_data'] = $getLeadDet[0];
			if($data['quote_data']['lead_stage'] == '36' || $data['quote_data']['lead_stage'] == '37'){
				$data['close_codes'] = $this->Close_codes_model->get_closecode_bystg($data['quote_data']['lead_stage']);
				$data['close_code_id'] = $data['quote_data']['close_code_id'];
			}
            $data['view_quotation'] = true;
			$data['user_accounts'] = $this->Welcome_model->get_users();
			//$data['order_age'] = $this->get_order_age($id,$data['quote_data']['date_created'],$data['quote_data']['lead_stage'],$data['quote_data']['dateofchange']);
                        $order_age= $this->Welcome_model->get_three_tat($id);

			if(isset($order_age[0]))
				$data['order_age'] = $order_age[0];
                        
			if (!strstr((string)$data['quote_data']['log_view_status'], $this->userdata['userid']))
			{
				$log_view_status['log_view_status'] = $data['quote_data']['log_view_status'] . ':' . $this->userdata['userid'];
				$logViewStatus = $this->Welcome_model->updt_log_view_status($id, $log_view_status);
			}
            $data['log_html'] = '';
			$getLogs = $this->Welcome_model->get_logs($id);
			
			$user_order_handle = explode(',',$this->userdata['order_handle_by']);
			$user_company_code = array_map('trim', explode(',',$this->userdata['company_list']));
//var_dump($getLeadDet[0]['company_code']);
//var_dump($user_company_code[0]);
//var_dump(in_array($getLeadDet[0]['order_handle_by'],$user_order_handle));

			$is_fax_send = 'no';
            if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) || (in_array($getLeadDet[0]['order_handle_by'],$user_order_handle) && (in_array($getLeadDet[0]['company_code'],$user_company_code)) || $user_company_code[0] == 'all'))
			{
				$is_fax_send = 'yes';
			}
			$data['is_fax_send']  = $is_fax_send;

			//Get completed close codes only.
            $data['close_codes_comp'] = $this->Close_codes_model->get_closecode_comp_resReq($this->config->item('crm')['completed_stage']);
			
            if (!empty($getLogs)) {
                $log_data = $getLogs;
                $this->load->helper('Url');
                foreach ($log_data as $ld) {
					$user_data = $this->Welcome_model->get_user_data_by_id($ld['userid_fk']);
					
					if (count($user_data) < 1)
					{
						echo '<!-- ', print_r($ld, TRUE), ' -->';
						continue;
					}
                    
                    // $log_content = nl2br(auto_link(special_char_cleanup(ascii_to_entities(htmlentities(str_ireplace('<br />', "\n", $ld['log_content'])))), 'url', TRUE));
                    $log_content = nl2br(auto_link(special_char_cleanup(ascii_to_entities(str_ireplace('<br><br />', "\n", str_replace('\r\n', "<br><br\>", $ld['log_content'])))), 'url', TRUE));
					
					// $log_content = nl2br(auto_link(special_char_cleanup(ascii_to_entities(htmlentities(str_ireplace('<br />', "\n", $data_log['log_content'])))), 'url', TRUE)) . $successful;
                    
					$fancy_date = date('l, jS F y h:iA', strtotime($ld['date_created']));
					
					$stick_class = ($ld['stickie'] == 1) ? ' stickie' : '';					
					$call_disposition = '';
					$record_type = '';
					$lead_stage  = '';
					$callback_date = '';
					$print_packet_lab = NULL;
					if($ld['disposition_id'] != '')
					{
						$call_disposition_det = $this->Welcome_model->get_call_status_info($ld['disposition_id']);
						if(isset($call_disposition_det['disposition_name']))
							$call_disposition = $call_disposition_det['disposition_name'];
					}
					
					if($ld['record_type'] != '')
					{
						$record = $this->Welcome_model->get_record_type_info($ld['record_type']);
						// echo "<pre>"; print_r($record); exit;
						if($record)
							$record_type = $record['rec_type_name'];
						else
							$record_type = '';
					}
					if($ld['lead_stage'] != '')
					{
						$lead_stage_det = $this->Welcome_model->get_lead_stg_name($ld['lead_stage']);
						// echo "<pre>"; print_r($lead_stage_det); exit;
						// $lead_stage = $lead_stage_det['lead_stage_name'];
						$lead_stage = isset($lead_stage_det['lead_stage_name']) ? $lead_stage_det['lead_stage_name'] : '';
					}
					if($ld['callback_date'] != '' && $ld['callback_date'] != '0000-00-00 00:00:00')
					{
						$callback_date = date($this->config->item('date_format'),strtotime($ld['callback_date']));
					}
					if($ld['close_code_id'] != ''){
						$close_code = $this->Close_codes_model->get_close_code($ld['close_code_id']);
						$cc_desp = stripslashes($close_code[0]['description']).' ('.$close_code[0]['close_code'].')';
					}else{
						$cc_desp = '';
					}

					if($ld['print_request_packet'] == 1){
						$print_packet_lab='Print Request Packet:';
					}else if($ld['print_without_packet'] == 1){
						$print_packet_lab='Print Request Packet w/out Payment:';
					}

					$table = '
					<tr class="log'.$stick_class.'" logid="'.$ld['logid'].'" style="border:none !important;"><td class="log'.$stick_class.'">';
					
					$table .= '	
							<p class="data log'.$stick_class.'">
								<span class="log'.$stick_class.'">'.$fancy_date.'</span>
								'.$user_data[0]['first_name'].' '.$user_data[0]['last_name'].'
							</p>';
						if($cc_desp != ''){
							$table .= '<p class="desc log'.$stick_class.'"><b>Close Code : </b>'.$cc_desp.'</p>';
						}	
						if($ld['payment_id'] != '' && $log_content != '')
						{
							$table .= '<p class="desc log'.$stick_class.'">
								'.stripslashes($log_content).'
							</p>';
						}else{
							$table .= '<p class="desc log'.$stick_class.'">
								<b>'.$record_type.'</b> '.stripslashes($log_content).'
							</p>';
						}	
						if($ld['payment_id'] != '' && $record_type != ''){
							$table .= '<p class="desc log'.$stick_class.'">
								<b>Record Type :</b>'.$record_type.'</p>';
						}	
						if($call_disposition != '')
						{
							$table .= '<p class="desc log'.$stick_class.'">
								<b>Event Type Changed :</b>'.$call_disposition.'</p>';
						}	
						if($lead_stage != '')
						{
							$table .= '<p class="desc log'.$stick_class.'">
								<b>Stage Changed :</b>'.$lead_stage;
						}	
						if($this->userdata['role_id'] == 29 && in_array($this->userdata['email'],$this->cfg['developer_admin']) && strtotime($ld['date_created']) >= strtotime($this->cfg['deletion_date']))
						{
							$table .= '<span style="float:right;"><button type="button" onclick="javascript:deleteLogContent('.$ld['logid'].','.$ld['jobid_fk'].');"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span></p>';
						}else{
							$table .= '</p>';
						}
						if($callback_date != '')
						{
							$table .= '<p class="desc log'.$stick_class.'">
								<b>Call Back Date Changed :</b>'.$callback_date.'</p>';
						}
						if($print_packet_lab){
							$table .= '<p class="desc log'.$stick_class.'">
								<b>'.$print_packet_lab.'</b>Yes</p>';
						}
					$table .='</td></tr>';

                    $data['log_html'] .= $table;
					unset($table, $user_data, $user, $log_content);
                }
            }
			/**
			 * Get files associated with this job
			 **/
			$fcpath = UPLOAD_PATH; 
		    $f_dir = $fcpath . 'files/' . $id . '/'; 
			//$data['job_files_html'] = $this->Welcome_model->get_job_files($f_dir, $fcpath, $data['quote_data']);
			$data['job_files_html'] = $this->Welcome_model->get_job_files($id);
 			
			$data['query_files1_html'] = $this->Welcome_model->get_query_files_list($id);

			/**
			 * Get URLs associated with this job
			 */
			$data['job_urls_html'] = $this->Welcome_model->get_job_urls($id);
			
			//this code will be reuse for calculate the actual worth of project
			
			// $actual_worths = $this->db->query("SELECT SUM(`".$this->cfg['dbpref']."items`.`item_price`) AS `project_cost`
			// 					FROM `{$this->cfg['dbpref']}items`
			// 					WHERE `jobid_fk` = '{$id}' GROUP BY jobid_fk");
			// $data['actual_worth'] = $actual_worths->result_array();	
			// echo "<pre>"; print_r($data['actual_worth']);
			
			
			$data['attachmnet_type'] = $this->Welcome_model->get_attachment_type();
			foreach($data['attachmnet_type'] as $ky => $attval){
				// if($data['quote_data']['aps_summarization'] == 1){
					if($attval['name'] == 'Results')
					unset($data['attachmnet_type'][$ky]);
				// }else{
				// 	if($attval['name'] == 'Results Pending')
				// 	unset($data['attachmnet_type'][$ky]);
				// }
			}
// echo "<pre>";print_r($data['attachmnet_type']);exit;
			$data['lead_stat_history'] = $this->Welcome_model->get_lead_stat_history($id);
			
			$data['record_type'] = $this->Welcome_model->get_record_type();
			
			$data['call_status'] = $this->Welcome_model->get_call_status();
			$data['result_doc_count'] = $this->Welcome_model->get_record_count($id);
			//This is for without aps summarization to hide new (2)event type.
			/* if($data['quote_data']['aps_summarization'] != 1){
				$rec_sendSumm = $this->config->item('crm')['records_received_and_sent_for_summarization'];
				$summ_comp = $this->config->item('crm')['summary_complete'];
				$arr_eventTyp = array($rec_sendSumm,$summ_comp);
				foreach($data['call_status'] as $ky => $vall){
					if(in_array($vall['id'],$arr_eventTyp)){
						unset($data['call_status'][$ky]);
					}
				}
			}

			$data['call_status'] = array_values($data['call_status']); */

			$data['payment_status'] = $this->Welcome_model->get_payment_status();
			
			$data['job_cate'] = $this->Welcome_model->get_lead_services();
			
			$data['pay_records'] = $this->Welcome_model->get_payment_records_list($id);
			$arr_RefPayId=array();
			foreach($data['pay_records'] as $val){
				if($val['ref_payment_id'] != ''){
					array_push($arr_RefPayId,$val['ref_payment_id']);
				}
			}
			$data['arr_RefPayId'] = $arr_RefPayId;
			$getCpySer = $this->Welcome_model->get_active_facility_copyService($data['quote_data']['custid_fk'],$data['quote_data']['company_type']);
			$data['Cnt_copySer'] = $getCpySer;

			$data['lead_stage'] = $this->Welcome_model->get_lead_stage();
			$data['lead_stage_removed'] = $this->Welcome_model->lead_stage_removed();
			
			//$data['get_total_topup_balance'] = $this->Welcome_model->get_total_topup_balance();
			
			//$data['get_total_debit_amount'] = $this->Welcome_model->get_total_debit_amount();
			
			//$data['balance_amount'] = $data['get_total_topup_balance'] - $data['get_total_debit_amount'];

			$data['balance_amount'] = 1000000000000;
			// $data['balance_amount'] = $this->refresh_topup_balance();
			
			$getLead = $this->Welcome_model->get_lead_all_detail($id);
			$company_code = $getLead['company_code'];
			$data['associate_orders'] = $this->Welcome_model->get_associate_orders($getLead['aps_order_no'], $id,$company_code);
			// var_dump($data['associate_orders']);exit;
			$data['facility_history'] = $this->Welcome_model->get_facility_history($id,$getLead['company_type']);
			$data['active_facility'] = $this->Welcome_model->get_facility_history($id,$getLead['company_type'],1);
			// echo $lead_agent_id.' '.$attorney_id.' '.$adjuster_id; die;
			$data['agent'] = $this->Welcome_model->get_agent_details($lead_agent_id);
			$data['attorney'] = $this->Welcome_model->get_agent_details($attorney_id);
			$data['adjuster'] = $this->Welcome_model->get_agent_details($adjuster_id);

			//Carrier manager data.
			$data['carr_manag'] = $this->Welcome_model->get_carrier_manager($carr_mang_id);
			
			$data['active_copy_service'] = $this->Welcome_model->get_mapped_copy_service($getLead['custid_fk'],$getLead['company_type'],1);
			
			//Digital vendors
			$data['digital_vendors_used_list'] = array();
			if($data['quote_data']['digital_retrieval_vendor_used'] == 1){
				$company_code = $data['quote_data']['company_code'];
				$company_id = $this->Company_list_model->get_company_id_by_company_code($company_code);
				$data['digital_vendors_used_list']  = $this->Company_list_model->get_particular_approved_digital_vendor($company_id);
			}

			// echo "<pre>"; print_r($data['associate_orders']); die;
			// echo "<pre>";print_r($data);exit;

			/*new*/
			if($this->config->item('crm')['enable_time_extend_popup']==1)
			{
				$start_view =time();
				// $start_view=new Date().getTime();
				$this->session->set_userdata('Start_view',$start_view);
			}

			$this->load->view('leads/welcome_view_quote', $data);
        }
		else 
		{
            // echo "Lead does not exist or you may not be authorised to view this";
			$this->session->set_flashdata('login_errors', array("Lead does not exist or you may not be authorised to view this."));
			redirect('welcome/quotation');
        }
    }
	
    public function redactedCall($is_retention_done,$archive_done_at=0, $val){
            if($is_retention_done == '1'){
                    return "Redacted";
            }else{
                    return $val;
            }
    }
	
	 /*
     * provides the list of items
     * that belong to a given job
     * @param lead_id
     * @param itemid (latest intsert)
     * @return echo json string
     */
    function ajax_quote_items($lead_id = 0, $itemid = 0, $return = false) 
	{
		$this->load->helper('text');
		$this->load->helper('fix_text');
		
		$quote = $this->Welcome_model->get_quote_items($lead_id);

        if (!empty($quote)) {
            $html = '';
            $sale_amount = 0;
            foreach ($quote as $row) {
			
                if (is_numeric($row['item_price']) && $row['item_price'] != 0) {
                    $sale_amount += $row['item_price'];
					$row['item_price'] = '$' . number_format($row['item_price'], 2, '.', ',');
					$row['item_price'] = preg_replace('/^\$\-/', '-$', $row['item_price']);
				} else {
                    $row['item_price'] = '';
                }
				
                if ($row['hours'] > 0) {
					$row['hours'] = 'Hours : ' . $row['hours'];
				} else {
                    $row['hours'] = '';
                }
				// $content_item = nl2br(cleanup_chars(ascii_to_entities($row['item_desc'])));

				if(!empty($row['item_price'])) {
					$html .= '<li id="qi-' . $row['itemid'] . '"><table cellpadding="0" cellspacing="0" class="quote-item" width="100%"><tr><td class="item-desc" width="85%">' . nl2br(cleanup_chars(ascii_to_entities($row['item_desc']))) . '</td><td width="14%" class="item-price width100px" align="right" valign="bottom">' . $row['item_price'] . '</td></tr></table></li>';
				} else {
					$html .= '<li id="qi-' . $row['itemid'] . '"><table cellpadding="0" cellspacing="0" class="quote-item" width="100%"><tr><td class="item-desc" colspan="2">' . nl2br(cleanup_chars(ascii_to_entities($row['item_desc']))) . '</td></tr></table></li>';
				}
            }
            
            $json['sale_amount'] = '$' . number_format($sale_amount, 2, '.', ',');
            $json['gst_amount'] = ($sale_amount > 0) ? '$' . number_format($sale_amount/10, 2, '.', ',') : '$0.00';
			
            $json['total_inc_gst'] = '$' . number_format($sale_amount*1.1, 2, '.', ',');
            $json['numeric_total_inc_gst'] = $sale_amount*1.1;
			
            $json['error'] = false;
            $json['html'] = $html;
        } else {
            $json['sale_amount'] = '0.00';
            $json['gst_amount'] = '0.00';
            $json['total_inc_gst'] = '0.00';
            $json['error'] = false;
            $json['html'] = '';
        }
			$json['itemid'] = $itemid;
			
			if ($return)
			return json_encode($json);
			else
			echo json_encode($json);
    }
	
	
    /*
	 * Create a new quote
	 * Loading just the view
	 * Quotes are created with Ajax functions
	 * @access public
	 */
	public function new_quote($lead = FALSE, $customer = FALSE) 
	{
		/* additional item list */
		// $data['item_mgmt_add_list'] = $data['item_mgmt_saved_list'] = array();
		$data['categories'] = $this->Welcome_model->get_categories();
		$c = count($data['categories']);
		for ($i = 0; $i < $c; $i++) {
			$data['categories'][$i]['records'] = $this->Welcome_model->get_cat_records($data['categories'][$i]['cat_id']);
		}
		$data['lead_source'] = $this->Welcome_model->get_lead_sources();
		$data['expect_worth'] = $this->Welcome_model->get_expect_worths();
		$data['job_cate'] = $this->Welcome_model->get_lead_services();
		$data['sales_divisions'] = $this->Welcome_model->get_sales_divisions();
		$data['p_states'] = $this->Welcome_model->get_pstates();
		$data['order_lead_stage'] = $this->Welcome_model->get_order_lead_stage();
		$data['carrier_name'] = $this->Welcome_model->get_carrier_name();
	
		
		$data['attachmnet_type'] = $this->Welcome_model->get_attachment_type();
		$data['agent'] = $this->Welcome_model->get_agent_list();
		// echo "<pre>"; print_r($data['agent']); exit;
		
		$this->load->view('leads/welcome_view', $data);
	}
	
	/**
	 *  Set the quote editing interface
	 */
    function edit_quote($id = 0) 
	{
		// Edit page order lock for another user and admin starts
		$get_order_access = $this->Welcome_view_validate_model->get_order_access($id);
		$db_date = $get_order_access['access_datetime'];
		$now = date("Y-m-d H:i:s");
		$add_time= date("Y-m-d H:i:s", strtotime("{$db_date} +10 minutes"));
		if(empty($get_order_access)){
			$usid = $this->session->userdata('logged_in_user');
			$order_access = $this->Welcome_view_validate_model->get_order_detail($id);
			$ins=array();
			$ins['order_id']  = (trim($order_access['lead_id']) != '' && $order_access['lead_id'] != 'NULL' ? $order_access['lead_id'] : $id);			
			$ins['user_id'] = $usid['userid'];
			$ins['access_datetime']  = date("Y-m-d H:i:s");	
			if($usid['userid'] == $order_access['lead_assign']){
				$ins['access_check']  = 0;
			}else{
				$ins['access_check']  = 1;	
			}
	
			$this->db->insert($this->cfg['dbpref'] . 'order_access_log', $ins);		
		}		
		else if(!empty($get_order_access) && $now > $add_time ){
			$usid = $this->session->userdata('logged_in_user');			
			$order_access = $this->Welcome_view_validate_model->get_order_detail($id);
			$this->db->where('order_id', $order_access['lead_id']);	
			if($usid['userid'] == $order_access['lead_assign']){
				$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>2,'no_of_extends'=>0));
			}else if ($usid['userid'] != $order_access['lead_assign']){
				$this->db->update($this->cfg['dbpref'].'order_access_log', array('user_id' => $usid['userid'],'access_datetime'=>date("Y-m-d H:i:s"),'access_check'=>3,'no_of_extends'=>0));
			}else{					
				echo "Update error";
			}
		}
		$getLeadDet = $this->Welcome_model->get_lead_detail($id);
		$this->access_order($getLeadDet);
		// Edit page order lock for another user and admin ends
	
        if ( ($data['quote_data'] = $this->Welcome_model->get_lead_all_detail($id)) !== FALSE )
        {	
            $data['edit_quotation'] = true;

			$data['categories'] = $this->Welcome_model->get_categories();
			
			$c = count($data['categories']);

			for ($i = 0; $i < $c; $i++) {
				$data['categories'][$i]['records'] = $this->Welcome_model->get_cat_records($data['categories'][$i]['cat_id']);
			}
			
			$data['lead_source_edit'] = $this->Welcome_model->get_lead_sources();
			// echo "<pre>"; print_r($data['quote_data']); exit;
			if(isset($data['quote_data']['add1_region']))
				$regid = $data['quote_data']['add1_region'];
			else
				$regid = '';
			
			if(isset($data['quote_data']['add1_country']))
				$cntryid = $data['quote_data']['add1_country'];
			else
				$cntryid = '';
			
			if(isset($data['quote_data']['add1_state']))
				$steid = $data['quote_data']['add1_state'];
			else
				$steid = '';
			
			if(isset($data['quote_data']['add1_location']))
				$locid = $data['quote_data']['add1_location'];
			else
				$locid = '';

			if(isset($data['quote_data']['order_handle_by']))
				$handleId = $data['quote_data']['order_handle_by'];
			else
				$handleId = '';

			//for new level concept - start here
			$userdata=$this->session->userdata('logged_in_user');
			if($userdata['role_id']=='27'){
				$userList=array();
			} else {
				$reg_lvl_id = array(5,4,3);
				$cont_lvl_id = array(5,4,2);
				$ste_lvl_id = array(5,3,2);
				$loc_lvl_id = array(4,3,2);
				
				$regUserList = $this->Welcome_model->get_lvl_users('levels_region', 'region_id', $regid, $reg_lvl_id);
				$cntryUserList = $this->Welcome_model->get_lvl_users('levels_country', 'country_id', $cntryid, $cont_lvl_id);
				$steUserList = $this->Welcome_model->get_lvl_users('levels_state', 'state_id', $steid, $ste_lvl_id);
				$locUserList = $this->Welcome_model->get_lvl_users('levels_location', 'location_id', $locid, $loc_lvl_id);
				$globalUserList = $this->Welcome_model->get_lvlOne_users();

				$userList = array_merge_recursive($regUserList, $cntryUserList, $steUserList, $locUserList, $globalUserList);
				$users[] = 0;
				foreach($userList as $us)
				{
					$users[] = $us['user_id'];
				}	
				
				$userList = array_unique($users);
				$userList = array_values($userList);
			}
			// $userList = implode(',', $userList);
			$data['lead_assign_edit'] = $this->Welcome_model->get_userlist($userList);
			//for new level concept - end here
			$data['carrier_name'] = $this->Welcome_model->get_carrier_name();
			$data['expect_worth'] = $this->Welcome_model->get_expect_worths();
			$data['lead_stage'] = $this->Welcome_model->get_lead_stage();
			$data['queue_lead_stage'] = $this->Welcome_model->get_queue_lead_stage('','','','');
			$data['job_cate'] = $this->Welcome_model->get_lead_services();
			$data['sales_divisions'] = $this->Welcome_model->get_sales_divisions();
			$data['p_states'] = $this->Welcome_model->get_pstates();
			$data['agent'] = $this->Welcome_model->get_agent_list();
			$this->session->set_userdata('edit',1);
			// echo "<pre>"; print_r($this->session->userdata('edit')); exit;
            $this->load->view('leads/welcome_view', $data);
        }
        else
        {
            $this->session->set_flashdata('header_messages', array("Status Changed Successfully."));
			header('Location: ' . $_SERVER['HTTP_REFERER']);
            //redirect('welcome/quotation');
        }
        
    }
	
	/**
	 * Initiates and create the quote based on an ajax request
	 */
	function ajax_create_quote() {
	// echo '<pre>';print_r($_POST);
	// print_r($_FILES);
	// print_r($this->input->post());
	// exit;
	// print_r($_FILES['file']);
	// echo $_FILES['file']['name'];
	// exit;
		if (trim($_POST['lead_title']) == '')
        {
			echo "{error:true, errormsg:'Order Title required field!'}";
		}
        else if ( !preg_match('/^[0-9]+$/', $_POST['custid_fk']) )
        {
			echo "{error:true, errormsg:'Customer ID must be numeric!'}";
		}
        else
        {   
			$data = real_escape_array($_POST);
			$proposal_expected_date = ($data['proposal_expected_date']!='') ? strtotime($data['proposal_expected_date']) : '';
			$patient_dob = strtotime($data['patient_dob']);
			$pdob=date('Y-m-d H:i:s', $patient_dob);
                        $ewa = '';
                        $patdob = $this->sha2lib->sha2encrypt($pdob);
			// $ins['lead_title'] = addslashes($data['lead_title']);
			// $ins['lead_title_last'] = addslashes($data['lead_title_last']);
			
			
			
				$ins['lead_title'] = stripslashes($data['lead_title']);
			$ins['lead_title_last'] = stripslashes($data['lead_title_last']);
			
			
			$ins['title'] = $data['title'];
			$ins['custid_fk'] = $data['custid_fk'];
			$ins['patient_dob'] = $patdob;
			$ss_no1 = $data['ss_no'];
			$ss_no2 = $data['ss_no2'];
			$ss_no3 = $data['ss_no3'];
			
			$ssn = $ss_no1 ."-".$ss_no2."-".$ss_no3;	
			$encssn = $this->sha2lib->sha2encrypt($ssn);
			$ins['ss_no'] = $encssn;	// SSN
			
			$ins['patient_adr'] = $data['patient_adr'];
			$ins['p_city'] 		= $data['p_city'];
			$ins['p_state']		= $data['p_state'];
			// $ins['lead_service'] = $data['lead_service'];
			// $ins['lead_source'] = $data['lead_source'];
			$ins['lead_assign'] = $data['lead_assign'];
			$ins['company_code'] = $data['carrier_name'];
			if($data['expect_worth']=='not_select')
			$ins['expect_worth_id'] = 1;
			else
			$ins['expect_worth_id'] = $data['expect_worth'];
			if($data['expect_worth_amount'] == '') {
				$ewa = '0.00';
			}
			else {
			$ewa = $data['expect_worth_amount'];
			}  
			$ins['expect_worth_amount'] = $ewa; 
			$ins['belong_to'] = isset($data['job_belong_to']) ? $data['job_belong_to'] : 1;
			$ins['division'] = isset($data['job_division']) ? $data['job_division'] : 1;		
			$ins['date_created'] = date('Y-m-d H:i:s');
			$ins['date_modified'] = date('Y-m-d H:i:s');
			$ins['lead_stage'] = isset($data['lead_stage']) ? $data['lead_stage'] : 32;
			$ins['p_contact']		= $data['p_contact'];
			$ins['lead_indicator'] = $data['lead_indicator'];
			$ins['proposal_expected_date'] = date('Y-m-d H:i:s');
			$ins['created_by']  = $this->userdata['userid'];
			$ins['modified_by'] = $this->userdata['userid'];
			$ins['lead_status'] = 1;
			$ins['orderref']    = $data['orderref'];
			$ins['policy_amount']    = $data['policy_amount'];
			$ins['p_fax']    	= $data['p_fax'];
			$ins['company_type'] = $data['company_type'];
			$timeframe = '';
			if(isset($data['time_frame']))
			{
				if($data['time_frame'] == 'other')
				{
					$ins['time_frame'] = date('d-m-Y',strtotime($data['start_date'])).' '.date('d-m-Y',strtotime($data['to_date']));
					$timeframe = date('m/d/Y',strtotime($data['start_date'])).' '.date('m/d/Y',strtotime($data['to_date']));
				}
				else{
					$ins['time_frame']  = $data['time_frame'];	
					$timeframe = $data['time_frame'];	
				}
			}
			// $ins['special_instructions'] = stripslashes("Please provide the last ".$timeframe." of medical records to include, but not limited to, office notes, test results, labs, ekg's, prescriptions, etc.");
			$ins['special_instructions'] = stripslashes($data['special_instructions']);
			//$ins['lead_stage']  = $data['lead_stage'];
			$ins['order_stage_reason']  = $data['reason'];
			$ins['agent_id']  = $data['agent_id'];
			$ins['title'] = $data['title'];
			$ins['p_othername'] = addslashes($data['p_othername']);
			$ins['p_gender']  = $data['p_gender'];
			$ins['p_email']   = $data['p_email'];
			$ins['p_zipcode'] = $data['p_zipcode'];
			$ins['p_phone1']  = $data['p_phone1'];
			$ins['p_ext1'] 	  = $data['p_ext1'];
			$ins['p_phonetype1'] = $data['p_phonetype1'];
			$ins['company_type'] = $data['company_type'];
			if($data['order_handle_by']!=""){
				$ins['order_handle_by'] = $data['order_handle_by'];
			}
			if($data['is_order_assigned_permanent']!=""){
            $ins['is_order_assigned_permanent'] = $data['is_order_assigned_permanent'];
			}
			if($data['p_state'] != '')
			{
				$this->load->model('Api_model');
				$ins['timezone'] = $this->Api_model->get_state_timezone($data['p_state']);
			}
			if ($this->db->insert($this->cfg['dbpref'] . 'leads', $ins))
            {
				$insert_id = $this->db->insert_id();
				$invoice_no = (int) $insert_id;
				if($_FILES['file']){
					$this->payment_file_upload($invoice_no,$_FILES['file']);
				}
				$invoice_no = str_pad($invoice_no, 5, '0', STR_PAD_LEFT);
				$records_data=array('jobid'=>$invoice_no,'record_title'=>$data['record_title'],'record_file'=>$_FILES['file']['name'],'ori_record_file'=>$_FILES['file']['name'],'record_desc'=>'');
				$this->set_records(false,$records_data);
				$inv_no['invoice_no'] = $invoice_no;
				$inv_no['order_no']   = (int) $insert_id;
				$updt_job = $this->Welcome_model->update_row('leads', $inv_no, $insert_id);
				
				//history - lead_stage_history
				$lead_hist['lead_id'] = $insert_id;
				$lead_hist['dateofchange'] = date('Y-m-d H:i:s');
				$lead_hist['previous_status'] = 1;
				$lead_hist['changed_status'] = 1;
				$lead_hist['lead_status'] = 1;
				$lead_hist['modified_by'] = $this->userdata['userid'];
				$insert_lead_stg_his = $this->Welcome_model->insert_row('lead_stage_history', $lead_hist);
				
				//history - lead_status_history
				$lead_stat_hist['lead_id'] = $insert_id;
				$lead_stat_hist['dateofchange'] = date('Y-m-d H:i:s');
				$lead_stat_hist['changed_status'] = 1;
				$lead_stat_hist['modified_by'] = $this->userdata['userid'];
				// $this->db->insert('lead_status_history', $lead_stat_hist);
				$insert_lead_stat_his = $this->Welcome_model->insert_row('lead_status_history', $lead_stat_hist);
				
				$json['error'] = false;
                $json['fancy_insert_id'] = $invoice_no;
                $json['insert_id'] = $insert_id;
                $json['lead_title'] = htmlentities($data['lead_title'], ENT_QUOTES);
                $json['lead_service'] = $data['lead_service'];
                $json['lead_source'] = $data['lead_source'];
                $json['lead_assign'] = $data['lead_assign'];
				
				$json['expect_worth_id'] = $data['expect_worth_id'];
                $json['expect_worth_amount'] = $data['expect_worth_amount'];
				
				// $this->quote_add_item($insert_id, "\nThank you for entrusting eNoah  iSolution with your web technology requirements.\nPlease see below an itemised breakdown of our service offering to you:", 0, '', FALSE);

				//insert records for multiple doctors
				$sub_char = 'A';
				foreach($data['hide_doctors_name'] as $new_cust){
					if($new_cust!=''){
						$ins['custid_fk']     = $new_cust;
						$ins['order_no']      = (int) $insert_id;
						$ins['order_sub_no']  = $sub_char;
						// echo "<pre>"; print_r($ins); exit;
						$this->db->insert($this->cfg['dbpref'] . 'leads', $ins);
						$last_insert_id = $this->db->insert_id();

						$invoice_nos = (int) $last_insert_id;
						$invoice_nos = str_pad($invoice_nos, 5, '0', STR_PAD_LEFT);
						$inv_nos['invoice_no'] = $invoice_nos;
						$updt_job = $this->Welcome_model->update_row('leads', $inv_nos, $last_insert_id);
						
						//history - lead_stage_history
						$lead_hist['lead_id'] = $last_insert_id;
						$lead_hist['dateofchange'] = date('Y-m-d H:i:s');
						$lead_hist['previous_status'] = 1;
						$lead_hist['changed_status'] = 1;
						$lead_hist['lead_status'] = 1;
						$lead_hist['modified_by'] = $this->userdata['userid'];
						$insert_lead_stg_his = $this->Welcome_model->insert_row('lead_stage_history', $lead_hist);
						
						//history - lead_status_history
						$lead_stat_hist['lead_id'] = $last_insert_id;
						$lead_stat_hist['dateofchange'] = date('Y-m-d H:i:s');
						$lead_stat_hist['changed_status'] = 1;
						$lead_stat_hist['modified_by'] = $this->userdata['userid'];
						// $this->db->insert('lead_status_history', $lead_stat_hist);
						$insert_lead_stat_his = $this->Welcome_model->insert_row('lead_status_history', $lead_stat_hist);
						$sub_char++;
					}
				}
				
				echo json_encode($json);
				
			}
            else
            {
				//echo $this->db->last_query(); exit;
				echo "{error:true, errormsg:'Data insert failed!'}";
			}
			
			$get_det = $this->Welcome_model->get_lead_det($insert_id);
			$customer = $this->Welcome_model->get_customer_det($get_det['custid_fk']);
			
			$lead_assign_mail = $this->Welcome_model->get_user_data_by_id($get_det['lead_assign']);

			$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
		
			$from=$this->userdata['email'];
			$arrEmails = $this->config->item('crm');
			$arrSetEmails=$arrEmails['director_emails'];
			$mangement_email = $arrEmails['management_emails'];
			$mgmt_mail = implode(',',$mangement_email);
			$admin_mail=implode(',',$arrSetEmails);
			
			$param['email_data'] = array('first_name'=>$customer['first_name'],'last_name'=>$customer['last_name'],'company'=>$customer['company'],'base_url'=>$this->config->item('base_url'),'insert_id'=>$insert_id);

			$param['to_mail'] = $mgmt_mail.','. $lead_assign_mail[0]['email'];
			$param['bcc_mail'] = $admin_mail;
			$param['from_email'] = $from;
			$param['from_email_name'] = $user_name;
			$param['template_name'] = "New Order Creation Notification";
			$param['subject'] = 'New Order Creation Notification';
			
			$this->Email_template_model->sent_email($param);
		}
	}
	
	 /*
     * provides details of the customer
     * for a given id
     * @param custid
     * @return string (json formatted)
     */
	function ajax_customer_details($custid)
	{
        $this->load->model('Customer_model');
		$result = $this->Customer_model->get_customer($custid);
		if (is_array($result) && count($result) > 0)
        {
            echo json_encode($result[0]);
		}
    }
	
	/*
     * provide the list of users
     * for a region id, country id, state id, location id
     * @param regId, cntryId, stId, locId
     * @return string (json formatted)
     */
	function user_level_details($regId, $cntryId, $stId, $locId)
	{
        $this->load->model('User_model');
		$result = $this->User_model->get_userslist($regId, $cntryId, $stId, $locId);
		
		if (is_array($result) && count($result) > 0)
        {
            echo json_encode($result);
		}
    }
	
	/*
	 *Set the Expected proposal date for the lead.
	 *@lead_id
	 */
	public function set_proposal_date()
	{
		$updt_data = real_escape_array($this->input->post());
		
		$data['error'] = FALSE;
		
		$timestamp = strtotime($updt_data['date']);
		
		if ($updt_data['date_type'] != 'start')
		{
			$data['error'] = 'Invalid date status supplied!';
		}
		else if ( ! $timestamp)
		{
			$data['error'] = 'Invalid date supplied!';
		}
		else
		{
			if ($updt_data['date_type'] == 'start')
			{
				$leadDet = $this->Welcome_model->get_lead_det($updt_data['lead_id']);
				$ins['jobid_fk'] = $updt_data['lead_id'];
				$ins['userid_fk'] = $this->userdata['userid'];
				$ins['date_created'] = date('Y-m-d H:i:s');
				$ins['disposition_id'] = $leadDet['order_disposition_id'];
				$ins['lead_stage'] = $leadDet['lead_stage'];
				$ins['record_type'] = $leadDet['record_type'];
				$ins['callback_date'] = date('Y-m-d H:i:s', $timestamp);
				$ins['log_content'] = 'Changed Call Back Date '.date('Y-m-d H:i:s', $timestamp);
				$log_content = '<b>Changed Call Back Date :</b>'.date('d-m-Y', $timestamp);
				// inset the new log
				$this->db->insert($this->cfg['dbpref'] . 'logs', $ins);
				
				$updt['proposal_expected_date'] = date('Y-m-d H:i:s', $timestamp);
				$fancy_date = date('l, jS F y h:iA');
				$updt_date = $this->Welcome_model->update_row('leads', $updt, $updt_data['lead_id']);
				$data['html'] = <<<HDOC
<tr id="log" class="logstickie">
<td id="log" class="log">
<p class="data">
        <span>{$fancy_date}</span>
    {$this->userdata['first_name']} {$this->userdata['last_name']}
    </p>
    <p class="desc">
        {$log_content}
    </p>
</td>
</tr>
HDOC;
			}		
		}
		echo json_encode($data);
	}
	
	/*
	 *Change the Lead Creation for the lead.
	 *@lead_id
	 */
	public function set_lead_creation_date()
	{
		$updt_data = real_escape_array($this->input->post());
		
		$data['error'] = FALSE;
		
		$timestamp = strtotime($updt_data['date']);
		
		if ( ! $timestamp)
		{
			$data['error'] = 'Invalid date supplied!';
		}
		else
		{
			$updt['date_created'] = date('Y-m-d H:i:s', $timestamp);
			$updt_date = $this->Welcome_model->update_row('leads', $updt, $updt_data['lead_id']);
		}
		echo json_encode($data);
	}
	/*
     * adds an item to the lead based on the ajax request
     */
	function ajax_add_item()
	{
        $errors = '';
        if (trim($_POST['hours']) != '' && !is_numeric($_POST['hours']))
        {
			$errors[] = 'Hours can only be numeric values!';
		}
        if (trim($_POST['item_desc']) == '')
        {
            $errors[] = 'You must provide a description!';
        }
        if (trim($_POST['item_price']) != '' && !is_numeric($_POST['item_price']))
        {
			$errors[] = 'Price can only be numeric values!';
		}
        if (!preg_match('/^[0-9]+$/', $_POST['lead_id']))
        {
			$errors[] = 'Lead ID must be numeric!';
		}
        
        if (is_array($errors))
        {
            $json['error'] = true;
            $json['errormsg'] = implode("\n", $errors);
            echo json_encode($json);
        }
        else
        {
			if (!preg_match('/^\n/', $_POST['item_desc']))
			{
				// $_POST['item_desc'] = "\n" . $_POST['item_desc'];
			}
			$this->quote_add_item($_POST['lead_id'], $_POST['item_desc'], $_POST['item_price'], $_POST['hours']);
			
		}
		
	}
	
	/**
	 * Add an item to a quotation (job)
	 * on the system
	 * Accepts direct ajax call as well as calls from other methods
	 */
	function quote_add_item($lead_id, $item_desc = '', $item_price = 0, $hours='', $ajax = TRUE) {
		
		$ins['item_desc'] = $item_desc;
        $ins['jobid_fk'] = $lead_id;
		
		if(empty($hours)) {
			$ins['hours'] = '0.00';
		} else {
			$ins['hours'] = $hours;
		}
        if(empty($item_price)) {
			$ins['item_price']='0.00';
		} else {
			$ins['item_price'] = $item_price;
		}
        
        if (is_numeric(trim($hours)))
        {
            $ins['hours'] = $hours;
            $ins['item_price'] = $_POST['item_price'] * $hours;
        }
        
		$posn = $this->Welcome_model->get_item_position($lead_id);
        
        $ins['item_position'] = $posn[0]['item_position']+1;

		// $ins = real_escape_array($ins);
		// $ins['item_desc'] = @str_replace('\r\n', '', $ins['item_desc']);

		$insert_item = $this->Welcome_model->insert_row_return_id('items', $ins);

        if ($insert_item>0)
        {
            $itemid = $insert_item;
            
            if ($ajax == TRUE)
            {
                $this->ajax_quote_items($ins['jobid_fk'], $itemid);
            }
            else
            {
                return TRUE;
            }
        }
        else
        {
            if ($ajax == TRUE)
            {
                echo "{error:true, errormsg:'Data insert failed!'}";
            }
            else
            {
                return FALSE;
            }
        }
    }
	
	/**
	 * Edits the basic quotation details (title, services etc)
	 * via an ajax request
	 */
	function ajax_edit_quote() {
		// echo "<pre>"; print_r($this->session->userdata); exit;
		$data = real_escape_array($this->input->post());
		// echo "<pre>"; print_r($data); exit;
        if (trim($data['lead_title']) == '') {
			echo "{error:true, errormsg:'Order Title required fields!'}";
		} else if ( !preg_match('/^[0-9]+$/', trim($data['jobid_edit'])) ) {
			echo "{error:true, errormsg:'quote ID must be numeric!'}";
		} else {
		
			$patient_dob 		    = strtotime($data['patient_dob']);
			$pdob=date('Y-m-d H:i:s', $patient_dob);
                    // $ins['lead_title'] 		= addslashes($data['lead_title']);
                    // $ins['lead_title_last'] = addslashes($data['lead_title_last']);
                    $ins['lead_title'] 		= stripslashes($data['lead_title']);
                    $ins['lead_title_last'] = stripslashes($data['lead_title_last']);
			if(isset($data['job_division']))
				$division = $data['job_division'];
			else
				$division = '';
			$ins['division'] 		= $division;
			$patdob = $this->sha2lib->sha2encrypt($pdob);
			$ins['patient_dob'] 	= $patdob;
			$ss_no1 = $data['ss_no'];
			$ss_no2 = $data['ss_no2'];
			$ss_no3 = $data['ss_no3'];	
			if($ss_no1 != '' && $ss_no2 != '' && $ss_no3 != '')
			{				
				$ssn=$ss_no1 ."-".$ss_no2."-".$ss_no3;
				$encssn = $this->sha2lib->sha2encrypt($ssn);
			}
			else{
				$encssn = '';
			}
			$ins['ss_no'] = $encssn;	
			$ins['orderref']	    = $data['orderref'];
			$ins['policy_amount']	= $data['policy_amount'];
			$ins['patient_adr']		= $data['patient_adr'];
			$ins['p_city'] 			= $data['p_city'];
			$ins['p_state']			= $data['p_state'];
			if($data['p_state'] != '')
			{
				$this->load->model('Api_model');
				$ins['timezone'] = $this->Api_model->get_state_timezone($data['p_state']);
			}
			$ins['p_zipcode']		= $data['p_zipcode'];
			$ins['p_contact']		= $data['p_contact'];
			$ins['p_phone1']    	= $data['p_phone1'];
			$ins['p_fax']    		= $data['p_fax'];
			$ins['p_phonetype1']    = $data['p_phonetype1'];
			$ins['p_phone2']    	= $data['p_phone2'];
			$ins['p_phonetype2']    = $data['p_phonetype2'];
			$ins['p_phone3']    	= $data['p_phone3'];
			$ins['p_phonetype3']    = $data['p_phonetype3'];
			if(isset($data['lead_service']))
				$lead_service = $data['lead_service'];
			else
				$lead_service = '';
			$ins['lead_service'] = $lead_service;
			if(isset($data['lead_source_edit']))
				$lead_source_edit = $data['lead_source_edit'];
			else
				$lead_source_edit = '';
			$ins['lead_source']  = $lead_source_edit;
			$ins['expect_worth_id'] = isset($data['expect_worth_edit']) ? $data['expect_worth_edit'] : 0;
			$ins['expect_worth_amount'] = isset($data['expect_worth_amount']) ? $data['expect_worth_amount'] : 0;
			if (isset($data['actual_worth']) && empty($data['actual_worth'])) {
				$ins['actual_worth_amount'] = 0.00;
			}
			if(isset($data['expect_worth_amount_dup']) && ($data['actual_worth'] != $data['expect_worth_amount_dup'])) {			
				$ins['proposal_adjusted_date'] = date('Y-m-d H:i:s');
			}
			
			//$userdata = $this->session->userdata('logged_in_user');
			//if($userdata['role_id'] == 1){
                        if(isset($data['lead_assign_edit'])){
				if($data['lead_assign_edit_hidden'] == null || $data['lead_assign_edit_hidden'] == 0) {
					$ins['lead_assign'] = $data['lead_assign_edit'];
				} else {
					$ins['lead_assign'] = $data['lead_assign_edit_hidden'];
				}
                        }
			//}
			$timeframe = '';
			if(isset($data['time_frame']))
			{
				if($data['time_frame'] == 'other')
				{
					$ins['time_frame'] = date('d-m-Y',strtotime($data['start_date'])).' '.date('d-m-Y',strtotime($data['to_date']));
					$timeframe = date('m/d/Y',strtotime($data['start_date'])).' '.date('m/d/Y',strtotime($data['to_date']));
				}
				else{
					$ins['time_frame']  = $data['time_frame'];	
					$timeframe = $data['time_frame'];	
				}
			}
			//$ins['special_instructions'] = stripslashes("Please provide the last ".$timeframe." of medical records to include, but not limited to, office notes, test results, labs, ekg's, prescriptions, etc.");
			$ins['special_instructions'] = stripslashes($data['special_instructions']);
			if($data['p_state'] != '')
			{
				$this->load->model('Api_model');
				$ins['timezone'] = $this->Api_model->get_state_timezone($data['p_state']);
			}
			if(isset($data['agent_id']))
				$ins['agent_id']  = isset($data['agent_id']) ;
			$ins['title'] = $data['title'];
			if(isset($data['p_othername']))
				$p_othername = $data['p_othername'];
			else
				$p_othername = '';
			// $ins['p_othername'] = addslashes($p_othername);
			$ins['p_gender']  = $data['gender'];
			$ins['p_email']   = $data['p_email'];
			$ins['p_zipcode'] = $data['p_zipcode'];
			$ins['p_phone1']  = $data['p_phone1'];
			$ins['p_ext1'] 	  = $data['p_ext1'];
			$ins['p_phone2']  = $data['p_phone2'];
			$ins['p_ext2']    = $data['p_ext2'];
			$ins['p_phone3']  = $data['p_phone3'];
			$ins['p_ext3']    = $data['p_ext3'];
			$ins['p_othername'] = addslashes($data['other_names']);
			//$ins['company_code'] = $data['carrier_name'];
			if(isset($data['order_handle_by']) && $data['order_handle_by'] > 0)
			{
			$ins['order_handle_by'] = $data['order_handle_by'];
			}
			$ins['is_order_assigned_permanent'] = $data['is_order_assigned_permanent'];
			// for lead status history - starts here
			if(isset($data['lead_status']) && isset($data['lead_status_hidden']) && ($_POST['lead_status'] != $_POST['lead_status_hidden'])) {
				$lead_stat_hist['lead_id'] = $_POST['jobid_edit'];
				$lead_stat_hist['dateofchange'] = date('Y-m-d H:i:s');
				$lead_stat_hist['changed_status'] = $_POST['lead_status'];
				$lead_stat_hist['modified_by'] = $this->userdata['userid'];
				$insert_lead_stat_his = $this->Welcome_model->insert_row('lead_status_history', $lead_stat_hist);
			}
			// for lead status history - ends here	
			
			if($this->userdata['role_id'] != 26){
			/* lead owner starts here */
			if(isset($data['lead_owner_edit_hidden']) && ($data['lead_owner_edit_hidden'] == null || $data['lead_owner_edit_hidden'] == 0)) {
				if(isset($data['lead_owner_edit']))
					$lead_owner_edit = $data['lead_owner_edit'];
				else
					$lead_owner_edit = '';
				$ins['belong_to'] = $lead_owner_edit;
			} else {
				if(isset($data['lead_owner_edit_hidden']))
					$lead_owner_edit_hidden = $data['lead_owner_edit_hidden'];
				else
					$lead_owner_edit_hidden = '';
				$ins['belong_to'] = $lead_owner_edit_hidden;
			}

			if($ins['belong_to'] == '')
			{
				$ins['belong_to'] = $this->userdata['userid'];
			}
			}
			
			/*lead owner ends  here*/
			$ins['lead_indicator'] = isset($data['lead_indicator']) ? $data['lead_indicator'] : 'NORMAL';
			$ins['lead_status'] = 1;

			$leadDet = $this->Welcome_model->get_lead_det($data['jobid_edit']);

			// echo "<pre>"; print_r($leadDet); exit;

			if(isset($data['lead_stage']) && $data['lead_stage'] != '' && $data['lead_stage'] != 'null')
				$ins['lead_stage']  = $data['lead_stage'];
			else
				$ins['lead_stage']  = $leadDet['lead_stage'];

			$ins['lead_hold_reason'] = $data['reason']; 
			$ins['date_modified'] = date('Y-m-d H:i:s');
			$ins['modified_by'] = $this->userdata['userid'];
			//$ins['company_type'] = $data['company_type'];
			/* belong to assigned editing the lead owner */
			
			/* for onhold reason insert */	
			$inse['log_content'] = "Lead Onhold Reason: "; 
			$inse['log_content'] .= $data['reason'];
                        $inse['jobid_fk'] = $data['jobid_edit'];
                        $inse['userid_fk'] = $this->userdata['userid'];
			$inse['disposition_id'] = $leadDet['order_disposition_id'];
			$inse['lead_stage'] = $leadDet['lead_stage'];
			$inse['record_type'] = $leadDet['record_type'];
			$inse['callback_date'] = $leadDet['proposal_expected_date'];
			if($data['reason'] != '' && $data['reason'] != 'null')
			$insert_log = $this->Welcome_model->insert_row('logs', $inse);
			/* end of onhold reason insert */
		
			/* for proposal adjust date insert */
			$ins_ad['log_content'] = 'Actual Worth Amount Modified On :' . ' ' . date('M j, Y g:i A'); 
			$ins_ad['jobid_fk'] = $data['jobid_edit'];
			$ins_ad['userid_fk'] = $this->userdata['userid'];
			$ins_ad['disposition_id'] = $leadDet['order_disposition_id'];
			$ins_ad['lead_stage'] = $leadDet['lead_stage'];
			$ins_ad['record_type'] = $leadDet['record_type'];
			$ins_ad['callback_date'] = $leadDet['proposal_expected_date'];
			if(isset($data['actual_worth']) && isset($data['expect_worth_amount_dup']) && ($data['actual_worth'] != $data['expect_worth_amount_dup'])) {
				$insert_log = $this->Welcome_model->insert_row('logs', $ins_ad);
			}
			/* end proposal adjust date insert */
			$lead_id = $data['jobid_edit'];
			// echo "<pre>"; 
			// print_r($data);
			// echo "<hr>";
			// print_r($ins); exit;
			// echo "<pre>"; print_r($data); exit;
			$updt_job = $this->Welcome_model->update_row('leads', $ins, $data['jobid_edit']);
			if ($updt_job)
			{				
				if(isset($data['lead_status']))
					$lead_status = $data['lead_status'];
				else
					$lead_status = '';
				$his['lead_status'] = $lead_status; //lead_stage_history - lead_status update
				
				$updt_lead_stage_his = $this->Welcome_model->update_row('lead_stage_history', $his, $lead_id);
				
				if($data['lead_assign_edit_hidden'] ==  $data['lead_assign_edit'])
				{
					$ins['userid_fk'] = $this->userdata['userid'];
					$ins['jobid_fk'] = $lead_id;
					
					$lead_det = $this->Welcome_model->get_lead_det($lead_id); //after update.
					$lead_assign_mail = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
					$lead_owner = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
					
					$inserts['userid_fk'] = $this->userdata['userid'];
					$inserts['jobid_fk'] = $lead_id;
					$inserts['disposition_id'] = $lead_det['order_disposition_id'];
					$inserts['record_type'] = $lead_det['record_type'];
					$inserts['lead_stage'] = $lead_det['lead_stage'];
					$inserts['callback_date'] = $lead_det['proposal_expected_date'];
					$inserts['date_created'] = date('Y-m-d H:i:s');
					$inserts['log_content'] = "Order has been Re-assigned to: " . preg_replace('/\\\\/','',$lead_assign_mail[0]['first_name']) .' '.preg_replace('/\\\\/','',$lead_assign_mail[0]['last_name']) .'<br />'. 'For Order .' .word_limiter(preg_replace('/\\\\/','',$lead_det['lead_title']), 4). ' ';
					
					// inset the new log
					$insert_log = $this->Welcome_model->insert_row('logs', $inserts);
					
					$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
					$dis['date_created'] = date('Y-m-d H:i:s');
					$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));
					
					$arrEmails = $this->config->item('crm');
					$arrSetEmails=$arrEmails['director_emails'];
					$mangement_email = $arrEmails['management_emails'];
					$mgmt_mail = implode(',',$mangement_email);
					$admin_mail=implode(',',$arrSetEmails);

					//email sent by email template
					$param = array();
					
					$param['email_data'] = array('print_fancydate'=>$print_fancydate,'user_name'=>$user_name,'log_content'=>$inserts['log_content'],'signature'=>$this->userdata['signature']);

					$param['to_mail'] = $mgmt_mail.','.$lead_assign_mail[0]['email'].','.$lead_owner[0]['email'];
					$param['bcc_mail'] = $admin_mail;
					$param['from_email'] = $this->userdata['email'];
					$param['from_email_name'] = $user_name;
					$param['template_name'] = "Order Re-assignment Notification";
					$param['subject'] = 'Order Re-assigned Notification';

					// $this->Email_template_model->sent_email($param);

				} /* lead owner edit mail notifiction starts here */
				else if(($data['lead_owner_edit_hidden'] ==  $data['lead_owner_edit'])) 
				{
					$ins['userid_fk'] = $this->userdata['userid'];
					$ins['jobid_fk'] = $lead_id;
					
					$lead_det = $this->Welcome_model->get_lead_det($lead_id); //after update.
					$lead_assign_mail = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
					$lead_owner = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
					
					$inserts['userid_fk'] = $this->userdata['userid'];
					$inserts['jobid_fk'] = $lead_id;
					$inserts['disposition_id'] = $lead_det['order_disposition_id'];
					$inserts['lead_stage'] = $lead_det['lead_stage'];
					$inserts['callback_date'] = $lead_det['proposal_expected_date'];
					$inserts['record_type'] = $lead_det['record_type'];
					$inserts['date_created'] = date('Y-m-d H:i:s');
					$inserts['log_content'] = "Lead has been Re-assigned to: " . preg_replace('/\\\\/','',$lead_owner[0]['first_name']) .' '.preg_replace('/\\\\/','',$lead_owner[0]['last_name']) .'<br />'. 'For Lead ' .word_limiter(preg_replace('/\\\\/','',$lead_det['lead_title']), 4). ' ';
					// insert the new log
					$insert_log = $this->Welcome_model->insert_row('logs', $inserts);
					
					$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
					$dis['date_created'] = date('Y-m-d H:i:s');
					$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));

					$from=$this->userdata['email'];
					$arrEmails = $this->config->item('crm');
					$arrSetEmails=$arrEmails['director_emails'];
					$mangement_email = $arrEmails['management_emails'];
					$mgmt_mail = implode(',',$mangement_email);
					$admin_mail=implode(',',$arrSetEmails);
					
					//email sent by email template
					$param = array();
					
					$param['email_data'] = array('print_fancydate'=>$print_fancydate,'user_name'=>$user_name,'log_content'=>$inserts['log_content'],'signature'=>$this->userdata['signature']);

					$param['to_mail'] = $mgmt_mail.','. $lead_owner[0]['email'];
					$param['bcc_mail'] = $admin_mail;
					$param['from_email'] = $this->userdata['email'];
					$param['from_email_name'] = $user_name;
					$param['template_name'] = "Order Re-assignment Notification";
					$param['subject'] = 'Order Re-assigned Notification';

					// $this->Email_template_model->sent_email($param);
				}
				/* lead owener eidt mail notification ends here */

                $json['error'] = false;
                $json['lead_title'] = htmlentities(preg_replace('/\\\\/','',$data['lead_title']), ENT_QUOTES);

				if(isset($data['lead_service']))
					$lead_service = $data['lead_service'];
				else
					$lead_service = '';
                $json['lead_service'] = $lead_service;
				
				$this->session->set_flashdata('header_messages', array("Details Updated Successfully."));
				
				echo json_encode($json);
			}
			else
			{
				$json['error'] = true;
				$json['errormsg'] = 'Data update failed!';
				echo json_encode($json);
			}            
		}
	}
	
	/*
     * Update the quote to a given status
     * @access public
     * @param lead_id
     * @param status => desired status
     * @return echo json string
     */
    public function ajax_update_quote($lead_id = 0, $status='', $log_status = '')
    {
		$this->load->model('User_model');	
		$res = array();
        if ($lead_id != 0 && preg_match('/^[0-9]+$/', $lead_id) && preg_match('/^[0-9]+$/', $status) && $the_job = $this->Welcome_model->get_lead_all_detail($lead_id))
        {
			if($status>0) {
				//Lead Status History - Start here
				$lead_det = $this->Welcome_model->get_lead_det($lead_id);
				$lead_his['lead_id'] = $lead_id;
				$lead_his['dateofchange'] = date('Y-m-d H:i:s');
				$lead_his['previous_status'] = $lead_det['lead_stage'];
				$lead_his['changed_status'] = $status;
				$lead_his['lead_status'] = $lead_det['lead_status'];
				$lead_his['modified_by'] = $this->userdata['userid'];
				//Lead Status History - End here
				// print_r($lead_det);exit;
				//get the actual worth amt for the lead
				$actWorthAmt = $lead_det['actual_worth_amount']; 
				$update['lead_stage'] = $status;
					
				$updt_lead_stg = $this->Welcome_model->updt_lead_stg_status($lead_id, $update);
				if ($updt_lead_stg) 
				{
					$ins['userid_fk'] = $this->userdata['userid'];
					$ins['jobid_fk'] = $lead_id;
					$ins['disposition_id'] = $lead_det['order_disposition_id'];
					$ins['lead_stage'] = $lead_det['lead_stage'];
					$ins['record_type'] = $lead_det['record_type'];
					$ins['callback_date'] = $lead_det['proposal_expected_date'];
					$disarray = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
					
					$lead_owner = $this->Welcome_model->get_user_data_by_id($lead_det['belong_to']);
					// print_r($lead_owner);exit;
					
					$ins['date_created'] = date('Y-m-d H:i:s');
					
					$status_res = $this->Welcome_model->get_lead_stg_name($status);
					$lead_title = preg_replace('/\\\\/','',$lead_det['lead_title']);
					if(!empty($lead_det['lead_title_last'])) {
						$lead_title .= ' '.preg_replace('/\\\\/','',$lead_det['lead_title_last']);
					}
				
					$ins['log_content'] = "Status Changed to:" .' '. urldecode($status_res['lead_stage_name']) .' ' . 'Sucessfully for the Order - ' .$lead_title. ' ';
					
					$ins_email['log_content_email'] = "Status Changed to:" .' '. urldecode($status_res['lead_stage_name']) .' ' . 'Sucessfully for the Order - <a href='.$this->config->item('base_url').'welcome/view_quote/'.$lead_id.'>' .$lead_title. ' </a>';
					
					// insert the new log
					$insert_log = $this->Welcome_model->insert_row('logs', $ins);
					// insert the lead stage history
					$insert_lead_stage_his = $this->Welcome_model->insert_row('lead_stage_history', $lead_his);
					
					$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
					$dis['date_created'] = date('Y-m-d H:i:s');
					$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));
					

					$arrEmails = $this->config->item('crm');
					$arrSetEmails=$arrEmails['director_emails'];
					
					$admin_mail=implode(',',$arrSetEmails);
					
					//email sent by email template
					$param = array();

					$param['email_data'] = array('user_name'=>$user_name, 'print_fancydate'=>$print_fancydate, 'log_content_email'=>$ins_email['log_content_email'], 'signature'=>$this->userdata['signature']);

					$param['to_mail'] = $disarray[0]['email'] .','. $lead_owner[0]['email'];
					$param['bcc_mail'] = $admin_mail;
					$param['from_email'] = $user_data[0]['email'];
					$param['from_email_name'] = $user_name;
					$param['template_name'] = "Order - Status Change Notification";
					$param['subject'] = "Order - Status Change Notification";

					// $this->Email_template_model->sent_email($param);

					$res['error'] = false;
				}
				else 
				{
					$res['error'] = true;
					$res['errormsg'] = 'Database update failed!';
				}	
			} 
			else 
			{
				$res['error'] = false;
			}
			
        }
		else 
		{
			$res['error'] = true;
			$res['errormsg'] = 'Invalid Lead ID or Stage!';
        }
		echo json_encode($res);
		exit;
    }
	
	/**
	 * Edits an existing item on a lead 
	 */
	function ajax_edit_item() {
		
		$data = real_escape_array($this->input->post());
        $errors = '';
        if (trim($_POST['item_desc']) == '')
        {
			$errors[] = 'You must provide a description!';
        }
        if (trim($data['item_price']) != '' && !is_numeric($data['item_price']))
        {
			$errors[] = 'Price can only be numeric values!';
		}
        if (!preg_match('/^[0-9]+$/', $data['itemid']))
        {
			$errors[] = 'item ID must be numeric!';
		}
        if (is_array($errors))
        {
            $json['error'] = true;
            $json['errormsg'] = implode("\n", $errors);
            echo json_encode($json);
        }
        else
        {
			$ins['item_desc'] = $_POST['item_desc'];
			$ins['item_price'] = $data['item_price'];
			// echo "<pre>"; print_r($ins); exit;
			
			$updt_item = $this->Welcome_model->update_row_item('items', $ins, $data['itemid']);
			$res = array();
			if ($updt_item)
			{
				$res['error'] = false;
			}
			else
			{
				$res['error'] = true;
			}
			echo json_encode($res);
			exit;
        }
    }
	
	 /*
     * deletes the given item from a lead
     * @return echo json string
     */
    function ajax_delete_item() {
		
		$data = real_escape_array($this->input->post());
        $errors = '';
        if (!isset($data['itemid']) || !preg_match('/^[0-9]+$/', $data['itemid']))
        {
            $errors[] = 'A valid item ID is not supplied';
        }
        if (is_array($errors))
        {
            $json['error'] = true;
            $json['errormsg'] = implode("\n", $errors);
            echo json_encode($json);
        }
        else
        {
            $this->db->where('itemid', $data['itemid']);
            $this->db->select('jobid_fk');
            $q = $this->db->get($this->cfg['dbpref'] . 'items');
            if ($q->num_rows() > 0)
            {
                $lead_id = $q->result_array();
                $this->db->where('itemid', $data['itemid']);
                if ( $this->db->delete($this->cfg['dbpref'] . 'items') )
                {
                    $this->ajax_quote_items($lead_id[0]['jobid_fk']);
                }
                else
                {
                    $json['error'] = true;
                    $json['errormsg'] = 'Database error! Item not deleted.';
                    echo json_encode($json);
                }
            }
            else
            {
                $json['error'] = true;
                $json['errormsg'] = 'Specified item ID does not exist';
                echo json_encode($json);
            }
        }
    }
	
	 /*
     * saves the new positions items
     * for a given lead
     */
    function ajax_save_item_order() 
	{
		// $data = real_escape_array($this->input->post());
		$data = $_POST;
		
        $errors = '';
        if (!isset($data['qi']) || !is_array($data['qi']))
        {
            $errors[] = 'Incorrect order format!';
        }
        
        if (is_array($errors))
        {
            $json['error'] = true;
            $json['errormsg'] = implode("\n", $errors);
            echo json_encode($json);
        }
        else
        {
            $when = '';
            foreach ($data['qi'] as $k => $v)
            {
                $when .= "WHEN {$v} THEN {$k} \n";
            }
            $sql = "UPDATE {$this->cfg['dbpref']}items SET `item_position` = CASE `itemid`
                    {$when}
                    ELSE `item_position` END";
            
            if ($this->db->query($sql))
            {	
                $json['error'] = false;
                echo json_encode($json);
            }
            else
            {
                $json['error'] = true;
                $json['errormsg'] = 'Database error occured!';
                echo json_encode($json);
            }
        }
    }
	
	/**
	 *uploading files - creating log
	 */
	public function lead_fileupload_details($lead_id,$filename, $orifname,$userid) {
	   
		$lead_files['lead_files_name'] = $filename;
		$lead_files['lead_ori_file_name'] = $orifname;
		$lead_files['lead_files_created_by'] = $userid;
		$lead_files['lead_files_created_on'] = date('Y-m-d H:i:s');
		$lead_files['lead_id'] = $lead_id;
		$insert_logs = $this->Welcome_model->insert_row('lead_files', $lead_files);
		
		$leadDet = $this->Welcome_model->get_lead_det($lead_id);
		$logs['jobid_fk'] = $lead_id;
		$logs['userid_fk'] = $this->userdata['userid'];
		$logs['disposition_id'] = $leadDet['order_disposition_id'];
		$logs['lead_stage'] = $leadDet['lead_stage'];
		$logs['record_type'] = $leadDet['record_type'];
		$logs['callback_date'] = $leadDet['proposal_expected_date'];
		$logs['date_created'] = date('Y-m-d H:i:s');
		$logs['log_content'] = $orifname.' is added.';
		$logs['attached_docs'] = $orifname;
		$insert_logs = $this->Welcome_model->insert_row('logs', $logs);
	}
	
	/**
	 * Deletes lead from the list
	 */
	function delete_quote($id) {

		if ($this->session->userdata('del')==1) {
			if ($id > 0) {
			
				$lead_det = $this->Welcome_model->get_lead_det($id);
				$lead_assign_mail = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
				$lead_owner = $this->Welcome_model->get_user_data_by_id($lead_det['belong_to']);
				
				$delete_job = $this->Welcome_model->delete_lead('leads', $id);
				if ($delete_job) 
				{
					$delete_item = $this->Welcome_model->delete_row('items', 'jobid_fk', $id);
					$delete_log = $this->Welcome_model->delete_row('logs', 'jobid_fk', $id);
					$delete_task = $this->Welcome_model->delete_row('tasks', 'jobid_fk', $id);
					$delete_file = $this->Welcome_model->delete_row('lead_files', 'lead_id', $id);
					$delete_query = $this->Welcome_model->delete_row('lead_query', 'lead_id', $id);
					
					# Lead Delete Mail Notification
					$ins['userid_fk'] = $this->userdata['userid'];
					$ins['jobid_fk'] = $id;
					$ins['date_created'] = date('Y-m-d H:i:s');			
		
					$ins['log_content'] = 'Order Deleted Sucessfully - Order No ' .$lead_det['invoice_no']. ' ';
				    $insert_logs = $this->Welcome_model->insert_row('logs', $ins);
					$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
					$dis['date_created'] = date('Y-m-d H:i:s');
					$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));
					
					$from=$this->userdata['email'];
					$arrEmails = $this->config->item('crm');
					$arrSetEmails=$arrEmails['director_emails'];
					$mangement_email = $arrEmails['management_emails'];
					$mgmt_mail = implode(',',$mangement_email);
					$admin_mail=implode(',',$arrSetEmails);
					
					//email sent by email template
					$param = array();
					
					$param['email_data'] = array('user_name'=>$user_name, 'print_fancydate'=>$print_fancydate, 'log_content'=>$ins['log_content'], 'signature'=>$this->userdata['signature']);

					$param['to_mail'] = $mgmt_mail.','.$lead_assign_mail[0]['email'].','.$lead_owner[0]['email'];
					$param['bcc_mail'] = $admin_mail;
					$param['from_email'] = $this->userdata['email'];
					$param['from_email_name'] = $user_name;
					$param['template_name'] = "Order - Delete Notification Message";
					$param['subject'] = "Order Delete Notification";

					// $this->Email_template_model->sent_email($param);
					
					$this->session->set_flashdata('confirm', array("Order deleted from the system"));

					redirect('welcome/quotation');
				}
				else 
				{
					$this->session->set_flashdata('login_errors', array("Error in Deletion."));
					redirect('welcome/quotation');
				}
			}
			else 
			{
				$this->session->set_flashdata('login_errors', array("Quote does not exist or you may not be authorised to delete quotes."));
				redirect('welcome/quotation');
			}
		} 
		else 
		{
			$this->session->set_flashdata('login_errors', array("You have no rights to access this page"));
			redirect('welcome/quotation');
		}
		
	}
	
	//Closed lead - move to project
	public function ajax_update_lead_status($lead_id) 
	{
        if ($lead_id != 0 && preg_match('/^[0-9]+$/', $lead_id))
        {
			$update['pjt_status'] = 1;
			$update['modified_by'] = $this->userdata['userid'];
			$update['date_modified'] = date('Y-m-d H:i:s');
			
			$updt_job = $this->Welcome_model->update_row('leads', $update, $lead_id);
			$json = array();
			if ($updt_job) 
			{
				$lead_det = $this->Welcome_model->get_lead_det($lead_id);
				$ins['userid_fk'] = $this->userdata['userid'];
				$ins['jobid_fk'] = $lead_id;
				$ins['disposition_id'] = $lead_det['order_disposition_id'];
				$ins['lead_stage'] = $lead_det['lead_stage'];
				$ins['record_type'] = $lead_det['record_type'];
				$ins['callback_date'] = $lead_det['proposal_expected_date'];
				$ins['date_created'] = date('Y-m-d H:i:s');
				$ins['log_content'] = 'The Lead "'.word_limiter($lead_det['lead_title'], 4).'" is Successfully Moved to Project.';
				$ins_email['log_content_email'] = 'The Lead <a href='.$this->config->item('base_url').'project/view_project/'.$lead_id.'> ' .word_limiter($lead_det['lead_title'], 4).' </a> is Successfully Moved to Project.';

				$lead_assign_mail = $this->Welcome_model->get_user_data_by_id($lead_det['lead_assign']);
				$lead_owner = $this->Welcome_model->get_user_data_by_id($lead_det['belong_to']);
				
				// insert the new log
				$insert_log = $this->Welcome_model->insert_row('logs', $ins);
				
				$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
				$dis['date_created'] = date('Y-m-d H:i:s');
				$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));
				

				$arrEmails = $this->config->item('crm');
				$arrSetEmails=$arrEmails['director_emails'];
				
				$admin_mail=implode(',',$arrSetEmails);
			
				//email sent by email template
				$param = array();
				
				$param['email_data'] = array('user_name'=>$user_name, 'print_fancydate'=>$print_fancydate, 'log_content_email'=>$ins_email['log_content_email'], 'signature'=>$this->userdata['signature']);

				$param['to_mail'] = $lead_assign_mail[0]['email'] .','. $lead_owner[0]['email'];
				$param['bcc_mail'] = $admin_mail;
				$param['from_email'] = $this->userdata['email'];
				$param['from_email_name'] = $user_name;
				$param['template_name'] = "Lead to Project Change Notification";
				$param['subject'] = "Lead to Project Change Notification";

				// $this->Email_template_model->sent_email($param);
				
				$json['error'] = false;
				echo json_encode($json);
			}
			else
			{
				$json['error'] = true;
				$json['errormsg'] = 'Database update failed!';
				echo json_encode($json);
			}	
        }
        else
		{
			$json['error'] = true;
			$json['errormsg'] = 'Invalid Lead ID or Stage!';
			echo json_encode($json);
		}
    }
	
	function get_special_auth_tat($leadId){
		// require_once(APPPATH.'controllers/aps_retrieval.php');
		// $apsRetObj = new aps_retrieval();
		
		$result = $this->Welcome_model->get_special_auth_dates($leadId);
		$total_days = 0;
		foreach($result as $sp){
			$total_business_days = $this->get_business_days_spctat($sp['date_created'],$sp['EndDate']);
			$total_holidays = $this->get_holidays($sp['date_created'],$sp['EndDate']);
			// echo 'buss day - '.$total_business_days.' holiday day - '.$total_holidays.'<br>';
			$total_days += ($total_business_days - $total_holidays);
		}
		return $total_days;		
	}

	/*
	 *Exporting data(leads) to the excel
	 */
	public function excelExport() {
		ini_set('max_execution_time',600);
		ini_set('memory_limit', '6000M');
		log_message('error','Order DashBoard Export '.$this->session->userdata['logged_in_user']['userid'].' at '.date('Y-m-d H:i:s'));
		$stage 		  = 'null';
		$customer 	  = 'null';
		$customerph   = 'null';
		$timezone     = 'null';
		$from_date    = 'null';
		$to_date      = 'null';
		$leadassignee = 'null';
		$regionname   = 'null';
		$countryname  = 'null';
		$statename 	  = 'null';
		$locname 	  = 'null';
		$lead_status  = 'null';
		$lead_indi 	  = 'null';
		$keyword 	  = 'null';
		$event_type	  = 'null';
		$carrier_name	  = 'null';
		$queue_management_group 	  = 'null';
		$queue_management_status 	  = 'null';
		$proposal_expected_from_date  = 'null';	
		$proposal_expected_to_date    = 'null';	
		$filter_changes	  = NULL;
		$company_type	  = NULL;
		$filt = real_escape_array($this->input->post());
		$exporttoexcel =$filt;// $this->session->userdata['excel_download'];
		if (count($exporttoexcel)>0) {
			$stage 		  			= isset($exporttoexcel['stage'])? $exporttoexcel['stage']:'';
			$customer 	  			= isset($exporttoexcel['customer'])? $exporttoexcel['customer']:'';
			$customerph   			= isset($exporttoexcel['customerph'])? $exporttoexcel['customerph']:'';
			$timezone     			= isset($exporttoexcel['timezone'])? $exporttoexcel['timezone']:'';
			$from_date    			= isset($exporttoexcel['start_date'])? $exporttoexcel['start_date']:'';
			$to_date      			= isset($exporttoexcel['to_date'])? $exporttoexcel['to_date']:'';
			$leadassignee 			= isset($exporttoexcel['leadassignee'])? $exporttoexcel['leadassignee']:'';
			$regionname   			= isset($exporttoexcel['regionname'])? $exporttoexcel['regionname']:'';
			$countryname  			= isset($exporttoexcel['countryname'])? $exporttoexcel['countryname']:'';
			$statename 	  			= isset($exporttoexcel['statename'])? $exporttoexcel['statename']:'';
			$locname 	  			= isset($exporttoexcel['locname'])? $exporttoexcel['locname']:'';
			$lead_status  			= isset($exporttoexcel['lead_status'])? $exporttoexcel['lead_status']:'';
			$lead_indi 	  			= isset($exporttoexcel['lead_indi'])? $exporttoexcel['lead_indi']:'';
			$keyword 	  			= isset($exporttoexcel['keyword'])? $exporttoexcel['keyword']:'';
			$queue_management_group = isset($exporttoexcel['queue_group'])? $exporttoexcel['queue_group']:'';
			$queue_management_status= isset($exporttoexcel['queue_status'])? $exporttoexcel['queue_status']:'';
			$event_type 		  	= isset($exporttoexcel['event_type'])? $exporttoexcel['event_type']:'';
			$carrier_name 		  	= isset($exporttoexcel['carrier_name'])? $exporttoexcel['carrier_name']:'';
			$filter_changes 		= isset($exporttoexcel['filter_changes'])? $exporttoexcel['filter_changes']:'';
			$company_type 			= $exporttoexcel['company_type'];
			$order_handle_by 		= isset($exporttoexcel['order_handle_by'])?$exporttoexcel['order_handle_by']:'';
			$search_category 		= isset($exporttoexcel['search_category'])?$exporttoexcel['search_category']:'';

			$proposal_expected_from_date = (isset($exporttoexcel['proposal_expected_start_date']) && ($exporttoexcel['proposal_expected_start_date']!='')) ? date('Y-m-d',strtotime($exporttoexcel['proposal_expected_start_date'])): '';
			$proposal_expected_to_date = (isset($exporttoexcel['proposal_expected_to_date']) && ($exporttoexcel['proposal_expected_to_date']!='')) ? date('Y-m-d',strtotime($exporttoexcel['proposal_expected_to_date'])): '';
			if($queue_management_group != '' && $queue_management_group != 'null'){
				$queue_management_group = explode('_',$queue_management_group);
				if($queue_management_group[0]==$this->config->item('crm')['check_payment_requested_stage'] || $queue_management_group[0]==$this->config->item('crm')['check_payment_mailroom_stage']){
					$event_type = $queue_management_group[1];
					$queue_management_group = $queue_management_group[0];
					$filter_changes = 'Y';
				}
			}
		}
		if($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29){
			$lead_assign = null;
		}else{
			$lead_assign = $this->userdata['userid'];
		}
		
		$order_handle_by = "";

		if( isset($exporttoexcel['carrier_name']) &&  ($exporttoexcel['carrier_name'] == 'Please Choose') )
		{
			$carrier_name = '';
		}

		if(!isset($search_category))
			$search_category = '';

		$filter_res = $this->Welcome_model->get_export_filter_results($stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status, $lead_indi, $keyword, $queue_management_group, $queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$event_type,$lead_assign,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category);
		// echo $this->db->last_query();exit;
		//load our new PHPExcel library
		$this->load->library('Excel');
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
		$cacheSettings = array( ' memoryCacheSize ' => '-1');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('DashBoard');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Order No');
		$this->excel->getActiveSheet()->setCellValue('B1', 'APS Order No');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Real TAT');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Calc TAT');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Facility City');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Facility State');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Facility Zipcode');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Stop TAT');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Special Auth Case In History?');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Special Auth Required?');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Special Auth TAT');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Special Auth Route Reason');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Subordinate / Order');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Policy/ Claim Number');
		$this->excel->getActiveSheet()->setCellValue('O1', 'O/E date');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Completion date');
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Confirmed Order Close');
		$this->excel->getActiveSheet()->setCellValue('R1', 'Patient – first name');
		$this->excel->getActiveSheet()->setCellValue('S1', 'Patient – last name');
		$this->excel->getActiveSheet()->setCellValue('T1', 'Doctor/Facility/Custodian Name');
		$this->excel->getActiveSheet()->setCellValue('U1', 'Facility address');
		$this->excel->getActiveSheet()->setCellValue('V1', 'Ordering Office');
		$this->excel->getActiveSheet()->setCellValue('W1', 'Record Type');
		$this->excel->getActiveSheet()->setCellValue('X1', 'Fee – card');
		$this->excel->getActiveSheet()->setCellValue('Y1', 'Fee – ASM');
		$this->excel->getActiveSheet()->setCellValue('Z1', 'Expected Worth');
		$this->excel->getActiveSheet()->setCellValue('AA1', 'Assigned CaseWorker');
		$this->excel->getActiveSheet()->setCellValue('AB1', 'Updated On');
		$this->excel->getActiveSheet()->setCellValue('AC1', 'Updated By');
		$this->excel->getActiveSheet()->setCellValue('AD1', 'Call Back Date');
		$this->excel->getActiveSheet()->setCellValue('AE1', 'Time Zone');
		$this->excel->getActiveSheet()->setCellValue('AF1', 'Client Name');
		$this->excel->getActiveSheet()->setCellValue('AG1', 'Stage');
		$this->excel->getActiveSheet()->setCellValue('AH1', 'Last Comments');
		
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1:AH1')->getFont()->setSize(10);
		$i=2;
		
		// $this->db->select('oa.*');
		$this->db->select('oa.sa_required,oa.flag,oa.disposition_name,oa.getReq,oa.created,oa.total_days,rc.lead_id,rc.real_tat as completed_age,rc.calc_tat as order_age,rc.stop_tat as stop_time');
                $this->db->from('orderage as oa');
		$this->db->join($this->cfg['dbpref'] . 'real_calc_stop_tat as rc', 'rc.lead_id = oa.lead_id','LEFT');
		$orderage_history = $this->db->get();
		$orderage_p =  $orderage_history->result_array();
		$age_full=array();
		foreach($orderage_p as $age_array) {
			//array_push($age_full,array($age_array['lead_id'],array($age_array)));
			$age_full[$age_array['lead_id']]=array($age_array);
		}
		foreach($filter_res as $excelarr) {
			//$age = $this->get_order_age($excelarr['leadid'],$excelarr['date_created'],$excelarr['lead_stage'],$excelarr['dateofchange']);
			$age = array("order_age"=>$age_full[$excelarr['leadid']][0]['order_age'],"sa_required"=>$age_full[$excelarr['leadid']][0]['sa_required'],"completed_age"=>$age_full[$excelarr['leadid']][0]['completed_age'],"stop_time"=>$age_full[$excelarr['leadid']][0]['stop_time']);
			// This is for special auth calculation process.
			//$age_spc = $this->get_special_auth_tat($excelarr['leadid']);
			$age_spc = $age_full[$excelarr['leadid']][0]['total_days'];

			$age_spc = floor($age_spc);
			//$getReq_case = $this->Welcome_model->get_special_auth_case($excelarr['leadid']);
			$getReq_case = array("flag"=>$age_full[$excelarr['leadid']][0]['flag'],"disposition_name"=>$age_full[$excelarr['leadid']][0]['disposition_name']);
			//$getReq = $this->Welcome_model->get_special_auth_required_service($excelarr['leadid']);
			$getReq = $age_full[$excelarr['leadid']][0]['getReq'];
			// print_r($getReq_case['flag']); echo '<br>';//die;

			if($getReq >= 1){
				$isReqSpc = 'Yes';
			}else{
				$isReqSpc = 'No';
			}
			if($getReq_case['flag'] >= 1){
				$isReqSpc_case = 'Yes';
			}else{
				$isReqSpc_case = 'No';
			}

			if($age['order_age']==1){
				$ord_age='day';
			}
			else{
				$ord_age='days';
			}
			if($age_spc==1){
				$sa_age='day';
			}
			else{
				$sa_age='days';
			}

			if($age['completed_age']==1){
				$name='day';
			}
			else{
				$name='days';
			}

			if($age['stop_time']==1){
				$st_t='day';
			}
			else{
				$st_t='days';
			}
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $excelarr['order_no']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $excelarr['aps_order_no'].$excelarr['order_sub_no']);

			$this->excel->getActiveSheet()->setCellValue('C'.$i, (int)$age['completed_age'].' '.$name);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, (int)$age['order_age'].' '.$ord_age);
                        
                        if($excelarr['company_type'] == 0)
			{
				$this->excel->getActiveSheet()->setCellValue('E'.$i, preg_replace('/\\\\/','',$excelarr['add1_location']));
				$this->excel->getActiveSheet()->setCellValue('F'.$i, preg_replace('/\\\\/','',$excelarr['state_name']));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $excelarr['add1_postcode']);
			}else{
				$this->excel->getActiveSheet()->setCellValue('E'.$i, preg_replace('/\\\\/','',$excelarr['c_add1_location']));
				$this->excel->getActiveSheet()->setCellValue('F'.$i, preg_replace('/\\\\/','',$excelarr['c_state_name']));
				$this->excel->getActiveSheet()->setCellValue('G'.$i, $excelarr['c_add1_postcode']);
			}

			$this->excel->getActiveSheet()->setCellValue('H'.$i, (int)$age['stop_time'].' '.$st_t);
			$this->excel->getActiveSheet()->setCellValue('I'.$i, $isReqSpc_case);
			$this->excel->getActiveSheet()->setCellValue('J'.$i, $isReqSpc);
			if($isReqSpc_case == 'Yes' || $isReqSpc == 'Yes'){
				$this->excel->getActiveSheet()->setCellValue('K'.$i, (int)$age_spc.' '.$sa_age);
			}else{
				$this->excel->getActiveSheet()->setCellValue('K'.$i, '0 days');
			}
			if($isReqSpc == 'Yes'){
				$this->excel->getActiveSheet()->setCellValue('L'.$i, $excelarr['disposition_name']);
			}else if($isReqSpc_case == 'Yes'){
				$this->excel->getActiveSheet()->setCellValue('L'.$i, $getReq_case['disposition_name']);
			}
			else{
				$this->excel->getActiveSheet()->setCellValue('L'.$i, '');
			}
			
			$this->excel->getActiveSheet()->setCellValue('M'.$i, (isset($excelarr['order_sub_no']) ? $excelarr['order_sub_no'] : '-'));
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $excelarr['orderref']);
			$this->excel->getActiveSheet()->setCellValue('O'.$i, date('m-d-Y', strtotime($excelarr['date_created'])));
			$this->excel->getActiveSheet()->setCellValue('P'.$i, ((isset($excelarr['completion_date']) && $excelarr['completion_date'] != '0000-00-00') ? date('m-d-Y', strtotime($excelarr['completion_date'])) : ''));
			$this->excel->getActiveSheet()->setCellValue('Q'.$i, ((isset($excelarr['confirmed_close_date']) && $excelarr['confirmed_close_date'] != '0000-00-00') ? date('m-d-Y', strtotime($excelarr['confirmed_close_date'])) : ''));
			$this->excel->getActiveSheet()->setCellValue('R'.$i, preg_replace('/\\\\/','',trim($excelarr['lead_title'])));
			$this->excel->getActiveSheet()->setCellValue('S'.$i, preg_replace('/\\\\/','',$excelarr['lead_title_last']));
			if($excelarr['company_type'] == 0)
			{
				$this->excel->getActiveSheet()->setCellValue('T'.$i, preg_replace('/\\\\/','',$excelarr['first_name'].' '.$excelarr['last_name'].' '.$excelarr['company']));
				$this->excel->getActiveSheet()->setCellValue('U'.$i, preg_replace('/\\\\/','',$excelarr['add1_line1'].' '.$excelarr['add1_line2']));
			}
			else if($excelarr['company_type'] == 1){
				$this->excel->getActiveSheet()->setCellValue('T'.$i, preg_replace('/\\\\/','',$excelarr['c_first_name'].' '.$excelarr['c_last_name'].' '.$excelarr['c_company']));
				$this->excel->getActiveSheet()->setCellValue('U'.$i, preg_replace('/\\\\/','',$excelarr['c_add1_line1'].' '.$excelarr['c_add1_line2']));
			}
			if($excelarr['office_name'] != NULL && $excelarr['office_name'] != '')
			{
				$office_name =  $excelarr['office_name']; 
			}			
			else{
				$office_name =  $excelarr['orderingoffice']; 
			}
			$this->excel->getActiveSheet()->setCellValue('V'.$i, $office_name);
			$this->excel->getActiveSheet()->setCellValue('W'.$i, $excelarr['rec_type_name']);
			$this->excel->getActiveSheet()->setCellValue('X'.$i, ((isset($excelarr['payment_type']) && $excelarr['payment_type']==1) ? '$'.number_format($excelarr['paid_amount'],2) : ''));
			$this->excel->getActiveSheet()->setCellValue('Y'.$i, ((isset($excelarr['payment_type']) && $excelarr['payment_type']!=1) ? '$'.number_format($excelarr['paid_amount'],2) : ''));
			$this->excel->getActiveSheet()->setCellValue('Z'.$i, '$'.$excelarr['expect_worth_amount']);
			//$getassigne = $this->Welcome_model->get_assignedName($excelarr['leadid']);
			if($excelarr['is_order_assigned_permanent'] == 0 && $excelarr['lead_assign'] > 0){
				$this->excel->getActiveSheet()->setCellValue('AA'.$i, $excelarr['ufname'].' '.$excelarr['ulname']);
			}else{
				if($excelarr['lead_assign'] > 0){
					$this->excel->getActiveSheet()->setCellValue('AA'.$i, $excelarr['ufname'].' '.$excelarr['ulname']);
				}else{
					$this->excel->getActiveSheet()->setCellValue('AA'.$i, $excelarr['assigned_fname'].' '.$excelarr['assigned_lname']);
				}
			}
			
			$this->excel->getActiveSheet()->setCellValue('AB'.$i, date('m-d-Y', strtotime($excelarr['date_modified'])));
			$this->excel->getActiveSheet()->setCellValue('AC'.$i, $getassigne[0]['assigned_fname'].' '.$getassigne[0]['assigned_lname']);
			$this->excel->getActiveSheet()->setCellValue('AD'.$i, ((isset($excelarr['proposal_expected_date']) && $excelarr['proposal_expected_date'] != '0000-00-00 00:00:00') ? date('m-d-Y', strtotime($excelarr['proposal_expected_date'])) : ''));
			$this->excel->getActiveSheet()->setCellValue('AE'.$i, $excelarr['timezone']);
			$this->excel->getActiveSheet()->setCellValue('AF'.$i, $excelarr['carrier_name']);
			$this->excel->getActiveSheet()->setCellValue('AG'.$i, $excelarr['lead_stage_name']);
			
			if($excelarr['lead_stage_name'] == 'Completed'){
				$last_log = 'Completed';
			} else if($excelarr['lead_stage_name'] != 'Completed'){
				$last_log = $this->get_last_log($excelarr['leadid'],$excelarr['payment_id']);
			}
			$this->excel->getActiveSheet()->setCellValue('AH'.$i, preg_replace('/\\\\/','',$last_log));
			$i++;
		}
		
			//make the font become bold
			$this->excel->getActiveSheet()->getStyle('A1:AH1')->getFont()->setBold(true);
			//merge cell A1 until D1
			//$this->excel->getActiveSheet()->mergeCells('A1:D1');
			//set aligment to center for that merged cell (A1 to D1)
			
			//Set width for cells
			/*$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);			
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(30);		
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
                        $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
                        $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
			//	$this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
			
			//cell format
			// $this->excel->getActiveSheet()->getStyle('A2:A'.$i)->getNumberFormat()->setFormatCode('00000');
			$this->excel->getActiveSheet()->getStyle('A2:R'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$this->excel->getActiveSheet()->getStyle('A1:S1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$this->excel->getActiveSheet()->getStyle('L2:N'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$this->excel->getActiveSheet()->getStyle('L1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			/*$filename='Order_Dashboard.xls'   ; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel; charset=utf-8'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			//header('Cache-Control: max-age=0'); //no cache
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false);
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format*/
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="Order_Dashboard.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');  
			//force user to download the Excel file without writing it to server's HD
			header_remove('Set-Cookie');
			$objWriter->save('php://output');
	}

	/*
	 *Exporting data(leads) to the excel using xlsxwrite - saravanan.si
	 */
	public function excelExport_new() {
		ini_set('max_execution_time',1200000);
		ini_set('memory_limit', '8124M');
		log_message('info','Order DashBoard Export '.$this->session->userdata['logged_in_user']['userid'].' at '.date('Y-m-d H:i:s'));
		$stage 		  = 'null';
		$customer 	  = 'null';
		$customerph   = 'null';
		$timezone     = 'null';
		$from_date    = 'null';
		$to_date      = 'null';
		$leadassignee = 'null';
		$regionname   = 'null';
		$countryname  = 'null';
		$statename 	  = 'null';
		$locname 	  = 'null';
		$lead_status  = 'null';
		$lead_indi 	  = 'null';
		$keyword 	  = 'null';
		$event_type	  = 'null';
		$carrier_name	  = 'null';
		$queue_management_group 	  = 'null';
		$queue_management_status 	  = 'null';
		$proposal_expected_from_date  = 'null';	
		$proposal_expected_to_date    = 'null';	
		$filter_changes	  = NULL;
		$company_type	  = NULL;
		$filt = real_escape_array($this->input->post());
		$exporttoexcel =$filt;// $this->session->userdata['excel_download'];
		if (count($exporttoexcel)>0) {
			$stage 		  			= isset($exporttoexcel['stage'])? $exporttoexcel['stage']:'';
			$customer 	  			= isset($exporttoexcel['customer'])? $exporttoexcel['customer']:'';
			$customerph   			= isset($exporttoexcel['customerph'])? $exporttoexcel['customerph']:'';
			$timezone     			= isset($exporttoexcel['timezone'])? $exporttoexcel['timezone']:'';
			$from_date    			= isset($exporttoexcel['start_date'])? $exporttoexcel['start_date']:'';
			$to_date      			= isset($exporttoexcel['to_date'])? $exporttoexcel['to_date']:'';
			$leadassignee 			= isset($exporttoexcel['leadassignee'])? $exporttoexcel['leadassignee']:'';
			$regionname   			= isset($exporttoexcel['regionname'])? $exporttoexcel['regionname']:'';
			$countryname  			= isset($exporttoexcel['countryname'])? $exporttoexcel['countryname']:'';
			$statename 	  			= isset($exporttoexcel['statename'])? $exporttoexcel['statename']:'';
			$locname 	  			= isset($exporttoexcel['locname'])? $exporttoexcel['locname']:'';
			$lead_status  			= isset($exporttoexcel['lead_status'])? $exporttoexcel['lead_status']:'';
			$lead_indi 	  			= isset($exporttoexcel['lead_indi'])? $exporttoexcel['lead_indi']:'';
			$keyword 	  			= isset($exporttoexcel['keyword'])? $exporttoexcel['keyword']:'';
			$queue_management_group = isset($exporttoexcel['queue_group'])? $exporttoexcel['queue_group']:'';
			$queue_management_status= isset($exporttoexcel['queue_status'])? $exporttoexcel['queue_status']:'';
			$event_type 		  	= isset($exporttoexcel['event_type'])? $exporttoexcel['event_type']:'';
			$carrier_name 		  	= isset($exporttoexcel['carrier_name'])? $exporttoexcel['carrier_name']:'';
			$filter_changes 		= isset($exporttoexcel['filter_changes'])? $exporttoexcel['filter_changes']:'';
			$company_type 			= $exporttoexcel['company_type'];
			$order_handle_by 		= isset($exporttoexcel['order_handle_by'])?$exporttoexcel['order_handle_by']:'';
			$search_category 		= isset($exporttoexcel['search_category'])?$exporttoexcel['search_category']:'';
			$cvalue 		= isset($exporttoexcel['cvalue'])?$exporttoexcel['cvalue']:'';

			$proposal_expected_from_date = (isset($exporttoexcel['proposal_expected_start_date']) && ($exporttoexcel['proposal_expected_start_date']!='')) ? date('Y-m-d',strtotime($exporttoexcel['proposal_expected_start_date'])): '';
			$proposal_expected_to_date = (isset($exporttoexcel['proposal_expected_to_date']) && ($exporttoexcel['proposal_expected_to_date']!='')) ? date('Y-m-d',strtotime($exporttoexcel['proposal_expected_to_date'])): '';
			if($queue_management_group != '' && $queue_management_group != 'null'){
				$queue_management_group = explode('_',$queue_management_group);
				if($queue_management_group[0]==$this->config->item('crm')['check_payment_requested_stage'] || $queue_management_group[0]==$this->config->item('crm')['check_payment_mailroom_stage']){
					$event_type = $queue_management_group[1];
					$queue_management_group = $queue_management_group[0];
					$filter_changes = 'Y';
				}
			}
		}
		if($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29){
			$lead_assign = null;
		}else{
			$lead_assign = $this->userdata['userid'];
		}
		if($order_handle_by == NULL)
		{
			$order_handle_by = "";
		}
		if($exporttoexcel['carrier_name'] == 'Please Choose')
		{
			$carrier_name = '';
		}
		
		//121 $filter_res = $this->Welcome_model->get_export_filter_results($stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status, $lead_indi, $keyword, $queue_management_group, $queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$event_type,$lead_assign,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category);
		/* 121 $lstart = 0;
		 $lend = 5000;
		 $filter_resOne = 0;
		 $i=1;
		 do{
			 
		//
		if($lstart == 0){ */ //121 $this->Welcome_model->get_export_filter_resultsone
		$this->load->model('Sample_exceldownloadone_model');
			$filter_res = $this->Sample_exceldownloadone_model->get_export_filter_results($stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status, $lead_indi, $keyword, $queue_management_group, $queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$event_type,$lead_assign,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category,'');
			//echo 'lstart---'.$lstart.'---lend--'. $lend; 
			//exit;
		/*121 }else{
			//echo "--";echo $lstart+1;
			//unset($lstart);
			echo 'lstart---'.$lstart.'---lend--'. $lend; 
			//$lstart += 50000 ;
			$lstart = $lstart - 1;
		}
		echo"<br/>";
		// echo $this->db->last_query();exit;
				$lstart += $lend+1;
				 
				 $filter_resOne = $filter_res;
				 
				//$filter_resOne = $lstart; echo"<br/>";
				
				$i++;
		 }while($cvalue >= $filter_resOne); */
		
		
		//xlsx write session - saravanan.si
		$writer = new XLSXWriter();
		$styles1 = array( 'font'=>'sans-serif','font-size'=>'9px','font-style'=>'bold', 'halign'=>'left', 'height'=>20, 'widths'=>[15,15,20,20,15,15,15,20,25,25,20,50,20,25,25,25,25,20,20,40,20,20,25,20,20,20,20,20,20,20,20,20,100,100]);
		$styles2 = array( 'font'=>'sans-serif','font-size'=>'9px');
		$col_options = ['widths'=>[15,15,20,20,15,15,15,20,25,25,20,50,20,25,25,25,25,20,20,40,20,20,25,20,20,20,20,20,20,20,20,20,100,100]];
		
		$writer->writeSheetHeader('DashBoard',array('Order No'=>'string','APS Order No'=>'string','Real TAT'=>'string','Calc TAT'=>'string','Facility City'=>'string','Facility State'=>'string','Facility Zipcode'=>'string','Stop TAT'=>'string','Special Auth Case In History?'=>'string',	'Special Auth Required?'=>'string','Special Auth TAT'=>'string','Special Auth Route Reason'=>'string','Subordinate / Order'=>'string','Policy/ Claim Number'=>'string','O/E date'=>'string','Completion date'=>'string','Confirmed Order Close'=>'string','Patient - first name'=>'string','Patient - last name'=>'string','Doctor/Facility/Custodian Name'=>'string','Facility address'=>'string','Ordering Office'=>'string','Record Type'=>'string','Fee - card'=>'string','Fee - ASM'=>'string','Expected Worth'=>'string','Assigned CaseWorker'=>'string','Updated On'=>'string','Updated By'=>'string','Call Back Date'=>'string','Time Zone'=>'string','Client Name'=>'string','Stage'=>'string','Last Comments'=>'string'),$styles1);

		foreach($filter_res as $excelarr) {
                            $is_retention_done = $excelarr['is_retention_done'];
				$A =  $excelarr['leadid'];                $B =  $excelarr['aps_order_no'].''.$excelarr['order_sub_no'];
			
				$C =   (isset($excelarr['real_tat']) && $excelarr['real_tat'] !='' ? $excelarr['real_tat'].' Days' : '0 Days');
				$D =   (isset($excelarr['calc_tat']) && $excelarr['calc_tat'] !='' ? $excelarr['calc_tat'].' Days' : '0 Days');
							
				   
				$E = $excelarr['F_City']; 	$F =  $excelarr['F_State']; 	$G =  $excelarr['F_Zipcode'];
				

				$H =  (isset($excelarr['Stop_TAT']) && $excelarr['Stop_TAT'] !='' ? $excelarr['Stop_TAT'] : '0 Days');		
				
				//$I =  (isset($excelarr['Special_Auth_Case_In_History']) && $excelarr['Special_Auth_Case_In_History'] !=' ' ? $excelarr['Special_Auth_Case_In_History'] : 'No');		
				$I =  (isset($excelarr['Special_Auth_Case_In_History']) && $excelarr['Special_Auth_Case_In_History'] !='' ? $excelarr['Special_Auth_Case_In_History'] : 'NO');		
				$J =  (isset($excelarr['Special_Auth_Required']) && $excelarr['Special_Auth_Required'] !='' ? $excelarr['Special_Auth_Required'] : 'NO');
				
				$K =  (isset($excelarr['Special_Auth_TAT']) && $excelarr['Special_Auth_TAT'] !='' ? $excelarr['Special_Auth_TAT'] : '0 Days');

				$L =  $excelarr['Special_Auth_Route_Reason'];
				
				$M =  (isset($excelarr['order_sub_no']) ? $excelarr['order_sub_no'] : '-');
				$N =  $excelarr['Policy_Claim_Number'];		$O =  $excelarr['O_E_Date'];			
				$P =  ((isset($excelarr['paycompletion_date']) && $excelarr['paycompletion_date'] != '0000-00-00') ? date('m-d-Y', strtotime($excelarr['paycompletion_date'])) : '');
				$Q =  ((isset($excelarr['jconfirmed_close_date']) && $excelarr['jconfirmed_close_date'] != '0000-00-00') ? date('m-d-Y', strtotime($excelarr['jconfirmed_close_date'])) : '');
				//121 $P =  $excelarr['Completion_date'];
				
				//121 $Q =  $excelarr['Confirmed_Order_Close'];
				// $R =  $is_retention_done == 1 ? 'Redacted' : preg_replace('/\\\\/','',trim($excelarr['Patient_first_name']));
				// $S =  $is_retention_done == 1 ? 'Redacted' : preg_replace('/\\\\/','',$excelarr['Patient_last_name']);
				// $R =  preg_replace('/\\\\/','',trim($excelarr['Patient_first_name']));
				// $S =  preg_replace('/\\\\/','',$excelarr['Patient_last_name']);
				if($is_retention_done == '1' && $ctime > $test_time && $this->config->item('crm')['enable_PHI_info_hiding']==1){ $R = 'Redacted'; }else{ $R = preg_replace('/\\\\/','',trim($excelarr['Patient_first_name']));}
				if($is_retention_done == '1' && $ctime > $test_time && $this->config->item('crm')['enable_PHI_info_hiding']==1){ $S = 'Redacted'; }else{ $S = preg_replace('/\\\\/','',trim($excelarr['Patient_last_name']));}
				
				$T =  preg_replace('/\\\\/','',$excelarr['D_F_C_Name']);
				
				
				$U =  $excelarr['Facility_address']; 	
				if($excelarr['office_name'] != NULL && $excelarr['office_name'] != '')
				{
					$office_name =  $excelarr['office_name']; 
				}			
				else{
					$office_name =  $excelarr['orderingoffice']; 
				}
				$V = $office_name;
				//121 $V =  $excelarr['Ordering_Office'];	
				$W =  ($excelarr['Record_Type']);
				
				$X =  $excelarr['Fee_card']; $Y =  $excelarr['Fee_ASM']; $Z =  $excelarr['Expected_Worth'];
				
				//121 $AA =  $excelarr['Assigned_CaseWorker']; 	
				if($excelarr['is_order_assigned_permanent'] == 0 && $excelarr['lead_assign'] > 0){
					$AA = $excelarr['ufname'].' '.$excelarr['ulname'];
				}else{
					if($excelarr['lead_assign'] > 0){
						$AA = $excelarr['ufname'].' '.$excelarr['ulname'];
					}else{
						$AA = implode(' ',explode('@#$',$excelarr['assigned_fname']));
					}
				}
				$AB =  $excelarr['Updated_On']; 	$AC =  @$excelarr['Updated_By'];
				
				$AD =  ($excelarr['Call_Back_Date']);	$AE =  $excelarr['Time_Zone'];	$AF =  $excelarr['Client_Name'];

				$AG =  $excelarr['Stage'];	//121 $AH =  $excelarr['Last_Comments'];
				
				if(@$excelarr['Stage'] == 'Completed'){
					$last_log = 'Completed';
				} else if(@$excelarr['Stage'] != 'Completed'){
					$last_log = $this->get_last_log($excelarr['leadid'],$excelarr['payid']);
					//$last_log = "saaa";
				}
			$AH = preg_replace('/\\\\/','',$last_log);
			
			$writer->writeSheetRow('DashBoard', array($A, $B, $C, $D, $E, $F, $G, $H, $I, $J, $K, $L, $M, $N, $O, $P, $Q, $R, $S, $T, $U, $V, $W, $X, $Y, $Z,$AA, $AB, $AC, $AD, $AE, $AF, $AG, $AH),  $row_options = array('height'=>15,'wrap_text'=>true));
			
		}
		
		
		$path = FCPATH."uploads/"."Order_DashBoard.xlsx";
		//$path = "OrderDashBoard.xlsx";
		$writer->writeToFile($path);
		
		//121  $M1 = '#Memory Used -- '.floor((memory_get_peak_usage())/1024/1024)."MB"."\n";
		//121 log_message('error','Order DashBoard Export using XLSXWriter ( '.$M1.')  '.$this->session->userdata['logged_in_user']['userid'].' at '.date('Y-m-d H:i:s'));
		$this->downloadfile($path);
		exit;
		
	}
	function downloadfile($filename){
		if(file_exists($filename)){
			header('Content-Description: File Transfer');
			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header("Cache-Control: no-cache, no-store, must-revalidate");
			header("Pragma: no-cache");
			header('Content-Length: ' . filesize($filename));
			ob_clean();
			flush();
			readfile($filename);
			unlink($filename);
			$filextention = explode('.', $filename);
			//unlink($filextention[0].'.txt');
			exit;
			
		}
	}
	/// XLSX create End - saravanan.si 
	
	function get_last_log($leadid,$payment_id)
	{
		$logs = '';	
		if($payment_id==''){
			$this->db->order_by('date_created', 'desc');
			$query = $this->db->get_where($this->cfg['dbpref'] . 'logs', array('jobid_fk' => $leadid));
		}
		if($payment_id!=''){
			$this->db->order_by('date_created', 'desc');
			$query = $this->db->get_where($this->cfg['dbpref'] . 'logs', array('jobid_fk' => $leadid,'payment_id' => $payment_id));
		}
		$getLog  = $query->row_array();//echo $this->db->last_query(); echo'<pre>';print_r($getLog);die;
		if(!empty($getLog) && count($getLog)>0){
			$logs = stripslashes($getLog['log_content']);
			$logs = str_replace(array('\r\n', '\n', '<br />', '<br>', '<br/>'), "\r", $logs);
			$logs = str_replace(array('<b>', '</b>'), "", $logs);
			$logs = str_replace('&#8230;', '', $logs);
		}
		// echo $this->db->last_query(); exit;
		return trim($logs);
	}
	/* public function excelExport() {

		$stage 		  = 'null';
		$customer 	  = 'null';
		$customerph   = 'null';
		$timezone     = 'null';
		$from_date    = 'null';
		$to_date      = 'null';
		$leadassignee = 'null';
		$regionname   = 'null';
		$countryname  = 'null';
		$statename 	  = 'null';
		$locname 	  = 'null';
		$lead_status  = 'null';
		$lead_indi 	  = 'null';
		$keyword 	  = 'null';

		$exporttoexcel = $this->session->userdata['excel_download'];

		if (count($exporttoexcel)>0) {
			$stage 		  = $exporttoexcel['stage'];
			$customer 	  = $exporttoexcel['customer'];
			$customerph   = $exporttoexcel['customerph'];
			$timezone     = $exporttoexcel['timezone'];
			$from_date    = $exporttoexcel['from_date'];
			$to_date      = $exporttoexcel['to_date'];
			$leadassignee = $exporttoexcel['leadassignee'];
			$regionname   = $exporttoexcel['regionname'];
			$countryname  = $exporttoexcel['countryname'];
			$statename 	  = $exporttoexcel['statename'];
			$locname 	  = $exporttoexcel['locname'];
			$lead_status  = $exporttoexcel['lead_status'];
			$lead_indi 	  = $exporttoexcel['lead_indi'];
			$keyword 	  = $exporttoexcel['keyword'];
		}
		$filter_res = $this->Welcome_model->get_filter_results($stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status, $lead_indi, $keyword);

		//load our new PHPExcel library
		$this->load->library('Excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('DashBoard');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Order No.');
		$this->excel->getActiveSheet()->setCellValue('B1', 'Patient Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Doctor/Facility Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Region');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Order Owner');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Order Assigned To');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Currency Type');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Expected Worth');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Order Creation Date');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Updated On');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Updated By');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Order Stage');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Call Back Date');
		$this->excel->getActiveSheet()->setCellValue('N1', 'EIC Claim No');
		//$this->excel->getActiveSheet()->setCellValue('N1', 'Order Indicator');
		//$this->excel->getActiveSheet()->setCellValue('O1', 'Status');
		
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setSize(10);
		$i=2;
		foreach($filter_res as $excelarr) {
			if($excelarr['orderno'])
			$orderno = $excelarr['orderno'];
			else
			$orderno = $excelarr['order_no'];
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $orderno);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $excelarr['lead_title'].' '.$excelarr['lead_title_last']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $excelarr['first_name'].' '.$excelarr['last_name'].' - '.$excelarr['company']);
			$this->excel->getActiveSheet()->setCellValue('D'.$i, $excelarr['region_name']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $excelarr['ubfn'].' '.$excelarr['ubln']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $excelarr['ufname'].' '.$excelarr['ulname']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $excelarr['expect_worth_name']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $excelarr['expect_worth_amount']);
			//display only date
			$this->excel->getActiveSheet()->setCellValue('I'.$i, date('m-d-Y', strtotime($excelarr['date_created'])));
			$this->excel->getActiveSheet()->setCellValue('J'.$i, date('m-d-Y', strtotime($excelarr['date_modified'])));
			$this->excel->getActiveSheet()->setCellValue('K'.$i, $excelarr['usfname'].' '.$excelarr['usslname']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, $excelarr['lead_stage_name']);
			if($excelarr['proposal_expected_date'] != null) {
				$this->excel->getActiveSheet()->setCellValue('M'.$i, date('m-d-Y', strtotime($excelarr['proposal_expected_date'])));
			}		
			$this->excel->getActiveSheet()->setCellValue('N'.$i, $excelarr['orderref']);
			//$this->excel->getActiveSheet()->setCellValue('N'.$i, $excelarr['lead_indicator']);
					
			//$this->excel->getActiveSheet()->setCellValue('O'.$i, $status);

			$i++;
		}
		
			//make the font become bold
			$this->excel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);
			//merge cell A1 until D1
			//$this->excel->getActiveSheet()->mergeCells('A1:D1');
			//set aligment to center for that merged cell (A1 to D1)
			
			//Set width for cells
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);			
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
		//	$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
			
			//cell format
			// $this->excel->getActiveSheet()->getStyle('A2:A'.$i)->getNumberFormat()->setFormatCode('00000');
			$this->excel->getActiveSheet()->getStyle('A2:A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$filename='Order Dashboard.xls'   ; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
						 
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD
			$objWriter->save('php://output');
	} */
	
	/**
	 * Adds a log to a job
	 * based on post data
	 *
	 */
	function add_log()
	{	
	// echo "test";die;
		$data_log = real_escape_array($this->input->post());
		$data_log['log_result'] = str_replace('\n', "<br><br\>", $data_log['log_content']);
		
		// Remove the '\' slahes.
		$data_log['log_content'] = stripslashes(preg_replace('/\\\\/','',str_replace('\n', "<br><br\>", $data_log['log_content'])));
		$data_log['log_result'] = stripslashes($data_log['log_result']);
		// echo"<pre>";print_r($data_log);exit;
		$audio_file_name = $this->input->post('audio_file_name');
				// echo"<pre>";print_r($audio_file_name);die;
		$rec_type_info = $this->Welcome_model->get_record_type_info($data_log['record_type']);
		$record_type = "";
		
		if($data_log['log_content']!="") {
			$record_type = $rec_type_info['rec_type_name'];
			$data_log['log_content'] = preg_replace('/\\\\/','',$data_log['log_result']);
		}else {
			$data_log['log_content']="";
		}
		$lead_id = $data_log['lead_id'];
		$res = array();
		$json = array();

        if (isset($data_log['lead_id']) && isset($data_log['userid']) && isset($data_log['log_content']) && isset($data_log['record_type'])) {
			$this->load->helper('text');
			$this->load->helper('fix_text');
			
			$job_details = $this->Welcome_model->get_lead_det($data_log['lead_id']);

			// Get time zone for facility based & add 2 hours from current date and time.
			$facId = $job_details['custid_fk'];
			if($job_details['company_type'] == 0){
				$fac_Det = $this->Welcome_model->get_customer_det($facId);
			}else{
				$fac_Det = $this->Welcome_model->get_custodian_det($facId);
			}
			$fac_Det['timezone'];
			$current_time = date('H:i:s',strtotime('+2 hour'));

			$order_no = $job_details['order_no'];
			
				
			if(!empty($job_details['order_sub_no'])){
				$order_no .= '-'.$job_details['order_sub_no'];	
			}
			// echo"<pre>";print_r($job_details['order_sub_no']);die;
			$job_title = '';
			if(!empty($job_details['lead_title'])){
				$job_title .= preg_replace('/\\\\/','',$job_details['lead_title']);
			}
			if(!empty($job_details['lead_title_last'])){
				$job_title .= ' '.preg_replace('/\\\\/','',$job_details['lead_title_last']);
			}
			$eicclaimno = '';
			if(!empty($job_details['orderref'])){
				$eicclaimno = $job_details['orderref'];
			}
			$callback_available = $this->Welcome_model->get_callback_available($data_log['call_status']);
			if($callback_available[0]['is_callback_available']==0){
				$update['proposal_expected_date'] = '';
			}
			$update['lead_stage'] = $data_log['lead_status'];
                        
            $this->db->select('proposal_expected_date,lead_stage,confirmed_close_date');
			$this->db->from($this->cfg['dbpref'].'leads');
			$this->db->where('lead_id',$data_log['lead_id']);
			$query = $this->db->get();
			$lead_result = $query->result_array();
			// echo "<pre>";
			// echo "qqqqqqqqqq";
			// print_r($lead_result);
		
			$chck_CnfClsDt = $lead_result[0]['confirmed_close_date'];
                        
			$updt_lead_stg = $this->Welcome_model->updt_lead_stg_status($data_log['lead_id'], $update);
			//Lead Status History - Start here
			$lead_det = $this->Welcome_model->get_lead_det($data_log['lead_id']);
			$status_res = $this->Welcome_model->get_lead_stg_name($data_log['lead_status']);
			$lead_stage = $status_res['lead_stage_name'];
			
			$call_status   = $this->Welcome_model->get_call_status_info($data_log['call_status']);
			$call_disposition = $call_status['disposition_name'];
			$call_disposition_id = $call_status['id'];
			
			if($updt_lead_stg && $job_details['lead_stage'] != $data_log['lead_status'])
			{
				$lead_title = preg_replace('/\\\\/','',$job_details['lead_title']);
				if(!empty($lead_det['lead_title_last'])) {
					$lead_title .= ' '.preg_replace('/\\\\/','',$lead_det['lead_title_last']);
				}
				$lead_his['lead_id'] = $data_log['lead_id'];
				$lead_his['dateofchange'] = date('Y-m-d H:i:s');
				$lead_his['previous_status'] = $job_details['lead_stage'];
				$lead_his['changed_status'] = $data_log['lead_status'];
				$lead_his['lead_status'] = $data_log['lead_status'];
				$lead_his['modified_by'] = $this->userdata['userid'];
				
				$data_log['log_content'] = preg_replace('/\\\\/','',$data_log['log_content'])."<br/><b> Stage :</b>" .' '. urldecode($status_res['lead_stage_name']);
				// insert the lead stage history
				$insert_lead_stage_his = $this->Welcome_model->insert_row('lead_stage_history', $lead_his);
			}
			$call_status   = $this->Welcome_model->get_call_status_info($data_log['call_status']);
			$disp_status = $call_status['time_service_stop'];
			$lead_status   = $this->Welcome_model->get_lead_status_info($data_log['lead_status']);	
			// echo "lead status"."<pre>";print_r($lead_status);die;
			$lead_check_status = $lead_status['time_service_stop'];
			if($data_log['call_status'] != $job_details['order_disposition_id'])
			{
				$data_log['log_content'] = preg_replace('/\\\\/','',$data_log['log_content'])."<br/><b>Event Type : </b>".$call_status['disposition_name']."</b>";
				$upd['order_disposition_id']   = $data_log['call_status'];
			}
            if (count($job_details) > 0) 
            {
				$user_data = $this->Welcome_model->get_user_data_by_id($data_log['userid']);
				
				if($job_details['company_type'] == $this->config->item('crm')['insurance'])
				{
					$client = $this->Welcome_model->get_client_data_by_id($job_details['custid_fk']);
				}else{ 
					$client = $this->Welcome_model->get_cust_data_by_id($job_details['custid_fk']);
				}
				
                $this->load->helper('Url');
				
				$emails = trim($data_log['emailto'], ':');
				
				$successful = $received_by = '';
				
				if ($emails != '' || isset($data_log['email_to_customer']))
				{
					$emails = explode(':', $emails);
					$mail_id = array();
					foreach ($emails as $mail)
					{
						$mail_id[] = str_replace('email-log-', '', $mail);
					}

					$data['user_accounts'] = array();
					$this->db->where_in('userid', $mail_id);
					$users = $this->db->get($this->cfg['dbpref'] . 'users');
					
					if ($users->num_rows() > 0)
					{
						$data['user_accounts'] = $users->result_array();
					}
					
					
					foreach ($data['user_accounts'] as $ua)
					{
						# default email
						$to_user_email = $ua['email'];

						$send_to[] = array($to_user_email, $ua['first_name'] . ' ' . $ua['last_name'],'');
						
						$received_by .= $ua['first_name'] . ' ' . $ua['last_name'] . ', ';
					}
					$successful = '';
					
					$log_subject = "eXpert Notification - {$job_title} - Order#{$order_no} - Policy #{$eicclaimno} ";

					$log_subject = "eXpert Notification - {$job_title} - Order#{$order_no} - Policy #{$eicclaimno} ";
							


					$json['debug_info'] = '0';
					
					if (isset($data_log['email_to_customer']) && isset($data_log['additional_client_emails']))
					{
						// we're emailing the client, so remove the VCS log  prefix
						$log_subject = preg_replace('/^eXpert Notification \- /', '', $log_subject);

						$log_subject = preg_replace('/^eXpert Notification \- /', '', $log_subject);
						
						/* for ($cei = 1; $cei < 5; $cei ++)
						{
							if (isset($data_log['client_emails_' . $cei]))
							{
								$send_to[] = array($data_log['client_emails_' . $cei], '');
								$received_by .= $data_log['client_emails_' . $cei] . ', ';
							}
						}
						 */
						if (isset($data_log['additional_client_emails']) && trim($data_log['additional_client_emails']) != '')
						{
							$additional_client_emails = explode(',', trim($data_log['additional_client_emails'], ' ,'));
							if (is_array($additional_client_emails)) foreach ($additional_client_emails as $aces)
							{
								$aces = trim($aces);
								if (preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $aces))
								{
									$send_to[] = array($aces, '');
									$received_by .= $aces . ', ';
								}
							}
						}		
						$param['email_data'] = array('first_name'=>$client[0]['first_name'],'last_name'=>$client[0]['last_name'],'company' =>$client[0]['company'] ,'print_fancydate'=>$print_fancydate,'order_no'=>$order_no,'log_content'=>$data_log['log_content'],'received_by'=>$received_by,'signature'=>$this->userdata['signature']);		
					}
					else
					{
						$dis['date_created'] = date('Y-m-d H:i:s');
						$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));

						$param['email_data'] = array('first_name'=>$client[0]['first_name'],'last_name'=>$client[0]['last_name'],'company' =>$client[0]['company'],'print_fancydate'=>$print_fancydate,'order_no'=>$order_no,'log_content'=>$data_log['log_content'],'received_by'=>$received_by,'signature'=>$this->userdata['signature']);

					}

					foreach($send_to as $recps) 
					{
						$arrRecs[]=$recps[0];
					}
					$senders=implode(',',$arrRecs);
					
					$param['to_mail'] = $senders;
					$param['from_email'] = $user_data[0]['email'];
					$param['from_email_name'] = $user_data[0]['first_name'];
					$param['template_name'] = "Order Notificatiion Message";
					$param['subject'] = $log_subject;
					
					// $this->Email_template_model->sent_email($param);
					if($this->Email_template_model->sent_email($param))
					{
						$successful .= '<br/><br/>This log has been emailed to:<br />'.trim($received_by, ', ');
					}
					else
					{
						//echo 'failure';
					}
					

					if (isset($full_file_path) && is_file($full_file_path)) unlink ($full_file_path);
					
					if ($successful == 'This log has been emailed to:<br />')
					{
						$successful = '';
					}
				}
				// $callback_date = $data_log['callback_date'];
				$callback_status = "";
				/** call back date */
				if($data_log['lead_status'] != '36' && $data_log['lead_status'] != '37')
				{
					if($data_log['callback_date'] != "")
					{
						$upd['proposal_expected_date'] = date('Y-m-d 00:00:00',strtotime($data_log['callback_date']));
						$ins['callback_date'] = date('Y-m-d 00:00:00',strtotime($data_log['callback_date']));
						$callback_date = date($this->config->item('date_format'),strtotime($data_log['callback_date']));
						$callback_status = "Call Back Date Changed:";
					}
				}
				$ins['jobid_fk'] = $data_log['lead_id'];
				$ins['record_type'] = $data_log['record_type'];
				$ins['disposition_id'] = $data_log['call_status'];
				$ins['lead_stage'] = $data_log['lead_status'];
				//$ins['is_sent_to_eop'] = 3; //Added by KK reviewed

				// cancelled order - update call back date
				if($data_log['lead_status'] == '37'){
					$upd['proposal_expected_date']='0000-00-00 00:00:00';
				}

				
				
				// use this to update the view status
				$ins['userid_fk'] = $upd['log_view_status'] = $data_log['userid'];
				// $ins['payment_id'] = '';
				$upd['order_disposition_id'] = $data_log['call_status'];
				$upd['record_type'] = $data_log['record_type'];
				// $upd['special_instructions'] = $data_log['special_instructions'];
				$this->db->select('proposal_expected_date,confirmed_close_date');
				$this->db->from($this->cfg['dbpref'].'leads');
				$this->db->where('lead_id',$ins['jobid_fk']);
				$query = $this->db->get();
				$lead_result = $query->result_array();
				if(isset($lead_result['proposal_expected_date'])){
					$dt = new DateTime($lead_result['proposal_expected_date']);
					$proposal_expected_date= $dt->format('m/d/yy');
				}
				/* if($data_log['callback_date']==$proposal_expected_date){
				}else{ */
					// check if order is temporary assignment then allow to change lead_assign
					if($job_details['is_order_assigned_permanent'] == $this->config->item('crm')['order_temporary_flag']) 
					{
						$upd['lead_assign'] = 0; // make order lead assign empty for queue allocation
					}
				if($audio_file_name!="") {
					$today_date = date('Y-m-d');
					$audio_html = "<audio controls='' controlslist='nodownload'>
						  <source src='https://etouch-dev-bucket.s3.ap-south-1.amazonaws.com/uploads/".$today_date."/".$audio_file_name."' type='audio/wav'>
						</audio>";
				}else{
					$audio_html="";
				}
				// echo $audio_html;die;
						
				$ins['date_created'] = date('Y-m-d H:i:s');
			
				$ins['log_content'] = preg_replace('/\\\\/','',$data_log['log_result']).$audio_html.''. $successful;
				
				$stick_class = '';
				if (isset($data_log['log_stickie']))
				{
					$ins['stickie'] = 1;
					$stick_class = ' stickie';
				}
				
				if (isset($data_log['time_spent']))
				{
					$ins['time_spent'] = (int) $data_log['time_spent'];
				}

				$print_packet_status = $print_packet_val = '';
				if(isset($data_log['is_print_request_packet'])){
					//This is for call doc print process when choose 'yes' & click add post.
					if($data_log['is_print_request_packet'] == 1){
						$this->callDocPrintLog($ins['jobid_fk'],0);
						$print_packet_status = 'Print Request Packet w/out Payment:';
						$print_packet_val = 'Yes';
					}
					$ins['print_without_packet'] = $data_log['is_print_request_packet'];
				}else{
					$ins['print_without_packet'] = 0;
				}
				//$ins['is_sent_to_eop'] = 3; //Added by KK 24-06-2022 reviewed vanitha

				// echo "<pre>";
				// echo "---1---------";
				// print_r($ins);
				// // exit;
				// inset the new log
				$this->db->insert($this->cfg['dbpref'] . 'logs', $ins);
				$new_log_id = $this->db->insert_id();
				
				//Only completed & cancelled stage - order closed date default set.
				
				$desp_cc ='';
				if($data_log['lead_status'] == '36' || $data_log['lead_status'] == '37'){
					// echo "if";
					if($chck_CnfClsDt == '' || $chck_CnfClsDt == '0000-00-00'){
					$logs = array();
					//Log for order closed date updated process.
					$logs['jobid_fk'] = $data_log['lead_id'];
					$logs['userid_fk'] = $this->userdata['userid'];
					$logs['disposition_id'] = $data_log['call_status'];
					$logs['lead_stage'] = $data_log['lead_status'];
					$logs['record_type'] = $data_log['record_type'];
					$logs['callback_date'] = ($data_log['callback_date']!='')?date('Y-m-d 00:00:00',strtotime($data_log['callback_date'])) : '';
					$logs['date_created'] = date('Y-m-d H:i:s');
					$logs['log_content'] = 'Order Close Date - '.date('Y-m-d').' is updated.';
				
					$this->Welcome_model->insert_row('logs', $logs);

					$upd['confirmed_close_date']=date('Y-m-d');
                    }
					$upd['close_code_id'] = $data_log['close_code'];
					$ins['close_code_id'] = $data_log['close_code'];
					if($data_log['close_code_desp'] != ''){
						$desp_cc = '<p class="desc"><b>Close Code : </b>'.str_replace('\\', '"',stripslashes($data_log['close_code_desp'])).'</p>';
					}else{
						$desp_cc = NULL;
					}
				}
				
				// echo"sdsd";print_r($lead_check_status);die;
				$this->db->where('lead_id', $ins['jobid_fk']);
            	$this->db->select('*');
				$q = $this->db->get($this->cfg['dbpref'] . 'lead_stage_stop_time_service');
				$result=$q->last_row();
				
				if($result){
					$today = date('Y-m-d H:i:s');
					if($result->time_service_stop_time=='0000-00-00 00:00:00') {
						// echo 'test';
						$servicestop['time_service_stop_time'] = $today;
						$this->db->where('lead_id', $ins['jobid_fk']);
						$this->db->where('id', $result->id);
						$this->db->update($this->cfg['dbpref'] . 'lead_stage_stop_time_service', $servicestop);
					}			
					
					$disp['lead_id'] = $ins['jobid_fk'];
					$disp['time_service_start_time'] = $today;
					if($data_log['lead_status']==36) {
						$disp['time_service_stop_time'] = $today;
					}
					else {
						$disp['time_service_stop_time'] = "0000-00-00 00:00:00";
					}
					$disp['disposition_id'] = $data_log['lead_status'];
					$this->db->insert($this->cfg['dbpref'] . 'lead_stage_stop_time_service', $disp);
				}
				else{
					$today = date('Y-m-d H:i:s');
					$disp['lead_id'] = $ins['jobid_fk'];
					$disp['time_service_start_time'] = $today;
					if($data_log['lead_status']==36) {
						$disp['time_service_stop_time'] = $today;
					}
					else {
						$disp['time_service_stop_time'] = "0000-00-00 00:00:00";
					}
					
					$disp['disposition_id'] = $data_log['lead_status'];
					$this->db->insert($this->cfg['dbpref'] . 'lead_stage_stop_time_service', $disp);
				}
		
				// print_r($lead_check_status);
				// echo "<br>";
				// echo "----wwww----------";
				// die;
				if($lead_check_status==1){
					$sql = "SELECT * FROM ".$this->cfg['dbpref']."stop_tat WHERE lead_id = ".$ins['jobid_fk']. " AND lead_stage_id = ".$data_log['lead_status'];
					$query = $this->db->query($sql);

				// $this->db->last_query();
				
				if($query->num_rows()==0) {
					$today = date('Y-m-d H:i:s');
					$leads['lead_id'] = $ins['jobid_fk'];
					$leads['lead_stage_id'] = $data_log['lead_status'];
					$leads['lead_start_time'] = $today;
					$leads['lead_stop_time'] = '0000-00-00 00:00:00';
					
					$this->db->insert($this->cfg['dbpref'] . 'stop_tat', $leads);
					$this->db->last_query();
					
				}
				else {
					
					$stoptime['lead_stop_time'] = date('Y-m-d H:i:s');
					$this->db->where('lead_id', $ins['jobid_fk']);
					$this->db->where('lead_stage_id', $data_log['lead_status']);
					$update = $this->db->update($this->cfg['dbpref'] . 'stop_tat', $stoptime);
					$this->db->last_query();
				}

			}
				// update the leads table
				if(date('Y-m-d',strtotime($upd['proposal_expected_date'])) == date('Y-m-d')){
					$upd['flag_queue_restrict'] = 1;
					$upd['proposal_expected_date'] = str_replace('00:00:00',$current_time,$upd['proposal_expected_date']);
				}else{
					$upd['flag_queue_restrict'] = 0;
				}
				
				$this->db->where('lead_id', $ins['jobid_fk']);
				$this->db->update($this->cfg['dbpref'] . 'leads', $upd);
				
                $log_content = nl2br(auto_link(special_char_cleanup(ascii_to_entities(str_ireplace('<br><br />', "\n", preg_replace('/\\\\/','',$data_log['log_result'])))), 'url', TRUE)) . $successful;
                $lead_det = $this->Welcome_model->get_lead_det($data_log['lead_id']);
				if($lead_det['aps_order_no'] != '')
				{
					// include_once(APPPATH.'controllers/web_service_v2/post_trigger_api.php');
					// $post_trigger_api = new post_trigger_api();
					
					$order_id = $lead_det['lead_id'];
					$trigger['eXpertLogId'] 	 	=  $new_log_id;
					$trigger['eNaohOrderId'] 	 	=  $lead_det['aps_order_no'].$lead_det['order_sub_no'];
					$trigger['companyCode'] 	 	=  $lead_det['company_code'];
					$trigger['companyType'] 	 	=  $lead_det['company_type'];
					$trigger['status'] 				=  $status_res['lead_stage_name'];
					$trigger['eventType']			=  $call_disposition;
					$trigger['eventTypeId ']		=  $call_disposition_id;
					//$trigger['comments']  			=  $record_type.' : '.$call_disposition.' : '.$status_res['lead_stage_name'];
					$trigger['comments']  			=  '';
					//if(isset($data_log['is_ai_comment']) && $data_log['is_ai_comment'] == 1) // only send manual comment
					//{
						$trigger['comments']  .= str_replace(array('<br/>', '<br>'), ' ', $data_log['log_result']);
						// $trigger['comments']  	   .= ' - '.htmlentities($data_log['log_result']);
					//}
					// $result_post_api = $post_trigger_api->addpost($trigger,$order_id);
					$trigger['created_at']			= $ins['date_created'];
					//if($data_log['lead_status']==36 || $data_log['lead_status']==37) {
					if($lead_det['confirmed_close_date'] == '0000-00-00')
					{		
						$trigger['orderClosedDate']		= '';
					}else{
						$trigger['orderClosedDate']		= $lead_det['confirmed_close_date'];
					}
					//}
					if($this->cfg['enable_decoupling_comments'] == 0)
					{
						$result_post_api = $this->post_trigger_api->addpost($trigger,$order_id);
						if(is_object(json_decode($result_post_api)))
						{
							$result = json_decode($result_post_api);
							$trigger_result = "Order #".$lead_det['aps_order_no'].$lead_det['order_sub_no'].' '.$result->statusMessage;
							if(isset($result->statusCode) && $result->statusCode == 100)
							{
								# update is_sent_to_eop flag
								$this->db->where('logid', $new_log_id);
								$update = $this->db->update($this->cfg['dbpref'] . 'logs', array('is_sent_to_eop' => 1));
							}else{
								# update failure is_sent_to_eop flag
								$this->db->where('logid', $new_log_id);
								$update = $this->db->update($this->cfg['dbpref'] . 'logs', array('is_sent_to_eop' => 2));
							}	
						}
						else{
							$trigger_result =  "Order #".$lead_det['aps_order_no'].$lead_det['order_sub_no'].' '.$result_post_api;
							# update failure is_sent_to_eop flag
							$this->db->where('log_id', $new_log_id);
							$update = $this->db->update($this->cfg['dbpref'] . 'logs', array('is_sent_to_eop' => 2));
						}
					}else{
                                            if($job_details['is_retention_done']!=1){
                                                $this->load->model('Api_decoupling_model');
                                                $this->Api_decoupling_model->insert_comment($trigger,$ins['jobid_fk']);
                                            }
					} 
				} 
				
				$fancy_date = date('l, jS F y h:iA', strtotime($ins['date_created']));
				
				// echo"<pre>";print_r($fancy_date);die;
	//show 2 log - only completed & cancelled stage.
	if($data_log['lead_status'] == '36' || $data_log['lead_status'] == '37'){
		if($chck_CnfClsDt == '' || $chck_CnfClsDt == '0000-00-00'){
			if($this->userdata['role_id'] == 29 && in_array($this->userdata['email'],$this->cfg['developer_admin']))
			{
				$table = <<<HDOC
<tr id="log" class="log{$stick_class}" logid="{$new_log_id}">	
	<td id="log" class="log">
	<p class="data">
		<span>{$fancy_date}</span>
		{$user_data[0]['first_name']} {$user_data[0]['last_name']}
	</p>
	{$desp_cc}
	<p class="desc">
		<b>{$record_type} :</b> {$logs['log_content']}
	</p>
	<p class="desc">
		<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
		<b>Stage Changed :</b>{$lead_stage}
		<span style="float:right;"><button  type="button"  onclick="javascript:deleteLogContent({$new_log_id},{$lead_id});"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span>
	</p>
	</td>
</tr>

<tr id="log" class="log{$stick_class}" logid="{$new_log_id}">	
<td id="log" class="log">
<p class="data">
		<span>{$fancy_date}</span>
	{$user_data[0]['first_name']} {$user_data[0]['last_name']}
	</p>
	{$desp_cc}
	<p class="desc">
	<b>{$record_type} :</b> {$log_content}
	</p>
	<p class="desc">
	{$audio_html}
	</p>
	<p class="desc">
	<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}<span style="float:right;"><button  type="button" onclick="javascript:deleteLogContent({$new_log_id},{$lead_id});"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span>
	</p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	</p>
	<p class="desc">
	<b>{$print_packet_status}</b>{$print_packet_val}
	</p>
</td>
</tr>
HDOC;
			}else{
				$table = <<<HDOC
<tr id="log" class="log{$stick_class}" >
	<td id="log" class="log">
	<p class="data">
		<span>{$fancy_date}</span>
		{$user_data[0]['first_name']} {$user_data[0]['last_name']}
	</p>
	<p class="desc">
		<b>{$record_type} :</b> {$logs['log_content']}
	</p>
	<p class="desc">
		<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
		<b>Stage Changed :</b>{$lead_stage}
	</p>
	</td>
</tr>

<tr id="log" class="log{$stick_class}">
<td id="log" class="log">
<p class="data">
		<span>{$fancy_date}</span>
	{$user_data[0]['first_name']} {$user_data[0]['last_name']}
	</p>
	{$desp_cc}
	<p class="desc">
	<b>{$record_type} :</b> {$log_content}
	</p>
	<p class="desc">
	{$audio_html}
	</p>
	<p class="desc">
	<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}
	</p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	</p>
	<p class="desc">
	<b>{$print_packet_status}</b>{$print_packet_val}
	</p>
</td>
</tr>
HDOC;
			}
			
		}else{
			if($this->userdata['role_id'] == 29 && in_array($this->userdata['email'],$this->cfg['developer_admin']))
			{
				$table = <<<HDOC
<tr id="log" class="log{$stick_class}" logid="{$new_log_id}">
<td id="log" class="log">
<p class="data">
        <span>{$fancy_date}</span>
    {$user_data[0]['first_name']} {$user_data[0]['last_name']}
    </p>
	{$desp_cc}
    <p class="desc">
	<b>{$record_type} :</b> {$log_content}
    </p>
	<p class="desc">
    {$audio_html}
    </p>
	<p class="desc">
	<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}<span style="float:right;"><button  type="button" onclick="javascript:deleteLogContent({$new_log_id},{$lead_id});"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span>
	</p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	</p>
	<p class="desc">
	<b>{$print_packet_status}</b>{$print_packet_val}
	</p>
</td>
</tr>
HDOC;
			}
			else{
			$table = <<<HDOC
<tr id="log" class="log{$stick_class}">
<td id="log" class="log">
<p class="data">
        <span>{$fancy_date}</span>
    {$user_data[0]['first_name']} {$user_data[0]['last_name']}
    </p>
	{$desp_cc}
    <p class="desc">
	<b>{$record_type} :</b> {$log_content}
    </p>
	<p class="desc">
    {$audio_html}
    </p>
	<p class="desc">
	<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}
	</p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	</p>
	<p class="desc">
	<b>{$print_packet_status}</b>{$print_packet_val}
	</p>
</td>
</tr>
HDOC;
			}
		}
	}else{
		if($this->userdata['role_id'] == 29 && in_array($this->userdata['email'],$this->cfg['developer_admin']))
		{
			$table = <<<HDOC
<tr id="log" class="log{$stick_class}" logid="{$new_log_id}">
<td id="log" class="log">
<p class="data">
        <span>{$fancy_date}</span>
    {$user_data[0]['first_name']} {$user_data[0]['last_name']}
    </p>
	{$desp_cc}
    <p class="desc">
	<b>{$record_type} :</b> {$log_content}
    </p>
	<p class="desc">

    {$audio_html}
    </p>
	<p class="desc">

	<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}<span style="float:right;"><button  type="button" onclick="javascript:deleteLogContent({$new_log_id},{$lead_id});"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span>
	</p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	</p>
	<p class="desc">
	<b>{$print_packet_status}</b>{$print_packet_val}
	</p>
</td>
</tr>
HDOC;
		}
		else{
$table = <<<HDOC
<tr id="log" class="log{$stick_class}">
<td id="log" class="log">
<p class="data">
		<span>{$fancy_date}</span>
	{$user_data[0]['first_name']} {$user_data[0]['last_name']}
	</p>
	<p class="desc">
	<b>{$record_type} :</b> {$log_content}
	</p>
	<p class="desc">

	{$audio_html}
	</p>
	<p class="desc">

	<b>Event Type Changed :</b>{$call_disposition}
	</p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}
	</p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	</p>
	<p class="desc">
	<b>{$print_packet_status}</b>{$print_packet_val}
	</p>
</td>
</tr>
HDOC;
	}
	}
// $table = <<<HDOC
// <tr id="log" class="log{$stick_class}">
// <td id="log" class="log">
// <p class="data">
//         <span>{$fancy_date}</span>
//     {$user_data[0]['first_name']} {$user_data[0]['last_name']}
//     </p>
//     <p class="desc">
// 	<b>{$record_type} :</b> {$log_content}
//     </p>
// 	<p class="desc">
				
//     {$audio_html}
//     </p>
// 	<p class="desc">

// 	<b>Event Type Changed :</b>{$call_disposition}
// 	<p>
// 	<p class="desc">
// 	<b>Stage Changed :</b>{$lead_stage}
// 	<p>
// 	<p class="desc">
// 	<b>{$callback_status}</b>{$callback_date}
// 	<p>
// </td>
// </tr>
// HDOC;
				
                $json['error'] = FALSE;
                $json['html'] = $table;			
				$json['role_id'] = $this->userdata['role_id'];	
				$json['trigger_message'] = $trigger_result;			
				
                echo json_encode($json);
				exit;
            }
            else
            {
				$res['error'] = true;
				$res['errormsg'] = 'Post insert failed';
            }
        }
        else
        {
            // echo "{error:true, errormsg:'Invalid data supplied'}";
			$res['error'] = true;
			$res['errormsg'] = 'Invalid data supplied';
			
        }
		exit;
    }
	
	
	function service_stop_time() {
		$lead_id = $this->input->post('lead_id');
		$lead_stage_id = $this->input->post('lead_stage');
		
		$lead_status   = $this->Welcome_model->get_lead_status_info($lead_stage_id);
		// print_r($lead_status);die;
		$disp_status = $lead_status['time_service_stop'];
		// print_r($disp_status);die;
		if($disp_status==1){
					
			$this->db->select('*');
			$this->db->from($this->cfg['dbpref'].'lead_stage_stop_time_service');
			$this->db->where("lead_id",$lead_id);		
			$this->db->where("disposition_id",$lead_stage_id);	
			$query = $this->db->get(); 
			$this->db->last_query();
			
			if($query->num_rows()!=0) {
				
				$servicestop['time_service_stop_time'] = date('Y-m-d H:i:s');
				
				$this->db->where('lead_id', $lead_id);
				$this->db->where('disposition_id', $lead_stage_id);
				$this->db->update($this->cfg['dbpref'] . 'lead_stage_stop_time_service', $servicestop);
				
				
			}
			
			
		}
		
	}

	function request()
	{
		$data['results'] = array();
		if (isset($_POST['keyword']) && trim($_POST['keyword']) != '' && ($_POST['keyword'] != 'Order No, Order Title, Doctor or Facility Name'))
		{	
			$keyword = mysqli_real_escape_string(get_instance()->db->conn_id,$_POST['keyword']);
			
					
			$sql = "SELECT * FROM '".$this->cfg['dbpref']."'customers,'".$this->cfg['dbpref']."'leads a WHERE `custid_fk` = `custid` AND lead_stage IN (1,4,2,5,3,7,6,9,10,11,12,13) AND ( `lead_title` LIKE '%{$keyword}%' OR `invoice_no` LIKE '%{$keyword}%' OR `custid_fk` IN ( SELECT `custid` FROM '".$this->cfg['dbpref']."'customers WHERE CONCAT_WS(' ', `first_name`, `last_name`) LIKE '%{$keyword}%' OR `first_name` LIKE '%{$keyword}%' OR `last_name` LIKE '%{$keyword}%' OR `company` LIKE '%{$keyword}%' ) ) ORDER BY `lead_stage`, `lead_title`";
			
			$resul = $this->Welcome_model->search_res($keyword);
					
			$q = $this->db->query($sql);
			//echo $this->db->last_query();
			if ($q->num_rows() > 0)
			{				
				$result = $q->result_array();
				$i = 0;
				foreach ($this->cfg['lead_stage'] as $k => $v)
				{
					while (isset($result[$i]) && $k == $result[$i]['lead_stage'])
					{
						$data['results'][$k][] = $result[$i];
						$i++;
					}
				}
				
				if (count($result) == 1)
				{
					$this->session->set_flashdata('header_messages', array('Only one result found! You have been redirect to the job.'));
					
					//$status_type = (in_array($result[0]['lead_stage'], array(4,5,6,7,8,25))) ? 'invoice' : 'welcome';
					//$status_type = (in_array($result[0]['lead_stage'])) ? 'invoice' : 'welcome';
					
					//redirect($status_type . '/view_quote/' . $result[0]['lead_id']);
					redirect('welcome/view_quote/' . $result[0]['lead_id'] . '/draft');
				}
				else 
				{	//echo "tljlj";
					$this->session->set_flashdata('header_messages', array('Results found! You have been redirect to the job.'));
					redirect('welcome/view_quote/' . $result[0]['lead_id'] . '/draft');
				}
			  
		    }
			else {
				$this->session->set_flashdata('header_messages', array('No record found!'));
				redirect('welcome/view_quote/' . $_POST['quoteid'] . '/draft');
			}
		}
	}
	
	/*
	*Lead from eNoah Website
	*/
	public function add_lead() {
	    //Create Customer 		
		if(sizeof($_POST)==0){
		    echo 0;
			return false;
		}
		
		if(!empty($_POST['contact_us'])){		
			$ins_cus['first_name']     = $_POST['firstname']; 
			$ins_cus['last_name']      = $_POST['lastname']; 
			$ins_cus['company']        = $_POST['organization']; 
			$ins_cus['position_title'] = $_POST['title']; 
			$ins_cus['email_1']        = $_POST['email']; 
			$ins_cus['email_2']        = $_POST['businessemail']; 
			$ins_cus['phone_1']        = $_POST['phonenumber'];
			$ins_cus['add1_line1']     = $_POST['address'];
			$ins_cus['comments']       = $_POST['message'];		
		} else {		
			$ins_cus['first_name']    = $_POST['name']; 
			$ins_cus['email_1']       = $_POST['email']; 
			$ins_cus['company']       = $_POST['company']; 
			$ins_cus['comments']      = $_POST['content'];
	    }
		//insert customer and retrive last insert id
		$insert_id = $this->Customer_model->get_customer_insert_id($ins_cus);
		//Create Jobs
		$ins['lead_title']           = 'Ask the Expert';
		$ins['custid_fk']           = $insert_id;
		$ins['lead_service']        = $_POST['lead_service'];
		//$ins['lead_source']       = '';
		$ins['lead_assign']         = 59;
		$ins['expect_worth_id']     = 5;
		$ins['expect_worth_amount'] = '0.00';
		$ins['belong_to']           = 59; // lead owner
		// $ins['division']         = $_POST['job_division'];
		$ins['date_created']        = date('Y-m-d H:i:s');
		$ins['date_modified']       = date('Y-m-d H:i:s');
		$ins['lead_stage']          = 1;
		// $ins['lead_indicator']   = $_POST['lead_indicator'];
		$ins['created_by']          = 59;
		$ins['modified_by']         = 59;
		$ins['lead_status']         = 1;
		$new_job_id = $this->Welcome_model->insert_job($ins);
		if (!empty($new_job_id)) {
			$invoice_no = (int) $new_job_id;
			$invoice_no = str_pad($invoice_no, 5, '0', STR_PAD_LEFT);
			$up_args = array('invoice_no' => $invoice_no);
			$this->Welcome_model->update_job($insert_id, $up_args);
			$this->quote_add_item($insert_id, "\nThank you for entrusting eNoah  iSolution with your web technology requirements.\nPlease see below an itemised breakdown of our service offering to you:", 0, '', FALSE);
		}
		echo 1;
		exit;
	}
	
	//For Countries
	public function loadCountrys()
	{
		$data = real_escape_array($this->input->post());
		$region_id = join(",", $data['region_id']);

	    $output = '';
		$data = $this->Welcome_model->getcountry_list($region_id);
		if(!empty($data)) {
		foreach($data as $country) {
		    $output .= '<option value="'.$country['countryid'].'">'.$country['country_name'].'</option>';
		}
		} else {
			$output = '';
		}
		echo $output;exit;
	}
	
	//For States
	public function loadStates()
	{
		$data = real_escape_array($this->input->post());
		$cnt_id = join(",", $data['coun_id']);
		
	    $output = '';
		$data = $this->Welcome_model->getstate_list($cnt_id);
		foreach($data as $st) {
		    $output .= '<option value="'.$st['stateid'].'">'.$st['state_name'].'</option>';
		}
		echo $output;
	}
	
	//For Locations
	public function loadLocns()
	{
		$data = real_escape_array($this->input->post());
		$loc_id = join(",", $data['st_id']);
		
	    $output = '';
		$data = $this->Welcome_model->getlocation_list($loc_id);
		//print_r($data);
		foreach($data as $st) {
		    $output .= '<option value="'.$st['locationid'].'">'.$st['location_name'].'</option>';
		}
		echo $output;
	}
	
	/**
	 * Adds a addInternalNotes to a Order
	 * based on post data
	 *
	 */
	function addInternalNotes()
	{
		$data_log = real_escape_array($this->input->post());
		$data['internal_notes'] = str_replace('\n', "", $data_log['log_content']);
		$res = array();
		$json = array();
        if (isset($data_log['lead_id']) && isset($data_log['userid']) && isset($data['internal_notes'])) 
		{
			$data = array('internal_notes' => $data['internal_notes']);
			
			// update the leads table
			$this->db->where('lead_id', $data_log['lead_id']);
			$this->db->update($this->cfg['dbpref'] . 'leads', $data);

			$table = '';
			
			$json['error'] = FALSE;
			$json['html'] = $table;			
			
			echo json_encode($json);
			exit;
        }
        else
        {
            // echo "{error:true, errormsg:'Invalid data supplied'}";
			$res['error'] = true;
			$res['errormsg'] = 'Invalid data supplied';
			
        }
		exit;
    }
	
	/**
	 * Adds more doctor to a Order
	 * based on post data
	 *
	 */
	function ajax_add_doctors() 
	{
		$hide_doctors_name = array();
		$doctors_name	   = array();
		$json	   		   = array();
		$lead_det 		   = $this->Welcome_model->get_lead_det($this->input->post('order_no'));
		$sub_character	   = $this->Welcome_model->get_ordersubno($this->input->post('order_no'));
		$post 			   = $this->input->post();
		$json['error']     = false;
		
		if(isset($sub_character) && !empty($sub_character['order_sub_no'])) {
			$sub_char = $sub_character['order_sub_no'];
			$sub_char++;
		} else {
			$sub_char = 'A';
		}
	
		$insa = array();
		$insa['order_no']     		   = $lead_det['order_no'];
		$insa['orderref']   		   = $lead_det['orderref'];
		$insa['lead_title']  		   = preg_replace('/\\\\/','',$lead_det['lead_title']);
		$insa['lead_title_last']  	   = preg_replace('/\\\\/','',$lead_det['lead_title_last']);
		$insa['patient_dob']		   = $lead_det['patient_dob'];
		$insa['ss_no']   			   = $lead_det['ss_no'];
		$insa['patient_adr']  		   = $lead_det['patient_adr'];
		$insa['p_city']  		   	   = $lead_det['p_city'];
		$insa['p_state']  		   	   = $lead_det['p_state'];
		$insa['lead_assign']    	   = $lead_det['lead_assign'];
		$insa['expect_worth_id']       = $lead_det['expect_worth_id'];
		$insa['expect_worth_amount']   = $lead_det['expect_worth_amount'];
		$insa['actual_worth_amount']   = $lead_det['actual_worth_amount'];
		$insa['invoice_no']            = $lead_det['invoice_no'];
		// $insa['lead_stage']  		   = 1;
		$insa['proposal_expected_date'] = $lead_det['proposal_expected_date'];
		$insa['lead_status']   		   = 1;
		$insa['pjt_status']   		   = $lead_det['pjt_status'];
		$insa['lead_indicator']    	   = $lead_det['lead_indicator'];
		$insa['rag_status']     	   = $lead_det['rag_status'];
		$insa['project_type']   	   = $lead_det['project_type'];
		$order_stage				   = $post['order_stage'];
		
		if(empty($post['hide_doctors_name'])){
			$json['error'] = true;
			$json['msg']   = 'Valid doctors must be selected';
		} else {
			$k=0;
			foreach($post['hide_doctors_name'] as $new_cust){
				if($new_cust!=''){
					$insa['custid_fk']     = $new_cust;
					$insa['order_sub_no']  = $sub_char;
					$insa['created_by']    = $this->userdata['userid'];
					$insa['modified_by']   = $this->userdata['userid'];
					$insa['belong_to']     = $this->userdata['userid'];
					$insa['date_created']  = date('Y-m-d H:i:s');
					$insa['lead_stage']    = $order_stage[$k];
					// echo "<pre>"; print_r($insa); exit;
					$this->db->insert($this->cfg['dbpref'] . 'leads', $insa);
					$last_insert_id = $this->db->insert_id();
					
					$invoice_nos = (int) $last_insert_id;
					$invoice_nos = str_pad($invoice_nos, 5, '0', STR_PAD_LEFT);
					$inv_nos['invoice_no'] = $invoice_nos;
					$updt_job = $this->Welcome_model->update_row('leads', $inv_nos, $last_insert_id);
					
					//history - lead_stage_history
					$lead_hist['lead_id'] 			= $last_insert_id;
					$lead_hist['dateofchange'] 		= date('Y-m-d H:i:s');
					$lead_hist['previous_status'] 	= 1;
					$lead_hist['changed_status'] 	= 1;
					$lead_hist['lead_status'] 		= 1;
					$lead_hist['modified_by'] 		= $this->userdata['userid'];
					$insert_lead_stg_his 			= $this->Welcome_model->insert_row('lead_stage_history', $lead_hist);
					
					//history - lead_status_history
					$lead_stat_hist['lead_id'] 			= $last_insert_id;
					$lead_stat_hist['dateofchange'] 	= date('Y-m-d H:i:s');
					$lead_stat_hist['changed_status'] 	= 1;
					$lead_stat_hist['modified_by'] 		= $this->userdata['userid'];
					$insert_lead_stat_his = $this->Welcome_model->insert_row('lead_status_history', $lead_stat_hist);
					
					$sub_char++;
					
					//sent notification email only when initial stage is chosen
					if($order_stage[$k]==1){
						$this->sent_notification_email_ajax_add_doctor($last_insert_id);
					}
				}
				$k++;
			}
			$json['error'] = false;
			$json['msg']   = 'Doctor Added';
		}
		echo json_encode($json); exit;
	}
	
	function sent_notification_email_ajax_add_doctor($caseId)
	{
		$get_det 					= $this->Welcome_model->get_lead_det($caseId);
		$customer 					= $this->Welcome_model->get_customer_det($get_det['custid_fk']);
		$lead_assign_mail 			= $this->Welcome_model->get_user_data_by_id($get_det['lead_assign']);

		$user_name 					= $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
		$from						= $this->userdata['email'];
		$arrEmails 					= $this->config->item('crm');
		$arrSetEmails				= $arrEmails['director_emails'];
		$mangement_email 			= $arrEmails['management_emails'];
		$mgmt_mail 					= implode(',',$mangement_email);
		$admin_mail					= implode(',',$arrSetEmails);
		
		$param['email_data'] 		= array('first_name'=>$customer['first_name'],'last_name'=>$customer['last_name'],'company'=>$customer['company'],'base_url'=>$this->config->item('base_url'),'insert_id'=>$caseId);

		$param['to_mail'] 			= $mgmt_mail.','. $lead_assign_mail[0]['email'];
		$param['bcc_mail'] 			= $admin_mail;
		$param['from_email'] 		= $from;
		$param['from_email_name'] 	= $user_name;
		$param['template_name'] 	= "New Order Creation Notification";
		$param['subject'] 			= 'New Order Creation Notification';
		$this->Email_template_model->sent_email($param);
	}
	
	/**
	 * sets the payment terms
	 * for the project
	 */
	function set_records($update = false,$records_data='')
	{
		// exit('yes');
		$json 	  = array();
		$ins_data = array();
		$msg = '';
		if($records_data){
			$data      = $records_data;
		}else{
			$data      = real_escape_array($this->input->post());
		}
		$api_data = array();
	
		$user_data = $this->session->userdata('logged_in_user');
		
		if($update==false){
			$ins_data['jobid'] 		   = $data['jobid'];
			$ins_data['record_title']  = $data['record_title'];
			$ins_data['record_desc']   = $data['record_desc'];
			$ins_data['page_count']   = $data['page_count'];
			$ins_data['record_remark'] = $data['record_remark'];
			$ins_data['record_file']   = $data['record_file'];
			$ins_data['ori_record_file'] = $data['ori_record_file'];
			$ins_data['created_by']    = $user_data['userid'];
			$ins_data['created_on']    = date('Y-m-d H:i:s');
			$ins_data['modified_by']   = $user_data['userid'];
			 
				// echo "<pre>";print_R($ins_data);exit;
			if($this->db->insert($this->cfg['dbpref'] . 'records', $ins_data)){
				// $json['error'] = false;
				// $json['msg']   = 'Record Added Successfully';
				$update = $this->db->insert_id();
				$msg   = 'Record Added Successfully';
			} else {
				// $json['error'] = true;
				// $json['msg']   = 'Record Insertion failed';
				$msg   = 'Record Insertion failed';
			}
		} else {
			/* Array
			(
				[record_title] => 123ads fasdf asdf
				[record_desc] => 12313 asdf asdf
				[record_remark] => 123123 sdaaf asdf
				[record_file] => phpnotice-error-theblend.png
				[jobid] => 259
			) */
			
			$update_record = array('record_title' => $data['record_title'], 'record_desc' => $data['record_desc'], 'page_count' => $data['page_count'],'record_remark' => $data['record_remark'], 'modified_by' => $user_data['userid']);
			
			if(isset($data) && $data['record_file'])
			$update_record['record_file'] =  $data['record_file'];
			if(isset($data['ori_record_file'])){
				$update_record['ori_record_file'] = $data['ori_record_file'];
			}
		
			$wh_condn = array('id' => $update, 'jobid' => $data['jobid']);
			$updt_rec = $this->Welcome_model->update_rec('records', $update_record, $wh_condn);
			if($updt_rec) {
				$msg   = 'Record updated.';
			} else {
				$msg   = 'Record updation failed.';
			}

		}
		$document_detail =  $this->Welcome_model->get_record_detail($update);
	
		$api_data['DocumentId'] = $document_detail['aps_record_id'];
		$lead_detail = $this->Welcome_model->get_lead_all_detail($data['jobid']);
		if($lead_detail['aps_order_no'] != '' && ($data['record_title'] == $this->config->item('crm')['invoice_title']) && $data['record_file'] != NULL)
		{
			/** 
			* @var hit APS API for document upload
			*/
			include_once(APPPATH.'controllers/web_service_v2/document_upload_trigger.php');
			$file_path = 'crm_data/records/'.$data['jobid'].'/'.$data['record_file'];
			if(isset($data['record_file'])){
				$filename_azure = $this->Azure_model->azure_upload($file_path,$file_path);
			}
			$api_data['eNaohOrderId'] = $lead_detail['aps_order_no'].$lead_detail['order_sub_no'];
			$api_data['companyCode']  = $lead_detail['company_code'];
			$api_data['Companytype']  = $lead_detail['lead_company_type'];
			$api_data['eXpertDocId']  = $document_detail['id'];
			$api_data['title'] 		  = $data['record_title'];
			$api_data['description']  = $data['record_desc'];
			$api_data['remarks']  	  = $data['record_remark'];
			$api_data['fileName']  	  = $data['record_file'];
			$api_data['azure_path']	  = $file_path;
			$document_upload_trigger  = new document_upload_trigger();
			$response = $document_upload_trigger->request_auth_token($api_data,'upload_file');
			$response = json_decode($response,true);
			if($response['statusCode'] == 100)
			{
				if(isset($response['documentId']) && $response['documentId'] != '' && $response['documentId'] != NULL)
				{
					$output['aps_record_id'] = isset($response['documentId']) ? $response['documentId'] : 0;
					$this->db->where('id',$update);
					$this->db->update($this->cfg['dbpref'] . 'records', $output);
				}
			}
		}
		if($records_data){

		}
		else{
			echo $msg; 
		exit;
		}
		// echo json_encode($json); exit;
	}

	//function to update the lead table with the copy service id
	function set_service_records(){
		$data = real_escape_array($this->input->post());	
		$copy_service_id['copy_service_id'] = $data['service_id'];
		$lead_id['lead_id'] = $data['jobid'];
		$add_record = $this->Welcome_model->update_service_rec('leads',$copy_service_id,$lead_id);
		if($add_record) {
			$msg   = 'Record Added.';
		} else {
			$msg   = 'Record Adding failed.';
		}
		echo $msg; exit;
	}
	
	function loadRecordAddView($jid)
	{
		$val['jobid'] = $jid;
		$getLeadDet = $this->Welcome_model->get_lead_detail($jid);
		$aps_summ = $getLeadDet[0]['aps_summarization'];
		$val['attachmnet_type'] = $this->Welcome_model->get_attachment_type();
		foreach($val['attachmnet_type'] as $ky => $attval){
			// if($aps_summ == 1){
				if($attval['name'] == 'Results')
				unset($val['attachmnet_type'][$ky]);
			// }else{
			// 	if($attval['name'] == 'Results Pending')
			// 	unset($val['attachmnet_type'][$ky]);
			// }
		}
		$this->load->view("leads/add_records", $val);
	}
	
	public function payment_file_upload($lead_id=null,$payment_ajax_file_uploader=null)
	{
		$json  		  = array();
		$res_file     = array();
		
		$strenckey = $this->cfg['encryptkey']['enckey'];
		
		// get company lead details
		$lead_details = $this->Welcome_model->get_upload_file_size($lead_id);
		$this->load->model('Pdf_model');
		
		
		if(isset($lead_details[0]) && round($_FILES['payment_ajax_file_uploader']['size']  / 1024) >= $lead_details[0]['upload_file_size'])
		{
			$json['error'] = TRUE;
			$json['msg']   = "Uploaded File Size Exceeds more than ".number_format($lead_details[0]['upload_file_size'] / 1024)." MB, Please compress the PDF size then try again.";
			echo json_encode($json);exit;
		}
		
		if($payment_ajax_file_uploader){
		$f_name = preg_replace('/[^a-z0-9\.]+/i', '-', $payment_ajax_file_uploader['name']);
		$_FILES['payment_ajax_file_uploader']=$payment_ajax_file_uploader;
		}else{
		$f_name = preg_replace('/[^a-z0-9\.]+/i', '-', $_FILES['payment_ajax_file_uploader']['name']);	
		}
		
		if (preg_match('/\.(php|js|exe)+$/', $f_name, $matches)) // basic sanity
		{
			$json['error'] = TRUE;
			$json['msg']   = "Uploaded a file type that is not allowed!\nYour file extension : {$matches[1]}";
			echo json_encode($json);exit;
		}
		if($_FILES['payment_ajax_file_uploader']['size'] <= 0)
		{
			$json['error'] = TRUE;
			$json['msg']   = "Uploaded File Size is not valid.";
			echo json_encode($json);exit;
		}
	
		$user_data = $this->session->userdata('logged_in_user');

		//creating records folder name
		$f_dir = UPLOAD_PATH.'records/';
		if (!is_dir($f_dir)) {
			mkdir($f_dir);
			chmod($f_dir, 0777);
		}
		
		//creating lead_id folder name
		$f_dir = $f_dir.$lead_id;
		if (!is_dir($f_dir)) {
			mkdir($f_dir);
			chmod($f_dir, 0777);
		}
		$fdir=$f_dir;
		$f_dir = $f_dir . '/' . $f_name;
		
//echo $full_path;
		if(is_file($f_dir)){
		$tmpfname=$_FILES['payment_ajax_file_uploader']['tmp_name'];
		$orifname=$_FILES['payment_ajax_file_uploader']['name'];
		$f_name = time() . $f_name;
		$fullpath = $fdir . '/' . $f_name;
		// $encf=$this->fileencryption->encryptFile($tmpfname,$strenckey,$fullpath);
		// $full_path = $encf;
		} else {
		$orifname=$_FILES['payment_ajax_file_uploader']['name'];
			$tmpfname=$_FILES['payment_ajax_file_uploader']['tmp_name'];
			// $encf=$this->fileencryption->encryptFile($tmpfname,$strenckey,$f_dir);
			// $full_path = $encf;
			$fullpath = $fdir . '/' . $f_name;
		}
		// move_uploaded_file($_FILES['payment_ajax_file_uploader']['tmp_name'], $f_dir); exit;
		
		if(move_uploaded_file($_FILES['payment_ajax_file_uploader']['tmp_name'], $fullpath)) {
		if($fullpath!=false) {
			$no_page = shell_exec('pdftk '.$fullpath.' dump_data | grep "NumberOfPages" | cut -d":" -f2');
			if(is_null($no_page))
			{
				$json['error']    = TRUE;
				$json['msg']      = "Uploaded File is not valid/corrupted. Please upload a new file!";
				echo json_encode($json);exit;
			}
			
			$json['error']    = FALSE;
			$json['msg']      = "File successfully uploaded!";
			$json['res_file'] = $f_name;
			$json['ori_res_file'] = $orifname;
		
		} else {
			$json['error']    = TRUE;
			$json['msg']      = "File upload failed!";
		}
		if($payment_ajax_file_uploader){
			}
		else{
			echo json_encode($json);
		}
		}
	}
	
	/**
	 * sets the payment terms
	 * for the project
	 */
	function get_records($jobid)
	{
		$this->load->helper('text');
		// echo "<prE>";print_R($jobid);exit;
		$records = $this->Welcome_model->get_records_list($jobid);
		// echo "<prE>";print_R($records);exit;
        $order_details = $this->Welcome_model->get_lead_detail($jobid);
		// echo "<prE>";print_R($records);
		$html = "<table class='data-table'>";
		$html .= "<thead>";
		$html .= "<tr><th>Record Title</th><th>Description</th><th>Page Count</th><th>Remarks</th><th>File Name</th><th>Uploaded By</th><th>Date Created</th><th>Action</th></tr></thead>";
		$html .= "<tbody>";
		if(isset($records[0]['record_file'])){
			$record_file = $records[0]['record_file'];
			$record_file_path = $this->config->item('base_url').'crm_data/'.'records/'.$jobid.'/'.$records[0]['record_file'];
		}
		// print_r($record_file_path);exit;
		// onclick="window.location.href='location.html'"
		$coversheet = $this->Welcome_model->get_coversheet_log_detail($jobid);
		// echo "<pre>";print_R(count((array)$coversheet['jobid'])); exit;
		if(isset($coversheet['jobid'])){
			$crtDt = date('mdY',strtotime($coversheet['created_on']));
			$onclick = "delete_coversheet('".$coversheet['jobid']."')";
		}
		if(isset($coversheet['jobid']) && count((array)$coversheet['jobid'])==1){
			
				$record_title = "Request Letter";
				$description = "Request Letter";
				$remarks = "Request Letter";
				// Cover_sheet-
				
				$file_name = 'Request_Letter_'.$crtDt.'_'.$coversheet['jobid'].'.pdf';			
				
				$path1 = FCPATH.'cover_sheets/Request_Letter_'.$crtDt.'_'.$coversheet['jobid'].'.pdf' ;	

				$path2 = FCPATH.'cover_sheets/Cover_sheet-'.$coversheet['jobid'].'.pdf' ;
				if(file_exists($path1)){
					$path = $this->config->item('base_url').'cover_sheets/Request_Letter_'.$crtDt.'_'.$coversheet['jobid'].'.pdf';
				}else{
					$path = $this->config->item('base_url').'cover_sheets/Cover_sheet-'.$coversheet['jobid'].'.pdf';
				}

				$html .= "<tr>";
				$html .= "<td>".preg_replace('/\\\\/','',$record_title)."</td>";
				$html .= "<td style='width:70px;display: inline-block;border: none;vertical-align: middle;'>".preg_replace('/\\\\/','',$description)."</td>";
				$html .= "<td></td>";
				$html .= "<td>".preg_replace('/\\\\/','',$remarks)."</td>";
				// if(isset($order_details[0]['is_retention_done']) && $order_details[0]['is_retention_done'] == 0){
				if( empty($order_details[0]['is_retention_done']) || ($order_details[0]['is_retention_done'] == 0) ){
				$html .= "<td><p class='record_file'><a target='_new' style = 'color:#009999' href='".$path."'>".$file_name."</a></p></td>";
				}else{
					$html .= "<td><p class='record_file'><a target='_new' style = 'color:#009999' href='".$this->config->item('base_url')."archive/Archive_Replacement_Document.pdf'>Archive_Replacement_Document.pdf</a></p></td>";
				}
				$created_on = '';
				if($coversheet['created_on'] != NULL && $coversheet['created_on'] != '0000-00-00 00:00:00')
				{
					$created_on = date('m-d-Y',strtotime($coversheet['created_on']));
				}
				$user_details = $this->Welcome_model->get_user_data_by_id($coversheet['userid']);	
				$html .= "<td>".$user_details[0]['first_name'].' '.$user_details[0]['last_name']."</td>";
				$html .= "<td>".$created_on."</td>";	
				// if(isset($order_details[0]['is_retention_done']) && $order_details[0]['is_retention_done'] == 0){
				if( empty($order_details[0]['is_retention_done']) || ($order_details[0]['is_retention_done'] == 0) ){
					$html .= "<td><a href='javascript:void(0);' class='file-delete' onclick=".$onclick." ><i style = 'color:red;float: left;margin-left: 18px;' class='fa fa-trash-o aria-hidden='true' del_icon'></i></a></td>";
				}else{
					$html .= "<td>-</td>";
				}
				$html .= "</tr>"; 
		}
		
		// if summary processed file from eaps display 
		$summary_files = $this->Summarization_model->get_summary_result_files($jobid);
		//var_dump($summary_files);exit;
		// echo "</br>";
		// echo "<pre>";print_r($summary_files);
		foreach($summary_files as $rec1){	
			$html .= "<tr>";
			$html .= "<td>Results</td>";
			if($rec1['status_flag'] == $this->cfg['summary_cancelled'])
			{
				$html .= "<td>".$rec1['record_desc']."</td>";
			}else{
			$html .= "<td>Summary File</td>";
			}
			$html .= "<td>".$rec1['page_count']."</td>";
			if($rec1['status_flag'] == $this->cfg['summary_cancelled'])
			{	
				$html .= "<td>".$rec1['record_remark']."</td>";
			}else{
			$html .= "<td>Summary File</td>";
			}
			$test_path=$rec1['file_path'];
			$sasurl =$this->Azure_sas_model->generateBlobDownloadLink($test_path);
			// $html .= "<td><p class='record_file'><a style = 'color:#009999' href='".$rec1['file_path']."'>".$rec1['file_name']."</a></p></td>";

			// if(isset($order_details[0]['is_retention_done']) && $order_details[0]['is_retention_done'] == 0){
			if( empty($order_details[0]['is_retention_done']) || ($order_details[0]['is_retention_done'] == 0) ){
                            // $html .= "<td><p class='record_file'><a style = 'color:#009999' href='".$this->cfg['azure_path']."-".$rec1['file_path']."' target='__blank'>----".$rec1['file_name']."</a></p></td>";
                            $html .= "<td><p class='record_file'><a style = 'color:#009999' href='".$sasurl."' target='__blank'>".$rec1['file_name']."</a></p></td>";
			}
			else
			{
				$html .= "<td><p class='record_file'><a target='__blank' style = 'color:#009999' href='".$this->config->item('base_url')."archive/Archive_Replacement_Document.pdf'>Archive_Replacement_Document</a></p></td>";
			}
			$html .= "<td>eAPS System</td>";
			if($rec1['updated_at'] == NULL)
			{
				$html .= "<td>".date('m-d-Y',strtotime($rec1['created_at']))."</td>";
			}else{
				$html .= "<td>".date('m-d-Y',strtotime($rec1['updated_at']))."</td>";	
			}
			$html .= "<td>-</td>";
			$html .= "</tr>";	
		}
		if($records){
			foreach($records as $rec){		
	    
					$html .= "<tr>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec['name'])."</td>";
					$html .= "<td style='width:70px;display: inline-block;border: none;vertical-align: middle;'>".word_limiter(preg_replace('/\\\\/','',$rec['record_desc']), 5)."</td>";
					$html .= "<td>".$rec['page_count']."</td>";
					$html .= "<td>".word_limiter(preg_replace('/\\\\/','',$rec['record_remark']), 3)."</td>";
					//$html .= "<td><p class='record_file'><a style = 'color:#009999' onclick='download_files(".$rec['jobid'].",".$rec['id']."); return false;'>".$rec['ori_record_file']."</a></p></td>";

				if( empty($order_details[0]['is_retention_done']) || ($order_details[0]['is_retention_done'] == 0) ){
					$html .= "<td><p class='record_file'><a style = 'color:#009999' href='welcome/download_file/".$rec['jobid']."/".$rec['auto_id']."'>".stripslashes(preg_replace('/\\\\/','',$rec['ori_record_file']))."</a></p></td>";
				}
				else
				{
				$html .= "<td><p class='record_file'><a style = 'color:#009999' href='welcome/download_file/".$rec['jobid']."/".$rec['auto_id']."'>Archive_Replacement_Document.pdf</a></p></td>";
				}

				// $html .= "<td><p class='record_file'><a style = 'color:#009999' href='welcome/download_file/".$rec['jobid']."/".$rec['auto_id']."'>".stripslashes(preg_replace('/\\\\/','',$rec['ori_record_file']))."</a></p></td>";
					// $html .= "<td><a target='_new' style = 'color:#009999' href='".$record_file_path."'>".$record_file."</a></td>";	
					
					// $html .= "<td><a target='_new' style = 'color:#009999' onclick='window.location.href='".$record_file_path."'>".$record_file."</a></td>";	
				if($rec['created_by'] == 1)
				{
					$created_by = 'EOP System';
				}
				else if($rec['created_by'] == 0)
				{
					$created_by = 'eMap System';
				}else if($rec['created_by'] == 3){
					$created_by = 'eRecord System';
				}
				else{
				$user_details = $this->Welcome_model->get_user_data_by_id($rec['created_by']);	
				$created_by = $user_details[0]['first_name'].' '.$user_details[0]['last_name'];
				}
				$html .= "<td>".$created_by."</td>";	
				$html .= "<td>".date('m-d-Y',strtotime($rec['created_on']))."</td>";
                            if(empty($order_details[0]['is_retention_done']) || $order_details[0]['is_retention_done'] == 0)
                            {
				if($rec['record_title'] == 13 || $rec['record_title'] == 17)
				{
					$html .= "<td>-</td>";
				}
				else{
				$html .= "<td><a onclick='edit_records(".$rec['auto_id']."); return false;' href='javascript:void(0)'><i style = 'color:#009999' class='fa fa-pencil-square-o' aria-hidden='true'></i></i></a> | <a href='javascript:void(0)' onclick='delete_records(".$rec['auto_id']."); return false;' ><i style = 'color:red;' class='fa fa-trash-o aria-hidden='true'  del_icon'></i></a></td>";
				}
                            }else{
                                    $html .= "<td>-</td>";
                            }
				$html .= "</tr>";	
			}
		}
		$html .= "</tbody>";
		$html .= "</table>";
		
		echo $html; exit;
	}

	function get_service_records($jobid)
	{
		$this->load->helper('text');
		// echo "<pre>"; print_r($jobid); exit;
		$records = $this->Welcome_model->get_service_records_list($jobid);
		$lead_details = $this->Welcome_model->get_lead_detail($jobid);
		
		$copy_service_details = $this->Welcome_model->get_facility_copy_service_details($jobid,'',$lead_details[0]['company_type']);
		
		$active_copy_service = $this->Welcome_model->get_mapped_copy_service($lead_details[0]['custid_fk'],$lead_details[0]['company_type'],1);
		
		$count = 0;
		
		$html = "<table class='data-table'>";
		$html .= "<tbody>";
		$html .= "<tr><th>Facility File#</th><th style='white-space: nowrap !important;'>Facility Name</th><th>Service Name</th><th>Address</th><th>State</th><th>Phone</th><th>Activated On</th><th>Activated By</th><th>Deactivated On</th><th>Deactivated By</th></tr>";
		// echo "<pre>"; print_r($active_copy_service); exit;
		if($active_copy_service){
			$active_record = FALSE;
			foreach($copy_service_details as $rec1)
			{
				if($rec1['company'] !='' && $rec1['first_name_c'] !='' && $rec1['last_name_c']!= ''){
					$facility_name= $rec1['first_name_c'].' '.$rec1['last_name_c'].' - '.$rec1['company'];
					$facility_name = preg_replace('/\\\\/','',$facility_name);
				}elseif($rec1['first_name_c'] =='' && $rec1['last_name_c'] != '' && $rec1['company'] !=''){
					$facility_name = $rec1['last_name'].' - '.$rec1['company'];
					$facility_name = preg_replace('/\\\\/','',$facility_name);
				}elseif($rec1['first_name_c'] !='' && $rec1['last_name_c'] == '' && $rec1['company'] !=''){
					$facility_name = $rec1['first_name_c'].' - '.$rec1['company'];
					$facility_name = preg_replace('/\\\\/','',$facility_name);
				}elseif($rec1['first_name_c'] =='' && $rec1['last_name_c'] == '' && $rec1['company'] !=''){
					$facility_name = $rec1['company'];
					$facility_name = preg_replace('/\\\\/','',$facility_name);
				}else{
					$facility_name= $rec1['first_name_c'].' '.$rec1['last_name_c'];
					$facility_name = preg_replace('/\\\\/','',$facility_name);
				}
												
				if($active_record == FALSE && $active_copy_service[0]['copy_service_id'] == $rec1['copy_service_id'] && $active_copy_service[0]['facility_id'] == $rec1['facility_id'])
				{ 
					$html .= "<tr class='active_record'>";
					$active_record = TRUE;
				}
				else{
					$html .= "<tr>";
				}
					$html .= "<td>".$rec1['facility_id']."</td>";
					$html .= "<td>".$facility_name."</td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec1['Name'])."</td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec1['Address'])."</td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec1['State'])."</td>";
					$html .= "<td>".$rec1['Phone']."</td>";
					// $html .= "<td><a onclick='view_records(".$rec1['copy_service_id']."); return false;' id='get_services' href='javascript:void(0)'>View</a></td>";
					$html .= "<td>".date('m/d/Y H:i:s',strtotime($rec1['created_on']))."</td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec1['first_name']).' '.preg_replace('/\\\\/','',$rec1['last_name'])."</td>";
					if($rec1['updated_on'] != NULL && $rec1['updated_on'] != '0000-00-00 00:00:00')
					{
					$html .= "<td>".date('m/d/Y  H:i:s',strtotime($rec1['updated_on']))."</td>";
					}
					else
					{
						$html .= "<td></td>";
					}
					$html .= "<td>".preg_replace('/\\\\/','',(string)$rec1['user_first_name']).' '.preg_replace('/\\\\/','',(string)$rec1['user_last_name'])."</td>";
					$html .= "</tr>";
					$count++;
			}
		}
		
		if($records){
			foreach($records as $rec)
			{
				if($active_copy_service[0]['copy_service_id'] != $rec['service_record_id'])
				{
					$html .= "<tr>";
				}
				else{
					$html .= "<tr class='active_record'>";
				}
					$html .= "<tr>";
					$html .= "<td></td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec['Name'])."</td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec['Address'])."</td>";
					$html .= "<td>".preg_replace('/\\\\/','',$rec['State'])."</td>";
					$html .= "<td>".$rec['Phone']."</td>";
					// $html .= "<td><a onclick='view_records(".$rec['service_record_id']."); return false;' id='get_services' href='javascript:void(0)'>View</a></td>";
					$html .= "<td></td>";
					$html .= "<td></td>";
					$html .= "<td></td>";
					$html .= "<td></td>";
					$html .= "</tr>";
					$count++;
			}
		}

		if($count == 0)
		{
				$html .= "<tr>";
					$html .= "<td colspan='10'>Facility has no copy service</td>";
				$html .= "</tr>";
				
		}
		$html .= "</tbody>";
		$html .= "</table>";
		
		echo $html; exit;
	}

	function view_copy_services_record($service_record_id){
		$data = $this->Welcome_model->view_service_records_list($service_record_id);
		// echo 'r'; exit;
		echo json_encode($data); exit;
		// echo $data; exit;
	}
	//Coversheet Delete
	function agreedCoversheetDelete($rid, $jid)
	{
		// print_R($jid);exit;
		$coversheet = $this->Welcome_model->get_coversheet_log_detail($jid);
		$crtDt = date('mdY',strtotime($coversheet['created_on']));
		if(file_exists(FCPATH.'cover_sheets/Cover_sheet-'.$jid.'.pdf'))
		{
			unlink(FCPATH.'cover_sheets/Cover_sheet-'.$jid.'.pdf');
		}
		else if(file_exists(FCPATH.'cover_sheets/Request_Letter_'.$crtDt.'_'.$jid.'.pdf')){
		unlink(FCPATH.'cover_sheets/Request_Letter_'.$crtDt.'_'.$jid.'.pdf');
		}
		$condn = array('jobid' => $jid, 'is_coversheet' => 1);
		$del      = $this->Welcome_model->delCoversheet($condn);
		if($rid != '')
			$this->get_records($jid);
	}
	function agreedRecordDelete($rid, $jid)
	{
		// print_R($rid);echo"<br>";
		// print_R($jid);exit;
		// $rec_det = $this->Welcome_model->get_records_det($rid, $jid);
		$rec_det =  $this->Welcome_model->get_record_detail($rid);
		// echo "<pre>";print_r($rec_det);exit;
		$file_name = $rec_det['record_file'];
		$rec_dir = UPLOAD_PATH.'records/'.$jid.'/';
		$dirname = $rec_dir . $file_name;
		if (is_file($dirname) || is_link($dirname)) {
			@unlink($dirname);
		}
		// $wh_condn = array('record_title' => $rid, 'jobid' => $jid);
		$wh_condn = array('id' => $rec_det['id']);
		$lead_detail = $this->Welcome_model->get_lead_all_detail($jid);
		$document_detail =  $this->Welcome_model->get_record_detail($rec_det['id']);
		$api_data = array();
		$api_data['DocumentId'] = $document_detail['aps_record_id'];
		if($lead_detail['aps_order_no'] != '')
		{
			/** 
			* @var hit APS API for document upload
			*/
			// include_once(APPPATH.'controllers/web_service_v2/document_upload_trigger.php');
			$file_path = 'crm_data/records/'.$jid.'/'.$rec_det['record_file'];
			
			$api_data['eNoahOrderId'] = $lead_detail['aps_order_no'].$lead_detail['order_sub_no'];
			//$api_data['eXpertDocId']  = $document_detail['id'];
			$api_data['companyCode']  = $lead_detail['company_code'];
			$api_data['Companytype']  = $lead_detail['lead_company_type'];
			$api_data['title'] 		  = $rid;
			$api_data['fileName']  	  = $file_name;
			// $document_upload_trigger  = new document_upload_trigger();
			// $document_upload_trigger->request_auth_token($api_data,'delete_file');
			$this->document_upload_trigger->request_auth_token($api_data,'delete_file');

		}
		
		file_log(array('file_name'=>$file_name),$jid,'agreedRecordDelete - Delete document',$this->userdata,$rec_det['id'],'delete',$rec_det['record_title']);
		
		# document_id 
		file_log_table(array('file_name'=>$file_name),$jid,'agreedRecordDelete - Delete document',$this->userdata,$rec_det['id'],'delete',$rec_det['record_title']);
			
		$del      = $this->Welcome_model->delRecord('records', $wh_condn);
		$this->get_records($jid);
	}
	
	function recordEdit($rid, $jid)
	{
		$val = array();
		$rec_det = $this->Welcome_model->get_records_det($rid, $jid);
		
		$getLeadDet = $this->Welcome_model->get_lead_detail($jid);
		$aps_summ = $getLeadDet[0]['aps_summarization'];
		$val['attachmnet_type'] = $this->Welcome_model->get_attachment_type();
		foreach($val['attachmnet_type'] as $ky => $attval){
			// if($aps_summ == 1){
				if($attval['name'] == 'Results')
				unset($val['attachmnet_type'][$ky]);
			// }else{
			// 	if($attval['name'] == 'Results Pending')
			// 	unset($val['attachmnet_type'][$ky]);
			// }
		}
		
		$val['id']            = $rec_det['auto_id'];
		$val['record_title']  = $rec_det['record_title'];
		$val['record_desc']   = $rec_det['record_desc'];
		$val['page_count']   = $rec_det['page_count'];
		$val['record_remark'] = $rec_det['record_remark'];
		$val['record_file']   = $rec_det['record_file'];
		$val['ori_record_file'] = $rec_det['ori_record_file'];
		$val['record_id']     = $rec_det['id'];
		$val['record_jobid']  = $rec_det['jobid'];

		$this->load->view("leads/update_records", $val);
	}
	
	function download_file_live($job_id,$rec_id)
	{
		$this->load->helper('download');
		$this->load->library('Fileencryption');
		$strenckey = $this->cfg['encryptkey']['enckey'];
		$rec_det = $this->Welcome_model->get_records_det($rec_id, $job_id);
		$file_dir = UPLOAD_PATH.'records/'.$job_id.'/'.$rec_det['record_file'];
		$name = $rec_det['ori_record_file'];
		$encf=$this->fileencryption->decrypt_File($file_dir,$strenckey);
		//$data = file_get_contents($file_dir); // Read the file's contents
		$data = $encf; // Read the file's contents
		
		force_download($name, $data);
	}
	function download_file($job_id,$rec_id)
	{
		$this->load->helper('download');
		$this->load->library('Fileencryption');
		$strenckey = $this->cfg['encryptkey']['enckey'];
		$rec_det = $this->Welcome_model->get_records_det($rec_id, $job_id);
		$order_det = $this->Welcome_model->get_lead_detail($job_id);
		
		// if(isset($order_det[0]['is_retention_done']) && $order_det[0]['is_retention_done'] == 0)		
		if( empty($order_det[0]['is_retention_done']) || ($order_det[0]['is_retention_done'] == 0) )
		{			
		$file_dir = UPLOAD_PATH.'records/'.$job_id.'/'.$rec_det['record_file'];
                        $name = $rec_det['ori_record_file'];
		
		}else{			
			$file_dir = FCPATH.'archive/Archive_Replacement_Document.pdf';
		$name = "Archive_Replacement_Document.pdf";

		}
		
		// echo "<prE>";print_R($file_dir);exit;
		// $name = $rec_det['ori_record_file'];
		// print_r($name);exit;
		// $encf=$this->fileencryption->decrypt_File($file_dir,$strenckey);
		$data = file_get_contents($file_dir);
		// print_r($data);exit;		// Read the file's contents
		// $data = $file_dir; // Read the file's contents
		// $data = $encf; // Read the file's contents
		
		force_download($name, $data);
	}
//Added download query file
	function download_filequery($job_id,$rec_id)
	{
		$this->load->helper('download');
		$this->load->library('Fileencryption');
		$strenckey = $this->cfg['encryptkey']['enckey'];
		$rec_det = $this->Welcome_model->get_query_details($rec_id, $job_id);
		$file_dir = UPLOAD_PATH.'query/'.$job_id.'/'.$rec_det['query_file_name'];
		$encf=$this->fileencryption->decrypt_File($file_dir,$strenckey);
		//$data = file_get_contents($file_dir); // Read the file's contents
		$data = $encf; // Read the file's contents
		$name = $rec_det['ori_query_file_name'];
		force_download($name, $data);
	}
	function download_fileid($jobfid,$fileid,$orifileid)
	{
	
		$this->load->helper('download');
		$this->load->library('Fileencryption');
		$strenckey = $this->cfg['encryptkey']['enckey'];
		//$rec_det = $this->Welcome_model->get_query_details($rec_id, $job_id);	
		$file_dir = UPLOAD_PATH.'files/'.$jobfid.'/'.$fileid;
		$encf=$this->fileencryption->decrypt_File($file_dir,$strenckey);
		//$data = file_get_contents($file_dir); // Read the file's contents
		$data = $encf; // Read the file's contents		
		$name = $orifileid;
		force_download($name, $data);
	}
	function download_fid($job_id,$rec_id)
	{
		$this->load->helper('download');
		$this->load->library('Fileencryption');
		$strenckey = $this->cfg['encryptkey']['enckey'];
		$rec_det = $this->Welcome_model->get_file_details($rec_id, $job_id);
		$file_dir = UPLOAD_PATH.'files/'.$job_id.'/'.$rec_det['lead_files_name'];
		$encf=$this->fileencryption->decrypt_File($file_dir,$strenckey);		
		$data = $encf; // Read the file's contents		
		$name = $rec_det['lead_ori_file_name'];
		force_download($name, $data);
	}
	function download_qfileid($jobfid,$fileid)
	{
	
		$this->load->helper('download');
		$this->load->library('Fileencryption');
		$strenckey = $this->cfg['encryptkey']['enckey'];
		//$rec_det = $this->Welcome_model->get_query_details($rec_id, $job_id);	
		$file_dir = UPLOAD_PATH.'query/'.$jobfid.'/'.$fileid;
		$encf=$this->fileencryption->decrypt_File($file_dir,$strenckey);
		//$data = file_get_contents($file_dir); // Read the file's contents
		$data = $encf; // Read the file's contents		
		$name = $fileid;
		force_download($name, $data);
	}
	public function ajax_date_update_quote($quote_id)
    {
		$post_data = real_escape_array($this->input->post());
		$order_close_date = $post_data['order_close_date'];
		$this->load->model('User_model');	
		$res = array();
		$lead_det = $this->Welcome_model->get_lead_det($quote_id);
		$result = $this->Welcome_model->updt_order_close_date($quote_id, $order_close_date);
		if($result){
			$logs['jobid_fk'] = $quote_id;
			$logs['userid_fk'] = $this->userdata['userid'];
			$logs['disposition_id'] = $lead_det['order_disposition_id'];
			$logs['lead_stage'] = $lead_det['lead_stage'];
			$logs['record_type'] = $lead_det['record_type'];
			$logs['callback_date'] = $lead_det['proposal_expected_date'];
			$logs['date_created'] = date('Y-m-d H:i:s');
			$logs['log_content'] = 'Order Close Date - '.$order_close_date.' is updated.';
			$insert_logs = $this->Welcome_model->insert_row('logs', $logs);

			//This code is display the log content without page load when manually set the order closed date.
			$stick_class='stickie';
			$fancy_date = date('l, jS F y h:iA', strtotime($logs['date_created']));
			$user_data = $this->Welcome_model->get_user_data_by_id($logs['userid_fk']);
			$rec_type_info = $this->Welcome_model->get_record_type_info($lead_det['record_type']);
			$record_type = $rec_type_info['rec_type_name'];
			
			$call_status   = $this->Welcome_model->get_call_status_info($logs['disposition_id']);
			$call_disposition = $call_status['disposition_name'];

			$status_res = $this->Welcome_model->get_lead_stg_name($logs['lead_stage']);
			$lead_stage = $status_res['lead_stage_name'];
			
			$table = <<<HDOC
<tr id="log" class="log{$stick_class}">
<td id="log" class="log">
<p class="data">
		<span>{$fancy_date}</span>
		{$user_data[0]['first_name']} {$user_data[0]['last_name']}
    </p>
    <p class="desc">
	<b>{$record_type} :</b> {$logs['log_content']}
	</p>
	<p class="desc">
	<b>Event Type Changed :</b>{$call_disposition}
	<p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}
	<p>
</td>
</tr>
HDOC;
			$res['error'] = false;
			$res['html'] = $table;	
		} else {
			$res['error'] = true;
			$res['errormsg'] = 'Database update failed!';
		}
		echo json_encode($res);
		exit;
    }
	function callDocPrintLog($leadid,$is_pay_mtd){
		# get order details 
		$leadDet = $this->Welcome_model->get_lead_detail($leadid);
		$covsht = $this->Welcome_model->get_coversheet_log_detail($leadid);

		// echo FCPATH.'/uploads/cover_sheets/'.'Cover_sheet-'.$leadid.'.pdf'; die;
		// checking coversheet
		$recordsList=[]; //$check_fax_cs = [];
		// if(file_exists(FCPATH.'/uploads/cover_sheets/'.'Cover_sheet-'.$leadid.'.pdf')){
		// 	$coversht = FCPATH.'/uploads/cover_sheets/'.'Cover_sheet-'.$leadid.'.pdf';
		// 	array_push($recordsList,$coversht);
		// 	array_push($check_fax_cs,$coversht);
		// }
		
		//checking request letter.
		$file_name=date('mdY',strtotime($covsht['created_on']));
		if(file_exists(FCPATH.'cover_sheets/'.'Request_Letter_'.$file_name.'_'.$leadid.'.pdf')){
			$coversht = FCPATH.'cover_sheets/'.'Request_Letter_'.$file_name.'_'.$leadid.'.pdf';
			array_push($recordsList,$coversht);
			// array_push($check_fax_cs,$coversht);
		}else{
			$this->prepare_cover_sheet($leadid);
			$covsht = $this->Welcome_model->get_coversheet_log_detail($leadid);
			$file_name=date('mdY',strtotime($covsht['created_on']));
			$coversht = FCPATH.'cover_sheets/'.'Request_Letter_'.$file_name.'_'.$leadid.'.pdf';
			array_push($recordsList,$coversht);
		}

		//checking LOR - ebase
		$lor = $this->Lor_model->get_company_expireLOR($leadDet[0]['comp_code'],$this->config->item('crm')['letter_of_rep'],$leadDet[0]['ordering_office_id']);
		if(count($lor)>0){
			foreach($lor as $vallor){
				$lorebase = UPLOAD_PATH.'lor_records/'.$vallor['lor_doc_file'];
				array_push($recordsList,$lorebase);
			}
		}
		
		//checking fomr/records - only LOR files.
		$record_oly_lor = $this->Welcome_model->get_records_list_w_L($leadid);
		if(is_countable($record_oly_lor) &&  count($record_oly_lor)>0){
			foreach($record_oly_lor as $reclor){
				$lor_rec = FCPATH.'crm_data/records/'.$leadid.'/'.$reclor['record_file'];
				array_push($recordsList,$lor_rec);
			}
		}
		
		//checking Questionnaire.
		$quest = $this->Lor_model->get_company_expireLOR($leadDet[0]['comp_code'],$this->config->item('crm')['questionnaire'],$leadDet[0]['ordering_office_id']);
		if(count($quest)>0){
			foreach($quest as $valqu){
				$quebase = UPLOAD_PATH.'lor_records/'.$valqu['lor_doc_file'];
				array_push($recordsList,$quebase);
			}
		}

		//checking fomr/records - only Questionnaire files.
		$record_oly_ques = $this->Welcome_model->get_records_list_w_Q($leadid);
		if(is_countable($record_oly_ques) && count($record_oly_ques)>0){
			foreach($record_oly_ques as $recques){
				$ques_rec = FCPATH.'crm_data/records/'.$leadid.'/'.$recques['record_file'];
				array_push($recordsList,$ques_rec);
			}
		}

		//checking form/records - only(All auth & special auth).
		$records_log = $this->Welcome_model->get_records_list_specific($leadid);
		if(is_countable($records_log) && count($records_log)>0){
			foreach($records_log as $rec){
				$recds = FCPATH.'crm_data/records/'.$leadid.'/'.$rec['record_file'];
				array_push($recordsList,$recds);
			}
		}

		$this->load->model('Pdf_model');
		$total_page_count = 0;
		foreach ($recordsList as $file1){
			$total_page_count += shell_exec('pdftk "'.$file1.'" dump_data | grep "NumberOfPages" | cut -d":" -f2');
		}
		// echo 'page count - '.$total_page_count.'<br>';

		$pdf = new FPDI();
		$filesize = 0;
		$mergepdf = ' ';
		foreach ($recordsList as $file){
			#check file extension is pdf
			$phpinfo = strtolower(pathinfo($file,PATHINFO_EXTENSION));
			if($phpinfo == 'pdf' && file_exists($file))
			{
				$mergepdf .= ' "'.$file.'"';
				$filesize += formatSizeUnits(filesize($file));
			}
		}

		// if(count($check_fax_cs)>0){
		$f_dir = FCPATH;
		if (!is_dir($f_dir))
		{
			mkdir($f_dir);
			chmod($f_dir, 0777);
		}
		$f_dir = $f_dir .'crm_data/printdocs/';
		if (!is_dir($f_dir))
		{
			mkdir($f_dir);
			chmod($f_dir, 0777);
		}
		$f_dir = $f_dir .''.$leadid;
		if (!is_dir($f_dir))
		{
			mkdir($f_dir);
			chmod($f_dir, 0777);
		}
		// print_R($mergepdf); echo '<br>';
		$curdat = date('Ymd_his');
		$output = shell_exec('pdftk '.$mergepdf.' 	cat output '.FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf');
		$merge_file=file(FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf');
		$endfile= trim($merge_file[count((array)$merge_file) - 1]);
		$n="%%EOF";		
		if ($endfile !== $n) {
			$curdat = date('Ymd_his');
			$output = shell_exec('pdftk '.$mergepdf.' 	cat output '.FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf');
                        $merge_file=file(FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf');
			$endfile= trim($merge_file[count((array)$merge_file) - 1]);
			$n="%%EOF";
			if ($endfile !== $n) {
				if (is_string($merge_file)) {
					unlink($merge_file);
				}
				$ins['order_no'] = $leadid;
				$ins['created_datetime'] = date('Y-m-d h:i:s');
				$ins['file_path'] = NULL;
				$ins['is_ready_for_print'] = 0;
				$ins['is_print_status'] = 0;
				$ins['page_count'] = $total_page_count;
				$ins['batch_log_id'] = NULL;
				$ins['is_payment'] = $is_pay_mtd;
				$ins['is_merge_failure'] = 1;
				$this->Welcome_model->insert_row('document_print_log',$ins);
				$mail=$this->sendMergeFailureMail($leadid);
				return false;
			}
		}
		// $output = shell_exec('pdftk '.$mergepdf.' 	cat output '.FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf');
		// echo 'output: '.$output;exit;
		// $filePath = 'printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf';
		
		// $curdatnew = date('Ymd_his'); // creating new file with watermark 
		
		shell_exec('pdftk '.FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf  stamp '.FCPATH.'crm_data/printdocs/Watermark.pdf output '.FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.
		'_'.$curdat.'.pdf');
		
		$filePath = 'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf';
		
		// unlink(FCPATH.'crm_data/printdocs/'.$leadid.'/document_'.$leadid.'_'.$curdat.'.pdf'); // removing before water mark file 
		
		if($output == NULL){
			$ins['order_no'] = $leadid;
			$ins['created_datetime'] = date('Y-m-d h:i:s');
			$ins['file_path'] = $filePath;
			$ins['is_ready_for_print'] = 0;
			$ins['is_print_status'] = 0;
			$ins['page_count'] = $total_page_count;
			$ins['batch_log_id'] = NULL;
			$ins['is_payment'] = $is_pay_mtd;
			$ins['is_merge_failure'] = 0;
			$this->Welcome_model->insert_row('document_print_log',$ins);
			return true;
		}else{
			return false;
		}
		// }else{
		// 	return 'Request Letter is not created.!';
		// }
	}
    //  Print Request Process Failure Notification Mail Start
	public function sendMergeFailureMail($leadid)
	{
		
		$subject = 'Print Request Process Failure Notification - Order # '.$leadid.'';
		$message = 'Dear Team,<br/>The Print Request Process is failed for the below Order <br />
					<ul>
						<li>Expert Order No :	'.$leadid.'</li>
						<li>Print Request  Generated On :	'.date('m-d-Y h:i:s').'</li>
						<li>Error Message 	: The Print Request Process is Failed While Merging.</li>
					</ul>
				<br />Regards,<br />Experts dev team';
		
		$from = $this->cfg['send_mail_from'];
		$fromname =$this->cfg['send_mail_from_name'];
		$send_mail_to = $this->cfg['print_job_to_mail'];
		$param['to_mail'] = $send_mail_to;
		$param['from_email'] = $from;
		$param['from_email_name'] = $fromname;
		$param['template_name'] = "Failure Notification";
		$param['content'] = $message;
		$param['subject'] = "UAT eXpert - $subject ";
		$param['email_value']="1";
		$this->load->model('Email_template_model');
		$this->Email_template_model->sent_email($param);
		return false;
	}
	//  Print Request Process Failure Notification Mail End
	function add_pay_records()
	{
		// exit('yes');
		$data = real_escape_array($this->input->post());
		if($data['payment_type']==1){
			$card_type = $data['card_type'];
			$user_card_type = $data['user_card_type'];
			$card_number = isset($data['card_number'])?$data['card_number']:0;
		}else{
			$card_type = 0;
			$user_card_type = 0;
			$card_number = 0;
		}
		$rst_prntLog = '';
		if($data['payment_type']==3 || $data['payment_type'] == 2){
			//Condition for e-check, traditional check with req packet-'yes'(Print docs).
			if($data['is_packet_required'] == 1){
				$rst_prntLog = $this->callDocPrintLog($data['quote_id'],1);
			}
			// die;
			$desp =  str_replace('\r', " ",str_replace('\n', "<br><br\>", $data['pay_description1']));
		}else{
			$desp = str_replace('\r', " ",str_replace('\n', "<br><br\>", $data['pay_description']));
		}
		if($data['payment_type'] == 2)
		{
			$check_payable_name = $data['cheque_payable_name1'];
		}else if($data['payment_type'] == 3)
		{
			$check_payable_name = $data['cheque_payable_name'];
		}else{
			$check_payable_name = '';
		}
		$user_id = $this->session->userdata['logged_in_user']['userid'];
		
		//Digital Vendors
		$digital_vendor_id = NULL;
		if($data['payment_to'] == 3)
		{
			if(isset($data['payment_vendor']))
				$digital_vendor_id = $data['payment_vendor'];
		}
      /*# get order details  
		$leadDet = $this->Welcome_model->get_lead_detail($data['quote_id']);
		$leadStage = $leadDet[0]['lead_stage'];
		#get active facility & copy service 
		$facility_copy_service = $this->Welcome_model->get_facility_copy_service_details($data['quote_id'],1,$leadDet[0]['company_type']);
		
		$facility_service = $this->Welcome_model->get_facility_history($data['quote_id'],$leadDet[0]['company_type'],1);
		
		#get old copy service details 
		$copy_service = $this->Welcome_model->get_copy_service_records($data['quote_id']);
		
		if(isset($facility_service))
			$active_facility_id = isset($facility_service[0]) ? $facility_service[0]['facility_id'] : $leadDet[0]['custid_fk'];
		else
			$active_facility_id = NULL;
		
		if(isset($facility_copy_service) && isset($copy_service))
			$active_copy_service = isset($facility_copy_service[0]) ? $facility_copy_service[0]['copy_service_id'] : $copy_service['copy_service_id'];
		else
			$active_copy_service = NULL;
		
		if($active_facility_id == NULL)
		{
			$active_facility_id = 0;
		}
		
		if($active_copy_service == NULL)
		{
			$active_copy_service = 0;
		}

		if(isset($data['approval_code']))
			$approval_code = $data['approval_code'];
		else
			$approval_code = '';
        */
		# get order details 
		$leadDet = $this->Welcome_model->get_lead_detail($data['quote_id']);
		$leadStage = $leadDet[0]['lead_stage'];
		#get active facility & copy service 
		$facility_copy_service = $this->Welcome_model->get_facility_copy_service_details($data['quote_id'],1,$leadDet[0]['company_type']);
		
		$facility_service = $this->Welcome_model->get_facility_history($data['quote_id'],$leadDet[0]['company_type'],1);
		
		#get old copy service details 
		$copy_service = $this->Welcome_model->get_copy_service_records($data['quote_id']);
		 
		$active_facility_id = isset($facility_service[0]) ? $facility_service[0]['facility_id'] : $leadDet[0]['custid_fk'];
		
		$active_copy_service = isset($facility_copy_service[0]) ? $facility_copy_service[0]['copy_service_id'] : $copy_service['copy_service_id'];
		
		if($active_facility_id == NULL)
		{
			$active_facility_id = 0;
		}
		
		if($active_copy_service == NULL)
		{
			$active_copy_service = 0;
		}
		
		$paymentDate = ($data['payment_date'])? date('Y-m-d',strtotime($data['payment_date'])) : NULL;
		$post_data = array('user_id'=>$user_id,'lead_id'=>$data['quote_id'],'rec_type_id'=>$data['record_type'],'payment_status_id'=>$data['payment_status'],'payment_type'=>$data['payment_type'],'completion_date'=>$paymentDate,'description'=>$desp,'paid_amount'=>$data['paid_amount'],'created_on' =>date("Y-m-d"),'card_type'=>$card_type,'user_card_type'=>$user_card_type,'card_number'=>$card_number,'cheque_payable_name'=>$check_payable_name,'cheque_number'=>$data['cheque_number'],'facility_id'=>$active_facility_id,'copy_service_id'=>$active_copy_service,'payment_to'=>$data['payment_to'],'invoice_number'=>$data['invoice_number'],'approval_code'=>$approval_code,'is_packet_required'=>$data['is_packet_required'],'invoice_required' => $data['payment_invoice_required'],'digital_vendor_id' => $digital_vendor_id);
		
		$rec_type_info = $this->Welcome_model->get_record_type_info($data['record_type']);
		$result = $this->Welcome_model->add_pay_records($post_data);

		if(isset($result))
		{
			if($post_data['payment_type']==1){$payment_type="Credit Card";}
			else if($post_data['payment_type']==2){$payment_type="E-Check";}
			else if($post_data['payment_type']==3){$payment_type="Traditional Check";}

			$callback_date = '';
			// if((isset($post_data['payment_invoice_required']) && $post_data['invoice_required'] == 1) || (isset($post_data['is_packet_required']) && $post_data['is_packet_required'] == 1) || (isset($post_data['check_number']) && $post_data['check_number'] != ''))
			// {
			// 	$updt['lead_stage'] = $this->config->item('crm')['check_payment_mailroom_stage'];
			// }
			# check payment 3 days call back date update
			// if($post_data['payment_type']==3)
			// {
			// 	if($leadStage != 36 && $leadStage != 37){	
			// 		$updt['proposal_expected_date'] = $this->get_business_days(date('d-m-Y'),3);
			// 	}
			// 	$updt['order_disposition_id'] = $this->config->item('crm')['check_payment_requested_disposition'];
			// 	if($post_data['payment_invoice_required'] == 0 || $post_data['is_packet_required'] == 0 || (isset($post_data['check_number']) && $post_data['check_number'] == ''))
			// 	{
			// 		$updt['lead_stage'] = $this->config->item('crm')['check_payment_requested_stage'];
			// 	}
			// }
			// else 
			
			if($leadStage != 36 && $leadStage != 37){	
				$updt['proposal_expected_date'] = $this->get_business_days(date('d-m-Y'),1);
			}
			$updt['order_disposition_id'] = $this->config->item('crm')['payment_made'];
			
			$updt['record_type'] = $this->config->item('crm')['medical_records'];
			// if($data['is_packet_required'] == 1 || $data['payment_invoice_required'] == 1){
			// 	$updt['lead_stage'] = $this->config->item('crm')['check_payment_mailroom_stage'];
			// 	$updt['order_disposition_id'] = $this->config->item('crm')['check_payment_created_disposition'];
			// }
			
			$this->Welcome_model->update_job($data['quote_id'],$updt);
			$leadDet = $this->Welcome_model->get_lead_det($data['quote_id']);
			
			$logs['jobid_fk'] = $data['quote_id'];
			$logs['userid_fk'] = $this->userdata['userid'];
			$logs['payment_id'] = $result;
			$logs['disposition_id'] = $leadDet['order_disposition_id'];
			$logs['lead_stage'] = $leadDet['lead_stage'];
			$logs['record_type'] = $leadDet['record_type'];
			$logs['callback_date'] = $leadDet['proposal_expected_date'];
			$logs['date_created'] = date('Y-m-d H:i:s');
			$logs['print_request_packet'] = $data['is_packet_required'];
			if($post_data['description']){
				$logs['log_content'] = "Payment made via"." ".$payment_type." "."$".number_format($post_data['paid_amount'],2)." "."for the"." ".$rec_type_info['rec_type_name'].'.<br/>'.ucfirst($post_data['description']);
			}else{
				$logs['log_content'] = "Payment made via"." ".$payment_type." "."$".number_format($post_data['paid_amount'],2)." "."for the"." ".$rec_type_info['rec_type_name'];
			}
					
			$getlist = $this->get_payment_records($data['quote_id'],'json');
			$insert_logs = $this->Welcome_model->insert_row('logs', $logs);

			//Hit post API to EOP, when check payment process.
			// if($post_data['payment_type']==3)
			// {
			// 	$cond['lead_stage_id'] = $leadDet['lead_stage'];
			// 	$getStage = $this->Manage_lead_stage_model->get_row('lead_stage', $cond);
			// 	$getEvntType = $this->Disposition_model->get_disposition($leadDet['order_disposition_id']);

			// 	$order_id = $data['quote_id'];
			// 	$trigger['eXpertLogId']  = $insert_logs;
			// 	$trigger['eNaohOrderId'] = $leadDet['aps_order_no'];
			// 	$trigger['companyCode']  = $leadDet['company_code'];
			// 	$trigger['companyType']  = $leadDet['company_type'];
			// 	$trigger['status'] 		 = $getStage[0]['lead_stage_name'];
				
			// 	$trigger['eventType']	 = $getEvntType[0]['disposition_name'];
			// 	$trigger['eventTypeId '] = $leadDet['order_disposition_id'];
			// 	$trigger['comments']  	 =  '';
			// 	$trigger['comments']  .= stripslashes(preg_replace('/\\\\/','',str_replace('<br/>', " ", $logs['log_content'])));

			// 	$result_postAPI = $this->post_trigger_api->addpost($trigger,$order_id);
			// 	if(is_object(json_decode($result_postAPI)))
			// 	{
			// 		$result1 = json_decode($result_postAPI);
			// 		$trigger_result = "Order #".$leadDet['aps_order_no'].' '.$result1->statusMessage;
			// 	}
			// 	else{
			// 		$trigger_result =  "Order #".$leadDet['aps_order_no'].' '.$result_postAPI;
			// 	}
			// }
			//End - for post API to EOP, when check payment process.
			
			echo json_encode(array('payment_id'=>$result,'lead_id'=>$data['quote_id'],'payment_type'=>$data['payment_type'],'card_type'=>$card_type,'paymentList'=>$getlist));exit;
		}
	}

	function add_service_records()
	{
		$data = real_escape_array($this->input->post());
		$user_id = $this->session->userdata['logged_in_user']['userid'];
		$post_data = array('lead_id'=>$data['quote_id'],'copy_service_id'=>$data['service_id']);
		
		// $rec_type_info = $this->Welcome_model->get_record_type_info($data['record_type']);
		$result = $this->Welcome_model->add_service_records($post_data);
		$service_name = $data['service_name'][0];
		// print_r($result);exit;
		if(isset($result))
		{			
			$leadDet = $this->Welcome_model->get_lead_det($data['quote_id']);
			$logs['jobid_fk'] = $data['quote_id'];
			$logs['userid_fk'] = $this->userdata['userid'];
			$logs['payment_id'] = 0;
			$logs['disposition_id'] = $leadDet['order_disposition_id'];
			$logs['lead_stage'] = $leadDet['lead_stage'];
			$logs['record_type'] = $leadDet['record_type'];
			$logs['callback_date'] = $leadDet['proposal_expected_date'];
			$logs['date_created'] = date('Y-m-d H:i:s');
			$logs['log_content'] = "Copy service -"." ".$service_name." "."has been added successfully";	
			$logs['copy_service_id'] = $result;	
			// echo'<pre>';print_r($logs);exit;
			$insert_logs = $this->Welcome_model->insert_row('logs', $logs);
			$this->get_service_records($data['quote_id']);
		}
	}
	
	function update_approval_code(){
		$data = real_escape_array($this->input->post());
		$post_data = array('approval_code'=>$data['approval_code']);
		$result = $this->Welcome_model->update_pay_records($post_data,$data['payment_id']);
		$this->get_payment_records($data['quote_id']);
	}
	public function cancelPayment(){
		$data = real_escape_array($this->input->post());
	
		$id = $data['payment_id'];
		// if($this->session->userdata('del')==1) {
		$this->Login_model->check_login();
		if(preg_match('/^[0-9]+$/', $id)) {
			$payData = $this->Welcome_model->get_payment_view($id);
			// update event type while payment cancelled.
			$updt['order_disposition_id'] = $this->config->item('crm')['payment_cancelled'];
			$updt['record_type'] = $this->config->item('crm')['medical_records'];
			$this->Welcome_model->update_job($payData['lead_id'],$updt);

			$leadDet = $this->Welcome_model->get_lead_det($payData['lead_id']);
			$dataCancelRsn = $this->Payment_cancel_model->get_cancel_reason($data['cancel_reason']);
			// $payData['digital_vendor_id']=$this->input->post('digital_vendor_id');
			$newId = $this->Welcome_model->cancel_payment($payData,$data['cancel_reason']);
			
			$logs['jobid_fk'] = $payData['lead_id'];
			$logs['userid_fk'] = $this->userdata['userid'];
			$logs['record_type'] = $leadDet['record_type'];
			$logs['payment_id'] = $newId;
			$logs['disposition_id'] = $leadDet['order_disposition_id'];
			$logs['lead_stage'] = $leadDet['lead_stage'];
			$logs['callback_date'] = $leadDet['proposal_expected_date'];
			$logs['date_created'] = date('Y-m-d H:i:s');
			$logs['log_content'] = "Payment Cancelled - [".$dataCancelRsn[0]['cancel_reason']."]";
			$insert_logs = $this->Welcome_model->insert_row('logs', $logs);
			$this->get_payment_records($payData['lead_id']);
			
		}
		// }else{
		// 	$this->session->set_flashdata('login_errors', array("You have no rights to access this page"));
		// 	redirect('welcome');
		// }
	}

	function update_pay_records()
	{
		$data = real_escape_array($this->input->post());
		$compDate = ($data['payment_date'])? date('Y-m-d',strtotime($data['payment_date'])) : NULL;
		$payment_type = ($data['payment_type']) ? $data['payment_type'] : 0;
		
		if($payment_type==3 || $payment_type == 2){
			$post_data = array('cheque_number'=>$data['cheque_number'],'completion_date'=>$compDate,'is_packet_required'=>$data['is_packet_req'],'invoice_required'=>$data['payment_inv_req'],'description'=>$data['description']);
		}else{
			$post_data = array('cheque_number'=>$data['cheque_number'],'is_packet_required'=>$data['is_packet_req'],'invoice_required'=>$data['payment_inv_req']);
		}
		// $post_data = array('rec_type_id'=>$data['record_type'],'payment_status_id'=>$data['payment_status'],'payment_type'=>$data['payment_type'],'completion_date'=>date('Y-m-d',strtotime($data['payment_date'])),'description'=>$data['pay_description'],'paid_amount'=>$data['paid_amount'],'cheque_payable_name'=>$data['cheque_payable_name']);
		$result = $this->Welcome_model->update_pay_records($post_data,$data['payment_id']);
		if(isset($result))
		{
			// if($post_data['payment_type']==1){$payment_type="Credit Card";}
			// else if($post_data['payment_type']==2){$payment_type="Online Payment";}
			// else if($post_data['payment_type']==3){$payment_type="Cheque";}
			
			//$proposal_expected_date = date('Y-m-d H:i:s');
			
			$leadDet = $this->Welcome_model->get_lead_det($data['quote_id']);
			// $leadStage = $leadDet['lead_stage'];
			# check payment update event type
			// if($data['payment_type'] == 3) 
			// {
			// 	if($leadStage != 36 && $leadStage != 37){
			// 		$updt['proposal_expected_date'] = $this->get_business_days(date('d-m-Y'),3);
			// 	}
			// 	if($data['cheque_number'] != ''){
			// 		$updt['order_disposition_id'] = $this->config->item('crm')['check_payment_created_disposition'];
			// 		$updt['lead_stage'] = $this->config->item('crm')['check_payment_mailroom_stage'];
			// 	}else{
			// 		$updt['order_disposition_id'] = $this->config->item('crm')['check_payment_requested_disposition'];
			// 		$updt['lead_stage'] = $this->config->item('crm')['check_payment_requested_stage'];
			// 	}
			// 	$this->Welcome_model->update_job($data['quote_id'],$updt);
			// } 

			# check payment 3 days call back date update
			/*if($post_data['payment_type']==3)
			{
				$proposal_expected_date = $this->get_business_days(date('d-m-Y'),3).' 00:00:00';
			}
			else if($post_data['payment_type'] == 1)
			{
				$proposal_expected_date = $this->get_business_days(date('d-m-Y'),1).' 00:00:00';
			} */
			// $rec_type_info = $this->Welcome_model->get_record_type_info($data['record_type']);
			$leadDet = $this->Welcome_model->get_lead_det($data['quote_id']);
			$logs['jobid_fk'] = $data['quote_id'];
			$logs['userid_fk'] = $this->userdata['userid'];
			$logs['record_type'] = $leadDet['record_type'];
			$logs['payment_id'] = $data['payment_id'];
			$logs['disposition_id'] = $leadDet['order_disposition_id'];
			$logs['lead_stage'] = $leadDet['lead_stage'];
			//$logs['callback_date'] = $proposal_expected_date;
			$logs['callback_date'] = $leadDet['proposal_expected_date'];
			$logs['date_created'] = date('Y-m-d H:i:s');
			if($data['description']){
				if($payment_type == 3)
				{
					$logs['log_content'] = "Traditional Check Number/Packet Request/Invoice Request updated successfully".'.<br/>'.ucfirst($data['description']);
				}else if($payment_type == 2){
					$logs['log_content'] = "E-Check Number/Packet Request/Invoice Request updated successfully".'.<br/>'.ucfirst($data['description']);
				}
				
			}else{
				if($payment_type == 3)
				{
					$logs['log_content'] = "Traditional Check Number/Packet Request/Invoice Request updated successfully.";
				}else if($payment_type == 2){
					$logs['log_content'] = "E-Check Number/Packet Request/Invoice Request updated successfully.";
				}
			}
			
			// echo'<pre>';print_r($logs);exit;
			$insert_logs = $this->Welcome_model->insert_row('logs', $logs);

			//Hit post API to EOP, when check payment process.
			// if($data['payment_type']==3)
			// {
			// 	$cond['lead_stage_id'] = $leadDet['lead_stage'];
			// 	$getStage = $this->Manage_lead_stage_model->get_row('lead_stage', $cond);
			// 	$getEvntType = $this->Disposition_model->get_disposition($leadDet['order_disposition_id']);

			// 	$order_id = $data['quote_id'];
			// 	$trigger['eXpertLogId']  = $insert_logs;
			// 	$trigger['eNaohOrderId'] = $leadDet['aps_order_no'];
			// 	$trigger['companyCode']  = $leadDet['company_code'];
			// 	$trigger['companyType']  = $leadDet['company_type'];
			// 	$trigger['status'] 		 = $getStage[0]['lead_stage_name'];
				
			// 	$trigger['eventType']	 = $getEvntType[0]['disposition_name'];
			// 	$trigger['eventTypeId '] = $leadDet['order_disposition_id'];
			// 	$trigger['comments']  	 =  '';
			// 	$trigger['comments']  .= stripslashes(preg_replace('/\\\\/','',str_replace('<br/>', " ", $logs['log_content'])));

			// 	$result_postAPI = $this->post_trigger_api->addpost($trigger,$order_id);
			// 	if(is_object(json_decode($result_postAPI)))
			// 	{
			// 		$result1 = json_decode($result_postAPI);
			// 		$trigger_result = "Order #".$leadDet['aps_order_no'].' '.$result1->statusMessage;
			// 	}
			// 	else{
			// 		$trigger_result =  "Order #".$leadDet['aps_order_no'].' '.$result_postAPI;
			// 	}
			// }
			//End - for post API to EOP, when check payment process.
			$this->get_payment_records($data['quote_id']);
		}
	}
	/**
	* Get Business Days
	* @param $from_date date string
	* @param $to_date date string
	* 
	* @return businessdays int 
	*/
	public function get_business_days($from_date,$interval)
	{
		$start = new DateTime($from_date);
		// $end   = new DateTime($to_date);

		$this->load->helper('us_holidays_helper');
		$this->holiday = getUSHolidays();

		$holiday = $this->holiday;
		$new = array();
		
		foreach($holiday as $array) 
		{
			$new[$array['leave_date']] = $array['holiday'];
		}
		//list of holidays from the given date
		$newarray = array();
		foreach ($new as $item=>$value){
			if ($item >= $from_date){
				$newarray[] = $item;
			}
		}
		$holidays = $newarray;

		$weekend = array('Sun','Sat');
		$nextDay = clone $start;
		if (in_array($nextDay->format('Y-m-d'), $holidays) || in_array($nextDay->format('D'), $weekend))
		{
			$nextDay = $nextDay->sub(new DateInterval('P1D'));
		}
		$i = 0; // We have 0 future dates to start with
		$nextDates = array(); // Empty array to hold the next 3 dates
		while ($i < $interval)
		{
			$nextDay->add(new DateInterval('P1D')); // Add 1 day
			if (in_array($nextDay->format('Y-m-d'), $holidays)) continue; // Don't include year to ensure the check is year independent
			// Note that you may need to do more complicated things for special holidays that don't use specific dates like "the last Friday of this month"
			if (in_array($nextDay->format('D'), $weekend)) continue;
			// These next lines will only execute if continue isn't called for this iteration
			$nextDates = $nextDay->format('Y-m-d');
			$i++;
		}
		return $nextDates;
	}
	
	function get_payment_records($lead_id,$type = '')
	{
		$this->load->helper('text');
		$records = $this->Welcome_model->get_payment_records_list($lead_id);
		$arr_RefPayId=array();
		foreach($records as $val){
			if($val['ref_payment_id'] != ''){
				array_push($arr_RefPayId,$val['ref_payment_id']);
			}
		}
		$html = "<table class='data-table' style='width: 150%;'>";
		$html .= "<tbody>";
		$html .= "<tr><th>Record Type</th><th>Received Status</th><th>Payment Type</th><th>Completion Date</th><th>Amount</th><th>Paid To</th><th>Created On</th><th>Created By</th><th>Action</th></tr>";
		foreach($records as $rec){
			if($rec['payment_type']==CREDIT_CARD){$payment_type="Credit Card";}
			else if($rec['payment_type']==ONLINE_PAYMENT){$payment_type="E-Check";}
			else if($rec['payment_type']==CHEQUE){$payment_type="Traditional Check";}
			$comp_date = ($rec['completion_date'] != NULL)? date('m/d/Y',strtotime($rec['completion_date'])) : NULL;
			$html .= "<tr>";
			$html .= "<td>".preg_replace('/\\\\/','',$rec['rec_type_name'])."</td>";
			$html .= "<td>".preg_replace('/\\\\/','',$rec['payment_status_name'])."</td>";
			$html .= "<td>".$payment_type."</td>";
			$html .= "<td>".$comp_date."</td>";
			if($rec['sign_amount_type'] == 1){
				$html .= "<td>"."-$".$rec['paid_amount']."</td>";
			}else{
				$html .= "<td>"."$".$rec['paid_amount']."</td>";
			}
			if($rec['mib_payment_to'] >= 3){
				$digital_vendors_used_list = $this->Company_list_model->getDigitalVendorWithReferenceNumber($rec['mib_payment_to']);
				if(!empty($digital_vendors_used_list)){
					$facility=$digital_vendors_used_list[0]['digital_vendor_name'];
				}
			}else{
				$facility=ltrim(ltrim($rec['paid_to']," "),"-");
				$facility=rtrim(rtrim($facility," "),"-");
			}
			$html .= "<td>".preg_replace('/\\\\/','',$facility)."</td>";
			$html .= "<td>".$rec['created_on']."</td>";
			$html .= "<td>".$rec['created_by']."</td>";
			
			if(($rec['sign_amount_type'] == 0) && !in_array($rec['payment_id'],$arr_RefPayId)){
				if($rec['edit_option']=='Edit'){
					$html .= "<td style='width: 50px;'><a style='padding-right: 4px;' onclick='edit_pay_records(".$rec['payment_id'].");' href='javascript:void(0)'>Edit</a>
					<a onclick='view_paid_records(".$rec['payment_id'].")' href='javascript:void(0)'>View</a><br>
					<a onclick='cancel_pay_records(".$rec['payment_id'].");' href='javascript:void(0)'>Cancel</a></td>";
				}else{
					$html .= "<td style='width: 50px;'><a onclick='view_paid_records(".$rec['payment_id'].")' href='javascript:void(0)'>View</a><br>
					<a onclick='cancel_pay_records(".$rec['payment_id'].");' href='javascript:void(0)'>Cancel</a></td>";
				}
			}else{
				if($rec['edit_option']=='Edit'){
					$html .= "<td style='width: 50px;'><a style='padding-right: 4px;' onclick='edit_pay_records(".$rec['payment_id'].");' href='javascript:void(0)'>Edit</a>
					<a onclick='view_paid_records(".$rec['payment_id'].")' href='javascript:void(0)'>View</a></td>";
				}else{
					$html .= "<td><a onclick='view_paid_records(".$rec['payment_id'].")' href='javascript:void(0)'>View</a></td>";
				}
			}
			//$html .= "<td><a onclick='edit_pay_records(".$rec['payment_id'].");' href='javascript:void(0)'>Edit</a></td>";
			if($this->userdata['role_id'] == 29 && in_array($this->userdata['email'],$this->cfg['developer_admin']))
			{
				$html .= " <a onclick='delete_pay_records(".$rec['payment_id'].",".$lead_id.");' href='javascript:void(0)'>Delete</a>";
			}
			$html .= "</td></tr>";
		}
		$html .= "</tbody>";
		$html .= "</table>";
		if($type != '')
		{
			return $html;
		}else{
			echo $html; exit;
		}
	}
	
	public function edit_payment_record($payment_id)
	{
		$data['pay_recds'] = $this->Welcome_model->get_payment_view($payment_id);
		$leadDet = $this->Welcome_model->get_lead_det($data['pay_recds']['lead_id']);
		$data['record_type'] = $this->Welcome_model->get_record_type();	
		$data['payment_status'] = $this->Welcome_model->get_payment_status();
		//$data['get_total_topup_balance'] = $this->Welcome_model->get_total_topup_balance();
		//$data['get_total_debit_amount'] = $this->Welcome_model->get_total_debit_amount();
		//$data['balance_amount'] = $data['get_total_topup_balance'] - $data['get_total_debit_amount'];
		$data['balance_amount'] =  10000000000000;
		$getCpySer = $this->Welcome_model->get_active_facility_copyService($leadDet['custid_fk'],$leadDet['company_type']);
		$data['Cnt_copySer'] = $getCpySer;
		$data['active_facility'] = $this->Welcome_model->get_facility_history($leadDet['lead_id'],$leadDet['company_type'],1);
		$data['active_copy_service'] = $this->Welcome_model->get_mapped_copy_service($leadDet['custid_fk'],$leadDet['company_type'],1);

		//KK
		$data['digital_vendors_used_list'] = array();
        if($data['pay_recds']['leads_company_code']){
            $company_code = $data['pay_recds']['leads_company_code'];
            $company_id = $this->Company_list_model->get_company_id_by_company_code($company_code);
            $data['digital_vendors_used_list']  = $this->Company_list_model->get_particular_approved_digital_vendor($company_id);
        }
		$this->load->view("leads/update_pay_records", $data);
	}
	
	public function cancel_payment_record($payment_id)
	{
		$data['pay_recds'] = $this->Welcome_model->get_payment_view($payment_id);
		$this->load->view("helper/cancel_payment", $data);
	}
	
	public function refresh_topup_balance()
	{
		//$data['get_total_topup_balance'] = $this->Welcome_model->get_total_topup_balance();
		//$data['get_total_debit_amount'] = $this->Welcome_model->get_total_debit_amount();
		//$data['balance_amount'] = $data['get_total_topup_balance'] - $data['get_total_debit_amount'];
		$data['balance_amount'] = 1000000000000;
		// $data['balance_amount'] = -10;
		// $data['balance_amount'] = number_format($data['balance_amount'],2);
		// echo'<pre>';print_r($data['balance_amount']);exit;
		echo json_encode($data['balance_amount']);
		exit;
	}
	// Ends here
	
	
	/**
	* sendFailureQRcodeMail 
	* @params $job_id string 
	* @params $aps_order_no string
	* @params $company_code string
	*/
	public function sendFailureQRcodeMail($job_id,$aps_order_no,$company_code,$message = '',$record_id = '',$fileName = '', $company_name='')
	{
		// $reporting_mail_to = implode(',',$this->cfg['aps_summarization_processed_mail']);
		
		$subject = 'QR Code Generation Failure Notification - Order # '.$job_id.' - '.$company_code.'';
		//121 $get_lead = $this->Welcome_model->get_lead_detail($job_id);
		$message = 'Dear Team,<br/>The QR code generation in Request letter is failed for the below Order'.$message.'<br />
					<ul>
						<li>Expert Order No :	'.$job_id.'</li>
						<li>Enoah Order No 	:	'.$aps_order_no.'</li>
						<li>Company Code 	:	'.$company_code.'</li>
						<li>Company Name 	:	'.preg_replace('/\\\\/','',$company_name).'</li>
						<li>Request Letter Generated On :	'.date('m-d-Y h:i:s').'</li>
						<li>Error Message 	: Get QR-Image method failure.</li>
					</ul>
				<br />Regards,<br />Experts dev team';
				//<li>Request Letter Generation Mode :'.$fileName.'</li>
		$post_data = array('expert_order_id'=>$job_id,'enoah_order_id'=>$aps_order_no,'log_content'=>$message,'company_code'=>$company_code,'status'=>0,'created_at'=>date('Y-m-d H:i:s'),'relqrcode_user_id'=>$this->userdata['userid']);
		$this->Welcome_model->requestLetter_QRcode_log($post_data);
		
		// $from='webmaster@enoahprojects.com';
		// $fromname = 'Webmaster';
		$from = $this->cfg['send_mail_from'];
		$fromname =$this->cfg['send_mail_from_name'];
		$send_mail_to = array('sisaravanan@enoahisolution.com','sjegansundharam@enoahisolution.com');
		$param['to_mail'] = implode(',',$send_mail_to);
		$param['from_email'] = $from;
		$param['from_email_name'] = $fromname;
		$param['template_name'] = "Failure Notification";
		$param['content'] = $message;
		//121 $param['subject'] = "QA eXpert - Failure Notification";
		$param['subject'] = "UAT eXpert - $subject ";
	
		$this->load->model('Email_template_model');
		$this->Email_template_model->sent_email($param);
	}
	
	public function prepare_cover_sheet($leadid = NULL){
		
	
		// $knights=$this->cfg['knights'];		

		$order_id = $this->input->post('id');
		// echo $order_id;exit;
		$pdf_check_id = $this->input->post('pdf_check_id');
		if($leadid != NULL){
			$order_id = $pdf_check_id = $leadid;  
		}
		$file_name = "cover_sheets/Request_Letter_".date('mdY').'_'.$pdf_check_id.".pdf";
		
		// if(file_exists(FCPATH.$file_name)==0){
		$post_data = array('jobid'=>$order_id,'is_coversheet'=>1,'created_on'=>date('Y-m-d H:i:s'),'userid'=>$this->userdata['userid']);
		
		$coversheet_records = $this->Welcome_model->coversheet_records($post_data);
		// $this->db->where('jobid', $order_id);
		//$decode_order_id= urldecode($order_id);
		$leadDet = $this->Welcome_model->get_lead_det($order_id);
		$getPdfDet = $this->Welcome_model->get_pdf_detail($order_id,$leadDet['company_type']);

		$qr_request_json = array();
		$qr_request_json['caseid'] = $leadDet['aps_order_no'].$leadDet['order_sub_no'];
		$qr_request_json['carriername'] = trim($leadDet['company_code']);
		$aps_order_no1 = $getPdfDet[0]['aps_order_no'].$getPdfDet[0]['order_sub_no'];
		$qr_code_sec = '';
		# request qr code from eRecord Review 22/03/2022 vanitha.k
		$this->load->library('CQRCodeOne');
		if($this->config->item('crm')['enable_erecord_review'] == 1)
		{
			/*121 $this->load->library('Request_qr_code');
			$api_response = $this->request_qr_code->call_method('request_qr_code',$qr_request_json);

			$api_response = json_decode($api_response,true);
			//$api_response = '{"Results":[{"URL":"eRecordReview/QRcode/12345-01/QRImageMar20222111550655.jpg"}],"code":"200","status":"true","message":"QR Generated Successfully!"}';

			
			if(isset($api_response['Data']['code']) && $api_response['Data']['code'] == 200)
			{
				if(isset($api_response['Data']['Results'][0]['URL']) && $api_response['Data']['Results'][0]['URL'] != '')
				{
					$f_dir = 'uploads/qr_code/';
					if (!is_dir($f_dir)) {
						mkdir($f_dir);
						chmod($f_dir, 0777);
					}
					$image_path = $this->Azure_model->azure_download($api_response['Data']['Results'][0]['URL'],'uploads/qr_code/'.$order_id.'.png');
				}
				//121 $qr_code_sec = "<td><div align='center'> <img src=".FCPATH."uploads/qr_code/".$order_id.".png style='width:100px;height:100px;'></div></td>";
			}
			*/
			
			// QR Code generate and Integrate in pdf file 13-10-2022 saravanan.si
				$f_dir = 'uploads/qr_code/';
				if (!is_dir($f_dir)) {
					mkdir($f_dir);
					chmod($f_dir, 0777);
				}
						
				//121 $textcontent = '{"Caseid":"'.$leadDet['aps_order_no'].$leadDet['order_sub_no'].'","CarrierName":"'.trim($leadDet['company_code']).'"}';
				
				$textcontent = '{"Caseid":"'.$leadDet['aps_order_no'].$leadDet['order_sub_no'].'","Company_Code":"'.trim($leadDet['company_code']).'"}';
				$path = FCPATH."uploads/qr_code/";
				$file = $path."$order_id.jpg";
				
				$qr2 = new CQRCodeOne($textcontent,ECL_L);
				$qr2->getQRImg("JPG","$order_id");
				if(file_exists($file)){
					$post_data = array('expert_order_id'=>$order_id,'enoah_order_id'=>$aps_order_no1,'log_content'=>'Successfully Generated QR Code','company_code'=>$leadDet['company_code'],'status'=>1,'created_at'=>date('Y-m-d H:i:s'),'relqrcode_user_id'=>$this->userdata['userid']);
					$this->Welcome_model->requestLetter_QRcode_log($post_data);
					$qr_code_sec = "<td><div align='center'><img src='$file' style='width:100px;height:100px;'></div></td>";
					
				}else{
					$this->sendFailureQRcodeMail($order_id,$aps_order_no1,$leadDet['company_code'],$message = '',$record_id = '',$fileName = '', $getPdfDet[0]['carrier_name']);
					$qr_code_sec = "";
				}

				// echo "if ".$qr_code_sec; exit;
				
				
			// END
		}else{
		
			// QR Code generate and Integrate in pdf file 13-10-2022 saravanan.si alt
				$f_dir = 'uploads/qr_code/';
				if (!is_dir($f_dir)) {
					mkdir($f_dir);
					chmod($f_dir, 0777);
				}
				//121 $textcontent = '{"Caseid":"'.$leadDet['aps_order_no'].$leadDet['order_sub_no'].'","CarrierName":"'.trim($leadDet['company_code']).'"}';
				
				$textcontent = '{"Caseid":"'.$leadDet['aps_order_no'].$leadDet['order_sub_no'].'","Company_Code":"'.trim($leadDet['company_code']).'"}';
				$path = FCPATH."uploads/qr_code/";
				$file = $path."$order_id.jpg";				
				$qr2 = new CQRCodeOne($textcontent,ECL_L);
				$qr2->getQRImg("JPG","$order_id");
				
				if(file_exists($file)){
					$post_data = array('expert_order_id'=>$order_id,'enoah_order_id'=>$aps_order_no1,'log_content'=>'Successfully Generated QR Code','company_code'=>$leadDet['company_code'],'status'=>1,'created_at'=>date('Y-m-d H:i:s'),'relqrcode_user_id'=>$this->userdata['userid']);
					$this->Welcome_model->requestLetter_QRcode_log($post_data);
					$qr_code_sec = "<td><div align='center'><img src='$file' style='width:100px;height:100px;'></div></td>";					
				}else{
					$this->sendFailureQRcodeMail($order_id,$aps_order_no1,$leadDet['company_code'],$message = '',$record_id = '',$fileName = '', $getPdfDet[0]['carrier_name']);
					$qr_code_sec = "";
				}

				// echo "else ".$qr_code_sec; exit;
				
				
				
			// END
		}
		$dob = $getPdfDet[0]['patient_dob'];	
		if(!empty($dob)){
			$pdob= $this->sha2lib->sha2decrypt($dob);
			$patient_dob = date('m/d/Y', strtotime($pdob));
		}else{
			$patient_dob = NULL;
		}
		if(isset($patient_dob) && !empty($patient_dob)){
			$patient_dob = date('m/d/Y', strtotime($pdob));
		}
		else{
			$patient_dob ='N\A';
		}
		$patient_firstname=  preg_replace('/\\\\/','',$getPdfDet[0]['lead_title']).' '.preg_replace('/\\\\/','',$getPdfDet[0]['lead_title_last']);
		
		if(isset($patient_firstname) && !empty($patient_firstname)){
			$patient_firstname= preg_replace('/\\\\/','',$getPdfDet[0]['lead_title']).' '.preg_replace('/\\\\/','',$getPdfDet[0]['lead_title_last']);
		}
		else{
			$patient_firstname ='N\A';
		}
		
		if(!empty($getPdfDet[0]['ss_no'])){
			$ssn_no = $this->sha2lib->sha2decrypt($getPdfDet[0]['ss_no']);
		}else{
			$ssn_no = NULL;
		}
		
		if(isset($ssn_no) && !empty($ssn_no)){
			$temp_ssn = $this->sha2lib->sha2decrypt($getPdfDet[0]['ss_no']);
			$ssn_no = ($temp_ssn)? substr_replace($temp_ssn,'XXX-XX-',0,7) : '';
		}
		else{
			$ssn_no ='N\A';
		}
		$patient_addr = preg_replace('/\\\\/','',$getPdfDet[0]['patient_adr']);
		$patient_address = preg_replace('/\\\\/','',(string) $getPdfDet[0]['add1_line1']).' '.preg_replace('/\\\\/','',(string) $getPdfDet[0]['add1_line2']).' '.preg_replace('/\\\\/','',(string) $getPdfDet[0]['add1_suburb']).', '.(string) $getPdfDet[0]['region_name'].', '.(string) $getPdfDet[0]['country_name'].', '.(string) $getPdfDet[0]['state_name'].', '.(string) $getPdfDet[0]['add1_postcode'];
		
		if(isset($patient_address) && !empty($patient_address)){
			$patient_address = preg_replace('/\\\\/','',(string) $getPdfDet[0]['add1_line1']).' '.preg_replace('/\\\\/','',(string) $getPdfDet[0]['add1_line2']).' '.preg_replace('/\\\\/','',(string) $getPdfDet[0]['add1_suburb']).', '.(string) $getPdfDet[0]['region_name'].', '.(string) $getPdfDet[0]['country_name'].', '.(string) $getPdfDet[0]['state_name'].', '.(string) $getPdfDet[0]['add1_postcode'];
		}
		else{
			$patient_address ='N\A';
		}		
		$orderref = preg_replace('/\\\\/','',$getPdfDet[0]['orderref']);
		if(isset($orderref) && !empty($orderref)){
			$orderref = preg_replace('/\\\\/','',$getPdfDet[0]['orderref']);
		}
		else{
			$orderref = 'N\A';
		}
		
		if($getPdfDet[0]['first_name'] !='' && $getPdfDet[0]['company'] !=''){
		$doc_name = preg_replace('/\\\\/','',$getPdfDet[0]['first_name']).' '.preg_replace('/\\\\/','',$getPdfDet[0]['last_name']).'/ '.preg_replace('/\\\\/','',$getPdfDet[0]['company']);		
		}else if($getPdfDet[0]['first_name'] !='' && $getPdfDet[0]['company'] ==''){
			$doc_name = preg_replace('/\\\\/','',$getPdfDet[0]['first_name']).' '.preg_replace('/\\\\/','',$getPdfDet[0]['last_name']);
		}else if($getPdfDet[0]['first_name'] =='' && $getPdfDet[0]['company'] !=''){
			$doc_name = preg_replace('/\\\\/','',$getPdfDet[0]['company']);
		}
		$city_st_zp= preg_replace('/\\\\/','',$getPdfDet[0]['p_city']).', '.$getPdfDet[0]['p_state'].' '.$getPdfDet[0]['p_zipcode'];
		$doc_ci_st_addr= preg_replace('/\\\\/','',$getPdfDet[0]['location_name']).', '.preg_replace('/\\\\/','',$getPdfDet[0]['state_name']).' '.$getPdfDet[0]['add1_postcode'];

		$phone_1= $getPdfDet[0]['phone_1'];
		$ower_name= preg_replace('/\\\\/','',$getPdfDet[0]['ownfname']).' '.preg_replace('/\\\\/','',$getPdfDet[0]['ownlname']);
		
		if(isset($ower_name) && !empty($ower_name)){
			$ower_name= preg_replace('/\\\\/','',$getPdfDet[0]['ownfname']).' '.preg_replace('/\\\\/','',$getPdfDet[0]['ownlname']);
		}
		else{
			$ower_name= 'N\A';
		}
		
		$fee_amount= $getPdfDet[0]['expect_worth_amount'].' '.$getPdfDet[0]['expect_worth_amount'];
		
		$special_inst = preg_replace('/\\\\/','',$getPdfDet[0]['special_instructions']);
		$aps_order_no = $getPdfDet[0]['aps_order_no'].$getPdfDet[0]['order_sub_no'];
		if(isset($fee_amount) && !empty($fee_amount)){
			$fee_amount= $getPdfDet[0]['expect_worth_amount'];
		}
		else{
			$fee_amount= 'N\A';
		}
		$img= $this->load->helper('Url');
		$date = date("Y");
		$current_date= date('m/d/Y');
		$this->load->library('Pdf');
		$html = "<!DOCTYPE html>
			<html>
			
			<head>
				<title>Cover Sheet</title>
			</head>
			<style>
				header {
					position: fixed;
					top: 0cm;
					left: 0cm;
					right: 0cm;
					height: 3cm;
				}
			
				/* Define the footer rules */
				footer {
					position: fixed;
					bottom: 0cm;
					left: 0cm;
					right: 0cm;
					height: 2.5cm;
				}
			
				footer h6 {
					font-family: 'Calibri';
					color: #666666;
					font-size:12px;
					font-weight: normal;
				}
			
				body {
					font-family: 'Calibri';
				}
				
				main .heading,
				header .heading,
				main .desc_text,
				header .desc_text{
					font-family: 'Calibri';
					font-size: 16px;
					font-weight: bold;
				}
				main .desc_text,
				header .desc_text{
					font-size: 14px;
					font-weight: normal;
				}
			</style>
			
			<body>
				<header>
					<table width='100%' style='margin-top:20px;'>
						<tr>
							<td>
								<div align='left'> <img src='assets/img/new-logo.png' style='width:125px'></div>
							</td>".$qr_code_sec."<td align='right' style='float:right; text-align:right;'>
								<table style='float:right; text-align:right;'>
									<tr>
										<td style='text-align:left;'>
											<h4 class='heading' style='margin:2px 0'> Date</h4>
										</td>
										<td class='desc_text' style='text-align:left;'> " . $current_date . "</td>
									</tr>
									<tr>
										<td>
											<h4 class='heading' style='margin:2px 0'>Case ID</h4>
										</td>
										<td class='desc_text' style='text-align:left;'> ".$aps_order_no."</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</header>
				<footer>
					<h6>The documents in this transmission contain confidential health information that is privileged and legally protected from disclosure by federal
						law, including the Health Insurance Portability and Accountability Act (HIPAA). This information is for the
						sole use of the individual or entity named above. If you are not the intended recipient, you are hereby
						notified that reading, disseminating, disclosing, distributing, copying, acting upon, or otherwise using the
						information contained in this transmission is strictly prohibited. If you received this information in
						error, please notify the sender immediately and destroy this document.</h6>
				</footer>
				<main style='margin-top:3.5cm;'>
					<h3 style='text-align:center; font-family: Calibri;'>**Request for Records**</h3>
					<hr>
					<div>
						<table style='width:100%; margin:auto;'>
							<tr>
								<td width='50%' style='padding:0px;'>
									<table>
										<tr>
											<td>
												<h4 class='heading' style='margin:2px 0'> Patient Name</h4>
											</td>
											<td class='desc_text'> ".$patient_firstname."</td>
										</tr>
										<tr>
											<td>
												<h4 class='heading' style='margin:2px 0'>DOB</h4>
											</td>
											<td class='desc_text'> " . $patient_dob . "</td>
										</tr>
										<tr>
											<td>
												<h4 class='heading' style='margin:2px 0'> SSN</h4>
											</td>
											<td class='desc_text'> " . $ssn_no . "</td>
										</tr>
										<tr>
											<td	>
												<h4 class='heading' style='margin:2px 0'>Policy/Claim #</h4>
											</td>
											<td class='desc_text'> " . $orderref . "</td>
										</tr>
									</table>
								</td>
								<td valign='top'>
									<table>
										<tr>
											<td style='width:90px;'>
												<h4 class='heading' style='margin:2px 0'> Address</h4>
											</td>
											<td class='desc_text'> ".preg_replace('/\\\\/','',$getPdfDet[0]['patient_adr'])."</td>
										</tr>
										<tr>
											<td style='width:90px;'>
												<h4 class='heading' style='margin:2px 0'> City, ST, Zip</h4>
											</td>
											<td class='desc_text'> " . $city_st_zp . "</td>
										</tr>
										<tr>
											<td style='width:90px;'>
												<h4 class='heading' style='margin:2px 0'> Phone</h4>
											</td>
											<td class='desc_text'> " . $phone_1 . "</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<hr>
					
					<table>
						<tr>
							<td style='padding:0'>
								<p style='margin:0 0 10px 0;'>".preg_replace('/\\\\/','',$special_inst)." from:</p>
							</td>
						</tr>
					</table>
					
					
					<table style='width:100%;margin:auto;'>
					<tr>
						<td style='width:90px; padding:0;'>
								<h4 class='heading' style='margin:2px 0'>".preg_replace('/\\\\/','',$doc_name)."</h4>
							</td>									
						</tr>
						<tr>
							<td width='50%' style='padding:0'>
								<table width='100%'>
									
									<tr>
										<td style='width:90px; padding:0;'>
											<h4 class='heading' style='margin:2px 0'>Address</h4>
										</td>
										<td class='desc_text'> ".preg_replace('/\\\\/','',$getPdfDet[0]['add1_line1'])."</td>
									</tr>
									<tr>
										<td style='width:90px; padding:0;'>
											<h4 class='heading' style='margin:2px 0'>City, ST, Zip</h4>
										</td>
										<td class='desc_text'> ".preg_replace('/\\\\/','',$doc_ci_st_addr)."</td>
									</tr>
								</table>
							</td>							
						</tr>
					</table> 					
					
					<hr>					
					<table>
						<tr>
							<td style='padding:0;'>
								<p style='margin:0 0 10px 0;'>Records are needed as quickly as possible as your patient has applied for life insurance, filed a claim, or has a pending legal matter. Please release records to:</p>
							</td>
						</tr>
					</table>					
					<table style='width:100%;margin:auto;'>
						<tr>
							<td width='50%' style='padding:0'>
								<table width='100%'>
									<tr style='border:2px red'>
										<td style='width:90px; padding:0;'>
											<h4 class='heading' style='margin:2px 0'>Return To</h4>
										</td>
										<td class='desc_text'> eNoah iSolutions, Inc.</td>
									</tr>
									
									<tr>
										<td style='width:90px; padding:0;'>
											<h4 class='heading' style='margin:2px 0'>PO Box</h4>
										</td>
										<td class='desc_text'> 23449</td>
									</tr>
									<tr>
										<td style='width:90px; padding:0;'>
											<h4 class='heading' style='margin:2px 0'>City, ST, Zip</h4>
										</td>
										<td class='desc_text'> Waco, TX 76702</td>
									</tr>
									<tr>
										<td style='width:90px; padding:0;'>
											<h4 class='heading' style='margin:2px 0'>Email</h4>
										</td>
										<td class='desc_text'>records@enoahisolution.com</td>
									</tr>
								</table>
							</td>	
							<td valign='top' style='padding:0'>
								<table>
									<tr>
										<td>
											<h4 class='heading' style='margin:2px 0'>Company</h4>
										</td>					
										<td class='desc_text'>".preg_replace('/\\\\/','',$getPdfDet[0]['carrier_name'])."</td>			
									</tr>
									
									<tr>
										<td>
											<h4 class='heading' style='margin:2px 0'> Phone</h4>
										</td>
										<td class='desc_text'> 1-855-955-3137 (M-F, 8am-5pm CST)</td>
									</tr>
									<tr>
										<td>
											<h4 class='heading' style='margin:2px 0'> Fax</h4>
										</td>
										<td class='desc_text'> 866-439-0091</td>
									</tr>
									
								</table>
							</td>
						</tr>
					</table> 
					<table>
						<tr>
							<td style='padding:0'>
								<b>
									<p style='margin:10px 0 0 0;'><strong>Fax or email is preferred to expedite receipt of records!</strong></p>
									<p style='margin:0px;'><strong>If prepayment is required, please call us to obtain immediate payment by Credit Card!</strong></p>
								</b>
							</td>
						</tr>
						<tr>
							<td style='padding:0'>
								<p style='margin:10px 0 0 0;'>Invoice must be received within 30 days after receipt of records to be processed for payment. Approval must
								be obtained for all fees over $50. Unapproved fees over this amount or fees which exceed state statute may
								delay and/or reduce payment.</p>
							</td>
						</tr>
					</table>				
					<hr> 
					<!--<label style='font-size:11px'>Internal Use Only</label><span style='font-size:11px'>: 123456789102</span>
					<label style='margin-left:12px;font-size:11px'>Internal Use Only</label><span style='font-size:11px'>:
						123456789102</span> <label style='margin-left:12px; font-size:11px'>Internal Use Only</label><span
						style='font-size:11px'>: 123456789102</span> <label style='font-size:11px;margin-left:10px'>Internal Use
						Only</label><span style='font-size:11px'>: 123456789102</span>-->
				</main>
			</body>
			
			</html>";
			
		$dompdf = new PDF();
		$dompdf->load_html($html);
		$dompdf->set_paper('A4','potrait');
		$dompdf->render();
		$output = $dompdf->output();
		// file_put_contents('cover_sheets/Cover_sheet-'.trim($getPdfDet[0]['orderno']).'.pdf', $output);
		// unlink(FCPATH.'cover_sheets/Cover_sheet-'.$order_id.'.pdf');
		// unlink($file);
		file_put_contents('cover_sheets/Request_Letter_'.date('mdY').'_'.$order_id.'.pdf', $output);
			// echo $order_id;
			
		// }
		// else {
		// 	echo 'failed';
		// }
	}		
	
	public function cover_file_remove(){
		$order_id = $this->input->post('id');
		$this->load->helper("file");
		$condn = array('jobid' => $order_id, 'is_coversheet' => 1);
		$del      = $this->Welcome_model->coversheet_record_delete($condn);
		
		unlink(FCPATH.'cover_sheets/Cover_sheet-'.$order_id.'.pdf');//die;
	
	}
	
	public function upload_audio() {
		$filename= $this->load->helper('file');
		$lead_id = $this->input->post('audio_lead_id');
		
		$date_time = strtotime(date('Y-m-d H:i:s'));
		$audio = $_FILES['audio_data'];
		$file_path = $audio['tmp_name'];
		
		 $new_name = "audio-".$lead_id."-".$date_time.".wav";
		 
		 $file_upload= $this->s3_upload->upload_file($new_name, $file_path,$this->config->item('audio_dir'));
		 
		print_r($new_name);die;
	}
	
	/**** Remove Audio ****/
	public function delete_audio() {
		// echo "Test";die;
		$lead_id = $this->input->post('lead_id');
		$datetime = date('mdy');
		$date = date('Y-m-d');
		$output = $lead_id;
		// print_r($output);die;
		$filename_path = $this->config->item('audio_dir').'/'.$date.'/'.$output; 
		// print_r($filename_path);die;
		$delete = $this->s3_upload->delete_file($filename_path);
		print_r($delete);die;
		
	}
	/**** Remove Audio Ends ****/

	/**** Insert Audio In DB****/
	public function add_audio_post() {
		// echo "test";die;
		$audio_file_name = $this->input->post('audio_file_name');
		// print_r($audio_file_name);die;
		$audio_lead_id = $this->input->post('audio_lead_id');
		$today_date = date('Y-m-d');
		$user_data = $this->session->userdata('logged_in_user');
		$user_id = $user_data['userid'];
		
		$audio['lead_id']=$audio_lead_id;
		$audio['audio_file']=$audio_file_name;
		$audio['user_id']=$user_id;
		$audio['audio_date']=$today_date;
		// echo"<pre>";print_r($audio);die;
		$this->db->insert('crm_audiocomments', $audio);
		
		// $audio_html = "<b>Audio Record:</b><br/><audio controls='' controlslist='nodownload'>
							  // <source src='https://etouch-dev-bucket.s3.ap-south-1.amazonaws.com/uploads/".$audio_file_name."' type='audio/wav'>
							// </audio>";
		// $audio_log['jobid_fk']=$audio_lead_id;
		// $audio_log['userid_fk']=$user_id;
		// $audio_log['log_content']=$audio_html;		
	// echo"<pre>";print_r($audio_log);die;	
		// $this->db->insert('crm_logs', $audio_log);

	}
	/**** Insert Audio In DB****/

	public function ajax_get_list_usercard($uid)
	{
		$post 			   = $this->input->post();
		$ucard_type = $post['ucard_type'];
		$data = $this->Welcome_model->get_list_usercard($uid,$ucard_type);
		echo json_encode($data); exit;
	}
	/* Order fetch start here */
	public function order_fetch()
	{
		$this->load->model('User_model');
		$this->load->model('Company_list_model');
	$user_id = $this->session->userdata['logged_in_user']['userid'];
	$user_lead_stage = $this->Welcome_model->get_user_lead_stage($user_id);
	$user_lead_stage[0]['lead_stage'] = str_replace('37,','',$user_lead_stage[0]['lead_stage']);
	$user_lead_stage[0]['lead_stage'] = str_replace('36,','',$user_lead_stage[0]['lead_stage']);
	
	// print_r($user_lead_stage[0]['lead_stage']);
	if($user_lead_stage[0]['lead_stage']){
		$user_lead_stage_values = explode(",",$user_lead_stage[0]['lead_stage']);
		$user_lead_stage_result = "'" .implode("','",$user_lead_stage_values)."'";
	}else{
		$user_lead_stage_result=NULL;
	}
	// $today  = date('Y-m-d',strtotime(US_TIME));
	$today  = date('Y-m-d');
	// $current_time = date('H:i:s',strtotime(US_DATE_TIME));
	$current_time = date('H:i:s');
		
		//Code for getting company type of respective user without admin
		$userData = $this->User_model->get_user($user_id);
		$comp_Typ = $comp_list = $orderhandle = array();
		if(isset($userData[0]['company_type'])){
			$comp_Typ = explode(',',$userData[0]['company_type']);
		}
		if(isset($userData[0]['company_list'])){
			if($userData[0]['company_list'] == 'all'){
				$getlist = $this->Company_list_model->getCompany_list($comp_Typ);
				foreach($getlist as $dt){
					array_push($comp_list,$dt['company_code']);
				}
			}else{
				$comp_list = explode(',',$userData[0]['company_list']);
			}
		}
		if(isset($userData[0]['order_handle_by'])){
			$orderhandle = explode(',',$userData[0]['order_handle_by']);
		}
		$user_leads = $this->Welcome_model->get_user_leads($user_lead_stage_result,$today,$current_time,$comp_Typ,$comp_list,$orderhandle);
	$lead_assign=array('lead_assign'=>$user_id);
	// echo $this->db->last_query();exit;

	$check_log = $this->Welcome_model->get_already_assigned_log($today,$current_time,$user_leads[0]['lead_id']);
	
	// echo $user_leads[0]['lead_id'];
	// echo 'welcome<br>';
	// echo $this->input->is_ajax_request();

		if($this->input->is_ajax_request())
		{
			// echo 'if blk';
			$user_leads = $this->Welcome_model->get_user_leads($user_lead_stage[0]['lead_stage'],$today,$current_time,$comp_Typ,$comp_list,$orderhandle);
			
			$leadDet = $this->Welcome_model->get_lead_det($user_leads[0]['lead_id']);

			// $flag_repeatlog=NULL;
			// if($leadDet['lead_assign'] == $lead_assign['lead_assign'] && $leadDet['flag_queue_restrict'] == 0){
			// 	$flag_repeatlog = true;
			// }
			$this->Welcome_model->update_lead_assign('leads',$lead_assign,$user_leads[0]['lead_id']);
			       
			if(isset($user_leads[0]['lead_id']))
			{
				// if($flag_repeatlog == NULL){
				if($check_log==0){
				$logs['jobid_fk'] = $user_leads[0]['lead_id'];
				$logs['userid_fk'] = $user_id;
				$logs['date_created'] = date('Y-m-d H:i:s');
				$logs['disposition_id'] = $leadDet['order_disposition_id'];
				$logs['lead_stage'] = $leadDet['lead_stage'];
				$logs['record_type'] = $leadDet['record_type'];
				$logs['callback_date'] = $leadDet['proposal_expected_date'];
				$logs['log_content'] = "Order Assigned on Queue to ".$userData[0]['first_name'];
				$insert_logs = $this->Welcome_model->insert_row('logs', $logs);
				}
				echo $user_leads[0]['lead_id'];exit;
			}
			$this->session->set_flashdata('header_messages', array("No Queue Orders"));
		}
		else
		{
			// echo 'els blk';
			// $lead_assign['flag_queue_restrict']=1;
			$this->Welcome_model->update_lead_assign('leads',$lead_assign,$user_leads[0]['lead_id']);
			if(isset($user_leads[0]['lead_id']))
			{
				if($check_log==0){
				$leadDet = $this->Welcome_model->get_lead_det($user_leads[0]['lead_id']);
				$logs['jobid_fk'] = $user_leads[0]['lead_id'];
				$logs['userid_fk'] = $user_id;
				$logs['date_created'] = date('Y-m-d H:i:s');
				$logs['disposition_id'] = $leadDet['order_disposition_id'];
				$logs['lead_stage'] = $leadDet['lead_stage'];
				$logs['record_type'] = $leadDet['record_type'];
				$logs['callback_date'] = $leadDet['proposal_expected_date'];
				$logs['log_content'] = "Order Assigned on Queue to ".$userData[0]['first_name'];
				$insert_logs = $this->Welcome_model->insert_row('logs', $logs);
				}
				return $user_leads[0]['lead_id'];
			}
			$this->session->set_flashdata('header_messages', array("No order in queue"));
		}
	}
	
	
	public function NextBtnLog($leadid)
	{
		$this->load->model('User_model');
		$user_id = $this->session->userdata['logged_in_user']['userid'];
		$leadDet = $this->Welcome_model->get_lead_det($leadid);
		$userData = $this->User_model->get_user($user_id);
		// $logs['jobid_fk'] = $leadid;
		// $logs['userid_fk'] = $user_id;
		// $logs['date_created'] = date('Y-m-d H:i:s');
		// $logs['disposition_id'] = $leadDet['order_disposition_id'];
		// $logs['lead_stage'] = $leadDet['lead_stage'];
		// $logs['record_type'] = $leadDet['record_type'];
		// $logs['callback_date'] = $leadDet['proposal_expected_date'];
		// $logs['log_content'] = "Order Assigned on Queue to ".$userData[0]['first_name'];
		// if($insert_logs = $this->Welcome_model->insert_row('logs', $logs))
		// {
			echo json_encode(array('success'=>true));exit;
		// }else{
			// echo json_encode(array('success'=>false));exit;
		// }
	}
	
	/* Order fetch end here */
	public function ajax_get_cards_list($type)
	{
		$post 			   = $this->input->post();	
		$condn = array('type' => $type);
		$data = $this->Welcome_model->get_cards_list_by_cardtype($type);
		echo json_encode($data); exit;
	}

	public function session_details(){
		echo json_encode($this->userdata);
	}
	/**
	* update customer order
	*/
	public function customer_update($update = false, $id = false, $ajax = false)
	{
		$this->load->library('Validation');
		$this->load->model('Customer_model');
		$data['regions'] = $this->Regionsettings_model->region_list();
		$data['state'] = $this->Regionsettings_model->getstate_list();
		$post_data = real_escape_array($this->input->post());
		
		if($post_data['company'] == '' && $post_data['first_name']== ''){
			$rules['company'] = "trim|required";
			$rules['first_name'] = "trim|required";
		}
		$rules['add1_region']    = "selected[add1_region]";
		$rules['add1_country'] = "selected[add1_country]";
		$rules['add1_state'] = "selected[add1_state]";
		$rules['add1_location'] = "selected[add1_location]";
		$rules['timezone'] = "selected[timezone]";
		$rules['phone_1'] = "trim|required";
		$rules['add1_postcode'] = "trim";
		$rules['order_id'] = "trim|required";
		
		$this->validation->set_rules($rules);		
		//if($post_data['company'] == '' && $post_data['first_name']== ''){
			$fields['first_name'] = "Dr First Name";
			$fields['company'] = "Facility Name";
		//}
		$fields['last_name'] = "Last Name";
		$fields['position_title'] = 'Position';
		$fields['add1_postcode'] = "Zip Code";
		$fields['add1_region'] = "Region";
		$fields['add1_line1'] = "Address";
		$fields['add1_country'] = "Country";
		$fields['add1_state'] = "State";
		$fields['add1_location'] = "City";
		$fields['phone_1'] = "Phone Number";
		$fields['phone_4'] = "Fax";
		$fields['email_1'] = "Primary Email Address";
		$fields['skype_name'] = 'Skype Name';
		$fields['timezone'] = 'TimeZone';
		$fields['www_1'] = "Primary Web Address";
		$fields['order_id'] = "Order Id";
		
		$this->validation->set_fields($fields);
        
        $this->validation->set_error_delimiters('<p class="form-error">', '</p>');

		
		if ($this->validation->run() == false) 
		{
			
            $json['error'] = true;
            $json['ajax_error_str'] = $this->validation->error_string;
            echo json_encode($json);exit;
		} 
		else 
		{
			// all good
            foreach($fields as $key => $val) 
			{
				if (isset($post_data[$key]))
				{
					$update_data[$key] = $post_data[$key];
				}
            }
		}
		if($update_data['add1_state'] != '')
		{
			$update_data['timezone'] = $this->Welcome_model->get_state_timezone($update_data['add1_state']); // get state timezone
		}
		
		#check data change on back end 20/01/2021 vanitha.k
		$res = $this->Welcome_model->checkIsExistCustomer('customers',$update_data);
		$table = '';
		$api_facility_data = array();
		$api_facility_data['first_name'] = preg_replace('/\\\\/','',$update_data['first_name']);
		$api_facility_data['last_name'] = preg_replace('/\\\\/','',$update_data['last_name']);
		$api_facility_data['address'] = preg_replace('/\\\\/','',$update_data['add1_line1']);
		$api_facility_data['company'] =  preg_replace('/\\\\/','',$update_data['company']);
		$api_facility_data['state'] = preg_replace('/\\\\/','',$update_data['add1_state']);
		$api_facility_data['city'] = preg_replace('/\\\\/','',$update_data['add1_location']);
		$api_facility_data['zipcode'] = preg_replace('/\\\\/','',$update_data['add1_postcode']);
		$api_facility_data['phone'] = preg_replace('/\\\\/','',$update_data['phone_1']);
		$api_facility_data['email_address'] = $update_data['email_1'];
		$api_facility_data['company_type'] = 0; // insurance
		$api_facility_data['fax'] = $update_data['phone_4'];
			
		// check if request letter exist if yes call Request letter delete function 
		$is_coversheet = $this->Welcome_model->get_coversheet_log_detail($post_data['order_id']);
		$get_lead_detail = $this->Welcome_model->get_lead_detail($post_data['order_id']);
		if(is_countable($is_coversheet) &&  sizeof($is_coversheet) > 0)
		{
			$this->agreedCoversheetDelete('',$post_data['order_id']);
		}	
		$table = $this->addLogFacilityModify($get_lead_detail[0]);
		if($post_data['isNewCustomer'] == 0)
		{
			$id  = $post_data['customer'];
			$api_facility_data['facilityId'] = $id;
			$customer = $this->Customer_model->get_customer($post_data['customer']);

			if($post_data['editinfo'] == 0)
			{
				if (is_array($customer) && count($customer) > 0) foreach ($customer[0] as $k => $v) 
				{
					if (isset($this->validation->$k)) $this->validation->$k = $v;
				}
			}
			else if($post_data['editinfo'] == 1)
			{
				unset($update_data['editinfo']);
				unset($update_data['order_id']);
				
				# update expert master
				$this->db->where('custid',$id);
				$res = $this->db->update($this->cfg['dbpref'].'customers',$update_data);
				
				# update eop master
				$result_post_api = $this->esubmission_generate_token->request_auth_token('facility_trigger_api',$api_facility_data,'update_facility');	
				$result_post_api = json_decode($result_post_api,true);
				/* 
				if(isset($result_post_api['statusCode']) && $result_post_api['statusCode'] == $this->config->item('crm')['api_success_code'])
				{
					$facility['aps_facility_id'] = $result_post_api['facilityId'];
					$this->db->where('custid',$id);
					$this->db->update($this->cfg['dbpref'].'customers',$facility);
				} */
				$str = 'Facility Details Updated Successfully';
			}
        }
        else if($post_data['isNewCustomer'] == 1)
        {
        	unset($update_data['order_id']);
			unset($update_data['editinfo']);
			
			# add new facility in expert
			if ($id = $this->Customer_model->insert_customer($update_data)) 
			{	
				$api_facility_data['facilityId'] = $id;

				# eop new facility creation
				$result_post_api = $this->esubmission_generate_token->request_auth_token('facility_trigger_api',$api_facility_data,'insert_facility');	
				
				$result_post_api = json_decode($result_post_api,true);
				if(isset($result_post_api['statusCode']) && $result_post_api['statusCode'] == $this->config->item('crm')['api_success_code'])
				{
					$facility['aps_facility_id'] = $result_post_api['facilityId'];
					$this->db->where('custid',$id);
					$this->db->update($this->cfg['dbpref'].'customers',$facility);
				}
				$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
				$dis['date_created'] = date('Y-m-d H:i:s');
				$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));	

				$from=$this->userdata['email'];
				$arrEmails = $this->config->item('crm');
				$arrSetEmails=$arrEmails['director_emails'];
				$mangement_email = $arrEmails['management_emails'];
				$mgmt_mail = implode(',',$mangement_email);
				$admin_mail=implode(',',$arrSetEmails);		
				$varEmailRecipients=implode(',',$arrSetEmails);
				$subject='New Customer Creation Notification';

				//email sent by email template
				$param = array();
				
				$param['email_data'] = array('print_fancydate'=>$print_fancydate,'user_name'=>$user_name,'first_name'=>$update_data['first_name'],'last_name'=>$update_data['last_name'],'company'=>$update_data['company'],'signature'=>$this->userdata['signature']);

				$param['to_mail'] = $mgmt_mail;
				$param['bcc_mail'] = $admin_mail;
				$param['from_email'] = $from;
				$param['from_email_name'] = $user_name;
				$param['template_name'] = "New Customer Creation";
				$param['subject'] = $subject;
			}
			$str = 'New Facility Activated';
		}
		
		# expert order assign facility
		if($get_lead_detail[0]['custid_fk'] != $id)
		{
			# inactive facility
			$this->Welcome_model->update_facility_order_inactive($post_data['order_id']);
			
			$facility_log = array();
			
			$facility_log['facility_id'] = $id;
			$facility_log['order_id'] = $post_data['order_id'];
			$facility_log['user_id'] = $this->userdata['userid'];
			$facility_log['status'] = 1;
			$facility_log['created_on'] = date('Y-m-d H:i:s');
			
			$this->Welcome_model->insert_row('facility_order_log',$facility_log);
			
			if($str == '')
				$str = 'Active Facility Changed Successfully';
		}
		$copy_service = $this->Welcome_model->get_mapped_copy_service($id,0,1);
		$json['active_copy_service'] = isset($copy_service[0]) ? $copy_service[0] : '';
		# expert order against mapping
		if($id > 0)
		{
			$Orderdata['custid_fk'] = $id;
			if($this->Welcome_model->update_row('leads',$Orderdata,$post_data['order_id']))
			{
				# eop order against mapping
				$lead_id = $post_data['order_id'];
				$get_lead_detail = $this->Welcome_model->get_lead_detail($lead_id);
				$api_facility_data['enoahorderid'] = $get_lead_detail[0]['aps_order_no'].$get_lead_detail[0]['order_sub_no'];
				$api_facility_data['company_code'] = $get_lead_detail[0]['company_code'];
				$api_facility_data['facilityId'] = $id;
				$result_api = $this->esubmission_generate_token->request_auth_token('facility_trigger_api',$api_facility_data,'update_facility_order');
				
				$result_api = json_decode($result_api,true);
				if(isset($result_api['statusCode']) && $result_api['statusCode'] == $this->config->item('crm')['api_success_code'])
				{
					$json['error'] = false;
					if($str == '')
						$str = 'Facility Updated Successfully';
					$json['ajax_error_str'] = $str;
					$json['html'] = $table;
					echo json_encode($json);exit;
				}
				$json['error'] = false;
				if($str == '')
					$str = 'Facility Updated Successfully';
				$str = $json['ajax_error_str'] = $str.' , EOP Order Facility Mapping Failed !';
				$json['html'] = $table;
				echo json_encode($json);exit;
			}
		}
		$json['error'] = true;
		$json['ajax_error_str'] = $str;
		$json['html'] = $table;
        echo json_encode($json);exit;
	}
	public function get_customer_search_list() {
		 $res = array();
         if ($this->input->get('cust_name') != '') {
		   $this->load->model('Customer_model');
		   $this->load->model('Custodian_model');
          $company_type = $this->input->get('company_type');
           $state = $this->input->get('state');
		   $config = $this->config->item('crm')['company_type_arr'];
		
		   if($company_type != '' && $company_type == $config['insurance'])
		   {
				$result = $this->Customer_model->customer_list(0, $this->input->get('cust_name'),'last_name','asc',$state);
		   }
		   else if($company_type != '' && (int)$company_type ==  $config['legal'])
		   {
				$result = $this->Custodian_model->customer_list(0, $this->input->get('cust_name'),'last_name','asc',$state);
		   }
		   else if($company_type == '' || $company_type == 'all')
		   {
			   $result2 = $this->Customer_model->customer_list(0, $this->input->get('cust_name'),'last_name','asc',$state);
			   $result1 = $this->Customer_model->customer_list(0, $this->input->get('cust_name'),'last_name','asc',$state);
			   $result = array_merge($result2,$result1);
		   }
		   
            $i=0;
            if (count($result) > 0) foreach ($result as $cust) {
                
				if(!empty($cust)) {
					$customer_name = '';
					if(!empty($cust['first_name']))
					$customer_name .= $cust['first_name'];
					if(!empty($cust['last_name']))
					$customer_name .= ' '.$cust['last_name'];
					if(!empty($cust['first_name']))
					$customer_name .= ' - '.$cust['company'];
					if(!empty($cust['phone_1']))
					$customer_name .= '('.$cust['phone_1'].')';
				
					$res[$i]['id'] = $cust['custid'];
					$res[$i]['text'] = $customer_name;
					$res[$i]['regId'] = $cust['add1_region'];
					$res[$i]['cntryId'] = $cust['add1_country'];
					$res[$i]['stateId'] = $cust['add1_state'];
					$res[$i]['facility_name'] = $cust['company'];
					$res[$i]['first_name'] = $cust['first_name'];
					$res[$i]['last_name'] = $cust['last_name'];
					$res[$i]['phone'] = $cust['phone_1'];
					$res[$i]['fax'] = $cust['phone_4'];
					$res[$i]['address'] = $cust['add1_line1'];
					$res[$i]['city'] = $cust['add1_location'];
					$res[$i]['zipcode'] = $cust['add1_postcode'];
				}
		 		$i++;
            }
        }
        echo json_encode($res); exit;
	}
	
	function access_order($order_details){
		
		$usid = $this->session->userdata('logged_in_user');			
		$order_access = $this->Welcome_view_validate_model->get_order_detail($order_details[0]['lead_id']);
		$get_order_access = $this->Welcome_view_validate_model->get_order_access($order_access['lead_id']);		
		$db_date = $get_order_access['access_datetime'];	
		$now = date("Y-m-d H:i:s");
		$add_time= date("Y-m-d H:i:s", strtotime("{$db_date} +10 minutes"));		
		if($usid['userid'] != $order_access['lead_assign'] && $get_order_access['access_check'] ==1 && $get_order_access['user_id'] == $usid['userid']){
			// echo "1";
			return true;
			// echo "allow admin for first login";exit;
		}else if($usid['userid'] == $order_access['lead_assign'] && $get_order_access['access_check'] == 2){
			// echo "2";
			return true;
			// echo "allow normal user after 10mins while admin working";exit;
		}else if($usid['userid'] != $order_access['lead_assign'] && $get_order_access['access_check'] == 3 && $get_order_access['user_id'] == $usid['userid']){
			//echo "3";
			return true;
			// echo "allow normal user after 10mins while normal user working";exit;
		}else if($usid['userid'] == $order_access['lead_assign'] && $get_order_access['access_check'] ==0 ){
			//echo "4";
			return true;
			// echo "allow user for first login";exit;
		}else{
			
			// echo "restrict access while other user working";exit;
			$_SESSION['login_errors']= array("This order already is in process by ".$get_order_access['user_name']);
			$this->session->set_flashdata('login_errors', array("This order already is in process by ".$get_order_access['user_name']));
			
			redirect('welcome/quotation', 'refresh');
		}
	}
function ajax_custodian_search() {
		$res = array();
        if ($this->input->get('cust_name')) {
            $state = $this->input->get('state');
            $result = $this->Custodian_model->customer_list(0, $this->input->get('cust_name'),'last_name','asc',$state);
            $i=0;
            if (count($result) > 0) foreach ($result as $cust) {
                
				if(!empty($cust)) {
					$customer_name = '';
					if(!empty($cust['first_name']))
					$customer_name .= $cust['first_name'];
					if(!empty($cust['last_name']))
					$customer_name .= ' '.$cust['last_name'];
					if(!empty($cust['first_name']))
					$customer_name .= ' - '.$cust['company'];
					if(!empty($cust['phone_1']))
					$customer_name .= '('.$cust['phone_1'].')';
				
					$res[$i]['id'] = $cust['custid'];
					$res[$i]['text'] = $customer_name;
					$res[$i]['regId'] = $cust['add1_region'];
					$res[$i]['cntryId'] = $cust['add1_country'];
					$res[$i]['stateId'] = $cust['add1_state'];
					$res[$i]['facility_name'] = $cust['company'];
					$res[$i]['first_name'] = $cust['first_name'];
					$res[$i]['last_name'] = $cust['last_name'];
					$res[$i]['phone'] = $cust['phone_1'];
					$res[$i]['fax'] = $cust['phone_4'];
					$res[$i]['address'] = $cust['add1_line1'];
					$res[$i]['city'] = $cust['add1_location'];
					$res[$i]['zipcode'] = $cust['add1_postcode'];
				
				}
		 		$i++;
            }
        }
        echo json_encode($res); exit;
	}
	
	
	function ajax_add_custodian() 
	{
		$hide_doctors_name = array();
		$doctors_name	   = array();
		$json	   		   = array();
		$lead_det 		   = $this->Welcome_model->get_lead_det($this->input->post('order_no'));
		// $sub_character	   = $this->Welcome_model->get_ordersubno($this->input->post('order_no'));
		$post 			   = $this->input->post();
		$json['error']     = false;
		
		// if(isset($sub_character) && !empty($sub_character['order_sub_no'])) {
			// $sub_char = $sub_character['order_sub_no'];
			// $sub_char++;
		// } else {
			// $sub_char = 'A';
		// }
	
		$insa = array();
		$insa['order_no']     		   = $lead_det['order_no'];
		$insa['orderref']   		   = $lead_det['orderref'];
		$insa['lead_title']  		   = preg_replace('/\\\\/','',$lead_det['lead_title']);
		$insa['lead_title_last']  	   = preg_replace('/\\\\/','',$lead_det['lead_title_last']);
		$insa['patient_dob']		   = $lead_det['patient_dob'];
		$insa['ss_no']   			   = $lead_det['ss_no'];
		$insa['patient_adr']  		   = $lead_det['patient_adr'];
		$insa['p_city']  		   	   = $lead_det['p_city'];
		$insa['p_state']  		   	   = $lead_det['p_state'];
		$insa['lead_assign']    	   = $lead_det['lead_assign'];
		$insa['expect_worth_id']       = $lead_det['expect_worth_id'];
		$insa['expect_worth_amount']   = $lead_det['expect_worth_amount'];
		$insa['actual_worth_amount']   = $lead_det['actual_worth_amount'];
		$insa['invoice_no']            = $lead_det['invoice_no'];
		// $insa['lead_stage']  		   = 1;
		$insa['proposal_expected_date'] = $lead_det['proposal_expected_date'];
		$insa['lead_status']   		   = 1;
		$insa['pjt_status']   		   = $lead_det['pjt_status'];
		$insa['lead_indicator']    	   = $lead_det['lead_indicator'];
		$insa['rag_status']     	   = $lead_det['rag_status'];
		$insa['project_type']   	   = $lead_det['project_type'];
		$order_stage				   = $post['order_stage'];
		
		if(empty($post['hide_doctors_name'])){
			$json['error'] = true;
			$json['msg']   = 'Valid doctors must be selected';
		} else {
			$k=0;
			foreach($post['hide_doctors_name'] as $new_cust){
				if($new_cust!=''){
					$insa['custid_fk']     = $new_cust;
					$insa['order_sub_no']  = $sub_char;
					$insa['created_by']    = $this->userdata['userid'];
					$insa['modified_by']   = $this->userdata['userid'];
					$insa['belong_to']     = $this->userdata['userid'];
					$insa['date_created']  = date('Y-m-d H:i:s');
					$insa['lead_stage']    = $order_stage[$k];
					// echo "<pre>"; print_r($insa); exit;
					$this->db->insert($this->cfg['dbpref'] . 'leads', $insa);
					$last_insert_id = $this->db->insert_id();
					
					$invoice_nos = (int) $last_insert_id;
					$invoice_nos = str_pad($invoice_nos, 5, '0', STR_PAD_LEFT);
					$inv_nos['invoice_no'] = $invoice_nos;
					$updt_job = $this->Welcome_model->update_row('leads', $inv_nos, $last_insert_id);
					
					//history - lead_stage_history
					$lead_hist['lead_id'] 			= $last_insert_id;
					$lead_hist['dateofchange'] 		= date('Y-m-d H:i:s');
					$lead_hist['previous_status'] 	= 1;
					$lead_hist['changed_status'] 	= 1;
					$lead_hist['lead_status'] 		= 1;
					$lead_hist['modified_by'] 		= $this->userdata['userid'];
					$insert_lead_stg_his 			= $this->Welcome_model->insert_row('lead_stage_history', $lead_hist);
					
					//history - lead_status_history
					$lead_stat_hist['lead_id'] 			= $last_insert_id;
					$lead_stat_hist['dateofchange'] 	= date('Y-m-d H:i:s');
					$lead_stat_hist['changed_status'] 	= 1;
					$lead_stat_hist['modified_by'] 		= $this->userdata['userid'];
					$insert_lead_stat_his = $this->Welcome_model->insert_row('lead_status_history', $lead_stat_hist);
					
					$sub_char++;
					
					//sent notification email only when initial stage is chosen
					if($order_stage[$k]==1){
						$this->sent_notification_email_ajax_add_doctor($last_insert_id);
					}
				}
				$k++;
			}
			$json['error'] = false;
			$json['msg']   = 'Custodian Added';
		}
		echo json_encode($json); exit;
	}
	
	
	public function custodian_update($update = false, $id = false, $ajax = false)
	{
		$this->load->library('Validation');
		$this->load->model('Custodian_model');
		$data['regions'] = $this->Regionsettings_model->region_list();
		$data['state'] = $this->Regionsettings_model->getstate_list();
		$post_data = real_escape_array($this->input->post());
		// echo "<pre>";print_R($post_data);exit;
		if($post_data['company'] == '' && $post_data['first_name']== ''){
			$rules['company'] = "trim|required";
			$rules['first_name'] = "trim|required";
		}
		$rules['add1_region']    = "selected[add1_region]";
		$rules['add1_country'] = "selected[add1_country]";
		$rules['add1_state'] = "selected[add1_state]";
		$rules['add1_location'] = "selected[add1_location]";
		$rules['timezone'] = "selected[timezone]";
		$rules['phone_1'] = "trim|required";
		$rules['add1_postcode'] = "trim";
		$rules['order_id'] = "trim|required";
		
		$this->validation->set_rules($rules);		
		//if($post_data['company'] == '' && $post_data['first_name']== ''){
			$fields['first_name'] = "Custodian First Name";
			$fields['company'] = "Custodian Facility Name";
		//}
		$fields['last_name'] = "Last Name";
		$fields['position_title'] = 'Position';
		$fields['add1_postcode'] = "Zip Code";
		$fields['add1_region'] = "Region";
		$fields['add1_line1'] = "Address";
		$fields['add1_country'] = "Country";
		$fields['add1_state'] = "State";
		$fields['add1_location'] = "City";
		$fields['phone_1'] = "Phone Number";
		$fields['phone_4'] = "Fax";
		$fields['email_1'] = "Primary Email Address";
		$fields['skype_name'] = 'Skype Name';
		$fields['timezone'] = 'TimeZone';
		$fields['www_1'] = "Primary Web Address";
		$fields['order_id'] = "Order Id";
		
		$this->validation->set_fields($fields);
        
        $this->validation->set_error_delimiters('<p class="form-error">', '</p>');

		
		
		if ($this->validation->run() == false) 
		{
			
            $json['error'] = true;
            $json['ajax_error_str'] = $this->validation->error_string;
            echo json_encode($json);exit;
		} 
		else 
		{
			// all good
            foreach($fields as $key => $val) 
			{
				if (isset($post_data[$key]))
				{
					$update_data[$key] = $post_data[$key];
				}
            }
		}
		if($update_data['add1_state'] != '')
		{
			$update_data['timezone'] = $this->Welcome_model->get_state_timezone($update_data['add1_state']); // get state timezone
		}
		$str = '';
		#check data change on back end 20/01/2021 vanitha.k
		$res = $this->Welcome_model->checkIsExistCustomer('custodian',$update_data);
	
		$api_facility_data = array();
		$api_facility_data['first_name'] = preg_replace('/\\\\/','',$update_data['first_name']);
		$api_facility_data['last_name'] =  preg_replace('/\\\\/','',$update_data['last_name']);
		$api_facility_data['address'] = preg_replace('/\\\\/','',$update_data['add1_line1']);
		$api_facility_data['company'] = preg_replace('/\\\\/','',$update_data['company']);
		$api_facility_data['state'] =  preg_replace('/\\\\/','',$update_data['add1_state']);
		$api_facility_data['city'] =  preg_replace('/\\\\/','',$update_data['add1_location']);
		$api_facility_data['zipcode'] = $update_data['add1_postcode'];
		$api_facility_data['phone'] = $update_data['phone_1'];
		$api_facility_data['fax'] = $update_data['phone_4'];
		$api_facility_data['email_address'] = $update_data['email_1'];
		$api_facility_data['company_type'] = 1; // legal
		$table = '';
		
		// check if request letter exist if yes call Request letter delete function 
		$get_lead_detail = $this->Welcome_model->get_lead_detail($post_data['order_id']);
		$is_coversheet = $this->Welcome_model->get_coversheet_log_detail($post_data['order_id']);
		if(is_countable($is_coversheet) &&  sizeof($is_coversheet) > 0)
		{
			$this->agreedCoversheetDelete('',$post_data['order_id']);
		}
		$table = $this->addLogFacilityModify($get_lead_detail[0]);
		if($post_data['isNewCustomer'] == 0)
		{
			$customer = $this->Customer_model->get_customer($post_data['customer']);
			$id  = $post_data['customer'];
			$api_facility_data['facilityId'] = $id;
			
			if($post_data['editinfo'] == 0)
			{
				if (is_array($customer) && count($customer) > 0) foreach ($customer[0] as $k => $v) 
				{
					if (isset($this->validation->$k)) $this->validation->$k = $v;
				}
			}
			else if($post_data['editinfo'] == 1) 
			{
				unset($update_data['editinfo']);
				unset($update_data['order_id']);
				
				# update expert master
				$this->db->where('custid',$id);
				$res = $this->db->update($this->cfg['dbpref'].'custodian',$update_data);
				
				# update eop master
				$result_post_api = $this->esubmission_generate_token->request_auth_token('facility_trigger_api',$api_facility_data,'update_facility');	
				$result_post_api = json_decode($result_post_api,true);
				
				/* if(isset($result_post_api['statusCode']) && $result_post_api['statusCode'] == $this->config->item('crm')['api_success_code'])
				{
					$facility['aps_facility_id'] = $result_post_api['facilityId'];
					$this->db->where('custid',$id);
					$this->db->update($this->cfg['dbpref'].'custodian',$facility);
				} */
				$str = 'Facility Details Updated Successfully';
			}
        }
        else if($post_data['isNewCustomer'] == 1)
        //else if($res == 0)
        {
			unset($update_data['editinfo']);
        	unset($update_data['order_id']);
			
			# add new facility in expert
			if ($id = $this->Custodian_model->insert_customer($update_data)) 
			{	
				$api_facility_data['facilityId'] = $id;
				
				# eop new facility creation
				$result_post_api = $this->esubmission_generate_token->request_auth_token('facility_trigger_api',$api_facility_data,'insert_facility');	
				// $result_post_api = $facility_trigger_api->request_auth_token('facility_trigger_api',$api_facility_data,'insert_facility');	
				$result_post_api = json_decode($result_post_api,true);
				if(isset($result_post_api['statusCode']) && $result_post_api['statusCode'] == $this->config->item('crm')['api_success_code'])
				{
					$facility['aps_facility_id'] = $result_post_api['facilityId'];
					$this->db->where('custid',$id);
					$this->db->update($this->cfg['dbpref'].'custodian',$facility);
				}
				$user_name = $this->userdata['first_name'] . ' ' . $this->userdata['last_name'];
				$dis['date_created'] = date('Y-m-d H:i:s');
				$print_fancydate = date('l, jS F y h:iA', strtotime($dis['date_created']));	

				$from=$this->userdata['email'];
				$arrEmails = $this->config->item('crm');
				$arrSetEmails=$arrEmails['director_emails'];
				$mangement_email = $arrEmails['management_emails'];
				$mgmt_mail = implode(',',$mangement_email);
				$admin_mail=implode(',',$arrSetEmails);		
				$varEmailRecipients=implode(',',$arrSetEmails);
				$subject='New Customer Creation Notification';

				//email sent by email template
				$param = array();
				
				$param['email_data'] = array('print_fancydate'=>$print_fancydate,'user_name'=>$user_name,'first_name'=>$update_data['first_name'],'last_name'=>$update_data['last_name'],'company'=>$update_data['company'],'signature'=>$this->userdata['signature']);

				$param['to_mail'] = $mgmt_mail;
				$param['bcc_mail'] = $admin_mail;
				$param['from_email'] = $from;
				$param['from_email_name'] = $user_name;
				$param['template_name'] = "New Customer Creation";
				$param['subject'] = $subject;
			}
			$str = 'New Facility Activated';
		}
		
		if($get_lead_detail[0]['custid_fk'] != $id)
		{
			# inactive facility
			$this->Welcome_model->update_facility_order_inactive($post_data['order_id']);
			
			$facility_log = array();
			
			$facility_log['facility_id'] = $id;
			$facility_log['order_id'] = $post_data['order_id'];
			$facility_log['user_id'] = $this->userdata['userid'];
			$facility_log['status'] = 1;
			$facility_log['created_on'] = date('Y-m-d H:i:s');
			
			$this->Welcome_model->insert_row('facility_order_log',$facility_log);
			
			if($str == '')
				$str = 'Active Facility Changed Successfully';
		}
		$copy_service = $this->Welcome_model->get_mapped_copy_service($id,1,1);
		$json['active_copy_service'] = isset($copy_service[0]) ? $copy_service[0] : '';
		# expert order against mapping
		if($id > 0)
		{
			$Orderdata['custid_fk'] = $id;
			if($this->Welcome_model->update_row('leads',$Orderdata,$post_data['order_id']))
			{
				# eop order against mapping
				
				$lead_id = $post_data['order_id'];
				$get_lead_detail = $this->Welcome_model->get_lead_detail($lead_id);
				$api_facility_data['enoahorderid'] = $get_lead_detail[0]['aps_order_no'].$get_lead_detail[0]['order_sub_no'];
				$api_facility_data['company_code'] = $get_lead_detail[0]['company_code'];
				$api_facility_data['facilityId'] = $id;
				$result_api = $this->esubmission_generate_token->request_auth_token('facility_trigger_api',$api_facility_data,'update_facility_order');
				// $result_api = $facility_trigger_api->request_auth_token('facility_trigger_api',$api_facility_data,'update_facility_order');
				$result_api = json_decode($result_api,true);
				if(isset($result_api['statusCode']) && $result_api['statusCode'] == $this->config->item('crm')['api_success_code'])
				{
					$json['error'] = false;
					if($str == '')
						$str = 'Facility Updated Successfully';
					$json['ajax_error_str'] = $str;
					$json['html'] = $table;
					echo json_encode($json);exit;
				}
				$json['error'] = false;
				if($str == '')
					$str = 'Facility Updated Successfully';
				$str = $json['ajax_error_str'] = $str.' , EOP Order Facility Mapping Failed !';
				$json['html'] = $table;
				echo json_encode($json);exit;
			}
		}
		$json['error'] = true;
		$json['ajax_error_str'] = $str;
		$json['html'] = $table;
        echo json_encode($json);exit;
	}
	

	public function lead_company_type_search(){
		
		$lead_company_type = $this->input->post('lead_company_type');
		// print_r($lead_company_type);die;
		$sql = "SELECT * FROM ".$this->cfg['dbpref']."company WHERE company_type=".$lead_company_type." ORDER BY company_name ASC";
		$query = $this->db->query($sql);
		// echo $this->db->last_query();exit;         
		$result = $query->result_array();
		
		$response = "<option>Please Select</option>";
		
		foreach($result as $rs) {
			$response .= "<option value='".$rs['company_code']."'>".$rs['company_name']."</option>";
		}
		
		echo $response;exit;
	}
	
	public function lead_company_type_filter(){
		$company_list = $this->userdata['company_list'];
		if($company_list != 0 && $company_list != 'all')
		{
			$company_list = explode(',',$company_list);
		} 
		$lead_company_type = $this->input->post('lead_company_type');
		
		$result = $this->Welcome_model->get_company_list($company_list,$lead_company_type);
		//var_dump($result);exit;
		$response = "<option>Please Choose</option>";
		
		foreach($result as $rs) {
			$response .= "<option value='".$rs['company_code']."'>".$rs['company_name']."</option>";
		}
		
		echo $response;exit;
		
	}
	/**
	* Log eventType in logs table and lead table
 	*/
	public function addLogFacilityModify($leadData = array())
	{
		$logData = array();
		$log_content = $logData['log_content'] = 'Facility detail updated and earlier request letter was deleted , Please regenerate request letter';
		$logData['jobid_fk']    = $leadData['lead_id'];
		$logData['userid_fk']   = $this->userdata['userid'];
		$logData['record_type'] = $leadData['record_type'];
		$logData['disposition_id'] = $leadData['order_disposition_id'];
		$callback_status = '';
		$callback_date = '';
		if($leadData['proposal_expected_date'] != '0000-00-00 00:00:00' && $leadData['proposal_expected_date'] != NULL)
		{
			$logData['callback_date'] = $leadData['proposal_expected_date'];
			$callback_date = date('m/d/Y',strtotime($logData['callback_date']));
			$callback_status = 'Call Back Date Changed :';
		}
		$logData['lead_stage'] = $leadData['lead_stage'];
		$logData['date_created'] = date('Y-m-d H:i:s');
		$table = '';
		
		$rec_type_info = $this->Welcome_model->get_record_type_info($leadData['record_type']);
		$record_type = isset($rec_type_info['rec_type_name']) ? $rec_type_info['rec_type_name'] : '';
		$call_status   = $this->Welcome_model->get_call_status_info($leadData['order_disposition_id']);
		$call_disposition = isset($call_status['disposition_name']) ? $call_status['disposition_name'] : '';
		$status_res = $this->Welcome_model->get_lead_stg_name($leadData['lead_stage']);
		$lead_stage = isset($status_res['lead_stage_name']) ? $status_res['lead_stage_name'] : '';
		$stick_class = '';
		
		// inset the new log
		if($this->db->insert($this->cfg['dbpref'] . 'logs', $logData))
		{
			$fancy_date = date('l, jS F y h:iA');
			$table = <<<HDOC
<tr id="log" class="log{$stick_class}">
<td id="log" class="log">
<p class="data">
        <span>{$fancy_date}</span>
    {$this->userdata['first_name']} {$this->userdata['last_name']}
    </p>
    <p class="desc">
	<b>{$record_type} :</b> {$log_content}
    </p>
	<p class="desc">

	<b>Event Type Changed :</b>{$call_disposition}
	<p>
	<p class="desc">
	<b>Stage Changed :</b>{$lead_stage}
	<p>
	<p class="desc">
	<b>{$callback_status}</b>{$callback_date}
	<p>
</td>
</tr>
HDOC;
		}
		return $table;
	}
	/**
	* Delete Log Content 
	* @params logid string
	* @return boolean false/true
	*/
	public function delete_log_content()
	{
		$logid = $this->input->post('logid');
		$lead_id = $this->input->post('leadid');
		$reasonComments = $this->input->post('reasonComments');
		$response['error'] = true;
		if($lead_id != '')
		{
			$leadDetails = $this->Welcome_model->get_lead_detail($lead_id);
			$company_code = isset($leadDetails[0]['company_code']) ? $leadDetails[0]['company_code'] : '';
			$eNaohOrderId = isset($leadDetails[0]['aps_order_no']) ? $leadDetails[0]['aps_order_no'].$leadDetails[0]['order_sub_no'] : '';
			if($company_code != '' && $eNaohOrderId != '')
			{
				if($this->Welcome_model->delete_log_content($logid))
				{
					// include_once(APPPATH.'controllers/web_service_v2/post_trigger_api.php');
					// $post_trigger_api = new post_trigger_api();
					
					$deleteContent = array();	
					$deleteContent['eNoahOrderId'] = $leadDetails[0]['aps_order_no'].$leadDetails[0]['order_sub_no'];
					$deleteContent['companyCode']  = $leadDetails[0]['company_code'];
					$deleteContent['eXpertOrderId']= $lead_id;
					$deleteContent['eXpertLogId']  = $logid;
					
					$result_delete_api = $this->post_trigger_api->deleteLogContent($deleteContent);
					// $result_delete_api = $post_trigger_api->deleteLogContent($deleteContent);
					
					$log_content = 'Log #'.$logid.' content deleted by '.$this->userdata['first_name'].' '.$this->userdata['last_name'];
					activity_log(array('lead_id'=>$lead_id,'aps_order_no'=>$eNaohOrderId,'company_code'=>$company_code,'content'=>$log_content,'userid'=>$this->userdata['userid'],'reason'=>$reasonComments));
					
					$response['error'] = false;
					$response['html']  = '';
				}
			}
		}
		echo json_encode($response);exit;
	}
	/**
	* Delete Payment Content 
	* @params paymentid string , leadid string
	* @return boolean false/true
	*/
	public function delete_payment_record()
	{
		$payment_id = $this->input->post('payment_id');
		$lead_id = $this->input->post('lead_id');
		$reasonComments = $this->input->post('reasonComments');
		$response['error'] = true;
		if($lead_id != '')
		{
			// find log id 
			$log_content_id = $this->Welcome_model->get_log_content_id($payment_id);
				
			$leadDetails = $this->Welcome_model->get_lead_detail($lead_id);
			$company_code = isset($leadDetails[0]['company_code']) ? $leadDetails[0]['company_code'] : '';
			$eNaohOrderId = isset($leadDetails[0]['aps_order_no']) ? $leadDetails[0]['aps_order_no'].$leadDetails[0]['order_sub_no'] : '';
			
			if($this->Welcome_model->delete_payment_record($payment_id,$lead_id))
			{
				$log_content = 'Payment #'.$payment_id.' and log content deleted by '.$this->userdata['first_name'].' '.$this->userdata['last_name'];
				activity_log(array('lead_id'=>$lead_id,'aps_order_no'=>$eNaohOrderId,'company_code'=>$company_code,'content'=>$log_content,'userid'=>$this->userdata['userid'],'reason'=>$reasonComments));
				 
				
				$response['error'] = false;
				$response['html']  = $this->get_payment_records($lead_id,'type');
				$response['logid'] = $log_content_id;
			}
		}
		echo json_encode($response);exit;
	}

		/**
	* Get Business Days - special tat
	*/
	public function get_business_days_spctat($start_time,$stop_time)
	{
		// echo $start_time.'<br>';
		// echo $stop_time.'<br>';
		$begin = strtotime($start_time);
		$end = strtotime($stop_time);
		$org_start_day = $start_day = date('N',$begin);
		if($start_day>5){
			$begin = date('Y-m-d 00:00:01', strtotime($start_time. ' +1 day'));
			$begin = strtotime($begin);
		}
		$no_days = 0;
		$weekends = 0;
		$arr_weekend = array(6,7);
		while($begin<$end){
			$next_end = $begin+86399;
			$end_day = date('N',$next_end); 
			if(in_array($org_start_day,$arr_weekend)){
				if($end_day <= 5 && $end_day!=1 && $next_end<$end)
				{		
					$no_days++;
				}
			}else{
				if($end_day <= 5 && $next_end<$end)
				{		
					$no_days++;
				}
			}
			$begin+=86400;
		};

		$working_days = $no_days;
		// echo 'check - '.$no_days.'<br>';
		return $working_days;
	}

		/**
	* Get Holiday Count
	*/
	public function get_holidays($from_date,$to_date)
	{
		$start = new \DateTime($from_date);
		
		$end = new \DateTime($to_date);

		$starttime = $start->format('Y-m-d H:i:s');
		$endtime = $end->format('Y-m-d H:i:s');

		return $holiday_count = $this->get_holiday_count($starttime,$endtime);
	}

	
	//function to convert old encryption to new encryption
	function convert_encryption(){
		$leads = $this->Welcome_model->get_leads();
		foreach($leads as $lead){
			$lead_id = $lead['lead_id'];
			echo $lead_id."<br/>";
			$patdob=$this->fileencryption->old_encrypt_decrypt('decrypt',$lead['patient_dob']);
			$patdob_new=$this->fileencryption->new_encrypt_decrypt('encrypt',$patdob);
			$ssn=$this->fileencryption->old_encrypt_decrypt('decrypt',$lead['ss_no']);
			$ssn_new=$this->fileencryption->new_encrypt_decrypt('encrypt',$ssn);
			$update['patient_dob'] = $patdob_new;
			$update['ss_no'] = $ssn_new;
			$updated = $this->Welcome_model->updt_lead_stg_status($lead_id, $update);
		}
	}

	/**
	 * special instructions start & end date
	 */
	public function ajax_set_special_instruction_date()
	{
		$order_id = $this->input->post('lead_id');
		$special_instruction_start_date = $this->input->post('special_instruction_start_date');
		$special_instruction_end_date = $this->input->post('special_instruction_end_date');

		if($special_instruction_start_date != '' && $special_instruction_end_date != '')
		{
			$order_update_data = array();

			$order_update_data['special_instruction_start_date'] = date('Y-m-d',strtotime($special_instruction_start_date));
			$order_update_data['special_instruction_end_date'] = date('Y-m-d',strtotime($special_instruction_end_date));
			
			$this->Welcome_model->update_row('leads',$order_update_data,$order_id);

			echo json_encode(['error'=>false,'message'=>'Successfully Updated']);exit;
		}

		echo json_encode(['error'=>true,'message'=>'Failed !']);exit;
	}

	/**
	 * customer service fee
	 */
	public function ajax_service_fee_customer()
	{
		$order_id = $this->input->post('order_id');
		$customer_service_fee = $this->input->post('service_fee_to_customer');

		if($order_id != '' && $customer_service_fee != '')
		{
			$order_update_data['customer_service_fee'] = $customer_service_fee;
			$order_update_data['order_id']	= $order_id;
			$order_update_data['added_by'] = $this->session->userdata['logged_in_user']['userid'];
			$order_update_data['created_on'] = date('Y-m-d H:i:s');
			$this->Welcome_model->order_update_data($order_update_data,$order_id);

			echo json_encode(['error'=>false,'message'=>'Successfully Updated']);exit;
		}
		echo json_encode(['error'=>true,'message'=>'Failed !']);exit;
	}
	
	/**
	 * sets the payment terms
	 * for the project
	 */
	function ajax_get_customer_service_fee()
	{
		$this->load->helper('text');
		$jobid = $this->input->post('order_id');
		$records = $this->Welcome_model->get_customer_service_records_list($jobid);
		
		$html = "<table class='data-table'>";
		$html .= "<thead>";
			$html .= "<tr><th>Activity</th><th>Added On</th><th>Added By</th></tr></thead>";
		$html .= "</thead>";	
		$html .= "<tbody>";
		foreach($records as $rec)
		{		
			$html .= "<tr>";
			if($rec['customer_service_fee'] == 1){
				$html .= "<td>Enabled</td>";
			}else{
				$html .= "<td>Disabled</td>";
			}
			$html .= "<td>".date('m-d-Y H:i:s',strtotime($rec['created_on']))."</td>";
			$html .= "<td>".$rec['first_name'].' '.$rec['last_name']."</td>";
			$html .= "</tr>";	
		}
		$html .= "</tbody>";
		$html .= "</table>";
		
		echo $html; exit;
	}
}	
?>
