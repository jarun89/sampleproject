<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Welcome_model extends CRM_Model {
    
    public function __construct()
    {
		parent::__construct();
		$this->userdata = $this->session->userdata('logged_in_user');
		$this->load->helper('lead_stage_helper');
		$this->load->model('Digital_retrieval_vendor_model');
		$this->stg = getLeadStage();
		$this->stages = @implode('","', $this->stg);
    }
	
	/*
	*Get the Lead Detail
	*/
	public function get_lead_detail($leadid) {
	
		// j.orderref,

		$this->db->select('j.lead_id,j.is_reopen,od.archive_done_at,od.is_retention_done,cm.is_archive_process,j.record_type,j.customer_service_fee,j.invoice_no,j.order_sub_no,j.reopen_order_no,j.p_ext1,j.p_ext2,j.p_ext3,j.p_phonetype1,j.p_phonetype2,j.p_phonetype3,j.custid_fk,j.p_fax,j.p_contact,j.lead_title,cm.aps_summary,cm.company_name as carrier_name,j.p_zipcode,j.p_phone1,j.p_phone2,j.p_phone3,j.p_email,j.p_gender,j.p_othername,j.lead_title_last, j.orderref, j.policy_amount, j.lead_service, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to, j.created_by, j.expect_worth_amount, j.actual_worth_amount, j.expect_worth_id, j.division, j.lead_indicator, j.lead_status, j.lead_assign, j.proposal_expected_date, j.log_view_status, j.lead_hold_reason, j.assigned_to, j.internal_notes, j.patient_dob, j.ss_no, j.patient_adr, j.p_city, j.p_state, j.order_no, j.confirmed_close_date, j.time_frame, c.*, c.position_title, c.company, c.first_name AS cfn, c.last_name AS cln, c.add1_region, c.add1_country, c.add1_state, c.add1_location, rg.region_name, coun.country_name, st.state_name, c.add1_location as location_name, cs.first_name AS cfn_c, cs.last_name AS cln_c, cs.add1_region as  add1_region_c, cs.add1_country as add1_country_c, cs.company as company_c,cs.add1_line1 as add1_line1_c, cs.position_title as position_title_c, cs.add1_state as add1_state_c, cs.add1_location as add1_location_c,cs.phone_1 as phone_1_c,cs.add1_postcode as add1_postcode_c,cs.phone_4 as phone_4_c,cs.email_1 as email_1_c, rgc.region_name as region_name_c, counc.country_name as country_name_c, stc.state_name as state_name_c, cs.add1_location as location_name_c, ass.first_name as assfname, ass.last_name as asslname, us.first_name as usfname, us.last_name as usslname, own.first_name as ownfname, own.last_name as ownlname, ls.lead_stage_name,ew.expect_worth_name,j.agent_id, lsrc.lead_source_name, jbcat.services as lead_service, sadiv.division_name,(SELECT GROUP_CONCAT(lsh.dateofchange) FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id) as dateofchange,j.order_disposition_id,j.aps_order_no,j.reopen_order_no,j.special_instructions,j.company_type,rt.request_type_name,j.user_field1,j.user_field2,j.user_field3,j.user_field4,j.service_type,j.special_instruction_type,j.certified_request,j.affidavit_request,j.custom_to_date,j.custom_from_date,j.adjuster_id,j.attorney_id,j.order_handle_by,j.is_order_assigned_permanent,j.date_of_incident,j.orderingoffice,j.aps_summarization,cm.company_code,j.close_code_id,cc.close_code,cc.description,orq.requestor_id,orq.requestor_name,orq.requestor_email,orq.requestor_phone,j.ordering_office_id,coo.office_name,coo.office_account_number,coo.email as office_email,coo.phone as office_phone,j.ordered_by,j.missing_facility,j.customer_details,j.azure_failed,j.azure_download_file_path,cm.company_code as comp_code,j.is_print_request_packet,j.carrier_manager_id,j.account_number,j.coversheet_notes,j.special_instruction_start_date,j.special_instruction_end_date,j.date_created,cm.digital_retrieval_vendor_used');

		$this->db->select("CONCAT(COALESCE(j.order_no, ''), ' ', COALESCE(j.order_sub_no, '')) AS orderno", FALSE);
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->join($this->cfg['dbpref'] . 'order_retention_details as od', 'j.lead_id = od.order_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');		
		$this->db->join($this->cfg['dbpref'] . 'custodian as cs', 'cs.custid = j.custid_fk', 'LEFT');		
		$this->db->join($this->cfg['dbpref'] . 'users as ass', 'ass.userid = j.lead_assign', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as own', 'own.userid = j.belong_to', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'country as coun', 'coun.countryid = c.add1_country', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
		
		$this->db->join($this->cfg['dbpref'] . 'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'country as counc', 'counc.countryid = cs.add1_country', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
		
	//	$this->db->join($this->cfg['dbpref'] . 'location as loc', 'loc.locationid = c.add1_location');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
		$this->db->join($this->cfg['dbpref']."lead_source as lsrc", 'lsrc.lead_source_id = j.lead_source', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'lead_services as jbcat', 'jbcat.sid = j.lead_service', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'sales_divisions as sadiv', 'sadiv.div_id = j.division', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code AND cm.company_type = j.company_type', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'request_type as rt', 'rt.request_type_id = j.request_type_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'close_codes as cc', 'cc.close_code_id = j.close_code_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'order_requestor as orq', 'orq.requestor_id = j.requestor_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
		// $this->db->join($this->cfg['dbpref'] . 'carrier_manager as cam', 'cam.carrier_manager_id = j.carrier_manager_id', 'LEFT');
		$this->db->where('j.lead_id = "'.$leadid.'" AND j.lead_stage IN ("'.$this->stages.'")');
		$this->db->where('j.pjt_status', 0);
		
		$sql = $this->db->get();
		// echo $this->db->last_query(); exit;
	    $res =  $sql->result_array();
	    return $res;
	}

	public function get_api_lead_detail($leadid) {
		$this->db->select('j.lead_id as Expert_Tool_id, j.lead_title as Patient_FName, j.lead_title_last as Patient_LName, j.patient_dob as Patient_DOB, j.ss_no as Social_Security_Number, j.patient_adr as Patient_Address, j.p_city as Patient_City, j.p_state as Patient_State,j.orderref as EIC_Claim, ew.expect_worth_name as Expected_Fee, j.time_frame as Time_Frame_of_Records, j.lead_indicator as Order_Priority, ls.lead_stage_id as Order_Stage_id, ls.lead_stage_name as Order_Stage, j.confirmed_close_date as Order_Closed_Date,j.date_modified,j.lead_indicator');
		$this->db->select("CONCAT(COALESCE(j.lead_title, ''), ' ', COALESCE(j.lead_title_last, '')) AS  Patient_Name", FALSE);
		$this->db->select("CONCAT(COALESCE(usr.first_name, ''), ' ', COALESCE(usr.last_name, '')) AS Order_Owner", FALSE);
		$this->db->select("CONCAT(COALESCE(u.first_name, ''), ' ', COALESCE(u.last_name, '')) AS Order_Assigned_To", FALSE);
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk','LEFT');	
		$this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as usr', 'usr.userid = j.created_by','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = j.belong_to','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage','LEFT');
		$this->db->where('j.reopen_order_no = "'.$leadid.'" AND j.lead_stage IN ("'.$this->stages.'")');
		$this->db->where('j.pjt_status', 0);
		
		$sql = $this->db->get();
		$result =  $sql->result_array();
	   
		return $result;
	}	
 	
 	public function get_api_lead_agent_detail($leadid) {
		$this->db->select('j.lead_id as Expert_Tool_id, j.lead_title as Patient_FName, j.lead_title_last as Patient_LName, j.patient_dob as Patient_DOB, j.ss_no as Social_Security_Number, j.patient_adr as Patient_Address, j.p_city as Patient_City, j.p_state as Patient_State,j.orderref as Policy_No, ew.expect_worth_name as Expected_Fee, j.time_frame as Special_Instructions, j.lead_indicator as Order_Priority, ls.lead_stage_id as Order_Stage_id, ls.lead_stage_name as Order_Stage, j.confirmed_close_date as Order_Closed_Date,j.date_modified,j.p_zipcode as Zip,j.p_contact as Prefered_Contact,j.p_othername as OtherName,j.p_email as Patient_Email,j.p_gender as Gender,j.p_phone1 as Patient_Phone1,j.p_phone2 as Patient_Phone2,j.p_ext1,j.p_ext2,j.p_phonetype1,j.p_phonetype2,j.p_fax,j.proposal_expected_date,a.agent_first_name,a.agent_last_name,a.agent_email_id,a.agent_work_phone,a.agent_ext,a.agent_cell_ph,a.agent_fax,a.aps_agent_id,j.aps_order_no,j.reopen_order_no');
		$this->db->select("CONCAT(COALESCE(j.lead_title, ''), ' ', COALESCE(j.lead_title_last, '')) AS  Patient_Name", FALSE);
		$this->db->select("CONCAT(COALESCE(usr.first_name, ''), ' ', COALESCE(usr.last_name, '')) AS Order_Owner", FALSE);
		$this->db->select("CONCAT(COALESCE(u.first_name, ''), ' ', COALESCE(u.last_name, '')) AS Order_Assigned_To", FALSE);
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk','left');	
		$this->db->join($this->cfg['dbpref'] . 'agents as a','a.id = j.agent_id','left');
		$this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id','left');
		$this->db->join($this->cfg['dbpref'] . 'users as usr', 'usr.userid = j.created_by','left');
		$this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = j.belong_to','left');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage','left');
		$this->db->where('(j.reopen_order_no = "'.$leadid.'") AND j.lead_stage IN ("'.$this->stages.'")');
		$this->db->where('j.pjt_status', 0);
		
		$sql = $this->db->get();
		
		$result =  $sql->result_array();
	   
		return $result;
	}
	
	public function get_facility_details($lead_id)
	{
		$this->db->select('c.custid as FacilityId, c.company as FacilityName, c.first_name as DrFirstName, c.last_name as DrLastName, c.add1_line1 as DrAddress, rg.region_name as DrCity, st.state_name as DrStateName, c.add1_postcode as DrZipCode, c.phone_1 as DrPhone, c.phone_4 as DrFax');
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region','left');
		$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state','left');
		$this->db->where('j.lead_id = "'.$lead_id.'" AND j.lead_stage IN ("'.$this->stages.'")');
		$this->db->where('j.pjt_status', 0);
		$sql = $this->db->get();
		//echo $this->db->last_query(); exit;
	    $result_facility =  $sql->result_array();
		return $result_facility;
	}
	
	public function get_order_comments($lead_id)
	{
        $this->db->select('l.logid as id, u.userid, u.first_name, u.last_name');
        $this->db->select('l.logid as id, rt.rec_type_name as Record_Type, dis.disposition_name as Call_Status, lds.lead_stage_name as Status_Follow_Up, l.log_content as comments_description,  l.date_created as last_updated_date');
        $this->db->select("CONCAT(COALESCE(u.first_name, ''), ' ', COALESCE(u.last_name, '')) AS last_updated_by", FALSE);
        $this->db->from($this->cfg['dbpref']."logs as l");
        $this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = l.userid_fk','LEFT');
        $this->db->join($this->cfg['dbpref'] . 'record_type as rt', 'rt.rec_type_id = l.record_type','LEFT');
        $this->db->join($this->cfg['dbpref'] . 'disposition as dis', 'dis.id = l.disposition_id','LEFT');
        $this->db->join($this->cfg['dbpref'] . 'lead_stage as lds', 'lds.lead_stage_id = l.lead_stage','LEFT');
        $this->db->where('jobid_fk',$lead_id); 
		    
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		$result = $query->result_array(); 
		return $result;
	}
	
	public function get_pdf_detail($id,$companyType) {
		
		$this->db->select('j.*,j.lead_id, j.invoice_no, j.lead_title, j.lead_title_last, j.orderref, j.lead_service, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to, j.created_by, j.expect_worth_amount, j.actual_worth_amount, j.expect_worth_id, j.division, j.lead_indicator, j.lead_status, j.lead_assign, j.proposal_expected_date, j.log_view_status, j.lead_hold_reason, j.assigned_to, j.internal_notes, j.patient_dob, j.ss_no, j.patient_adr, j.p_city, j.p_state, j.order_no, j.confirmed_close_date, j.time_frame, c.*, c.first_name AS cfn, c.last_name AS cln, c.add1_region, c.add1_country, c.add1_state, c.add1_location, rg.region_name, coun.country_name, st.state_name, c.add1_location as location_name, ass.first_name as assfname, ass.last_name as asslname, us.first_name as usfname, us.last_name as usslname, own.first_name as ownfname, own.last_name as ownlname, ls.lead_stage_name,ew.expect_worth_name, lsrc.lead_source_name, jbcat.services as lead_service, sadiv.division_name,(SELECT GROUP_CONCAT(lsh.dateofchange) FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id) as dateofchange');		
		$this->db->select("CONCAT(COALESCE(j.order_no,''),' ',COALESCE(j.order_sub_no,'')) AS orderno", FALSE);
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		if($companyType == 1){
			$this->db->join($this->cfg['dbpref'] . 'custodian as c', 'c.custid = j.custid_fk', 'LEFT');		
		}else{
			$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');		
		}
		$this->db->join($this->cfg['dbpref'] . 'users as ass', 'ass.userid = j.lead_assign', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as own', 'own.userid = j.belong_to', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'country as coun', 'coun.countryid = c.add1_country', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
		//$this->db->join($this->cfg['dbpref'] . 'location as loc', 'loc.locationid = c.add1_location');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'lead_source as lsrc', 'lsrc.lead_source_id = j.lead_source', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'lead_services as jbcat', 'jbcat.sid = j.lead_service', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'sales_divisions as sadiv', 'sadiv.div_id = j.division', 'LEFT');
		$this->db->where('j.lead_id = "'.$id.'" AND j.lead_stage IN ("'.$this->stages.'")');
		$this->db->where('j.pjt_status', 0);
		
		$sql = $this->db->get();
		// echo $this->db->last_query(); exit;
	    $res =  $sql->result_array();
	    return $res;
		
	}
	
	
	function get_lead_all_detail($id) 
	{
		$this->db->select('j.*, j.company_type as lead_company_type');
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->select("CONCAT(j.order_no,' ',j.order_sub_no) AS orderno", FALSE);
		$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as u', 'j.created_by = u.userid','LEFT');
		$this->db->where('lead_id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			$data = $query->result_array();
			return $data[0];
		}
		else
		{
			return FALSE;
		}
	}

	function get_ordersubno($id) 
	{
		$this->db->select('j.order_sub_no');
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->where('order_no', $id);
		$this->db->order_by("order_sub_no", "DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}
	function get_queue_lead_stage($proposal_expected_from_date,$proposal_expected_to_date,$lead_assign,$company_type) 
	{
		if($company_type != 'all' && $company_type != 'null' && $company_type != '')
		{
			$company_type = explode(',',$company_type);
		}
		$user_order_handle = array();
		$user_order_handle = explode(',',$this->userdata['order_handle_by']);
		
		$this->db->select("queue_management_group,queue_management_status,count('j.lead_stage') as lead_stage_count");
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage','LEFT');
		if($proposal_expected_from_date != 'null' && $proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date == '')
		{
			$proposal_expected_to_date = date('Y-m-d');
		}
		if($proposal_expected_to_date != null &&  $proposal_expected_to_date !='' && $proposal_expected_to_date !='null' && $proposal_expected_from_date == '')
		{
			$proposal_expected_from_date = date('Y-m-d',strtotime('-60 days'));
		}
		if($proposal_expected_from_date!='' && $proposal_expected_to_date!=''){
		$this->db->where('DATE(j.proposal_expected_date) between "'.$proposal_expected_from_date.'" and "'.$proposal_expected_to_date.'"');
		}
		$this->db->where('ls.is_deleted',0);
		$this->db->group_by('queue_management_group,queue_management_status');
		
		//  First Order handle by condition based on admin login, If not set advance filter order handle by apply 
		if($this->userdata['role_id'] != 1 && count($user_order_handle)>0){
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle))
			{
				$keyword = '';
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'){
					if($lead_assign && $this->userdata['role_id'] != 29){
						$this->db->where('j.lead_assign',$lead_assign);	
					}
					$this->db->where('j.order_handle_by',$this->config->item('crm')['onshore_flag']);
				} 
			}
			else
			{
				if($lead_assign && $this->userdata['role_id'] != 29){
					$this->db->where('j.lead_assign',$lead_assign);	
				}
				$this->db->where_in('j.order_handle_by',$user_order_handle);
			}
			$company_code='';
			if($this->userdata['company_list'] != 'all' && $this->userdata['company_list'] != 'null' && $this->userdata['company_list'] != '')
			{
				$company_code = explode(',',$this->userdata['company_list']);
			}
			if($company_code != 'all' && $company_code != '' && $company_code != 'null')
			{
				$this->db->where_in('j.company_code',$company_code);
			}
			if($this->userdata['company_type'] != 'all' && $this->userdata['company_type'] != 'null' && $this->userdata['company_type'] != '')
			{
				$user_company_type = explode(',',$this->userdata['company_type']);
			}
			if($company_type != '' && $company_type != 'null' && $company_type != 'all')
			{
				$this->db->where_in('j.company_type',$company_type);
			}else if($user_company_type != '' && $user_company_type != 'null' && $user_company_type != 'all')
			{
				$this->db->where_in('j.company_type',$user_company_type);
			}
		}
		else{
			if($company_type != '' && $company_type != 'null' && $company_type != 'all')
			{
				$this->db->where_in('j.company_type',$company_type);
			}
		}
		$query = $this->db->get();
		
		// echo '<pre>';print_r($this->db->last_query());
		// exit;
		if ($query->num_rows() > 0)
		{
			$results=$query->result_array();
			foreach($results as $result){
				$queue_lead_stage_values[$result['queue_management_group']][$result['queue_management_status']]=$result['lead_stage_count'];
			}
			return $queue_lead_stage_values;
		}
		else
		{
			return FALSE;
		}
	}
// stage bucket count not matching query start
	function get_not_queue_stages($queue_ind)
	{
		$this->db->select("queue_management_group,queue_management_status");
		$this->db->from($this->cfg['dbpref'] . 'lead_stage as ls');
		$this->db->where('ls.is_deleted',0);
		$this->db->where_not_in('ls.queue_management_group',$queue_ind);
		$query = $this->db->get();
		// echo '<pre>';print_r($this->db->last_query()); exit;
		if ($query->num_rows() > 0)
		{
			$results=$query->result_array();
			foreach($results as $result){
				$queue_lead_stage_values[$result['queue_management_group']][$result['queue_management_status']]=0;
			}
			return $queue_lead_stage_values;
		}
		else
		{
			return FALSE;
		}
	}
// stage bucket count not matching query end

	//function for get order if have event type based only check created & check requested.
	function get_queue_event_type($leadStage,$eventType,$lead_assign,$company_type = NULL){
		$this->db->select("count(lead_id) as orderCount,order_disposition_id");
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->where('j.order_disposition_id',$eventType);
		$this->db->where_not_in('j.lead_stage',$leadStage);
		if($lead_assign && $this->userdata['role_id'] != 29){
			$this->db->where('j.lead_assign',$lead_assign);	
		}
		if($company_type != NULL && $company_type != 'all'){
			$this->db->where_in('j.company_type',$company_type);
		}
		$query = $this->db->get();
		// echo '<pre>';print_r($this->db->last_query());//exit;
		return $query->result_array();
	}

	function get_queue_event_type_count($leadStage,$eventType,$lead_assign,$company_type = NULL,$proposal_expected_from_date_new,$proposal_expected_to_date_new){
		$this->db->select("count(lead_id) as orderCount,order_disposition_id");
		$this->db->from($this->cfg['dbpref'] . 'leads as j');
		$this->db->where('j.order_disposition_id',$eventType);
		$this->db->where_not_in('j.lead_stage',$leadStage);
		// if($proposal_expected_from_date!='' && $proposal_expected_to_date!=''){
			$this->db->where('DATE(j.proposal_expected_date) between "'.$proposal_expected_from_date_new.'" and "'.$proposal_expected_to_date_new.'"');
		// }	
		if($lead_assign && $this->userdata['role_id'] != 29){
			$this->db->where('j.lead_assign',$lead_assign);	
		}
		if($company_type != NULL && $company_type != 'all'){
			$this->db->where_in('j.company_type',$company_type);
		}
		$query = $this->db->get();
		// print_r($this->db->last_query()); exit;
		return $query->result_array();
	}

	//function to get US Holidays
	function get_us_holidays($from_date,$to_date) 
	{
		// print_r($from_date);exit;
		$this->db->select('*');
		$this->db->from($this->cfg['dbpref'] . 'us_holidays as r');
		$this->db->where('r.leave_date between "'.$from_date.'" and "'.$to_date.'"');
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	//function to get the date for lead status as completed
	function get_stage_completed_date($lead_id) 
	{
		// print_r($from_date);exit;
		$this->db->select('dateofchange');
		$this->db->from($this->cfg['dbpref'] . 'lead_stage_history as r');
		$this->db->where('lead_id', $lead_id);
		$this->db->order_by("r.dateofchange", "DESC");
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}
	
	function get_records_list($id) 
	{
		$this->db->select('*,r.id as auto_id,(select concat(first_name,last_name) from crm_users where userid = r.created_by) as upload_by');
		$this->db->from($this->cfg['dbpref'] . 'records as r');
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title');
		$this->db->where('r.jobid', $id);
		$this->db->order_by("r.id", "DESC");
		
		//print_r($this->db->last_query());exit;
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_records_list_wo_LQ($id){
		$arr_LQ = array($this->config->item('crm')['letter_of_rep'],$this->config->item('crm')['questionnaire']);

		$this->db->select('*,r.id as auto_id');
		$this->db->from($this->cfg['dbpref'] . 'records as r');
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title');
		$this->db->where_not_in('r.record_title', $arr_LQ);
		$this->db->where('r.jobid', $id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_records_list_specific($id){
		$arr_auth_spauth = array(5,11);

		$this->db->select('*,r.id as auto_id');
		$this->db->from($this->cfg['dbpref'] . 'records as r');
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title');
		$this->db->where_in('r.record_title', $arr_auth_spauth);
		$this->db->where('r.jobid', $id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_records_list_w_L($id){
		$lorId = $this->config->item('crm')['letter_of_rep'];

		$this->db->select('*,r.id as auto_id');
		$this->db->from($this->cfg['dbpref'] . 'records as r');
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title');
		$this->db->where('r.record_title', $lorId);
		$this->db->where('r.jobid', $id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_records_list_w_Q($id){
		$QuesId = array($this->config->item('crm')['questionnaire']);

		$this->db->select('*,r.id as auto_id');
		$this->db->from($this->cfg['dbpref'] . 'records as r');
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title');
		$this->db->where_in('r.record_title', $QuesId);
		$this->db->where('r.jobid', $id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
	
	function get_records_result_list($id,$record_id = '') 
	{
		$this->db->select('*,r.id as auto_id');
		$this->db->from($this->cfg['dbpref'] . 'records as r');
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title','LEFT');
		$this->db->where('r.jobid', $id);
		$this->db->where('r.id', $record_id);
		$this->db->where('r.record_title', 13);
		//$this->db->where('r.is_file_summary_processed', 0); // not processed result pending file
		$this->db->order_by("r.id", "DESC");
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}	
	 
	function get_service_records_list($id) 
	{
		$this->db->select('r.*,csr.id as service_record_id,csr.copy_service_id,csr.lead_id,ls.lead_id');
		$this->db->from($this->cfg['dbpref'] . 'copy_service as r');
		$this->db->join($this->cfg['dbpref'] . 'copy_service_record as csr', 'csr.copy_service_id = r.id','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'leads as ls', 'ls.lead_id = csr.lead_id','LEFT');
		
		$this->db->where('ls.lead_id', $id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
				        // echo $this->db->last_query();exit;

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}	

	function view_service_records_list($service_record_id) 
	{
		$this->db->select('csr.*,r.id as service_record_id,r.copy_service_id,r.lead_id,ls.lead_id');
		$this->db->from($this->cfg['dbpref'] . 'copy_service_record as r');
		$this->db->join($this->cfg['dbpref'] . 'copy_service as csr', 'csr.id = r.copy_Service_id','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'leads as ls', 'ls.lead_id = r.lead_id','LEFT');
		$this->db->where('r.id', $service_record_id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return FALSE;
		}
	}	
	
	function get_associate_orders($order_no, $id,$company_code)
	{
		$this->db->select('l.lead_id,l.order_no,l.order_sub_no,l.reopen_order_no,l.aps_order_no,ls.color_code');
		$this->db->from($this->cfg['dbpref'] . 'leads as l');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = l.lead_stage','LEFT');
		$this->db->where('aps_order_no = "'.trim($order_no).'" AND company_code = "'.$company_code.'"');
		$this->db->where('lead_id != ', $id);
		$this->db->order_by("lead_id", "ASC");
		$query = $this->db->get();
		// print_r($this->db->last_query()); echo '<br>';
		return $query->result_array();
	}
	
	function get_users()
	{
    	$this->db->select('userid, first_name, last_name, level, role_id, inactive,company_type,company_list');
		$this->db->where('inactive', 0);
		if($this->userdata['role_id'] != 1)
		{
			// $company_type = explode(',',$this->userdata['company_type']);
			// if($this->userdata['company_list'] != 'all')
			// {
				// $company_list = explode(',',$this->userdata['company_list']);
				// $this->db->where_in('company_list', $company_list);
			// }
			// $this->db->where_in('company_type', $company_type);
			$where = NULL;
			$company_type = explode(',',$this->userdata['company_type']);
			$where.='( ';
			foreach($company_type as $ctp){
				$where .= "FIND_IN_SET('".$ctp."',`company_type`) <> 0 OR ";
			} 
			$where = rtrim($where,"OR ");
			$where.=" )";
			$this->db->where($where);
		}
    	$this->db->order_by('first_name', "asc");
		$q = $this->db->get($this->cfg['dbpref'] . 'users');
		// print_r($this->db->last_query());exit;
		return $q->result_array();
    }
	
	function get_lead_services() 
	{
    	$this->db->select('sid, services');
		$this->db->where('status', 1);
    	$this->db->order_by('sid');
		$q = $this->db->get($this->cfg['dbpref'] . 'lead_services');
		return $q->result_array();
    }
	
	function get_sales_divisions() 
	{
    	$this->db->select('div_id, division_name');
		$this->db->where('status', 1);
    	$this->db->order_by('div_id');
		$q = $this->db->get($this->cfg['dbpref'] . 'sales_divisions');
		return $q->result_array();
    }

	function get_pstates() 
	{
    	$this->db->select('state_list, val');
    	$this->db->order_by('id');
		$q = $this->db->get($this->cfg['dbpref'] . 'state_list');
		return $q->result_array();
    }
	
	function get_userlist($userList) {
		$userdata=$this->session->userdata('logged_in_user');
		    	$this->db->select('userid,first_name,last_name,level,role_id,inactive');
		$this->db->where('inactive', 0);
		if(!empty($userList))
		$this->db->where_in('userid', $userList);
		if($userdata['role_id']=='27')
		$this->db->where_in('role_id', '1');
    	$this->db->order_by('first_name', "asc");
		$q = $this->db->get($this->cfg['dbpref'] . 'users');
		return $q->result_array();
    }
	
	function get_user_data_by_id($ld) {
		$this->db->where('userid', $ld);
		$user = $this->db->get($this->cfg['dbpref'] . 'users');
		return $user->result_array();
	}
	
	function get_user_byrole($role_id) {
    	$users = $this->db->get_where($this->cfg['dbpref'] . 'users', array('role_id'=>$role_id))->result_array();
    	return $users;
    }
	
	function get_customers() {
		$cusId = $this->level_restriction();
		
		$userdata = $this->session->userdata('logged_in_user');
		
	    $this->db->select('custid, first_name, last_name, company, phone_1');
	    $this->db->from($this->cfg['dbpref'] . 'customers');
		if ($this->userdata['level']!=1) {
			$this->db->where_in('custid', $cusId);
		}
		$this->db->order_by("first_name", "asc");
	    $customers = $this->db->get();
	    //echo $this->db->last_query();
	    $customers=  $customers->result_array();
	    return $customers;
	}
	
	function get_carrier_name(){
		// $this->db->distinct();
		$this->db->select('company_id,company_code,company_name as carrier_name');
		$this->db->from($this->cfg['dbpref'] . 'company as j');
		$this->db->where('is_deleted',0);
	    $carr_det = $this->db->get();
	    return $carr_det->result_array();
	}

	function get_customer_det($cid) {
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'customers');
	    $this->db->where('custid', $cid);
	    $cust_det = $this->db->get();
	    return $cust_det->row_array();
	}
	
	function get_custodian_det($cid) {
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'custodian');
	    $this->db->where('custid', $cid);
	    $cust_det = $this->db->get();
	    return $cust_det->row_array();
	}
	
	function get_client_data_by_id($cid) {
		$this->db->where('custid', $cid);
		$client = $this->db->get($this->cfg['dbpref'] . 'customers');
		return $client->result_array();
	}
	
	public function updt_log_view_status($id, $log) {
		$this->db->where('lead_id', $id);
		return $this->db->update($this->cfg['dbpref'] . 'leads', $log);
	}
	
	function get_logs($id) {
		$this->db->order_by('date_created', 'desc');
		$this->db->order_by('logid', 'desc');
		$query = $this->db->get_where($this->cfg['dbpref'] . 'logs', array('jobid_fk' => $id));
		return $query->result_array();
	}
    
function get_disposition($id) {
		$this->db->select('lsh.time_service_start_time, lsh.time_service_stop_time');
	    $this->db->from($this->cfg['dbpref']."lead_stage_stop_time_service as lsh");
	    $this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = lsh.disposition_id', 'LEFT');
		$this->db->where("lead_id", $id);
		$this->db->where("time_service_stop", 1);
	    $lead_stg_his = $this->db->get();
		// echo $this->db->last_query();die;
	    $lead_sh=  $lead_stg_his->result_array();
	
	}
	
	function get_stop_tat_service($id) {
		$this->db->select('st.lead_start_time, st.lead_stop_time');
	    $this->db->from($this->cfg['dbpref'].'stop_tat st');
		$this->db->where("lead_id", $id);
	    $lead_stg_his = $this->db->get();
		//echo $this->db->last_query();die;
	    return $lead_stg_his->result_array();
	
	}
	
	
	function get_real_ts_service($id) {
		$this->db->select('lsh.time_service_start_time, lsh.time_service_stop_time');
	    $this->db->from($this->cfg['dbpref'].'lead_stage_stop_time_service lsh');
	    $this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = lsh.disposition_id', 'LEFT');
		$this->db->where("lead_id", $id);
		$this->db->where("lead_status", 1);
	    $lead_stg_his = $this->db->get();
		//  echo $this->db->last_query();die;
	    return $lead_stg_his->result_array();
	
	}

	function get_stage_log($id) {
		$this->db->select('jobid_fk,lead_stage,DATE(date_created)');
	    $this->db->from($this->cfg['dbpref'] . 'logs');
	    $this->db->where('(lead_stage = 34 OR lead_stage = 35)');
	    $this->db->where('jobid_fk', $id);
		$this->db->order_by('logid', 'desc');
		$this->db->limit(1);
	    $cust_det = $this->db->get();
		// echo $this->db->last_query(); exit;
	    return $cust_det->num_rows();
	}

function get_quote_items($lead_id) {
		$this->db->where('jobid_fk', $lead_id);
        $this->db->order_by('item_position', 'asc');
        $sql = $this->db->get($this->cfg['dbpref'] . 'items');
		return $sql->result_array();
	}
    /*
    public function get_job_files($f_dir, $fcpath, $lead_details)
    {
		// echo $lead_details['lead_id']; exit;
		$userdata = $this->session->userdata('logged_in_user'); 
        $data['job_files_html'] = '';
        if (is_dir($f_dir))
        {
            $job_files = glob($f_dir . '*.*');
			
            if (is_array($job_files) && count($job_files))
            {
                foreach ($job_files as $jf)
                {
                    $data['job_files_html'] .= '<li>';
                     if ( $userdata['role_id'] == 1 || $lead_details['belong_to'] == $userdata['userid'] || $lead_details['lead_assign'] == $userdata['userid'] || $lead_details['assigned_to'] == $userdata['userid'] )  {  
                        $data['job_files_html'] .= '<a href="#" onclick="ajaxDeleteFile(\'/' . str_replace($fcpath, '', $jf) . '\', this); return false;" class="file-delete">delete file</a>';
						}
                    
                    // echo $fcpath . ' '.$jf; exit;
                    $fz = filesize($jf);
                    $kb = 1024;
                    $mb = 1024 * $kb;
                    if ($fz > $mb)
                    {
                      $out = round($fz/$mb, 2);
                      $out .= 'Mb';
                    }
                    else if ($fz > $kb) {
                      $out = round($fz/$kb, 2);
                      $out .= 'Kb';
                    } else {
                      $out = $fz . ' Bytes';
                    }
					
                    $data['job_files_html'] .= '<a href="crm_data/' . str_replace($fcpath, '', $jf) . '" onclick="window.open(this.href); return false;">' . str_replace($f_dir, '', $jf) . '</a> <span>' . $out . '</span>';
					// $data['job_files_html'] .= '<a onclick="download_files_id('.str_replace($f_dir, '', $jf).'); return false;">' . str_replace($f_dir, '', $jf) . '</a> <span>' . $out . '</span>';
		    $data['job_files_html'] .='</li>';
                }
            }
        }

        return $data['job_files_html'];
    }*/
	
	 public function get_job_files($id)
    {
		//  $lid = $lead_details['lead_id'];
		$userdata = $this->session->userdata('logged_in_user'); 
        $data['job_files_html'] = '';
       $file_tab = "SELECT * FROM ".$this->cfg['dbpref']."lead_files WHERE lead_id=".$id." ORDER BY lead_files_created_on DESC";
		
		$results = $this->db->query($file_tab);
		$results = $results->result_array();
		$path = 'crm_data/files/' . $id;
		$fcpath = UPLOAD_PATH; 
		$f_dir = $fcpath . 'files/' . $id . '/'; 
		foreach($results as $result) {
		$orifname=$result["lead_ori_file_name"];
				$jf1= $f_dir.$result["lead_files_name"];
				$jf= $f_dir.$result["lead_files_name"]; 
					$fz = filesize($jf);				   
                    $kb = 1024;
                    $mb = 1024 * $kb;
                    if ($fz > $mb)
                    {
                      $out = round($fz/$mb, 2);
                      $out .= 'Mb';
                    }
                    else if ($fz > $kb) {
                      $out = round($fz/$kb, 2);
                      $out .= 'Kb';
                    } else {
                      $out = $fz . ' Bytes';
                    }
					 if($fz!=''){
						$data['job_files_html'] .= '<li>';
						if ( $userdata['role_id'] == 1 || $userdata['role_id'] == 29 || $lead_details['belong_to'] == $userdata['userid'] || $lead_details['lead_assign'] == $userdata['userid'] || $lead_details['assigned_to'] == $userdata['userid'] )  {  
							$data['job_files_html'] .= '<a href="#" onclick="ajaxDeleteFile(\'/' . str_replace($fcpath, '', $jf1) . '\',\'' . $orifname . '\', this); return false;" class="file-delete">delete file</a>';
						}
                        // $data['job_files_html'] .= '<a href="crm_data/' . str_replace($fcpath, '', $jf) . '" onclick="window.open(this.href); return false;">' . str_replace($f_dir, '', $jf) . '</a> <span>' . $out . '</span>';					
						$data['job_files_html'] .= '<a onclick="download_filesid('.$result["lead_id"].','.$result["leadfileid"].'); return false;">' . $result["lead_ori_file_name"] . '</a><span>' . $out . '</span>';
						$data['job_files_html'] .='</li>';
					}
	   }

        return $data['job_files_html'];
    }
	
    public function get_query_files_list($lead_id)
    {
		$userdata = $this->session->userdata('logged_in_user');
		
		$this->db->select('lead_assign,belong_to');
		$this->db->where('lead_id', $lead_id);
        $sql = $this->db->get($this->cfg['dbpref'] . 'leads');
		$lead_det = $sql->row_array();
		
        $data['query_files1_html'] = '';       
		$query_tab = "SELECT lq.lead_id, us.first_name,us.last_name, lq.query_msg, lq.query_instruction, lq.query_id, lq.query_file_name, lq.query_sent_date, lq.replay_query,lq.ori_query_file_name
		FROM ".$this->cfg['dbpref']."lead_query as lq
		LEFT JOIN ".$this->cfg['dbpref']."users as us ON us.userid= lq.user_id WHERE lq.lead_id=".$lead_id." ORDER BY lq.query_sent_date DESC";
		
		$results = $this->db->query($query_tab);
		$results = $results->result_array();
		$path = 'crm_data/query/' . $lead_id. '/';
		
		foreach($results as $result) {	
			if($result['replay_query'] == 0) {
			$class = "Raised";
			} else  {
			$class = "Replied";
			}
			
			if($result['query_file_name']== 'File Not Attached') {
				$fname = "File Not Attached";
			} else {
				//$fname = '<a href="'.$path.$result['query_file_name'].'" onclick="window.open(this.href); return false;">'.$result['query_file_name'].'</a>';
				$fname = '<a onclick="download_files_query('.$result["lead_id"].','.$result["query_id"].'); return false;">'.$result['ori_query_file_name'].'</a>';
			}
			
			$data['query_files1_html'] .='<tr><td>
			<table border="0" cellpadding="5" cellspacing="5" class="task-list-item" id="task-table-15">
				<tbody><tr>
					<td valign="top" width="80">
						Query '.$class.'
					</td>
					<td colspan="3" class="task">
						'.urldecode($result['query_msg']).' 
					</td>
				</tr>
				<tr>
					<td valign="top" width="80">
						Instruction
					</td>
					<td colspan="3" class="task">
						'.urldecode($result['query_instruction']).' 
					</td>
				</tr>
				<tr>
					<td>
						Date & Time
					</td>
					<td class="item user-name" rel="59" width="100">
						'.$result['query_sent_date'].'
					</td>
					<td width="80">
						'.$class.' By 
					</td>
					<td class="item hours-mins" rel="4:0">
						'.$result['first_name'].' '.$result['last_name'].'
					</td>
				</tr>	
				<tr>
					<td colspan="1" valign="top">
							File Name		
					</td>
					<td colspan="3">
					'.$fname.'
					</td>
				</tr>';
				if ($lead_det['belong_to'] == $userdata['userid'] || $lead_det['lead_assign'] == $userdata['userid'] || $userdata['role_id'] == 1 || $userdata['role_id'] == 29 || $userdata['role_id'] == 2) {
					$data['query_files1_html'] .='<tr><td colspan="4" valign="top">
						<input type="button" class="positive" style="float:right;cursor:pointer;" id="replay" onclick="getReplyForm('.$result['query_id'].')" value="Reply" />
					</td></tr>';
				}
				$data['query_files1_html'] .='</tbody></table>
			</td></tr>';
	   }
        return $data['query_files1_html'];
    }
	
    public function get_job_urls($lead_id)
    {
        $sql = "SELECT *
                FROM `".$this->cfg['dbpref']."job_urls`
                WHERE `jobid_fk` = ?
                ORDER BY `urlid`";
        $rs = $this->db->query($sql, array($lead_id));
        
        $html = '';
        
        if ($rs->num_rows() > 0)
        {
            foreach ($rs->result() as $row)
            {
                $html .= '<li>';
                
                    $html .= '<a href="#" onclick="ajaxDeleteJobURL(' . $row->urlid . ', this); return false;" class="file-delete">delete URL</a>';
               
                
                $html .= '<span>' . auto_link($row->url) . '</span><p>' . htmlentities($row->content, ENT_QUOTES) . '</p></li>';
            }
        }
        
        return $html;
        
    }
	
	public function get_lead_det($jid) 
	{
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'leads');
	    $this->db->where('lead_id', $jid);
	    $lead_history = $this->db->get();
	    return $leads =  $lead_history->row_array();
	}
	
	function updt_lead_stg_status($id, $updt) 
	{
		$this->db->where('lead_id', $id);
		return $this->db->update($this->cfg['dbpref'] . 'leads', $updt);
	}
	
	function get_lead_stg_name($id) 
	{
		$query = $this->db->get_where($this->cfg['dbpref'].'lead_stage', array('lead_stage_id' => $id));
		return $query->row_array();
	}
	
	function insert_row($tbl, $ins) 
	{
		if($this->db->insert($this->cfg['dbpref'] . $tbl, $ins))
		{
			return $this->db->insert_id();
		}
		
		return false;
    }
	
	function insert_row_return_id($tbl, $ins) {
		$this->db->insert($this->cfg['dbpref'] . $tbl, $ins);
		return $this->db->insert_id();
    }
	
	function update_row($tbl, $updt, $jid) {
		$this->db->where('lead_id', $jid);
		$this->db->update($this->cfg['dbpref'] . $tbl, $updt);
		// return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		return true;
    }
	
	function update_row_item($tbl, $ins, $jid) {
		$this->db->where('itemid', $jid);
		$this->db->update($this->cfg['dbpref'] . $tbl, $ins);
		// return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		return true;
	}
	
	function update_lead_assign($tbl, $updt, $jid) {
		$this->db->where('lead_id', $jid);
		$this->db->update($this->cfg['dbpref'] . $tbl, $updt);
		// return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		return true;
    }

	function update_rec($tbl, $updt, $condn)
	{
		$this->db->where($condn);
		return $this->db->update($this->cfg['dbpref'] . $tbl, $updt);
	}
	
	function update_service_rec($tbl, $updt, $condn)
	{
		$this->db->update($this->cfg['dbpref'] . $tbl, $updt, $condn);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
	
	function get_expect_worths() {
    	$this->db->select('expect_worth_id, expect_worth_name');
		$this->db->where('status', 1);
    	$q = $this->db->get($this->cfg['dbpref'] . 'expect_worth');
    	return $q->result_array();
    }
		
	public function get_lead_stage() {
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'lead_stage');
	    // $this->db->limit(13);
		$this->db->where("status",1);
		$this->db->where("is_deleted",0);
		//$this->db->order_by("sequence", "asc");
		$this->db->order_by("lead_stage_name", "asc");
	    $lead_stage = $this->db->get();
	    $leads = $lead_stage->result_array();
	    return $leads;
	}	

	public function get_lead_stage_bySpcAuth() {
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'lead_stage');
		$this->db->where("status",1);
		$this->db->where("is_deleted",0);
		$this->db->where("is_special_auth",1);
		//$this->db->order_by("sequence", "asc");
		$this->db->order_by("lead_stage_name", "asc");
	    $lead_stage = $this->db->get();
	    $leads = $lead_stage->result_array();
	    return $leads;
	}	

	public function get_disposition_bySpcAuth() {
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'disposition');
		$this->db->where("status",1);
		$this->db->where("is_deleted",0);
		$this->db->where("is_special_auth",1);
		$this->db->order_by("disposition_name", "asc");
		$lead_stage = $this->db->get();
		// echo $this->db->last_query(); exit;
	    $leads = $lead_stage->result_array();
	    return $leads;
	}

	public function get_order_lead_stage() {
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'lead_stage');
	    // $this->db->limit(13);
		$this->db->where("status",1);
		$this->db->where("list_flag",2);
		$this->db->order_by("sequence", "asc");
	    $lead_stage = $this->db->get();
	    $leads = $lead_stage->result_array();
	    // echo $this->db->last_query(); exit;
	    return $leads;
	}
	function get_agent_list() 
	{
    	$this->db->select('*');
    	$this->db->order_by('id');
		$q = $this->db->get($this->cfg['dbpref'] . 'agents');
		return $q->result_array();
    }

	function get_agent_details($id) 
	{
    	$this->db->select('*');
    	$this->db->where('id',$id);
		$q = $this->db->get($this->cfg['dbpref'] . 'agents');
		
		return $q->result_array();
    }	
	//$rcsl(region, country, state, location)
	function get_lvl_users($tbl, $rcsl, $rcsl_id, $lvl_id) 
	{
		$this->db->select('user_id');
		$this->db->from($this->cfg['dbpref'] . $tbl);
		$this->db->where($rcsl, $rcsl_id);
		$this->db->where_not_in('level_id', $lvl_id);
		$sql = $this->db->get();
		return $res = $sql->result_array();
    }
	
	function get_lvlOne_users() 
	{
		$this->db->select('userid as user_id');
		$this->db->from($this->cfg['dbpref'] . 'users');
		$this->db->where('level', 1);
		$sql = $this->db->get();
		return $res = $sql->result_array();
    }	
	
	function get_item_position($jid) 
	{
		$this->db->select_max('item_position');
		$query = $this->db->get_where($this->cfg['dbpref'].'items', array('jobid_fk' => $jid));
		return $query->result_array();
	}
	
	function delete_lead($tbl, $lead_id) 
	{
		$this->db->where('lead_id', $lead_id);
		$this->db->delete($this->cfg['dbpref'] . $tbl);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	function delete_row($tbl, $condn, $lead_id) 
	{
		$this->db->where($condn, $lead_id);
		$this->db->delete($this->cfg['dbpref'] . $tbl);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	
	function delRecord($tbl, $condn) 
	{
		$this->db->where($condn);
		$this->db->delete($this->cfg['dbpref'] . $tbl);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	function delCoversheet($condn) 
	{
		$this->db->where($condn);
		$this->db->delete($this->cfg['dbpref'] . 'coversheet_log');
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	
	//coversheet delete
	public function get_coversheet($jid) 
	{
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'coversheet_log');
	    $this->db->where('jobid', $jid);
	    $cover_history = $this->db->get();
	    return $cover_history->row_array();
	}
	
	public function get_records_det($rid, $jid) 
	{
	    $this->db->select('am.*,r.*,r.id as auto_id');
	    $this->db->from($this->cfg['dbpref'] . 'records as r');
	    $this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title','LEFT');
	    $this->db->where('r.id', $rid);
	    $this->db->where('jobid', $jid);
	    $lead_history = $this->db->get();
	    return $lead_history->row_array();
	}

	public function get_records_det_rec($rid, $jid, $auto_id) 
	{
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'records as r');
	    // $this->db->join($this->cfg['dbpref'] . 'attachments_master as am', 'am.id = r.record_title','LEFT');
	    $this->db->where('record_title', $rid);
	    $this->db->where('jobid', $jid);
	     $this->db->where('id', $auto_id);
	    $lead_history = $this->db->get();
	    return $lead_history->row_array();
	}
	
// Added download query files
	public function get_query_details($rid, $jid) 
	{	
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'lead_query');
	    $this->db->where('query_id', $rid);
	    $this->db->where('lead_id', $jid);		
	    $lead_history = $this->db->get();
		//echo $this->db->last_query();die;
	    return $lead_history->row_array();
	}
	public function get_file_details($rid, $jid) 
	{	
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'lead_files');
	    $this->db->where('leadfileid', $rid);
	    $this->db->where('lead_id', $jid);		
	    $lead_history = $this->db->get();
		//echo $this->db->last_query();die;
	    return $lead_history->row_array();
	}
	// Ends here
	
	function get_categories() 
	{
    	$this->db->order_by('cat_id');
		$q = $this->db->get($this->cfg['dbpref'] . 'additional_cats');
		return $q->result_array();
    }
    
	function get_cat_records($id) 
	{
		$this->db->where('item_type', $id);
		$q = $this->db->get($this->cfg['dbpref'] . 'additional_items');
		return $q->result_array();
    }
  	   
    function get_lead_sources() 
	{
    	$this->db->where('status', 1);
		$q = $this->db->get($this->cfg['dbpref'] . 'lead_source');
		return $q->result_array();
    }
	
	// Get the Lead Status History
	public function get_lead_stat_history($id)
	{
	    $this->db->select('lsh.dateofchange, us.first_name, us.last_name, ls.lead_stage_name');
	    $this->db->from($this->cfg['dbpref'].'lead_stage_history lsh');
	    $this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = lsh.changed_status', 'LEFT');
	    $this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = lsh.modified_by', 'LEFT');
		$this->db->where("lead_id", $id);
		$this->db->order_by('dateofchange', 'desc');
	    $lead_stg_his = $this->db->get();
	    $lead_sh=  $lead_stg_his->result_array();
	    // echo $this->db->last_query(); exit;
	    return $lead_sh;
	}
	
	//unwanted
	public function get_customers_id()
	{
		$this->db->select('custid');
		$this->db->from($this->cfg['dbpref']. 'customers');
        $customers = $this->db->get();
        $customers =  $customers->result_array();
		return $customers;
	}
	
	public function get_filter_results($sort,$text,$limit,$stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status,$lead_indi, $keyword,$queue_management_group,$queue_management_status,$proposal_expected_from_date,$proposal_expected_to_date,$lead_assign,$event_type,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category,$offset = '',$page = '',$type = '')
	{
		$userdata 			     = $this->session->userdata('logged_in_user');
		$stage 				     = explode(',',$stage);
		$text				     = explode(',',$text);
		$sort				     = explode(',',$sort);
		$limit				     = explode(',',$limit);
		$customer 			     = explode(',',$customer);
		$customerph 	         = explode(',',$customerph);
		$timezone 		         = explode(',',$timezone);
		$leadassignee 	         = explode(',',$leadassignee);
		$regionname 	         = explode(',',$regionname);
		$countryname 	         = explode(',',$countryname);
		$statename 		         = explode(',',$statename);
		$locname 		         = explode(',',$locname);
		$lead_status 	         = explode(',',$lead_status);
		$lead_indi 		       	 = explode(',',$lead_indi);
		$event_type 			 = explode(',',$event_type);
		$carrier_name 			 = explode(',',$carrier_name);
		// echo "<pre>"; print_r($order_handle_by); exit;
		if(is_string($order_handle_by))
			$order_handle_by = explode(',',$order_handle_by);
		else
			$order_handle_by = '';
						
		$user_order_handle = array();
		$user_order_handle = explode(',',$this->userdata['order_handle_by']);
		
		if($company_type!='' && $company_type!='null' && $company_type!='all') {
		   $company_type = explode(',',$company_type);
		}
// j.orderref,
		if ($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29 || $this->userdata['level'] == 1 || $this->userdata['role_id'] == 2) {
			//,j.invoice_no,j.lead_source,j.date_modified,  j.created_by, j.expect_worth_amount, j.expect_worth_id,  j.proposal_expected_date,rg.region_name,u.first_name as ufname, u.last_name as ulname,us.first_name as usfname, us.last_name as usslname, ub.first_name as ubfn, ub.last_name as ubln,ew.expect_worth_name, (SELECT lsh.dateofchange FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id order by lsh.dateofchange desc limit 1) as dateofchange, ,coo.office_account_number,coo.email as office_email,coo.phone as office_phone
			if($offset == '' && $page == '' && $type == ''){
				$this->db->select('count(*)');
			}else{
				$this->db->select('j.lead_id,j.aps_order_no,od.archive_done_at,od.is_retention_done,j.order_sub_no,j.reopen_order_no,j.company_type, TRIM(cm.company_name) AS carrier_name, j.order_no, j.lead_title, j.lead_title_last, j.lead_stage,j.belong_to, j.lead_assign,j.date_created,  j.lead_indicator,TRIM(removeSpacialCharregex(j.lead_indicator)) as j_lead_indicator,  j.orderref, TRIM(removeSpacialCharregex(j.orderref)) as j_orderref, c.first_name, c.last_name, c.company,cs.first_name as c_first_name,cs.last_name as c_last_name,cs.company as c_company, st.timezone, stc.timezone as timezone_c, j.orderingoffice, ls.lead_stage_name,TRIM(removeSpacialCharregex(ls.lead_stage_name)) as ls_lead_stage_name,  st.state_name,stc.state_name as c_state_name, ls.color_code,j.order_handle_by,coo.office_name');
				$this->db->select("CONCAT(j.order_no,' ',j.order_sub_no) AS orderno", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(j.lead_title),' ',removeSpacialCharregex(j.lead_title_last),'',removeSpacialCharregex(c.company))) AS patientname", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(j.lead_title),' ',removeSpacialCharregex(j.lead_title_last))) AS patientname_sorting", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(c.first_name),' ',removeSpacialCharregex(c.last_name))) AS doctorname", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(cs.first_name),' ',removeSpacialCharregex(cs.last_name))) AS cdoctorname", FALSE);
				//121 $this->db->select("CONCAT( IF( TRIM(c.first_name) !='' && TRIM(c.first_name) !='NULL', TRIM(c.first_name),'z'),' ', IF(TRIM(c.last_name) !='' && TRIM(c.last_name) !='NULL' , TRIM(c.last_name), 'z'),'', IF(c.company != NULL, TRIM(c.company), 'z')) AS doctorname0", FALSE);
				//121 $this->db->select("CONCAT(IF( TRIM(cs.first_name) !='' && TRIM(cs.first_name) !='NULL', TRIM(cs.first_name),'z'),' ',IF(cs.last_name !='' && cs.last_name !='NULL' , TRIM(cs.last_name), 'z'),'', IF(cs.company != NULL, TRIM(cs.company), 'z')) AS cdoctorname1", FALSE);
				
				$this->db->select("CONCAT( IF( c.first_name !='' && TRIM(c.first_name) !='NULL', TRIM(removeSpacialCharregex(c.first_name)), (IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && TRIM(c.company) != '', TRIM(removeSpacialCharregex(c.company)), '')) ))) , 
					(IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && TRIM(c.company) != '', TRIM(removeSpacialCharregex(c.company)), '')) )),					
					IF(c.company != 'NULL' && c.company != '', TRIM(removeSpacialCharregex(c.company)), '') ) AS doctorname0", FALSE);
				$this->db->select("CONCAT(
				IF(cs.first_name !='' && cs.first_name !='NULL', TRIM(removeSpacialCharregex(cs.first_name)), ( IF(cs.last_name !='' && cs.last_name !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( cs.company != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')) ) ) ),
					IF(cs.last_name !='' && cs.last_name !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( cs.company != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')) )  ,
					IF( cs.company != 'NULL' && cs.company != '', TRIM(removeSpacialCharregex(cs.company)), '')) AS cdoctorname1", FALSE);
				
				$this->db->select("CONCAT(st.timezone,' ',stc.timezone) AS timezonecol", FALSE);
				$this->db->select("CONCAT(st.state_name,' ',stc.state_name) AS statecol", FALSE);

				$this->db->select("
					
					(IF(`j`.`company_type` = '0',(CONCAT(
					 IF( c.first_name !='' && c.first_name !='NULL', TRIM(removeSpacialCharregex(c.first_name)), (IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && c.company != '', LOWER(TRIM(removeSpacialCharregex(c.company))), '')) ))) , 
					(IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && TRIM(c.company) != '', LOWER(TRIM(removeSpacialCharregex(c.company))), '')) )),					
					IF(c.company != 'NULL' && TRIM(c.company) != '', LOWER(TRIM(removeSpacialCharregex(c.company))), '')
						) ), (IF(`j`.`company_type` = '1', CONCAT(
					IF(cs.first_name !='' && cs.first_name !='NULL', TRIM(removeSpacialCharregex(cs.first_name)), ( IF(cs.last_name !='' && cs.last_name !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( cs.company !='NULL', TRIM(removeSpacialCharregex(cs.company)), '')) ) ) ),
					IF(TRIM(cs.last_name) !='' && TRIM(cs.last_name) !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( TRIM(cs.company) != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')) )  ,
					IF( cs.company != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')
					), '' ))
					
					
					)
					
					) AS insurance_facility1", FALSE);
				
				
				$this->db->select("
				(CASE 
				WHEN j.company_type ='0' THEN TRIM(removeSpacialCharregex(st.state_name))
				ELSE TRIM(removeSpacialCharregex(stc.state_name))
				END)
				AS orderStates", FALSE);

				$this->db->select("
				(CASE 
				WHEN (coo.office_name !='' ||  coo.office_name != 'NULL') THEN TRIM(removeSpacialCharregex(coo.office_name)) 
				ELSE TRIM(removeSpacialCharregex(j.orderingoffice)) END) AS orderOrderingOffice", FALSE);
			
			}

			$this->db->from($this->cfg['dbpref']. 'leads as j');		
			$this->db->where('j.pjt_status', 0);
			$this->db->join($this->cfg['dbpref'] . 'order_retention_details as od', 'od.order_id = j.lead_id', 'LEFT');		
			$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');		
			$this->db->join($this->cfg['dbpref'] . 'custodian as cs', 'cs.custid = j.custid_fk', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = j.lead_assign', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'users as ub', 'ub.userid = j.belong_to', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
			
			if($filter_changes == 'D'){
				$this->db->where_not_in('j.lead_stage', array(36,37)); 
			}
			else if($stage[0] != 'null' && $stage[0] != 'all' && $stage[0] != '') {		
				$this->db->where_in('j.lead_stage', $stage); 
			}
			else{
				$this->db->where('j.lead_id != "null" AND j.lead_stage IN ("'.$this->stages.'")');
			}
			if($queue_management_group != $this->config->item('crm')['check_payment_requested_stage'] && $queue_management_group != $this->config->item('crm')['check_payment_mailroom_stage']){
			if($event_type[0] != 'null' && $event_type[0] != 'all' && $event_type[0] != ''){
				$this->db->where_in('j.order_disposition_id', $event_type); 
			}
			}
			if($customer[0] != 'null' && $customer[0] != 'all' && $customer[0] != ''){		
				$this->db->where_in('j.custid_fk', $customer); 
			}
			// Commented on 15/12/2015
			/*if($customerph[0] != 'null' && $customerph[0] != 'all'){
				$this->db->where_in('c.phone_1', $customerph); 
			}*/
			if($timezone[0] != 'null' && $timezone[0] != 'all' && $timezone[0] != ''){		
				$this->db->where('((st.timezone IN ("'.$timezone[0].'") AND j.company_type=0) OR (stc.timezone IN ("'.$timezone[0].'") AND j.company_type=1)) ');
			}
			
			//if($leadassignee[0] != '' && $leadassignee[0] != 'null' && $leadassignee[0] != 'all' && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'))
			if($leadassignee[0] != '' && $leadassignee[0] != 'null' && $leadassignee[0] != 'all') //PEX-3278-Removed the Keyword condtions
			{
				$this->db->where_in('j.lead_assign',$leadassignee);
				//Missmatch the data for HTML Grid vs Export excel so that comment below line.
				//$this->db->where('j.order_handle_by',$this->config->item('crm')['offshore_flag']); 
			}
			if($carrier_name[0] != '' && $carrier_name[0] != 'null' && $carrier_name[0] != 'all' && $carrier_name[0] != '' && $carrier_name[0] != 'Please Select'){
				$this->db->where_in('cm.company_code', $carrier_name);
			}
			if($order_handle_by!='' && $order_handle_by!='null')
			{
				$this->db->where_in('j.order_handle_by',$order_handle_by);
			}
			/*
			// Commented on 15/12/2015
			if($regionname[0] != 'null' && $regionname[0] != 'all'){
				$this->db->where_in('c.add1_region', $regionname);
			}
			if($countryname[0] != 'null' && $countryname[0] != 'all'){
				$this->db->where_in('c.add1_country', $countryname);
			}*/
			if($statename[0] != 'null' && $statename[0] != ''){	
				$this->db->where('((c.add1_state IN ('.$statename[0].') AND j.company_type=0) OR (cs.add1_state IN ('.$statename[0].') AND j.company_type=1)) ');
				// $this->db->where_in('cs.add1_state', $statename);
			}
			/*if($locname[0] != 'null' && $locname[0] != 'all'){	
				$this->db->where_in('c.add1_location', $locname);
			}
			if($lead_status[0] != 'null' && $lead_status[0] !='') {	
				$this->db->where_in('j.lead_status', $lead_status);
			}*/
			if($lead_indi[0] != 'null' && $lead_indi[0] !='') {	
				$this->db->where_in('j.lead_indicator', $lead_indi);
			}
			
			if($company_type!='' && $company_type!='null' && $company_type!='all')
			{
				$this->db->where_in('j.company_type',$company_type);
			}
			// var_dump($keyword);exit;
			
			if(($keyword != 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' && $keyword != 'null') && $search_category != ''){
		      
				// $invwhere = "( (j.invoice_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR j.orderref LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%'))";
				if($search_category == 'Expert_Order_No'){
					$invwhere = "( j.order_no LIKE '%$keyword%' )";
				}else if($search_category == 'APS_Order_No'){
					$invwhere = "( j.aps_order_no LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%')";
				}else if($search_category == 'Patient_First_Name'){
					$invwhere = "( j.lead_title LIKE '%$keyword%' )";
				}else if($search_category == 'Patient_Last_Name'){
					$invwhere = "( j.lead_title_last LIKE '%$keyword%' )";
				}else if($search_category=='Facility'){
	               $invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%'  OR CONCAT(c.first_name,' ',c.last_name,' ',c.company) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Facility_Name'){
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian_Name'){
					$invwhere = "((cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name) LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name,' ',cs.company) LIKE '%$keyword%') AND j.company_type = 1)";
				}
				// else if($search_category == 'Facility'){
				// 	$invwhere = "( (c.company LIKE '%$keyword%') AND j.company_type = 0)";
				// }
				else if($search_category == 'Custodian'){
					$invwhere = "( (cs.company LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Policy_no'){
					$invwhere = "( j.orderref LIKE '%$keyword%' )";
				}else if($search_category == 'Doctor_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						// foreach($arr as $k => $val){
						// 	if($k == 0){
						// 		$temp_format_num .= '('.$val;
						// 	}else if($k == 2){
						// 		$temp_format_num .= $val.') ';
						// 	}else if($k == 6){
						// 		$temp_format_num .= '-'.$val;
						// 	}else{
						// 		$temp_format_num .= $val;
						// 	}
						// }
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' 
						OR c.phone_3 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}else{
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}
				}else if($search_category == 'Custodian_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						// foreach($arr as $k => $val){
						// 	if($k == 0){
						// 		$temp_format_num .= '('.$val;
						// 	}else if($k == 2){
						// 		$temp_format_num .= $val.') ';
						// 	}else if($k == 6){
						// 		$temp_format_num .= '-'.$val;
						// 	}else{
						// 		$temp_format_num .= $val;
						// 	}
						// }
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}else{
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}
				}else{
					$invwhere = "( (j.order_no LIKE '%$keyword%' OR j.order_sub_no LIKE '%$keyword%'  OR j.reopen_order_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR  j.lead_title_last LIKE '%$keyword%' OR j.orderref LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%'  OR cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%' OR j.aps_order_no LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%'))";
				}
				$this->db->where($invwhere);
			}
		}
		else {
			$curusid = $this->session->userdata['logged_in_user']['userid'];
			
			//,j.invoice_no,j.lead_source,j.date_modified,  j.created_by, j.expect_worth_amount, j.expect_worth_id, j.lead_status, j.proposal_expected_date,rg.region_name,u.first_name as ufname, u.last_name as ulname,us.first_name as usfname, us.last_name as usslname, ub.first_name as ubfn, ub.last_name as ubln,ew.expect_worth_name, (SELECT lsh.dateofchange FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id order by lsh.dateofchange desc limit 1) as dateofchange, ,coo.office_account_number,coo.email as office_email,coo.phone as office_phone
			if($offset == '' && $page == '' && $type == ''){
				$this->db->select('count(*)');
			}else{
				$this->db->select('j.lead_id,od.is_retention_done,j.aps_order_no,j.order_sub_no,j.reopen_order_no,j.company_type, TRIM(cm.company_name) AS carrier_name, j.order_no, j.lead_title, j.lead_title_last, j.lead_stage,j.belong_to, j.lead_assign,j.date_created,  j.lead_indicator,TRIM(removeSpacialCharregex(j.lead_indicator)) as j_lead_indicator,  j.orderref, TRIM(removeSpacialCharregex(j.orderref)) as j_orderref, c.first_name, c.last_name, c.company,cs.first_name as c_first_name,cs.last_name as c_last_name,cs.company as c_company, st.timezone, stc.timezone as timezone_c, j.orderingoffice, ls.lead_stage_name,TRIM(removeSpacialCharregex(ls.lead_stage_name)) as ls_lead_stage_name,  st.state_name,stc.state_name as c_state_name, ls.color_code,j.order_handle_by,coo.office_name');
				$this->db->select("CONCAT(j.order_no,' ',j.order_sub_no) AS orderno", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(j.lead_title),' ',removeSpacialCharregex(j.lead_title_last),'',removeSpacialCharregex(c.company))) AS patientname", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(j.lead_title),' ',removeSpacialCharregex(j.lead_title_last))) AS patientname_sorting", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(c.first_name),' ',removeSpacialCharregex(c.last_name))) AS doctorname", FALSE);
				$this->db->select("TRIM(CONCAT(removeSpacialCharregex(cs.first_name),' ',removeSpacialCharregex(cs.last_name))) AS cdoctorname", FALSE);
				
				/*121 $this->db->select("CONCAT( IF( TRIM(c.first_name) !='' && TRIM(c.first_name) !='NULL', TRIM(c.first_name),'z'),' ', IF(c.last_name !='' && c.last_name !='NULL' , TRIM(c.last_name), 'z'),'', IF(c.company != NULL, TRIM(c.company), 'z')) AS doctorname0", FALSE);
				$this->db->select("CONCAT(IF( TRIM(cs.first_name) !='' && TRIM(cs.first_name) !='NULL', TRIM(cs.first_name),'z'),' ',IF(cs.last_name !='' && cs.last_name !='NULL' , TRIM(cs.last_name), 'z'),'', IF(cs.company != NULL, TRIM(cs.company), 'z')) AS cdoctorname1", FALSE);
				$this->db->select("CONCAT(st.timezone,' ',stc.timezone) AS timezonecol", FALSE);
				$this->db->select("CONCAT(st.state_name,' ',stc.state_name) AS statecol", FALSE); */
					
				$this->db->select("CONCAT( IF( c.first_name !='' && TRIM(c.first_name) !='NULL', TRIM(removeSpacialCharregex(c.first_name)), (IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && TRIM(c.company) != '', TRIM(removeSpacialCharregex(c.company)), '')) ))) , 
					(IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && TRIM(c.company) != '', TRIM(removeSpacialCharregex(c.company)), '')) )),					
					IF(c.company != 'NULL' && c.company != '', TRIM(removeSpacialCharregex(c.company)), '') ) AS doctorname0", FALSE);
				$this->db->select("CONCAT(
				IF(cs.first_name !='' && cs.first_name !='NULL', TRIM(removeSpacialCharregex(cs.first_name)), ( IF(cs.last_name !='' && cs.last_name !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( cs.company != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')) ) ) ),
					IF(cs.last_name !='' && cs.last_name !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( cs.company != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')) )  ,
					IF( cs.company != 'NULL' && cs.company != '', TRIM(removeSpacialCharregex(cs.company)), '')) AS cdoctorname1", FALSE);
					
				
				$this->db->select("CONCAT(st.timezone,' ',stc.timezone) AS timezonecol", FALSE);
				$this->db->select("CONCAT(st.state_name,' ',stc.state_name) AS statecol", FALSE);
				
				$this->db->select("
				(CASE 
				WHEN j.company_type ='0' THEN TRIM(removeSpacialCharregex(st.state_name)) 
				ELSE TRIM(removeSpacialCharregex(stc.state_name)) 
				END)
				AS orderStates", FALSE);

				$this->db->select("
				(CASE 
				WHEN (coo.office_name !='' ||  coo.office_name != 'NULL') THEN TRIM(removeSpacialCharregex(coo.office_name)) 
				ELSE TRIM(removeSpacialCharregex(j.orderingoffice)) END) AS orderOrderingOffice", FALSE);
				
				$this->db->select("
					
					(IF(`j`.`company_type` = '0',(CONCAT(
					 IF( c.first_name !='' && c.first_name !='NULL', TRIM(removeSpacialCharregex(c.first_name)), (IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && c.company != '', LOWER(TRIM(removeSpacialCharregex(c.company))), '')) ))) , 
					(IF(c.last_name !='' && c.last_name !='NULL', TRIM(removeSpacialCharregex(c.last_name)), (IF(c.company != 'NULL' && TRIM(c.company) != '', LOWER(TRIM(removeSpacialCharregex(c.company))), '')) )),					
					IF(c.company != 'NULL' && TRIM(c.company) != '', LOWER(TRIM(removeSpacialCharregex(c.company))), '')
						) ), (IF(`j`.`company_type` = '1', CONCAT(
					IF(cs.first_name !='' && cs.first_name !='NULL', TRIM(removeSpacialCharregex(cs.first_name)), ( IF(cs.last_name !='' && cs.last_name !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( cs.company !='NULL', TRIM(removeSpacialCharregex(cs.company)), '')) ) ) ),
					IF(TRIM(cs.last_name) !='' && TRIM(cs.last_name) !='NULL', TRIM(removeSpacialCharregex(cs.last_name)), (IF( TRIM(cs.company) != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')) )  ,
					IF( cs.company != 'NULL', TRIM(removeSpacialCharregex(cs.company)), '')
					), '' ))
					
					
					)
					
					) AS insurance_facility1", FALSE);
			}
			$this->db->from($this->cfg['dbpref'] . 'leads as j');
			$this->db->join($this->cfg['dbpref'] . 'order_retention_details as od', 'od.order_id = j.lead_id', 'LEFT');		
			$this->db->join($this->cfg['dbpref'].'customers as c', 'c.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] .'custodian as cs', 'cs.custid = j.custid_fk', 'LEFT');		
			// $this->db->join($this->cfg['dbpref'].'users as u', 'u.userid = j.lead_assign', 'LEFT');
			// $this->db->join($this->cfg['dbpref'].'users as us', 'us.userid = j.modified_by', 'LEFT');
			// $this->db->join($this->cfg['dbpref'].'users as ub', 'ub.userid = j.belong_to', 'LEFT');
			// $this->db->join($this->cfg['dbpref'].'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
			// $this->db->join($this->cfg['dbpref'].'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
			$this->db->where('j.pjt_status', 0); 
			// $this->db->where('j.lead_id != "null" AND j.lead_stage IN (1,2,3,4,5,6,7,8,9,10,11,12)');
		
			if($filter_changes == 'D')
			{
				$this->db->where_not_in('j.lead_stage', array(36,37)); 
			}
			else if($stage[0] != 'null' && $stage[0] != 'all' && $stage[0] != '') 
			{
				$this->db->where_in('j.lead_stage',$stage); 
			}
			else
			{
				$this->db->where('j.lead_id != "null" AND j.lead_stage IN ("'.$this->stages.'")');
			}

			if($queue_management_group != $this->config->item('crm')['check_payment_requested_stage'] && $queue_management_group != $this->config->item('crm')['check_payment_mailroom_stage'])
			{
				if($event_type[0] != 'null' && $event_type[0] != 'all')
				{
					$this->db->where_in('j.order_disposition_id', $event_type); 
				}
			}
			if($carrier_name[0] != 'null' && $carrier_name[0] != 'all' && $carrier_name[0] != '' && $carrier_name[0] != 'Please Select')
			{
				$this->db->where_in('cm.company_code', $carrier_name);
			}
			if($customer[0] != '' && $customer[0] != 'null' && $customer[0] != 'all') 
			{		
				$this->db->where_in('j.custid_fk',$customer);				
			}
			if($customerph[0] != '' && $customerph[0] != 'null' && $customerph[0] != 'all')
			{		
				$this->db->where_in('c.phone_1',$customerph);
			}
			if($timezone[0] != 'null' && $timezone[0] != 'all' && $timezone[0] != '')
			{		
				$this->db->where('((st.timezone IN ("'.$timezone[0].'") AND j.company_type=0) OR (stc.timezone IN ("'.$timezone[0].'") AND j.company_type=1)) ');
			}
			
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) )
			{
				if($leadassignee[0] != '' && $leadassignee[0] != 'null' && $leadassignee[0] != 'all' && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'))
				{
					$this->db->where_in('j.lead_assign',$leadassignee);	
				} 
			} 
			else
			{
				if($leadassignee[0] != '' && $leadassignee[0] != 'null' && $leadassignee[0] != 'all')
				{
					$this->db->where_in('j.lead_assign',$leadassignee);
				}
			}
			
			if(($keyword != 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' && $keyword != 'null') && $search_category != '')
			{		
				
				if($search_category == 'Expert_Order_No')
				{
					$invwhere = "( j.order_no LIKE '%$keyword%' )";
				}
				else if($search_category == 'APS_Order_No')
				{
					$invwhere = "( j.aps_order_no LIKE '%$keyword%'  OR j.reopen_order_no LIKE '%$keyword%')";
				}
				else if($search_category == 'Patient_First_Name')
				{
					$invwhere = "( j.lead_title LIKE '%$keyword%' )";
				}
				else if($search_category == 'Patient_Last_Name')
				{
					$invwhere = "( j.lead_title_last LIKE '%$keyword%' )";
				}
				else if($search_category == 'Facility_Name')
				{
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian_Name'){
					$invwhere = "((cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name) LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name,'',cs.company) LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Facility'){
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%'  OR CONCAT(c.first_name,' ',c.last_name,' ',c.company) LIKE '%$keyword%') AND j.company_type = 0)";

					// $invwhere = "( (c.company LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian'){
					$invwhere = "( (cs.company LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Policy_no'){
					$invwhere = "( j.orderref LIKE '%$keyword%' )";
				}else if($search_category == 'Doctor_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						// foreach($arr as $k => $val){
						// 	if($k == 0){
						// 		$temp_format_num .= '('.$val;
						// 	}else if($k == 2){
						// 		$temp_format_num .= $val.') ';
						// 	}else if($k == 6){
						// 		$temp_format_num .= '-'.$val;
						// 	}else{
						// 		$temp_format_num .= $val;
						// 	}
						// }
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' 
						OR c.phone_3 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}else{
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}
				}else if($search_category == 'Custodian_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						// foreach($arr as $k => $val){
						// 	if($k == 0){
						// 		$temp_format_num .= '('.$val;
						// 	}else if($k == 2){
						// 		$temp_format_num .= $val.') ';
						// 	}else if($k == 6){
						// 		$temp_format_num .= '-'.$val;
						// 	}else{
						// 		$temp_format_num .= $val;
						// 	}
						// }
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}else{
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}
				}else{
					$invwhere = "( (j.order_no LIKE '%$keyword%' OR j.order_sub_no LIKE '%$keyword%'  OR j.reopen_order_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR  j.lead_title_last LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%'))";
				}
				$this->db->where($invwhere);
			}
			if (isset($this->session->userdata['region_id']))
			$region = explode(',',$this->session->userdata['region_id']);
			if (isset($this->session->userdata['countryid']))
			$countryid = explode(',',$this->session->userdata['countryid']);
			if (isset($this->session->userdata['stateid']))
			$stateid = explode(',',$this->session->userdata['stateid']);
			if (isset($this->session->userdata['locationid']))
			$locationid = explode(',',$this->session->userdata['locationid']);

			if ( ($stage[0] == 'null' || $stage[0] == 'all') && ($customer[0] == 'null' || $customer[0] == 'all') && ($leadassignee[0] == 'null' || $leadassignee[0] == 'all') && ($regionname[0] == 'null' || $regionname[0] == 'all') && ($countryname[0] == 'null' || $countryname[0] == 'all') && ($statename[0] == 'null' || $statename[0] == 'all') && ($locname[0] == 'null' || $locname[0] == 'all') && $keyword == 'null' ) {
				
				if (isset($this->session->userdata['region_id']))
				$region = explode(',',$this->session->userdata['region_id']);
				if (isset($this->session->userdata['countryid']))
				$countryid = explode(',',$this->session->userdata['countryid']);
				if (isset($this->session->userdata['stateid']))
				$stateid = explode(',',$this->session->userdata['stateid']);
				if (isset($this->session->userdata['locationid']))
				$locationid = explode(',',$this->session->userdata['locationid']);

				$this->db->where_in('c.add1_region',$region);
				
				if (isset($this->session->userdata['countryid'])) {
					$this->db->where_in('c.add1_country',$countryid); 
				}
				if (isset($this->session->userdata['stateid'])) {
					$this->db->where_in('c.add1_state',$stateid);
				}
				if (isset($this->session->userdata['locationid'])) {
					$this->db->where_in('c.add1_location',$locationid); 
				}
				
				//or_where condition is used for to bring the lead owner leads when he creating the leads for different region.
				// $this->db->or_where('(j.belong_to = '.$curusid.' AND j.lead_stage IN (1,2,3,4,5,6,7,8,9,10,11,12))');
				// $this->db->or_where('(j.belong_to = '.$curusid.' AND j.lead_stage IN ("'.$this->stages.'") AND j.pjt_status = 0)');
			}
			
			//Advanced filter
				if($regionname[0] != 'null' && $regionname[0] != 'all'){		
					$this->db->where_in('c.add1_region',$regionname);
				} else {
					$this->db->where_in('c.add1_region',$region);
				}
				if($countryname[0] != 'null' && $countryname[0] != 'all') {
					$this->db->where_in('c.add1_country', $countryname);
				} else if ((($this->userdata['level'])==3) || (($this->userdata['level'])==4) || (($this->userdata['level'])==5)) {
					$this->db->where_in('c.add1_country',$countryid);
				}
				if($statename[0] != 'null' && $statename[0] != 'all') {	
					$this->db->where_in('c.add1_state', $statename);
				} else if ((($this->userdata['level'])==4) || (($this->userdata['level'])==5)) {
					$this->db->where_in('c.add1_state',$stateid);
				}
				if($locname[0] != 'null' && $locname[0] != 'all') {	
					$this->db->where_in('c.add1_location', $locname);
				} else if (($this->userdata['level'])==5) {
					$this->db->where_in('c.add1_location',$locationid);
				}
				if($lead_status[0] != 'null' && $lead_status[0] != ''){	
					$this->db->where_in('j.lead_status', $lead_status);
				}
				if($lead_indi[0] != 'null' && $lead_indi[0] !='') {	
					$this->db->where_in('j.lead_indicator', $lead_indi);
				}
				if($company_type!='' && $company_type!='null' && $company_type!='all')
				{
					$this->db->where_in('j.company_type',$company_type);
				}
				if($order_handle_by!='' && $order_handle_by!='null')
				{ 
					$this->db->where('j.order_handle_by',$order_handle_by);
				}
			//Advanced filter
			
		}
		
		if($from_date != 'null' && $to_date !='') {	
			$this->db->where("(Date(j.proposal_expected_date) >='".date('Y-m-d', strtotime($from_date))."' )", NULL, FALSE);
			$this->db->where("(Date(j.proposal_expected_date) <='".date('Y-m-d', strtotime($to_date))."' )", NULL, FALSE);
		} else if(!empty($from_date) && ($from_date!='null')) {
			$this->db->where("(Date(j.proposal_expected_date) >='".date('Y-m-d', strtotime($from_date))."' )", NULL, FALSE);
		}
		
		if(is_array($queue_management_group)){
			if(!empty($queue_management_group)){
				$this->db->where_in("ls.queue_management_group",$queue_management_group, NULL, FALSE);
			}
		}else{
			if($filter_changes == 'Y' && ($queue_management_group == $this->config->item('crm')['check_payment_requested_stage'] || $queue_management_group == $this->config->item('crm')['check_payment_mailroom_stage'])){
				$this->db->where("(ls.queue_management_group = ".$queue_management_group." OR j.order_disposition_id =".$event_type[0].")");
				// ." OR j.order_disposition_id =".$event_type[0]
			}else if($queue_management_group != 'null' && $queue_management_group !='' && $event_type == ''){
			$this->db->where("ls.queue_management_group",$queue_management_group, NULL, FALSE);
			}else if($queue_management_group != '' && $queue_management_group != 'null' && $event_type != ''){
				$this->db->where("(ls.queue_management_group = ".$queue_management_group." OR j.order_disposition_id =".$event_type[0].")");
				// ." OR j.order_disposition_id =".$event_type[0]
			}
		}
		
		if($queue_management_status != 'null' && $queue_management_status !=''){
			$this->db->where("ls.queue_management_status",$queue_management_status, NULL, FALSE);
		}
		if($proposal_expected_from_date != 'null' && $proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date == '')
		{
			$proposal_expected_to_date = date('Y-m-d');
		}
		if($proposal_expected_to_date != null &&  $proposal_expected_to_date !='' && $proposal_expected_to_date !='null' && $proposal_expected_from_date == '')
		{
			$proposal_expected_from_date = date('Y-m-d',strtotime('-60 days'));
		}
		if($filter_changes != 'N'){
		if($proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date != null && $proposal_expected_to_date !=''){
			$this->db->where('DATE(j.proposal_expected_date) between "'.$proposal_expected_from_date.'" and "'.$proposal_expected_to_date.'"');
		}
		}
		
		//  First Order handle by condition based on admin login, If not set advance filter order handle by apply 
		
		
		if($this->userdata['role_id'] != 1 && count($user_order_handle)>0 && $order_handle_by <= 0)
		{
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle))
			{
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'){
					$this->db->where('j.order_handle_by',$this->config->item('crm')['onshore_flag']);
				} 
			}
			else
			{ 
				$this->db->where_in('j.order_handle_by',$user_order_handle);
			}
		}
		if($lead_assign && $this->userdata['role_id'] != 29 && $this->userdata['role_id'] != 1){
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle)) {
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null') {
					$this->db->where('j.lead_assign',$lead_assign);	
				}
			}
			else if($keyword == '' || $keyword == 'null' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number'){
				$this->db->where('j.lead_assign',$lead_assign);	
			}
		}		
	
		if($this->userdata['role_id'] != 1 && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null')){
			if($this->userdata['company_list'] != '' && $this->userdata['company_list'] != 'all' && $this->userdata['company_list'] != 'null' && $this->userdata['company_list'] != '')
			{
			$user_company_code = explode(',',$this->userdata['company_list']);
			}
			if($this->userdata['company_type'] != '' && $this->userdata['company_type'] != 'all' && $this->userdata['company_type'] != 'null' && $this->userdata['company_type'] != '')
			{
				$user_company_type = explode(',',$this->userdata['company_type']);
			}
			
			if($user_company_code != 'all' && $user_company_code != '' && $user_company_code != 'null' && sizeof($user_company_code)>0)
			{
				$this->db->where_in('j.company_code',$user_company_code);
			}
			if($user_company_type != '' && $user_company_type != 'null' && $user_company_type != 'all' && sizeof($user_company_type)>0)
			{
				$this->db->where_in('j.company_type',$user_company_type);
			}
		}
		$this->db->where('ls.is_deleted',0);
		
		if(isset($sort['0']) && $sort['0'] != '' && isset($text['0']))
		{
			$sort_order = ($sort['0'] == 'up') ? 'ASC' : 'DESC';
	   		if($text['0']=="j.order_no"||$text['0']=="j.aps_order_no"||$text['0']=="ls.lead_stage_name"||$text['0']=="j.orderref"||$text['0']=="j.lead_indicator"||$text['0']=="carrier_name")
			{
				if($offset == '' && $page == '' && $type == ''){}else{
				$text_one = $text['0']=="ls.lead_stage_name" ? 'ls_lead_stage_name':( ($text['0']=="j.lead_indicator") ?"j_lead_indicator" : ( ($text['0']=="j_orderref") ? "j_orderref":$text['0'] ) );
				$this->db->order_by($text_one, $sort_order);
				}
			}	
			else if($text['0']=="patientname")
			{
				if($offset == '' && $page == '' && $type == ''){
				}else{
				$this->db->order_by("patientname_sorting", $sort_order); 
				}
			}
			else if($text['0']=="timezone" && $company_type['0'] == "0")
			{
				if($offset == '' && $page == '' && $type == ''){
					
				}else{
					$this->db->order_by("st.timezone", $sort_order); 
				}
			}  
			else if($text['0']=="coo.office_name")
			{
				if($offset == '' && $page == '' && $type == ''){}else{
				// $this->db->order_by("coo.office_name $sort_order,j.orderingoffice $sort_order" );
				$this->db->order_by("orderOrderingOffice", $sort_order);
				}
			}
	   		else if($text['0']=="timezone"&& $company_type['0'] == "1")
			{
				if($offset != '' && $page != '' && $type != ''){$this->db->order_by("timezone_c", $sort_order);}
			}
	    	else if($text['0']=="timezone"&& ($company_type['0'] !="0"||$company_type['0'] !="1" ))
			{
				if($offset != '' && $page != '' && $type != ''){
					$this->db->order_by("timezone $sort_order,timezone_c $sort_order" );
				}
			}
	   		else if($text['0']=="state_name"&&$company_type['0']=="0")
			{
				if($offset != '' && $page != '' && $type != ''){
				// $this->db->order_by("st.state_name", $sort_order);
				$this->db->order_by("orderStates", $sort_order );
				}
			}	
	   		else if($text['0']=="state_name"&&$company_type['0']=="1")
			{
				if($offset != '' && $page != '' && $type != ''){
				// $this->db->order_by("stc.state_name", $sort_order);
				$this->db->order_by("orderStates", $sort_order );
				}
			}	
	   		else if($text['0']=="state_name"&& ($company_type['0'] !="0" ||$company_type['0'] !="1" ))
			{
				if($offset != '' && $page != '' && $type != ''){
				// $this->db->order_by("st.state_name $sort_order,stc.state_name $sort_order" );
				$this->db->order_by("orderStates", $sort_order );
				}

			}
	   		else if($text['0']=="doctorname"&& $company_type['0'] == "0")
			{
	   			if($offset != '' && $page != '' && $type != ''){
					$this->db->order_by("doctorname0 $sort_order");
				}
			}
	   		else if($text['0']=="doctorname"&&$company_type['0']== "1")
	   		{
				if($offset != '' && $page != '' && $type != ''){
					$this->db->order_by("cdoctorname1 $sort_order");
				}
			}
	   		else if($text['0']=="doctorname" && ($company_type['0'] !="0" ||$company_type['0'] !="1" ))
			{
				if($offset == '' && $page == '' && $type == ''){
				}else{
	   			// $this->db->order_by("doctorname $sort_order,cdoctorname $sort_order ,c.company $sort_order,c_company $sort_order");
				//121 $this->db->order_by(" insurance_facility1 $sort_order,insurance_facility $sort_order,legal_facility $sort_order");
				$this->db->order_by(" insurance_facility1 $sort_order");
				}
	  		}
		}else{
			$this->db->order_by("j.order_no", 'desc');
		}

		$this->db->group_by("j.lead_id");
		if(isset($limit['0']) && $limit['0']!='')
		{
			$this->db->limit($limit['0']);
		}	

		if($type=='pagination')
		{
			$query = $this->db->limit($offset,$page)->get();
		}
		else
		{
			$query = $this->db->get();
		}
		//echo '<br>';
		// echo 'Page No '.$offset.' '.$page.' '.$sort_order;
		// echo $this->db->last_query();
		// exit;
		$res =  $query->result_array();
		// echo "<pre>";
		// print_r(count($res));
		// exit;       
		return $res;
	}
	
	public function get_export_filter_results($stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status,$lead_indi, $keyword, $queue_management_group, $queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$event_type,$lead_assign,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category)
	{
		$userdata 		= $this->session->userdata('logged_in_user');
		$stage 			= explode(',',$stage);
		$customer 		= explode(',',$customer);
		$customerph 	= explode(',',$customerph);
		$timezone 		= explode(',',$timezone);
		$leadassignee 	= explode(',',$leadassignee);
		$regionname 	= explode(',',$regionname);
		$countryname 	= explode(',',$countryname);
		$statename 		= explode(',',$statename);
		$locname 		= explode(',',$locname);
		$lead_status 	= explode(',',$lead_status);
		$lead_indi 		= explode(',',$lead_indi);
		$event_type		= explode(',',$event_type);
		$carrier_name	= explode(',',$carrier_name);
		$order_handle_by_arr = array();
		if($order_handle_by!="") {
		$order_handle_by_arr = explode(',',$order_handle_by);
		}
		$user_order_handle = array();
		$user_order_handle = explode(',',$this->userdata['order_handle_by']);
		
		$user_order_handle = array_diff($user_order_handle,array(NULL));
		if($company_type!='' && $company_type!='null' && $company_type!='all') {
		$company_type = explode(',',$company_type);
		}
		// print_r($order_handle_by);die;
		//var_dump($user_order_handle);exit;
// j.orderref,
		if ($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29 || $this->userdata['level'] == 1 || $this->userdata['role_id'] == 2) {
			$this->db->select("j.lead_id as leadid,j.aps_order_no,j.reopen_order_no, cm.company_name AS carrier_name,j.invoice_no, j.order_no, j.lead_title, j.lead_title_last, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to, j.created_by, j.expect_worth_amount, j.expect_worth_id, j.lead_indicator, j.lead_status, j.lead_assign, j.proposal_expected_date, j.orderref,j.company_type, c.first_name, c.last_name, c.company,cs.first_name as c_first_name, cs.last_name as c_last_name, cs.company as c_company, st.timezone, rg.region_name, stc.timezone as c_timezone, rgc.region_name as c_region_name, u.first_name as ufname, u.last_name as ulname,us.first_name as usfname, us.last_name as usslname, ub.first_name as ubfn, ub.last_name as ubln, ls.lead_stage_name, ew.expect_worth_name, st.state_name,stc.state_name as c_state_name, rec.*, pay.*, j.confirmed_close_date, j.order_sub_no, c.add1_line1, c.add1_line2,c.add1_postcode,c.add1_location, cs.add1_line1 as c_add1_line1, cs.add1_line2 as c_add1_line2,cs.add1_postcode as c_add1_postcode,cs.add1_location as c_add1_location,(SELECT lsh.dateofchange FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id order by lsh.dateofchange desc limit 1) as dateofchange,j.orderingoffice,j.is_order_assigned_permanent,dis.disposition_name,j.ordering_office_id,coo.office_name,coo.office_account_number,coo.email as office_email,coo.phone as office_phone, (select first_name from crm_users where userid = (SELECT userid_fk FROM `crm_logs` WHERE `jobid_fk` = j.lead_id ORDER BY `logid` DESC LIMIT 1)) as assigned_fname, (select last_name from crm_users where userid = (SELECT userid_fk FROM `crm_logs` WHERE `jobid_fk` = j.lead_id ORDER BY `logid` DESC LIMIT 1)) as assigned_lname");
			$this->db->select("CONCAT(j.order_no,' ',COALESCE(j.order_sub_no,'')) AS orderno", FALSE);

			$this->db->from($this->cfg['dbpref']. 'leads as j');
			$this->db->where('j.lead_id != "null" AND j.lead_stage IN ("'.$this->stages.'")');
			$this->db->where('j.pjt_status', 0);
			$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'custodian as cs','cs.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = j.lead_assign', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'users as ub', 'ub.userid = j.belong_to', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'payment_records as pay', 'j.lead_id = pay.lead_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'pay.rec_type_id = rec.rec_type_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'disposition as dis', 'dis.id = j.order_disposition_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'j.lead_id = rec.rec_type_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'real_calc_stop_tat as rcs', 'rcs.lead_id = j.lead_id', 'LEFT');
			
			if($stage[0] != 'null' && $stage[0] != 'all' && $stage[0] != '') {		
				$this->db->where_in('j.lead_stage', $stage); 
			}
			if($queue_management_group != $this->config->item('crm')['check_payment_requested_stage'] && $queue_management_group != $this->config->item('crm')['check_payment_mailroom_stage']){
			if($event_type[0] != 'null' && $event_type[0] != 'all' && $event_type[0] != ''){
				$this->db->where_in('j.order_disposition_id', $event_type); 
			}
			}
			if($customer[0] != 'null' && $customer[0] != 'all' && $customer[0] != ''){		
				$this->db->where_in('j.custid_fk', $customer); 
			}
			// Commented on 15/12/2015
			/*if($customerph[0] != 'null' && $customerph[0] != 'all'){
				$this->db->where_in('c.phone_1', $customerph); 
			}*/
			if($carrier_name[0] != 'null' && $carrier_name[0] != 'all' && $carrier_name[0] != '' && $carrier_name[0] != 'Please Select'){
				$this->db->where_in('cm.company_code', $carrier_name);
			}
			if($timezone[0] != 'null' && $timezone[0] != 'all' && $timezone[0] != ''){		
				$this->db->where_in('st.timezone', $timezone); 
			}
			/* if($leadassignee[0] != 'null' && $leadassignee[0] != 'all' && $leadassignee[0] != ''){		
				$this->db->where_in('j.lead_assign', $leadassignee);
			} */
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) )
			{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all' && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null')){
					$this->db->where_in('j.lead_assign',$leadassignee);	
				} 
			} 
			else{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all')
				{
					$this->db->where_in('j.lead_assign',$leadassignee);
				}
			}
			if($company_type != '' && $company_type != 'null'  && $company_type != 'all')
			{
				$this->db->where_in('j.company_type', $company_type);
			}
			if(count($order_handle_by_arr) > 0)
			{ 
				$this->db->where_in('j.order_handle_by',$order_handle_by_arr);
			}
			/*
			// Commented on 15/12/2015
			if($regionname[0] != 'null' && $regionname[0] != 'all'){
				$this->db->where_in('c.add1_region', $regionname);
			}
			if($countryname[0] != 'null' && $countryname[0] != 'all'){
				$this->db->where_in('c.add1_country', $countryname);
			}*/
			if($statename[0] != 'null' && $statename[0] != ''){	
				$this->db->where('((c.add1_state IN ('.$statename[0].') AND j.company_type=0) OR (cs.add1_state IN ('.$statename[0].') AND j.company_type=1)) ');
				// $this->db->where_in('c.add1_state', $statename);
			}
			/*if($locname[0] != 'null' && $locname[0] != 'all'){	
				$this->db->where_in('c.add1_location', $locname);
			}
			if($lead_status[0] != 'null' && $lead_status[0] !='') {	
				$this->db->where_in('j.lead_status', $lead_status);
			}*/
			if($lead_indi[0] != 'null' && $lead_indi[0] !='' && $lead_indi[0] != '') {	
				$this->db->where_in('j.lead_indicator', $lead_indi);
			}
			if($keyword != 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #' && $keyword != 'null' && $keyword != ''){		
				// $invwhere = "( (j.invoice_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR j.orderref LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%'))";
				if($search_category == 'Expert_Order_No'){
					$invwhere = "( j.order_no LIKE '%$keyword%' )";
				}else if($search_category == 'APS_Order_No'){
					$invwhere = "( j.aps_order_no LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%')";
				}else if($search_category == 'Patient_First_Name'){
					$invwhere = "( j.lead_title LIKE '%$keyword%' )";
				}else if($search_category == 'Patient_Last_Name'){
					$invwhere = "( j.lead_title_last LIKE '%$keyword%' )";
				}else if($search_category == 'Facility_Name'){
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian_Name'){
					$invwhere = "((cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name) LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Facility'){
					$invwhere = "( (c.company LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian'){
					$invwhere = "( (cs.company LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Policy_no'){
					$invwhere = "( j.orderref LIKE '%$keyword%' )";
				}else if($search_category == 'Doctor_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_1 LIKE '%$temp_format_num%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_2 LIKE '%$temp_format_num%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_3 LIKE '%$temp_format_num%' OR c.phone_4 LIKE '%$keyword%' OR c.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 0 )";
					}else{
					$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}
				}else if($search_category == 'Custodian_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_1 LIKE '%$temp_format_num%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$temp_format_num%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$temp_format_num%' OR cs.phone_4 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 1 )";
					}else{
					$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}
				}else{
					$invwhere = "( (j.order_no LIKE '%$keyword%' OR j.order_sub_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR  j.lead_title_last LIKE '%$keyword%' OR j.orderref LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR cs.company LIKE '%$keyword%' OR cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%'))";
				}
				$this->db->where($invwhere);
			}
		}
		else {
			$curusid = $this->session->userdata['logged_in_user']['userid'];
			
			$this->db->select('j.lead_id,j.aps_order_no,j.order_sub_no,j.invoice_no,cm.company_name AS carrier_name, j.order_no,j.company_type, j.lead_title, j.lead_title_last, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to, j.created_by, j.expect_worth_amount, j.expect_worth_id,j.orderingoffice, j.lead_indicator, j.lead_status, j.lead_assign, j.proposal_expected_date, j.orderref, 
			c.first_name, c.last_name, c.company,cs.first_name as c_first_name, cs.last_name as c_last_name, cs.company as c_company, st.timezone, stc.timezone as c_timezonec_add1_line1, rg.region_name,rgc.region_name as c_region_name, u.first_name as ufname, u.last_name as ulname, 
			us.first_name as usfname, us.last_name as usslname, ub.first_name as ubfn, ub.last_name as ubln, 
			ls.lead_stage_name, ew.expect_worth_name, st.state_name,stc.state_name as c_state_name, rec.*, pay.*, j.confirmed_close_date, j.order_sub_no, c.add1_line1, c.add1_line2,c.add1_postcode,c.add1_location,cs.add1_line1 as c_add1_line1, cs.add1_line2 as  as c_add1_line2,cs.add1_postcode as c_add1_postcode,cs.add1_location as c_add1_location,(SELECT lsh.dateofchange FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id order by lsh.dateofchange desc limit 1) as dateofchange,j.is_order_assigned_permanent,dis.disposition_name,j.ordering_office_id,coo.office_name,coo.office_account_number,coo.email as office_email,coo.phone as office_phone, (select first_name from crm_users where userid = (SELECT userid_fk FROM `crm_logs` WHERE `jobid_fk` = j.lead_id ORDER BY `logid` DESC LIMIT 1)) as assigned_fname, (select last_name from crm_users where userid = (SELECT userid_fk FROM `crm_logs` WHERE `jobid_fk` = j.lead_id ORDER BY `logid` DESC LIMIT 1)) as assigned_lname');
			$this->db->select("CONCAT(j.order_no,' ',COALESCE(j.order_sub_no,'')) AS orderno", FALSE);
			
			$this->db->from($this->cfg['dbpref'] . 'leads as j');
			$this->db->join($this->cfg['dbpref'].'customers as c', 'c.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] .'custodian as cs','cs.custid = j.custid_fk', 'LEFT');		
			$this->db->join($this->cfg['dbpref'].'users as u', 'u.userid = j.lead_assign', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'users as us', 'us.userid = j.modified_by', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'users as ub', 'ub.userid = j.belong_to', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
			$this->db->join($this->cfg['dbpref'].'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'disposition as dis', 'dis.id = j.order_disposition_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
			$this->db->where('j.pjt_status', 0); 
			// $this->db->where('j.lead_id != "null" AND j.lead_stage IN (1,2,3,4,5,6,7,8,9,10,11,12)');
			$this->db->where('j.lead_id != "null" AND j.lead_stage IN ("'.$this->stages.'")');
			
			
			if($stage[0] != 'null' && $stage[0] != 'all' && $stage[0] != '') {
				$this->db->where_in('j.lead_stage',$stage); 
				// $this->db->where('j.belong_to', $curusid);
			}
			if($queue_management_group != $this->config->item('crm')['check_payment_requested_stage'] && $queue_management_group != $this->config->item('crm')['check_payment_mailroom_stage']){
			if($event_type[0] != 'null' && $event_type[0] != 'all' && $event_type[0] != ''){
				$this->db->where_in('j.order_disposition_id', $event_type); 
			}
			}
			if($carrier_name[0] != 'null' && $carrier_name[0] != 'all' && $carrier_name[0] != '' && $carrier_name[0] != 'Please Select'){
				$this->db->where_in('cm.company_code', $carrier_name);
			}
			if($customer[0] != 'null' && $customer[0] != 'all' && $customer[0] != '') {		
				$this->db->where_in('j.custid_fk',$customer);				
			}
			if($customerph[0] != 'null' && $customerph[0] != 'all' && $customerph[0] != ''){		
				$this->db->where_in('c.phone_1',$customerph);
			}
			if($timezone[0] != 'null' && $timezone[0] != 'all' && $timezone[0] != ''){		
				$this->db->where_in('st.timezone', $timezone); 
			}
			/* if($leadassignee[0] != 'null' && $leadassignee[0] != 'all' && $leadassignee[0] != ''){		
				$this->db->where_in('j.lead_assign', $leadassignee);
			} */
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) )
			{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all' && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null')){
					$this->db->where_in('j.lead_assign',$leadassignee);	
				} 
			} 
			else{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all')
				{
					$this->db->where_in('j.lead_assign',$leadassignee);
				}
			}
			if(count($order_handle_by_arr) > 0)
			{ 
				$this->db->where_in('j.order_handle_by',$order_handle_by_arr);
			}
			if($company_type != '' && $company_type != 'null' && $company_type != 'all')
			{
				$this->db->where_in('j.company_type', $company_type);
			}
		
			if(($keyword != 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' && $keyword != 'null') || $search_category != ''){	
				if($search_category == 'Expert_Order_No'){
					$invwhere = "( j.order_no LIKE '%$keyword%' )";
				}else if($search_category == 'APS_Order_No'){
					$invwhere = "( j.aps_order_no LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%')";
				}else if($search_category == 'Patient_First_Name'){
					$invwhere = "( j.lead_title LIKE '%$keyword%' )";
				}else if($search_category == 'Patient_Last_Name'){
					$invwhere = "( j.lead_title_last LIKE '%$keyword%' )";
				}else if($search_category == 'Facility_Name'){
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian_Name'){
					$invwhere = "((cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name) LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Facility'){
					$invwhere = "( (c.company LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian'){
					$invwhere = "( (cs.company LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Policy_no'){
					$invwhere = "( j.orderref LIKE '%$keyword%' )";
				}else if($search_category == 'Doctor_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_1 LIKE '%$temp_format_num%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_2 LIKE '%$temp_format_num%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_3 LIKE '%$temp_format_num%' OR c.phone_4 LIKE '%$keyword%' OR c.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 0 )";
					}else{
					$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}
				}else if($search_category == 'Custodian_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_1 LIKE '%$temp_format_num%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$temp_format_num%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$temp_format_num%' OR cs.phone_4 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 1 )";
					}
					else{
					$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}
				}else{
					$invwhere = "( (j.order_no LIKE '%$keyword%' OR j.order_sub_no LIKE '%$keyword%'  OR j.lead_title LIKE '%$keyword%' OR  j.lead_title_last LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%'))";
				}	
				$this->db->where($invwhere);
			}
			if (isset($this->session->userdata['region_id']))
			$region = explode(',',$this->session->userdata['region_id']);
			if (isset($this->session->userdata['countryid']))
			$countryid = explode(',',$this->session->userdata['countryid']);
			if (isset($this->session->userdata['stateid']))
			$stateid = explode(',',$this->session->userdata['stateid']);
			if (isset($this->session->userdata['locationid']))
			$locationid = explode(',',$this->session->userdata['locationid']);

			if ( ($stage[0] == 'null' || $stage[0] == 'all' || $stage[0] == '') && ($customer[0] == 'null' || $customer[0] == 'all' || $customer[0] == '') && ($leadassignee[0] == 'null' || $leadassignee[0] == 'all' || $leadassignee[0] == '') && ($regionname[0] == 'null' || $regionname[0] == 'all' || $regionname[0] == '') && ($countryname[0] == 'null' || $countryname[0] == 'all' || $countryname[0] == '') && ($statename[0] == 'null' || $statename[0] == 'all' || $statename[0] == '') && ($locname[0] == 'null' || $locname[0] == 'all' || $locname[0] == '') && ($keyword == 'null' || $keyword == '') ) {
				
				if (isset($this->session->userdata['region_id']))
				$region = explode(',',$this->session->userdata['region_id']);
				if (isset($this->session->userdata['countryid']))
				$countryid = explode(',',$this->session->userdata['countryid']);
				if (isset($this->session->userdata['stateid']))
				$stateid = explode(',',$this->session->userdata['stateid']);
				if (isset($this->session->userdata['locationid']))
				$locationid = explode(',',$this->session->userdata['locationid']);

				$this->db->where_in('c.add1_region',$region);
				
				if (isset($this->session->userdata['countryid'])) {
					$this->db->where_in('c.add1_country',$countryid); 
				}
				if (isset($this->session->userdata['stateid'])) {
					$this->db->where_in('c.add1_state',$stateid);
				}
				if (isset($this->session->userdata['locationid'])) {
					$this->db->where_in('c.add1_location',$locationid); 
				}
				
				//or_where condition is used for to bring the lead owner leads when he creating the leads for different region.
				// $this->db->or_where('(j.belong_to = '.$curusid.' AND j.lead_stage IN (1,2,3,4,5,6,7,8,9,10,11,12))');
				// $this->db->or_where('(j.belong_to = '.$curusid.' AND j.lead_stage IN ("'.$this->stages.'") AND j.pjt_status = 0)');
			}
			
			//Advanced filter
				if($regionname[0] != 'null' && $regionname[0] != 'all'&& $regionname[0] != ''){		
					$this->db->where_in('c.add1_region',$regionname);
				} else {
					$this->db->where_in('c.add1_region',$region);
				}
				if($countryname[0] != 'null' && $countryname[0] != 'all' && $countryname[0] != '') {
					$this->db->where_in('c.add1_country', $countryname);
				} else if ((($this->userdata['level'])==3) || (($this->userdata['level'])==4) || (($this->userdata['level'])==5)) {
					$this->db->where_in('c.add1_country',$countryid);
				}
				if($statename[0] != 'null' && $statename[0] != 'all' && $statename[0] != '') {	
					$this->db->where_in('c.add1_state', $statename);
				} else if ((($this->userdata['level'])==4) || (($this->userdata['level'])==5)) {
					$this->db->where_in('c.add1_state',$stateid);
				}
				if($locname[0] != 'null' && $locname[0] != 'all' && $locname[0] != '') {	
					$this->db->where_in('c.add1_location', $locname);
				} else if (($this->userdata['level'])==5) {
					$this->db->where_in('c.add1_location',$locationid);
				}
				if($lead_status[0] != 'null' && $lead_status[0] != '' && $lead_status[0] != ''){	
					$this->db->where_in('j.lead_status', $lead_status);
				}
				if($lead_indi[0] != 'null' && $lead_indi[0] !='' && $lead_indi[0] != '') {	
					$this->db->where_in('j.lead_indicator', $lead_indi);
				}
			//Advanced filter
			
		}
		
		if($from_date != 'null' && $to_date !='') {	
			$this->db->where("(Date(j.proposal_expected_date) >='".date('Y-m-d', strtotime($from_date))."' )", NULL, FALSE);
			$this->db->where("(Date(j.proposal_expected_date) <='".date('Y-m-d', strtotime($to_date))."' )", NULL, FALSE);
		} else if(!empty($from_date) && ($from_date!='null')) {
			$this->db->where("(Date(j.proposal_expected_date) >='".date('Y-m-d', strtotime($from_date))."' )", NULL, FALSE);
		}

		if(is_array($queue_management_group)){
			if(!empty($queue_management_group)){
			$this->db->where_in("ls.queue_management_group",$queue_management_group, NULL, FALSE);
			}
		}else{
			if($filter_changes == 'Y' && ($queue_management_group == $this->config->item('crm')['check_payment_requested_stage'] || $queue_management_group == $this->config->item('crm')['check_payment_mailroom_stage'])){
				$this->db->where("(ls.queue_management_group = ".$queue_management_group." OR j.order_disposition_id =".$event_type[0].")");
			}else if($queue_management_group != 'null' && $queue_management_group !='' && $event_type == ''){
			$this->db->where("ls.queue_management_group",$queue_management_group, NULL, FALSE);
			}else if($queue_management_group !='' && $queue_management_group != 'null' && $event_type != ''){
				$this->db->where("(ls.queue_management_group = ".$queue_management_group." OR j.order_disposition_id =".$event_type[0].")");
			}
		}
		
		if($queue_management_status != 'null' && $queue_management_status !=''){
			$this->db->where("ls.queue_management_status",$queue_management_status, NULL, FALSE);
		}
		if($proposal_expected_from_date != 'null' && $proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date == '')
		{
			$proposal_expected_to_date = date('Y-m-d');
		}
		if($proposal_expected_to_date != null &&  $proposal_expected_to_date !='' && $proposal_expected_to_date !='null' && $proposal_expected_from_date == '')
		{
			$proposal_expected_from_date = date('Y-m-d',strtotime('-60 days'));
		}
		if($filter_changes != 'N'){
		if($proposal_expected_from_date != 'null' && $proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date != null &&  $proposal_expected_to_date !='' && $proposal_expected_to_date !='null'){
			$this->db->where('DATE(j.proposal_expected_date) between "'.$proposal_expected_from_date.'" and "'.$proposal_expected_to_date.'"');
		}
		}
		//  First Order handle by condition based on admin login, If not set advance filter order handle by apply 
		if($this->userdata['role_id'] != 1 && count($user_order_handle)>0 && sizeof($order_handle_by_arr) <= 0)
		{
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle))
			{
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'){
					$this->db->where('j.order_handle_by',$this->config->item('crm')['onshore_flag']);
				} 
			}
			else
			{
				$this->db->where_in('j.order_handle_by',$user_order_handle);
			}
		} 
		if($lead_assign && $this->userdata['role_id'] != 29 && $this->userdata['role_id'] != 1){
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle)) {
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null') {
					$this->db->where('j.lead_assign',$lead_assign);	
				}
			}
			else if($keyword == '' || $keyword == 'null' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number'){
				$this->db->where('j.lead_assign',$lead_assign);	
			}
		}
		if($this->userdata['role_id'] != 1){
			if($this->userdata['company_list'] != '' && $this->userdata['company_list'] != 'all' && $this->userdata['company_list'] != 'null' && $this->userdata['company_list'] != '')
			{
				$user_company_code = explode(',',$this->userdata['company_list']);
			}
			if($this->userdata['company_type'] != '' && $this->userdata['company_type'] != 'all' && $this->userdata['company_type'] != 'null' && $this->userdata['company_type'] != '')
			{
				$user_company_type = explode(',',$this->userdata['company_type']);
			}
			if($user_company_code != 'all' && $user_company_code != '' && $user_company_code != 'null' && sizeof($user_company_code)>0)
			{
				$this->db->where_in('j.company_code',$user_company_code);
			}
			if($user_company_type != '' && $user_company_type != 'null' && $user_company_type != 'all' && sizeof($user_company_type)>0)
			{
				$this->db->where_in('j.company_type',$user_company_type);
			}
		}
		$this->db->where('ls.is_deleted',0);
		$this->db->order_by("j.order_no", "desc");
		$this->db->order_by("j.order_sub_no", "asc");
		// $this->db->order_by("j.lead_id", "desc");
		$this->db->group_by("j.lead_id");
		$query = $this->db->get();
		// echo $this->db->last_query();
		$res =  $query->result_array();  
		// echo "<pre>";
		// print_r($res);
		// exit;     
		return $res;
	}
	
	//Bulk export excel with xlsx - start - sararavanan.si
	
	public function get_export_filter_resultsone($stage, $customer, $customerph, $timezone, $from_date, $to_date, $leadassignee, $regionname, $countryname, $statename, $locname, $lead_status,$lead_indi, $keyword, $queue_management_group, $queue_management_status, $proposal_expected_from_date, $proposal_expected_to_date,$event_type,$lead_assign,$carrier_name,$filter_changes,$company_type,$order_handle_by,$search_category)
	{   		
		$userdata 		= $this->session->userdata('logged_in_user');
		$stage 			= explode(',',$stage);
		$customer 		= explode(',',$customer);
		$customerph 	= explode(',',$customerph);
		$timezone 		= explode(',',$timezone);
		$leadassignee 	= explode(',',$leadassignee);
		$regionname 	= explode(',',$regionname);
		$countryname 	= explode(',',$countryname);
		$statename 		= explode(',',$statename);
		$locname 		= explode(',',$locname);
		$lead_status 	= explode(',',$lead_status);
		$lead_indi 		= explode(',',$lead_indi);
		$event_type		= explode(',',$event_type);
		$carrier_name	= explode(',',$carrier_name);
		// $filter_changes = $filter_changes;
		$order_handle_by_arr = array();
		if($order_handle_by!="") {
		$order_handle_by_arr = explode(',',$order_handle_by);
		}
		$user_order_handle = array();
		$user_order_handle = explode(',',$this->userdata['order_handle_by']);
		
		$user_order_handle = array_diff($user_order_handle,array(NULL));
		if($company_type!='' && $company_type!='null' && $company_type!='all') {
		$company_type = explode(',',$company_type);
		}
		
		$this->db->select("
	`j`.`lead_id` as 'leadid', `j`.`aps_order_no` AS 'aps_order_no',
IFNULL((SELECT rc.real_tat as completed_age FROM orderage as oa LEFT JOIN crm_real_calc_stop_tat as rc ON rc.lead_id = oa.lead_id WHERE rc.lead_id = `j`.`lead_id` LIMIT 1), '') as real_tat, 
IFNULL((SELECT rc.calc_tat as order_age FROM orderage as oa LEFT JOIN crm_real_calc_stop_tat as rc ON rc.lead_id = oa.lead_id WHERE rc.lead_id = `j`.`lead_id` LIMIT 1),'') as calc_tat,

(CASE WHEN `j`.`company_type` = '0' THEN `c`.`add1_location` ELSE `cs`.`add1_location` END) AS 'F_City', 
(CASE WHEN `j`.`company_type` = '0' THEN `st`.`state_name` ELSE `stc`.`state_name` END) AS 'F_State', 
(CASE WHEN `j`.`company_type` = '0' THEN `c`.`add1_postcode` ELSE `cs`.`add1_postcode` END) AS 'F_Zipcode', 

IFNULL((SELECT CONCAT(rc.stop_tat, '', ' Days') as stop_time FROM orderage as oa LEFT JOIN crm_real_calc_stop_tat as rc ON rc.lead_id = oa.lead_id WHERE rc.lead_id = `j`.`lead_id` LIMIT 0, 1), '') AS 'Stop_TAT', 

IFNULL((SELECT IF(oa.flag >= 1, 'YES', 'NO') FROM orderage as oa LEFT JOIN crm_real_calc_stop_tat as rc ON rc.lead_id = oa.lead_id WHERE rc.lead_id = `j`.`lead_id` LIMIT 0, 1), '') AS Special_Auth_Case_In_History, 

IFNULL((SELECT IF(oa.getReq >= 1, 'YES', 'NO') FROM orderage as oa LEFT JOIN crm_real_calc_stop_tat as rc ON rc.lead_id = oa.lead_id WHERE rc.lead_id = `j`.`lead_id` LIMIT 0, 1), '') AS Special_Auth_Required, 

IFNULL((SELECT IF(oa.total_days > 1, CONCAT(oa.total_days, ' ', 'Days'), CONCAT(oa.total_days, ' ', 'Days')) FROM orderage as oa LEFT JOIN crm_real_calc_stop_tat as rc ON rc.lead_id = oa.lead_id WHERE rc.lead_id = `j`.`lead_id` LIMIT 0, 1), '') AS Special_Auth_TAT, 

(CASE
		WHEN `j`.`company_type` = '0' THEN `dis`.`disposition_name`
		WHEN `j`.`company_type` = '1' THEN `dis`.`disposition_name`
		ELSE ''
	END) AS Special_Auth_Route_Reason,
	(CASE
     	WHEN `j`.`order_sub_no` != NULL THEN `j`.`order_sub_no`
     	ELSE '-'
    END) AS Subordinate_Order,

 `j`.`orderref` AS `Policy_Claim_Number`, 
 
 DATE_FORMAT(`j`.`date_created`, '%m-%d-%Y') AS `O_E_Date`, 
 
 (CASE WHEN (`pay`.`completion_date` != NULL && `pay`.`completion_date` != '0000-00-00') THEN DATE_FORMAT(`pay`.`completion_date`, '%m-%d-%Y') ELSE '' END)AS `Completion_date`, 
 
 (CASE WHEN (`j`.`confirmed_close_date` != NULL && `j`.`confirmed_close_date` != '0000-00-00') THEN DATE_FORMAT(`j`.`confirmed_close_date`, '%m-%d-%Y') ELSE '' END) AS `Confirmed_Order_Close`, 
 
 `j`.`lead_title` AS `Patient_first_name`, 
 `j`.`lead_title_last` AS `Patient_last_name`,
 IFNULL((IF(`j`.`company_type` = '0', CONCAT(`c`.`first_name`, '', `c`.`last_name`, '', `c`.`company`), CONCAT(`cs`.`first_name`, ' ', `cs`.`last_name`, ' ', `cs`.`company`)) ), '') AS `D_F_C_Name`, 
 
 IFNULL((IF(`j`.`company_type` = '0', CONCAT(`c`.`add1_line1`, ' ', `c`.`add1_line2`), CONCAT(`cs`.`add1_line1`, ' ', `cs`.`add1_line2`) ) ), '') AS `Facility_address`,
 IFNULL((IF( `coo`.`office_name` != NULL && `coo`.`office_name` != '', `coo`.`office_name`, `j`.`orderingoffice`)), '') AS `Ordering_Office`, 
 
 IFNULL(`rec`.`rec_type_name`, '') AS `Record_Type`, 
 
 IFNULL(((CASE WHEN `pay`.`payment_type` !='' && `pay`.`payment_type` = '1' THEN CONCAT('$', `pay`.`paid_amount`) ELSE '' END) ), '') AS Fee_card, 
 
 IFNULL(((CASE WHEN `pay`.`payment_type` !='' && `pay`.`payment_type` != '1' THEN CONCAT('$', `pay`.`paid_amount`) ELSE '' END)), '') AS Fee_ASM, 
 
 CONCAT('$ ', `j`.`expect_worth_amount`) AS Expected_Worth, 
 
 IFNULL((CASE WHEN `j`.`expect_worth_amount` = '0' && `j`.`lead_assign` > '0' THEN CONCAT(`u`.`first_name`, '', `u`.`last_name`) WHEN `j`.`lead_assign` = '1' THEN CONCAT(`u`.`first_name`, '', `u`.`last_name`) ELSE ' ' END), '') AS Assigned_CaseWorker,

 DATE_FORMAT(`j`.`date_modified`, '%m-%d-%Y') AS Updated_On, 
 
 `st`.`timezone` AS `Time_Zone`, 
 
 IFNULL((select `company_name` from `crm_company` as `cm` where `cm`.`company_code` = `j`.`company_code` limit 1), '') as `Client_Name`, 
 
 IFNULL(`ls`.`lead_stage_name`, '') AS `Stage`,
 `j`.`proposal_expected_date` AS Call_Back_Date,
 `j`.`is_order_assigned_permanent`, `j`.`lead_assign`, `u`.`first_name` as `ufname`, `u`.`last_name` as `ulname`, 
 `pay`.`payment_id` as `payid`, `j`.`orderingoffice`, `coo`.`office_name`, `j`.`order_sub_no`,
 (select CONCAT(`first_name`,'@#$',`last_name`) from crm_users where userid = (SELECT userid_fk FROM `crm_logs` WHERE `jobid_fk` = j.lead_id ORDER BY `logid` DESC LIMIT 1)LIMIT 1) as assigned_fname

	 ");
		
		if ($this->userdata['role_id'] == 1 || $this->userdata['role_id'] == 29 || $this->userdata['level'] == 1 || $this->userdata['role_id'] == 2) {
			
			$this->db->from($this->cfg['dbpref']. 'leads as j');
			$this->db->where('j.lead_id != "null" AND j.lead_stage IN ("'.$this->stages.'")');
			$this->db->where('j.pjt_status', 0);
			
			$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'custodian as cs','cs.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'disposition as dis', 'dis.id = j.order_disposition_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'payment_records as pay', 'j.lead_id = pay.lead_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'pay.rec_type_id = rec.rec_type_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'users as u', '`u`.`userid` = `j`.`lead_assign`', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code', 'LEFT');
			
			//121 $this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'users as ub', 'ub.userid = j.belong_to', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'j.lead_id = rec.rec_type_id', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'real_calc_stop_tat as rcs', 'rcs.lead_id = j.lead_id', 'LEFT');
			
			if($stage[0] != 'null' && $stage[0] != 'all' && $stage[0] != '') {		
				$this->db->where_in('j.lead_stage', $stage); 
			}
			if($queue_management_group != $this->config->item('crm')['check_payment_requested_stage'] && $queue_management_group != $this->config->item('crm')['check_payment_mailroom_stage']){
			if($event_type[0] != 'null' && $event_type[0] != 'all' && $event_type[0] != ''){
				$this->db->where_in('j.order_disposition_id', $event_type); 
			}
			}
			if($customer[0] != 'null' && $customer[0] != 'all' && $customer[0] != ''){		
				$this->db->where_in('j.custid_fk', $customer); 
			}
			// Commented on 15/12/2015
			/*if($customerph[0] != 'null' && $customerph[0] != 'all'){
				$this->db->where_in('c.phone_1', $customerph); 
			}*/
			if($carrier_name[0] != 'null' && $carrier_name[0] != 'all' && $carrier_name[0] != '' && $carrier_name[0] != 'Please Select'){
				$this->db->where_in('cm.company_code', $carrier_name);
			}
			if($timezone[0] != 'null' && $timezone[0] != 'all' && $timezone[0] != ''){		
				$this->db->where_in('st.timezone', $timezone); 
			}
			/* if($leadassignee[0] != 'null' && $leadassignee[0] != 'all' && $leadassignee[0] != ''){		
				$this->db->where_in('j.lead_assign', $leadassignee);
			} */
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) )
			{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all' && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null')){
					$this->db->where_in('j.lead_assign',$leadassignee);	
				} 
			} 
			else{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all')
				{
					$this->db->where_in('j.lead_assign',$leadassignee);
				}
			}
			if($company_type != '' && $company_type != 'null'  && $company_type != 'all')
			{
				$this->db->where_in('j.company_type', $company_type);
			}
			if(count($order_handle_by_arr) > 0)
			{ 
				$this->db->where_in('j.order_handle_by',$order_handle_by_arr);
			}
			/*
			// Commented on 15/12/2015
			if($regionname[0] != 'null' && $regionname[0] != 'all'){
				$this->db->where_in('c.add1_region', $regionname);
			}
			if($countryname[0] != 'null' && $countryname[0] != 'all'){
				$this->db->where_in('c.add1_country', $countryname);
			}*/
			if($statename[0] != 'null' && $statename[0] != ''){	
				$this->db->where('((c.add1_state IN ('.$statename[0].') AND j.company_type=0) OR (cs.add1_state IN ('.$statename[0].') AND j.company_type=1)) ');
				// $this->db->where_in('c.add1_state', $statename);
			}
			/*if($locname[0] != 'null' && $locname[0] != 'all'){	
				$this->db->where_in('c.add1_location', $locname);
			}
			if($lead_status[0] != 'null' && $lead_status[0] !='') {	
				$this->db->where_in('j.lead_status', $lead_status);
			}*/
			if($lead_indi[0] != 'null' && $lead_indi[0] !='' && $lead_indi[0] != '') {	
				$this->db->where_in('j.lead_indicator', $lead_indi);
			}
			if($keyword != 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #' && $keyword != 'null' && $keyword != ''){		
				// $invwhere = "( (j.invoice_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR j.orderref LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%'))";
				if($search_category == 'Expert_Order_No'){
					$invwhere = "( j.order_no LIKE '%$keyword%' )";
				}else if($search_category == 'APS_Order_No'){
					$invwhere = "( j.aps_order_no LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%')";
				}else if($search_category == 'Patient_First_Name'){
					$invwhere = "( j.lead_title LIKE '%$keyword%' )";
				}else if($search_category == 'Patient_Last_Name'){
					$invwhere = "( j.lead_title_last LIKE '%$keyword%' )";
				}else if($search_category == 'Facility_Name'){
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian_Name'){
					$invwhere = "((cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name) LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Facility'){
					$invwhere = "( (c.company LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian'){
					$invwhere = "( (cs.company LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Policy_no'){
					$invwhere = "( j.orderref LIKE '%$keyword%' )";
				}else if($search_category == 'Doctor_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_1 LIKE '%$temp_format_num%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_2 LIKE '%$temp_format_num%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_3 LIKE '%$temp_format_num%' OR c.phone_4 LIKE '%$keyword%' OR c.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 0 )";
					}else{
					$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}
				}else if($search_category == 'Custodian_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_1 LIKE '%$temp_format_num%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$temp_format_num%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$temp_format_num%' OR cs.phone_4 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 1 )";
					}else{
					$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}
				}else{
					$invwhere = "( (j.order_no LIKE '%$keyword%' OR j.order_sub_no LIKE '%$keyword%' OR j.lead_title LIKE '%$keyword%' OR  j.lead_title_last LIKE '%$keyword%' OR j.orderref LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR cs.company LIKE '%$keyword%' OR cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%'))";
				}
				$this->db->where($invwhere);
			}
			
		}
		else {
			$curusid = $this->session->userdata['logged_in_user']['userid'];
			
			$this->db->from($this->cfg['dbpref'] . 'leads as j');
			
			$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'custodian as cs','cs.custid = j.custid_fk', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'state as stc', 'stc.stateid = cs.add1_state', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'disposition as dis', 'dis.id = j.order_disposition_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'payment_records as pay', 'j.lead_id = pay.lead_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'carrier_ordering_office as coo', 'coo.office_id = j.ordering_office_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'pay.rec_type_id = rec.rec_type_id', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'users as u', '`u`.`userid` = `j`.`lead_assign`', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
			$this->db->join($this->cfg['dbpref'] . 'company as cm', 'cm.company_code = j.company_code', 'LEFT');
			
			//121 $this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'users as ub', 'ub.userid = j.belong_to', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'region as rgc', 'rgc.regionid = cs.add1_region', 'LEFT');
			//121 $this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'j.lead_id = rec.rec_type_id', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'real_calc_stop_tat as rcs', 'rcs.lead_id = j.lead_id', 'LEFT');
			
			$this->db->where('j.pjt_status', 0); 
			// $this->db->where('j.lead_id != "null" AND j.lead_stage IN (1,2,3,4,5,6,7,8,9,10,11,12)');
			$this->db->where('j.lead_id != "null" AND j.lead_stage IN ("'.$this->stages.'")');
			
			
			if($stage[0] != 'null' && $stage[0] != 'all' && $stage[0] != '') {
				$this->db->where_in('j.lead_stage',$stage); 
				// $this->db->where('j.belong_to', $curusid);
			}
			if($queue_management_group != $this->config->item('crm')['check_payment_requested_stage'] && $queue_management_group != $this->config->item('crm')['check_payment_mailroom_stage']){
			if($event_type[0] != 'null' && $event_type[0] != 'all' && $event_type[0] != ''){
				$this->db->where_in('j.order_disposition_id', $event_type); 
			}
			}
			if($carrier_name[0] != 'null' && $carrier_name[0] != 'all' && $carrier_name[0] != '' && $carrier_name[0] != 'Please Select'){
				$this->db->where_in('cm.company_code', $carrier_name);
			}
			if($customer[0] != 'null' && $customer[0] != 'all' && $customer[0] != '') {		
				$this->db->where_in('j.custid_fk',$customer);				
			}
			if($customerph[0] != 'null' && $customerph[0] != 'all' && $customerph[0] != ''){		
				$this->db->where_in('c.phone_1',$customerph);
			}
			if($timezone[0] != 'null' && $timezone[0] != 'all' && $timezone[0] != ''){		
				$this->db->where_in('st.timezone', $timezone); 
			}
			/* if($leadassignee[0] != 'null' && $leadassignee[0] != 'all' && $leadassignee[0] != ''){		
				$this->db->where_in('j.lead_assign', $leadassignee);
			} */
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) )
			{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all' && ($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null')){
					$this->db->where_in('j.lead_assign',$leadassignee);	
				} 
			} 
			else{
				if($leadassignee[0] != 'null' && $leadassignee[0] != '' && $leadassignee[0] != 'all')
				{
					$this->db->where_in('j.lead_assign',$leadassignee);
				}
			}
			if(count($order_handle_by_arr) > 0)
			{ 
				$this->db->where_in('j.order_handle_by',$order_handle_by_arr);
			}
			if($company_type != '' && $company_type != 'null' && $company_type != 'all')
			{
				$this->db->where_in('j.company_type', $company_type);
			}
		
			if(($keyword != 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' && $keyword != 'null') || $search_category != ''){	
				if($search_category == 'Expert_Order_No'){
					$invwhere = "( j.order_no LIKE '%$keyword%' )";
				}else if($search_category == 'APS_Order_No'){
					$invwhere = "( j.aps_order_no LIKE '%$keyword%' OR j.reopen_order_no LIKE '%$keyword%')";
				}else if($search_category == 'Patient_First_Name'){
					$invwhere = "( j.lead_title LIKE '%$keyword%' )";
				}else if($search_category == 'Patient_Last_Name'){
					$invwhere = "( j.lead_title_last LIKE '%$keyword%' )";
				}else if($search_category == 'Facility_Name'){
					$invwhere = "((c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%' OR CONCAT(c.first_name,' ',c.last_name) LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian_Name'){
					$invwhere = "((cs.first_name LIKE '%$keyword%' OR cs.last_name LIKE '%$keyword%' OR CONCAT(cs.first_name,' ',cs.last_name) LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Facility'){
					$invwhere = "( (c.company LIKE '%$keyword%') AND j.company_type = 0)";
				}else if($search_category == 'Custodian'){
					$invwhere = "( (cs.company LIKE '%$keyword%') AND j.company_type = 1)";
				}else if($search_category == 'Policy_no'){
					$invwhere = "( j.orderref LIKE '%$keyword%' )";
				}else if($search_category == 'Doctor_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_1 LIKE '%$temp_format_num%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_2 LIKE '%$temp_format_num%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_3 LIKE '%$temp_format_num%' OR c.phone_4 LIKE '%$keyword%' OR c.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 0 )";
					}else{
					$invwhere = "( (c.phone_1 LIKE '%$keyword%' OR c.phone_2 LIKE '%$keyword%' OR c.phone_3 LIKE '%$keyword%' OR c.phone_4 LIKE '%$keyword%') AND j.company_type = 0 )";
					}
				}else if($search_category == 'Custodian_Direct_Phone'){
					if(strlen($keyword) <= 10){
						$arr = str_split($keyword);
						$temp_format_num = '';
						foreach($arr as $k => $val){
							if($k == 0){
								$temp_format_num .= '('.$val;
							}else if($k == 2){
								$temp_format_num .= $val.') ';
							}else if($k == 6){
								$temp_format_num .= '-'.$val;
							}else{
								$temp_format_num .= $val;
							}
						}
						$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_1 LIKE '%$temp_format_num%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$temp_format_num%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$temp_format_num%' OR cs.phone_4 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$temp_format_num%') AND j.company_type = 1 )";
					}
					else{
					$invwhere = "( (cs.phone_1 LIKE '%$keyword%' OR cs.phone_2 LIKE '%$keyword%' OR cs.phone_3 LIKE '%$keyword%' OR cs.phone_4 LIKE '%$keyword%') AND j.company_type = 1 )";
					}
				}else{
					$invwhere = "( (j.order_no LIKE '%$keyword%' OR j.order_sub_no LIKE '%$keyword%'  OR j.lead_title LIKE '%$keyword%' OR  j.lead_title_last LIKE '%$keyword%' OR c.company LIKE '%$keyword%' OR c.first_name LIKE '%$keyword%' OR c.last_name LIKE '%$keyword%'))";
				}	
				$this->db->where($invwhere);
			}
			if (isset($this->session->userdata['region_id']))
			$region = explode(',',$this->session->userdata['region_id']);
			if (isset($this->session->userdata['countryid']))
			$countryid = explode(',',$this->session->userdata['countryid']);
			if (isset($this->session->userdata['stateid']))
			$stateid = explode(',',$this->session->userdata['stateid']);
			if (isset($this->session->userdata['locationid']))
			$locationid = explode(',',$this->session->userdata['locationid']);

			if ( ($stage[0] == 'null' || $stage[0] == 'all' || $stage[0] == '') && ($customer[0] == 'null' || $customer[0] == 'all' || $customer[0] == '') && ($leadassignee[0] == 'null' || $leadassignee[0] == 'all' || $leadassignee[0] == '') && ($regionname[0] == 'null' || $regionname[0] == 'all' || $regionname[0] == '') && ($countryname[0] == 'null' || $countryname[0] == 'all' || $countryname[0] == '') && ($statename[0] == 'null' || $statename[0] == 'all' || $statename[0] == '') && ($locname[0] == 'null' || $locname[0] == 'all' || $locname[0] == '') && ($keyword == 'null' || $keyword == '') ) {
				
				if (isset($this->session->userdata['region_id']))
				$region = explode(',',$this->session->userdata['region_id']);
				if (isset($this->session->userdata['countryid']))
				$countryid = explode(',',$this->session->userdata['countryid']);
				if (isset($this->session->userdata['stateid']))
				$stateid = explode(',',$this->session->userdata['stateid']);
				if (isset($this->session->userdata['locationid']))
				$locationid = explode(',',$this->session->userdata['locationid']);

				$this->db->where_in('c.add1_region',$region);
				
				if (isset($this->session->userdata['countryid'])) {
					$this->db->where_in('c.add1_country',$countryid); 
				}
				if (isset($this->session->userdata['stateid'])) {
					$this->db->where_in('c.add1_state',$stateid);
				}
				if (isset($this->session->userdata['locationid'])) {
					$this->db->where_in('c.add1_location',$locationid); 
				}
				
				//or_where condition is used for to bring the lead owner leads when he creating the leads for different region.
				// $this->db->or_where('(j.belong_to = '.$curusid.' AND j.lead_stage IN (1,2,3,4,5,6,7,8,9,10,11,12))');
				// $this->db->or_where('(j.belong_to = '.$curusid.' AND j.lead_stage IN ("'.$this->stages.'") AND j.pjt_status = 0)');
			}
			
			//Advanced filter
				if($regionname[0] != 'null' && $regionname[0] != 'all'&& $regionname[0] != ''){		
					$this->db->where_in('c.add1_region',$regionname);
				} else {
					$this->db->where_in('c.add1_region',$region);
				}
				if($countryname[0] != 'null' && $countryname[0] != 'all' && $countryname[0] != '') {
					$this->db->where_in('c.add1_country', $countryname);
				} else if ((($this->userdata['level'])==3) || (($this->userdata['level'])==4) || (($this->userdata['level'])==5)) {
					$this->db->where_in('c.add1_country',$countryid);
				}
				if($statename[0] != 'null' && $statename[0] != 'all' && $statename[0] != '') {	
					$this->db->where_in('c.add1_state', $statename);
				} else if ((($this->userdata['level'])==4) || (($this->userdata['level'])==5)) {
					$this->db->where_in('c.add1_state',$stateid);
				}
				if($locname[0] != 'null' && $locname[0] != 'all' && $locname[0] != '') {	
					$this->db->where_in('c.add1_location', $locname);
				} else if (($this->userdata['level'])==5) {
					$this->db->where_in('c.add1_location',$locationid);
				}
				if($lead_status[0] != 'null' && $lead_status[0] != '' && $lead_status[0] != ''){	
					$this->db->where_in('j.lead_status', $lead_status);
				}
				if($lead_indi[0] != 'null' && $lead_indi[0] !='' && $lead_indi[0] != '') {	
					$this->db->where_in('j.lead_indicator', $lead_indi);
				}
			//Advanced filter
			
		}
		
		if($from_date != 'null' && $to_date !='') {	
			$this->db->where("(Date(j.proposal_expected_date) >='".date('Y-m-d', strtotime($from_date))."' )", NULL, FALSE);
			$this->db->where("(Date(j.proposal_expected_date) <='".date('Y-m-d', strtotime($to_date))."' )", NULL, FALSE);
		} else if(!empty($from_date) && ($from_date!='null')) {
			$this->db->where("(Date(j.proposal_expected_date) >='".date('Y-m-d', strtotime($from_date))."' )", NULL, FALSE);
		}

		if(is_array($queue_management_group)){
			if(!empty($queue_management_group)){
			$this->db->where_in("ls.queue_management_group",$queue_management_group, NULL, FALSE);
			}
		}else{
			if($filter_changes == 'Y' && ($queue_management_group == $this->config->item('crm')['check_payment_requested_stage'] || $queue_management_group == $this->config->item('crm')['check_payment_mailroom_stage'])){
				$this->db->where("(ls.queue_management_group = ".$queue_management_group." OR j.order_disposition_id =".$event_type[0].")");
			}else if($queue_management_group != 'null' && $queue_management_group !='' && $event_type == ''){
			$this->db->where("ls.queue_management_group",$queue_management_group, NULL, FALSE);
			}else if($queue_management_group !='' && $queue_management_group != 'null' && $event_type != ''){
				$this->db->where("(ls.queue_management_group = ".$queue_management_group." OR j.order_disposition_id =".$event_type[0].")");
			}
		}
		
		if($queue_management_status != 'null' && $queue_management_status !=''){
			$this->db->where("ls.queue_management_status",$queue_management_status, NULL, FALSE);
		}
		if($proposal_expected_from_date != 'null' && $proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date == '')
		{
			$proposal_expected_to_date = date('Y-m-d');
		}
		if($proposal_expected_to_date != null &&  $proposal_expected_to_date !='' && $proposal_expected_to_date !='null' && $proposal_expected_from_date == '')
		{
			$proposal_expected_from_date = date('Y-m-d',strtotime('-60 days'));
		}
		if($filter_changes != 'N'){
		if($proposal_expected_from_date != 'null' && $proposal_expected_from_date != null && $proposal_expected_from_date !='' && $proposal_expected_to_date != null &&  $proposal_expected_to_date !='' && $proposal_expected_to_date !='null'){
			$this->db->where('DATE(j.proposal_expected_date) between "'.$proposal_expected_from_date.'" and "'.$proposal_expected_to_date.'"');
		}
		}
		//  First Order handle by condition based on admin login, If not set advance filter order handle by apply 
		if($this->userdata['role_id'] != 1 && count($user_order_handle)>0 && sizeof($order_handle_by_arr) <= 0)
		{
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle))
			{
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'){
					$this->db->where('j.order_handle_by',$this->config->item('crm')['onshore_flag']);
				} 
			}
			else
			{
				$this->db->where_in('j.order_handle_by',$user_order_handle);
			}
		} 
		if($lead_assign && $this->userdata['role_id'] != 29 && $this->userdata['role_id'] != 1){
			if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle)) {
				if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null') {
					$this->db->where('j.lead_assign',$lead_assign);	
				}
			}
			else if($keyword == '' || $keyword == 'null' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number'){
				$this->db->where('j.lead_assign',$lead_assign);	
			}
		}
		if($this->userdata['role_id'] != 1){
			if($this->userdata['company_list'] != '' && $this->userdata['company_list'] != 'all' && $this->userdata['company_list'] != 'null' && $this->userdata['company_list'] != '')
			{
				$user_company_code = explode(',',$this->userdata['company_list']);
			}
			if($this->userdata['company_type'] != '' && $this->userdata['company_type'] != 'all' && $this->userdata['company_type'] != 'null' && $this->userdata['company_type'] != '')
			{
				$user_company_type = explode(',',$this->userdata['company_type']);
			}
			if($user_company_code != 'all' && $user_company_code != '' && $user_company_code != 'null' && sizeof($user_company_code)>0)
			{
				$this->db->where_in('j.company_code',$user_company_code);
			}
			if($user_company_type != '' && $user_company_type != 'null' && $user_company_type != 'all' && sizeof($user_company_type)>0)
			{
				$this->db->where_in('j.company_type',$user_company_type);
			}
		}
		$this->db->where('ls.is_deleted',0);
		//$this->db->limit($offset,$page);
		$this->db->order_by("j.order_no", "desc");
		$this->db->order_by("j.order_sub_no", "asc");
		// $this->db->order_by("j.lead_id", "desc");
		$this->db->group_by("j.lead_id");
		$query = $this->db->get();
		$lastqry = $this->db->last_query();
		//121 $vals = $this->db->query_one($lastqry);
		//121 return $vals;
		/*$temp_usrids = $this->session->userdata['logged_in_user']['userid'];
		$this->db->query("DROP TABLE IF EXISTS temp_$temp_usrids");
		$this->db->query("CREATE TEMPORARY TABLE Masterinfo AS ($lastqry)");
		$this->db->select("*");
		$this->db->from("temp_$temp_usrids");
		$query = $this->db->get(); */
		return $res =  $query->result_array();
		//echo count($res);
		//exit;
		//121 $this->db->query("DROP TABLE IF EXISTS temp_$temp_usrids");
		// echo "<pre>";
		// print_r($res);
		// exit;     
		//return $res;
		
	//$this->db->query("DROP TABLE IF EXISTS Masterinfo");
//$this->db->query("CREATE TEMPORARY TABLE Masterinfo AS (select S.first_name, S.las...

	
	}
	//Bulk export excel with xlsx - end - sararavanan.si
	
	
	public function get_assignedName($leadid){
		$this->db->select('`first_name` as `assigned_fname`, `last_name` as `assigned_lname` FROM `crm_users` WHERE userid = (SELECT userid_fk FROM crm_logs WHERE jobid_fk = '.$leadid.' ORDER BY logid DESC LIMIT 1)');
		$query = $this->db->get();
		$rst =  $query->result_array();       
		return $rst;
	}

	//project
	public function assign_lists($stage, $customer, $worth, $owner, $keyword)
	{
		$userdata = $this->session->userdata('logged_in_user');
		 //print_r($userdata['userid']);
		 $this->db->select('j.lead_id, j.invoice_no, j.lead_title, j.lead_title_last, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to,
		j.created_by, j.expect_worth_amount, j.expect_worth_id, j.lead_indicator, j.lead_status, j.proposal_expected_date, 
		c.first_name, c.last_name, c.company, rg.region_name, u.first_name as ufname, u.last_name as ulname,us.first_name as usfname,
		us.last_name as usslname, ls.lead_stage_name,ew.expect_worth_name');
		$this->db->from($this->cfg['dbpref'] . 'customers as c');		
		$this->db->join($this->cfg['dbpref'] . 'leads as j', 'j.custid_fk = c.custid AND j.lead_id != "null"','LEFT');		
		$this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = j.lead_assign','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id' ,'LEFT');
		
		$this->db->where('j.lead_assign', $userdata['userid']);
		
		$region     = @explode(',',$this->session->userdata['region_id']);	
		$countryid  = @explode(',',$this->session->userdata['countryid']);
		$stateid    = @explode(',',$this->session->userdata['stateid']);
		$locationid = @explode(',',$this->session->userdata['locationid']);
		
		
		$this->db->where_in('c.add1_region',$region); 
		if($this->session->userdata['countryid'] != '') {
			$this->db->where_in('c.add1_country',$countryid); 
		}
		if($this->session->userdata['stateid'] != '') {
			$this->db->where_in('c.add1_state',$stateid); 
		}
		if($this->session->userdata['locationid'] != '') {
			$this->db->where_in('c.add1_location',$locationid); 
		}
		 
		 $query = $this->db->get();
		 
		 $customers =  $query->result_array();       
		 return $customers;
	}
	
	//advanced search functionality- new requirement
	function getcountry_list($val) {
		$val = explode(",", $val); //using as an array
		$userdata = $this->session->userdata('logged_in_user');	
		
		//restriction for country
		$coun_query = $this->db->query("SELECT country_id FROM ".$this->cfg['dbpref']."levels_country WHERE level_id = '".$userdata['level']."' AND user_id = '".$userdata['userid']."' ");
		$coun_details = $coun_query->result_array();
		foreach($coun_details as $coun) {
			$countries[] = $coun['country_id'];
		}
		if (!empty($countries)) {
			$countries_ids = array_unique($countries);
			$countries_ids = (array_values($countries)); //reset the keys in the array
		}
		
        $this->db->order_by('inactive', 'asc');
        $this->db->order_by('country_name', 'asc');
		
		$this->db->where_in('regionid', $val);
		if ($userdata['level'] == 3 || $userdata['level'] == 4 || $userdata['level'] == 5) {
			$this->db->where_in('countryid', $countries_ids);
		}
		$customers = $this->db->get($this->cfg['dbpref']. 'country');
		return $customers->result_array();	
    }
	
	//For States
	function getstate_list($val) {       
		$userdata = $this->session->userdata('logged_in_user');
		$val = explode(",", $val); //using as an array
		//restriction for state
		$ste_query = $this->db->query("SELECT state_id FROM ".$this->cfg['dbpref']."levels_state WHERE level_id = '".$userdata['level']."' AND user_id = '".$userdata['userid']."' ");
		$ste_details = $ste_query->result_array();
		foreach($ste_details as $ste)
		{
			$states[] = $ste['state_id'];
		}
		$states_ids = array_unique($states);
		$states_ids = (array_values($states)); //reset the keys in the array
		
        $this->db->order_by('inactive', 'asc');
        $this->db->order_by('state_name', 'asc');
		
		$this->db->where_in('countryid', $val);
		if ($userdata['level'] == 4 || $userdata['level'] == 5) {
			$this->db->where_in('stateid', $states_ids);
		}
		$stat = $this->db->get($this->cfg['dbpref'] . 'state');
		
		return $stat->result_array();	
    }
	
	//for locations
	function getlocation_list($val) {
		$userdata = $this->session->userdata('logged_in_user');
		$val = @explode(",", $val); //using as an array
		
		//restriction for location
		$loc_query = $this->db->query("SELECT location_id FROM ".$this->cfg['dbpref']."levels_location WHERE level_id = '".$userdata['level']."' AND user_id = '".$userdata['userid']."' ");
		$loc_details = $loc_query->result_array();
		foreach($loc_details as $loc)
		{
			$locations[] = $loc['location_id'];
		}
		$locations_ids = array_unique($locations);
		$locations_ids = (array_values($locations)); //reset the keys in the array
		
        $this->db->order_by('inactive', 'asc');
        $this->db->order_by('location_name', 'asc');
		
		$this->db->where_in('stateid', $val);
		if ($userdata['level'] == 5) {
			$this->db->where_in('locationid', $locations_ids);
		}
		$customers = $this->db->get($this->cfg['dbpref'] . 'location');
		//echo $this->db->last_query();
		return $customers->result_array();
    }
    
    //Insert new Job - Below functions are created by MAR
    function insert_job($ins) {
    	$this->db->insert($this->cfg['dbpref'] . 'leads', $ins);
    	return $this->db->insert_id();
    }
	
    function update_job($insert_id, $up_args) {
    	$this->db->where('lead_id', $insert_id);
		$this->db->update($this->cfg['dbpref'] . 'leads', $up_args);
    }
    
    function get_lead_assign($level) {
    	$this->db->select('userid', 'first_name');
    	if(!empty($level))
    	$this->db->where('level', $level);
    	$q = $this->db->get($this->cfg['dbpref'] . 'users');
    	return $q->result_array();
    }
	
	//level restriction
	public function level_restriction() {
		$userdata = $this->session->userdata('logged_in_user');
		if (($userdata['role_id'] == 1 && $userdata['level'] == 1) || ($userdata['role_id'] == 29 && $userdata['level'] == 1) || ($userdata['role_id'] == 2 && $userdata['level'] == 1)) 
		{
			$cusId = '';
		}
		else
		{
			$cusIds = array();
			$cusIds[] = 0;
			$reg = array();
			$cou = array();
			$ste = array();
			$loc = array();
			switch($userdata['level']){
				case 2:
					$regions = $this->getRegions($userdata['userid'], $userdata['level']); //Get the Regions based on Level
						foreach ($regions as $rgid) {
							$reg[] = $rgid['region_id'];
						}
					$CustomersId = $this->getCustomersIds($reg); //Get the Customer id based on Regions
						foreach ($CustomersId as $cus_id) {
							$cusIds[] = $cus_id['custid'];
						}
					$cusId = $cusIds;
				break;
				case 3:
					$countries = $this->getCountries($userdata['userid'], $userdata['level']); //Get the Countries based on Level
						foreach ($countries as $couid) {
							$cou[] = $couid['country_id'];
						}
					$CustomersId = $this->getCustomersIds($reg,$cou); //Get the Customer id based on Regions & Countries
						foreach ($CustomersId as $cus_id) {
							$cusIds[] = $cus_id['custid'];
						}
					$cusId = $cusIds;
				break;
				case 4:
					$states = $this->getStates($userdata['userid'], $userdata['level']); //Get the States based on Level
						foreach ($states as $steid) {
							$ste[] = $steid['state_id'];
						}
					$CustomersId = $this->getCustomersIds($reg,$cou,$ste); //Get the Customer id based on Regions & Countries
						foreach ($CustomersId as $cus_id) {
							$cusIds[] = $cus_id['custid'];
						}
					$cusId = $cusIds;
				break;
				case 5:
					$locations = $this->getLocations($userdata['userid'], $userdata['level']); //Get the Locations based on Level
						foreach ($locations as $locid) {
							$loc[] = $locid['location_id'];
						}	
					$CustomersId = $this->getCustomersIds($reg,$cou,$ste,$loc); //Get the Customer id based on Regions & Countries
						foreach ($CustomersId as $cus_id) {
							$cusIds[] = $cus_id['custid'];
						}
					$cusId = $cusIds;
				break;
			}
		}
		return $cusId;
	}
	
	/*Level Restrictions*/
	//For Regions
	public function getRegions($uid, $lvlid) {
		$this->db->select('region_id');
		$this->db->from($this->cfg['dbpref'].'levels_region');
		$this->db->where('user_id', $uid);
   		$this->db->where('level_id', $lvlid);
		$query = $this->db->get();
		$rg_query = $query->result_array();
		return $rg_query;
	}
	
	//For Countries
	public function getCountries($uid, $lvlid) {
		$this->db->select('country_id');
		$this->db->from($this->cfg['dbpref'].'levels_country');
		$this->db->where('user_id', $uid);
   		$this->db->where('level_id', $lvlid);
		$query = $this->db->get();
		$cs_query = $query->result_array();
		return $cs_query;
	}
	
	//For States
	public function getStates($uid, $lvlid) {
		$this->db->select('state_id');
		$this->db->from($this->cfg['dbpref'].'levels_state');
		$this->db->where('user_id', $uid);
   		$this->db->where('level_id', $lvlid);
		$query = $this->db->get();
		$ste_query = $query->result_array();
		return $ste_query;
	}
	
	//For Locations
	public function getLocations($uid, $lvlid) {
		$this->db->select('location_id');
		$this->db->from($this->cfg['dbpref'].'levels_location');
		$this->db->where('user_id', $uid);
   		$this->db->where('level_id', $lvlid);
		$query = $this->db->get();
		$loc_query = $query->result_array();
		return $loc_query;
	}
	
	//For Customers
	public function getCustomersIds($regId = FALSE, $couId = FALSE, $steId = FALSE, $locId = FALSE) {
		$this->db->select('custid');
		$this->db->from($this->cfg['dbpref'].'customers');
		if (!empty($regId)) {
			$this->db->where_in('add1_region', $regId);
		}
		if (!empty($couId)) {
			$this->db->where_in('add1_country', $couId);
		}
		if (!empty($steId)) {
			$this->db->where_in('add1_state', $steId);
		}
		if (!empty($locId)) {
			$this->db->where_in('add1_location', $locId);
		}
		$query = $this->db->get();
		$res_query =  $query->result_array();
		return $res_query;
	}
	/*Level Restrictions*/
	function updt_order_close_date($id, $date) 
	{
		$order_date = date('Y-m-d', strtotime($date));
		$this->db->where('lead_id', $id);
		return $this->db->update($this->cfg['dbpref'] . 'leads',array('confirmed_close_date' => $order_date));
	}
	
	public function add_pay_records($data)
	{
		// print_r($data);exit;
		$this->db->insert($this->cfg['dbpref'] . 'payment_records', $data);
    	return $this->db->insert_id();
	}

	public function add_service_records($data)
	{
		// print_r($data);exit;
		$this->db->insert($this->cfg['dbpref'] . 'copy_service_record', $data);
    	return $this->db->insert_id();
	}
	
	public function get_record_type()
	{
		$this->db->from($this->cfg['dbpref'].'record_type');
		$query = $this->db->get();
		$query = $query->result_array();
		return $query;
	}
	
	public function get_call_status()
	{
		$this->db->from($this->cfg['dbpref'].'disposition');
		$this->db->where('is_deleted',0);
		$this->db->where('status',1);
		$this->db->order_by('disposition_name','asc');
		$query = $this->db->get();
		
		$query = $query->result_array();
		return $query;
	}
	
	public function get_callback_available($id)
	{
		$this->db->from($this->cfg['dbpref'].'disposition');
		$this->db->where('id',$id);
		$this->db->where('status',1);
		$query = $this->db->get();
		$query = $query->result_array();
		return $query;
	}
	
	public function get_payment_status()
	{
		$this->db->from($this->cfg['dbpref'].'payment_status');
		$query = $this->db->get();
		$query = $query->result_array();
		return $query;
	}
	
	public function get_payment_records_list($lead_id)
	{
		$this->db->select("rt.rec_type_name,pay.lead_id,ps.payment_status_name,pay.payment_type,pay.completion_date,pay.payment_id, pay.paid_amount,pay.sign_amount_type,pay.ref_payment_id, if((`pay`.`payment_type`=3 OR `pay`.`payment_type` = 2) AND pay.sign_amount_type=0, 'Edit','No_edit') as edit_option, date_format(pay.created_on, '%m/%d/%Y') as created_on,CONCAT(`u`.`first_name`,' ',`u`.`last_name`) as created_by,IF(pay.payment_to=1,'Copy Service','Facility') as payment_to, IF(pay.payment_to=1,(select Name from crm_copy_service where id = pay.copy_service_id),IF(l.company_type=0,(select CONCAT(first_name,' ',last_name,' - ',COALESCE(company,'')) from crm_customers where custid = pay.facility_id),(select CONCAT(first_name,' ',last_name,' - ',COALESCE(company,'')) from crm_custodian where custid = pay.facility_id))) as paid_to,pay.payment_to as mib_payment_to");
	    $this->db->from($this->cfg['dbpref'].'payment_records pay');
	    $this->db->join($this->cfg['dbpref'] . 'payment_status as ps', 'ps.payment_status_id = pay.payment_status_id', 'LEFT');
	    $this->db->join($this->cfg['dbpref'] . 'record_type as rt', 'rt.rec_type_id = pay.rec_type_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'leads as l', 'l.lead_id = pay.lead_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'users as u', 'u.userid = pay.user_id', 'LEFT');
		$this->db->where("l.lead_id", $lead_id);
		$this->db->order_by('payment_id', 'desc');
	    $result = $this->db->get();
	    $data = $result->result_array();
		// echo $this->db->last_query(); exit;
	    return $data;
	}
	
	public function get_payment_view($payment_id)
	{
		$this->db->select("pay.*,rty.rec_type_name,pst.payment_status_name, IF(pay.payment_to=1,(select Name from crm_copy_service where id = pay.copy_service_id),IF(l.company_type=0,(select CONCAT(first_name,' ',last_name,IF(first_name!='' or last_name!='' ,' - ',''),COALESCE(company,'')) from crm_customers where custid = pay.facility_id),(select CONCAT(first_name,' ',last_name,IF(first_name!='' and last_name!='' ,' - ',''),COALESCE(company,'')) from crm_custodian where custid = pay.facility_id))) as paid_to, IF(pay.payment_to=1,'Copy Service','Facility') as payment_to, pay.payment_to as pay_to,invoice_required,is_packet_required,l.company_code as  leads_company_code,pay.payment_to as mib_payment_to");
		//$this->db->select("pay.*,rty.rec_type_name,pst.payment_status_name, IF(pay.payment_to=1,(select Name from crm_copy_service where id = pay.copy_service_id),IF(l.company_type=0,(select CONCAT(first_name,' ',last_name,' - ',COALESCE(company,'')) from crm_customers where custid = pay.facility_id),(select CONCAT(first_name,' ',last_name,' - ',COALESCE(company,'')) from crm_custodian where custid = pay.facility_id))) as paid_to, IF(pay.payment_to=1,'Copy Service','Facility') as payment_to, pay.payment_to as pay_to,invoice_required,is_packet_required,pay.payment_to as mib_payment_to");
	    $this->db->from($this->cfg['dbpref'].'payment_records as pay');
		$this->db->join($this->cfg['dbpref'] . 'record_type as rty', 'rty.rec_type_id = pay.rec_type_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'payment_status as pst', 'pst.payment_status_id = pay.payment_status_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'leads as l', 'l.lead_id = pay.lead_id', 'LEFT');
		$this->db->where("payment_id", $payment_id);
	    $result = $this->db->get();
	    $data = $result->row_array();
	    return $data;
	}
	
	function update_pay_records($data, $payment_id) 
	{
		$this->db->where('payment_id', $payment_id);
		$this->db->update($this->cfg['dbpref'] . 'payment_records',$data);
		$updated_status = $this->db->affected_rows();
		if($updated_status):
			return $payment_id;
		else:
			return false;
		endif;
	}
	
	function get_record_type_info($record_type) 
	{
		$this->db->select('rec_type_name');
	    $this->db->from($this->cfg['dbpref'].'record_type');
		$this->db->where("rec_type_id", $record_type);
	    $result = $this->db->get();
	    $data = $result->row_array();
	    return $data;
	}
	
	function get_call_status_info($call_status) 
	{
		$this->db->select('disposition_name,id,time_service_stop');
	    $this->db->from($this->cfg['dbpref'].'disposition');
		$this->db->where("id", $call_status);
	    $result = $this->db->get();
	    $data = $result->row_array();
	    return $data;
	}
	
function get_lead_status_info($lead_status) 
	{
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'lead_stage');
		$this->db->where("lead_stage_id", $lead_status);
	    $result = $this->db->get();
	    $data = $result->row_array();
	    return $data;
	}	
	public function get_cc_payment_results($filter=false)
	{
		// echo'<pre>';print_r($filter);exit;
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'payment_records pay');
		$this->db->join($this->cfg['dbpref'] . 'record_type as rt', 'rt.rec_type_id = pay.rec_type_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'leads as ld', 'ld.lead_id = pay.lead_id', 'LEFT');
		if(isset($filter['start_date']) && !empty($filter['start_date']) && empty($filter['end_date'])) {
			$dt_query =  'DATE(pay.completion_date) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'"';
			$this->db->where($dt_query);
		} else if(isset($filter['end_date']) && !empty($filter['end_date']) && empty($filter['start_date'])) {	
			$dt_query = 'DATE(pay.completion_date) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'"';
			$this->db->where($dt_query);
		} else if(isset($filter['start_date']) && !empty($filter['start_date']) && isset($filter['end_date']) && !empty($filter['end_date'])) {
			$dt_query = '(DATE(pay.completion_date) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'" AND DATE(pay.completion_date) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'")';
			$this->db->where($dt_query);
		}
		if(isset($filter['record_type']) && !empty($filter['record_type'])) {
			$this->db->where_in('pay.rec_type_id', $filter['record_type']);
		}
		if(isset($filter['payment_type']) && !empty($filter['payment_type'])) {
			$this->db->where_in('pay.payment_type', $filter['payment_type']);
		}
		$this->db->order_by('payment_id', 'desc');
	    $result = $this->db->get();
	    $data = $result->result_array();
	    return $data;
	}
	
	public function get_payment_record($filter=false,$type='',$cron='')
	{
		// echo'<pre>';print_r($filter);exit;
		$this->db->select('pay.*,od.is_retention_done,od.archive_done_at,cs.first_name as c_fname,ld.order_sub_no, cs.last_name as c_lname, cust.first_name, cust.last_name, ld.lead_id, ld.orderref, ld.order_no,ld.aps_order_no,ld.order_sub_no, ld.lead_title, ld.lead_title_last, cs.company as c_company,cust.company, cs.add1_line1 as c_add1_line1, cs.add1_line2 as c_add1_line2, cust.add1_line1, cust.add1_line2,cust.add1_location,cs.add1_location as c_add1_location,cust.add1_postcode,cs.add1_postcode as c_add1_postcode, s.state_name as c_state_name, s1.state_name as state_name,rt.rec_type_name,cc.cardnumber,pay.card_type,pay.payment_type,ld.company_type,ld.account_number,cof.office_account_number,cm.company_name,pay.invoice_number,pay.payment_to,pay.approval_code,copyservice.State,new_cs.Name as copy_service_name_new,new_cs.Address as copy_service_address_new,new_cs.City as copy_service_city_new,new_cs.State as state_new,cancel_reason.cancel_reason,u1.first_name as user_first_name,u1.last_name as user_last_name,ld.date_created,ld.confirmed_close_date,ccid.close_code,ccid.description as closecode_description,ld.orderref,ls.lead_stage_name');
		$this->db->select('GROUP_CONCAT(copyservice.Name SEPARATOR ",") as copy_service_name,GROUP_CONCAT(CONCAT(copyservice.Address," ",copyservice.City," ",copyservice.State) SEPARATOR ",") as copy_service_address',FALSE);
                $this->db->from($this->cfg['dbpref'].'payment_records as pay');
		$this->db->join($this->cfg['dbpref'] . 'record_type as rt', 'rt.rec_type_id = pay.rec_type_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'credit_card as cc', 'cc.id = pay.card_number', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'leads as ld', 'ld.lead_id = pay.lead_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'order_retention_details as od', 'od.order_id = ld.lead_id', 'LEFT');		
		$this->db->join($this->cfg['dbpref'].'users u','u.userid = ld.created_by','LEFT');
		$this->db->join($this->cfg['dbpref'] . 'customers as cust', 'cust.custid = pay.facility_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'custodian cs','cs.custid = pay.facility_id','LEFT');
		$this->db->join($this->cfg['dbpref'].'state s','cs.add1_state = s.stateid','LEFT');
		$this->db->join($this->cfg['dbpref'].'state s1','cust.add1_state = s1.stateid','LEFT');
		$this->db->join($this->cfg['dbpref'].'company as cm', 'cm.company_code = ld.company_code AND cm.company_type = ld.company_type', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'carrier_ordering_office as cof', 'cof.carrier_id = cm.company_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'copy_service_record as csr', 'ld.lead_id = csr.lead_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'copy_service as copyservice', 'copyservice.id = csr.copy_service_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'copy_service as new_cs', 'new_cs.id = pay.copy_service_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'users u1','u1.userid = pay.user_id','LEFT');
		$this->db->join($this->cfg['dbpref'].'close_codes ccid','ccid.close_code_id = ld.close_code_id','LEFT');
		
		$this->db->join($this->cfg['dbpref'].'payment_cancel_reason as cancel_reason', 'cancel_reason.id = pay.payment_cancel_id', 'LEFT');
		$this->db->join($this->cfg['dbpref'].'lead_stage as ls','ld.lead_stage = ls.lead_stage_id');
		// $this->db->join($this->cfg['dbpref'] . 'leads as ld', 'ld.lead_id = pay.lead_id', 'LEFT');
		if($cron != 'cron' && isset($filter['start_date']) && !empty($filter['start_date']) && empty($filter['end_date'])) {
			// $dt_query =  'DATE(pay.completion_date) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'"';
			$dt_query =  'DATE(pay.created_on) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'"';
			$this->db->where($dt_query);
		} else if($cron != 'cron' && isset($filter['end_date']) && !empty($filter['end_date']) && empty($filter['start_date'])) {
			// $dt_query = 'DATE(pay.completion_date) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'"';
			$dt_query = 'DATE(pay.created_on) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'"';
			$this->db->where($dt_query);
		} else if($cron != 'cron' && isset($filter['start_date']) && !empty($filter['start_date']) && isset($filter['end_date']) && !empty($filter['end_date'])) {
			// $dt_query = '(DATE(pay.completion_date) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'" AND DATE(pay.completion_date) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'")';
			$dt_query = '(DATE(pay.created_on) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'" AND DATE(pay.created_on) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'")';
			$this->db->where($dt_query);
		} else if(isset($filter['daily_start_date']) && !empty($filter['daily_start_date'])) {
			$dt_query = '(DATE(pay.created_on) = "'.date('Y-m-d', strtotime($filter['daily_start_date'])).'")';
			$this->db->where($dt_query);
		}
		
		if($cron == 'daily_cron')
		{
			# get all order's payment details
		}
		else if($cron != '')
		{
			$this->db->where('ld.lead_stage NOT IN (36,37)');
		}
		else{
			if(isset($filter['record_type']) && !empty($filter['record_type'])) {
				$this->db->where_in('pay.rec_type_id', $filter['record_type']);
			}
			if(isset($filter['payment_type']) && !empty($filter['payment_type'])) {
				$this->db->where_in('pay.payment_type', $filter['payment_type']);
			}
			if(isset($filter['card_type']) && !empty($filter['card_type']) && $filter['payment_type']==1) {
				$this->db->where_in('pay.card_type', $filter['card_type']);
			}

			if(isset($filter['card_number']) && !empty($filter['card_number']) && $filter['payment_type']==1) {
				$this->db->where_in('cc.id', $filter['card_number']);
			}
					
			if(!empty($filter['customer']) && $filter['customer'] != 'null')
			{
				$customer = explode(',', $filter['customer']);
				$this->db->where_in('ld.custid_fk',$customer);
			}  
			
			
			if(!empty($filter['leadassignee']) && $filter['leadassignee'] != 'null')
			{
				$leadassignee = explode(',', $filter['leadassignee']);
				$this->db->where_in('ld.lead_assign',$leadassignee);
			}
			
			
			if(!empty($filter['stage']) && $filter['stage'] != 'null')
			{
				$stage = explode(',', $filter['stage']);
				$this->db->where_in('ld.lead_stage',$stage);
			} 
			
			if(!empty($filter['statename']) && $filter['statename'] != '')
			{
				$this->db->where('(cs.add1_state = '.$filter['statename'].' OR cust.add1_state = '.$filter['statename'].')');
			}  
		
			if(!empty($filter['carrier_name']) && $filter['carrier_name'] != 'null' && $filter['carrier_name'] != 'Please Select' && $filter['carrier_name'] != 'Please Choose')
			{
				$this->db->where('cm.company_code',$filter['carrier_name']);
			}
			if(isset($filter['lead_company_type'])){
				if($filter['lead_company_type'] != 'null' && $filter['lead_company_type']!="")
				{
					if($filter['lead_company_type']=='all'){
						$this->db->where_in('ld.company_type',array(0,1));
					}else{
						$this->db->where_in('ld.company_type',$filter['lead_company_type']);
					}
				}
			}
			
			if($this->userdata['role_id'] != 1 && !empty($filter['owner']) && $filter['owner'] != 'null')
			{
				$owner = explode(',', $filter['owner']);
				$this->db->where_in('ld.created_by',$owner);
			}
			
			$order_handle_by_arr = array();
			// if($filter['order_handle_by']!="") {
			// $order_handle_by_arr = explode(',',$order_handle_by);
			// }
			$user_order_handle = array();
			$user_order_handle = explode(',',$this->userdata['order_handle_by']);
			$user_order_handle = array_diff($user_order_handle,array(NULL));
			
			// First Order handle by condition based on admin login, If not set advance filter order handle by apply 
			if($this->userdata['role_id'] != 1 && count($user_order_handle)>0 && sizeof($order_handle_by_arr) <= 0)
				$order_handle_by_arr = array();
			// if($options['order_handle_by']!="") {
			// $order_handle_by_arr = $options['order_handle_by'];
			// }
			$user_order_handle = array();
			$user_order_handle = explode(',',$this->userdata['order_handle_by']);
				// print_r($order_handle_by_arr);die;
			$user_order_handle = array_diff($user_order_handle,array(NULL));
			
			if(isset($company_type)){
				if($company_type!='' && $company_type!='null' && $company_type!='all') {
					$company_type = explode(',',$company_type);
					}
			}
			

			
			//  First Order handle by condition based on admin login, If not set advance filter order handle by apply 
			
			
			if($this->userdata['role_id'] != 1 && count($user_order_handle)>0)
			{
				if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle))
				{
					$keyword = '';
					if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null'){
						$this->db->where('ld.order_handle_by',$this->config->item('crm')['onshore_flag']);
					} 
				}
				else if($filter['order_handle_by'] != 'null' && !empty($filter['order_handle_by']) && count($filter['order_handle_by']) > 0)
				{
					$this->db->where_in('ld.order_handle_by',$filter['order_handle_by']);
				}
				else
				{ 
					$this->db->where_in('ld.order_handle_by',$user_order_handle);
				}
			}
			if(isset($lead_assign) && $this->userdata['role_id'] != 29 && $this->userdata['role_id'] != 1){
				if(in_array($this->config->item('crm')['onshore_flag'],$user_order_handle) && !in_array($this->config->item('crm')['offshore_flag'],$user_order_handle)) {
					if($keyword == '' || $keyword == 'Order No, Patient Name, Doctor or Facility or Custodian Name, Policy #, Phone Number' || $keyword == 'null') {
						$this->db->where('ld.lead_assign',$lead_assign);	
					}
				}
				else{
					$this->db->where('ld.lead_assign',$lead_assign);	
				}
			}
			
			if($this->userdata['role_id'] != 1 && empty($filter['lead_company_type']) && empty($filter['carrier_name']) && $this->userdata['company_list'] != NULL && $this->userdata['company_type'] != NULL){
				$user_company_code = explode(',',$this->userdata['company_list']);
				$user_company_type = explode(',',$this->userdata['company_type']);
				if($user_company_code != 'all' && $user_company_code != '' && $user_company_code != 'null' && sizeof($user_company_code)>0 && !in_array("all",$user_company_code))
				{
					$this->db->where_in('ld.company_code',$user_company_code);
				}
				if($user_company_type != '' && $user_company_type != 'null' && $user_company_type != 'all' && sizeof($user_company_type)>0)
				{
					$this->db->where_in('ld.company_type',$user_company_type);
				}
			}
		}
		$this->db->group_by('payment_id');
		$this->db->order_by('payment_id', 'desc');
	    $result = $this->db->get();
	    $data = $result->result_array();
	    // echo'<pre>';print_r($this->db->last_query());exit;
		
		
		$order_number=array();
		foreach($data as $each_data)
		{
			// $copy_service = sizeof(explode(',',$this->get_copy_service_records_multiple($each_data['lead_id'])[$each_data['lead_id']]));
			
				if($each_data['card_type']==0){
					$card_type = '';
				}
				elseif($each_data['card_type']==1){
					$card_type = 'Common';
				}elseif($each_data['card_type']==2){
					$card_type = 'External';
				}elseif($each_data['card_type']==3){
					$card_type = 'Internal';
				}else{
					$card_type = 'User Card';
				}

				$facFName = $facLName = $facAdd1 = $facAdd2 = $facomp = NULL;
				
				
				if($each_data['company_type'] == '1'){
					$facFName = $each_data['c_fname'];
					$facLName = $each_data['c_lname'];
					$facAdd1 = $each_data['c_add1_line1'];
					$facAdd2 = $each_data['c_add1_line2'];
					$facomp = $each_data['c_company'];
					$facLocation = $each_data['c_add1_location'];
					$facPstCode = $each_data['c_add1_postcode'];
				}else{
					$facFName = $each_data['first_name'];
					$facLName = $each_data['last_name'];
					$facAdd1 = $each_data['add1_line1'];
					$facAdd2 = $each_data['add1_line2'];
					$facomp = $each_data['company'];
					$facLocation = $each_data['add1_location'];
					$facPstCode = $each_data['add1_postcode'];
				}
				

					if($each_data['payment_type']==0){
						$payment_type_name = '';
					}
					elseif($each_data['payment_type']==1){
						$payment_type_name = 'Credit Card';
					}elseif($each_data['payment_type']==2){
						$payment_type_name = 'E-Check';
					}else{
						$payment_type_name = 'Traditional Check';
					}

				if($each_data['payment_type']==1){$rick_payments=''; $eNoah_payments=$each_data['paid_amount'];}
				else if($each_data['payment_type']==2){$rick_payments=''; $eNoah_payments=$each_data['paid_amount'];}
				else if($each_data['payment_type']==3){$rick_payments=$each_data['paid_amount']; $eNoah_payments='';}
				$eNoah_payments=$each_data['paid_amount'];

				if($each_data['payment_to']==1){
					$paymtTo = 'Copy Service';
				}else if($each_data['payment_to']==2){
					$paymtTo = 'Facility';
				}else if($each_data['payment_to']>=3){
					//KR
					//NOTE:: Digital vendor reference_number ID start with number 3
					//1 is for copy service, 2 for Facility, and above 3 is for a vendor
					//the reference_number of the vendor is vendor_id - 2
					$vendor_id = $each_data['payment_to'];
					$reference_number = (int)$vendor_id - 2;
					$vendor = $this->Digital_retrieval_vendor_model->getDigitalVendor($reference_number);
					$paymtTo = current($vendor)['digital_vendor_name'];
						}
				else{
					$paymtTo = NULL;
					}	
				
				// if(!empty($payment_to_arr))
				// 	$paymtTo = $this->cfg['payment_to'][$payment_to_arr[0]];
				// else
				// 	$paymtTo = NULL;

				if($type == 'excel')
				{
					$order_number[] = array(
					'lead_id' => $each_data['lead_id'],
					'order_no'=>$each_data['order_no'],
					'aps_order_no'=>$each_data['aps_order_no'],
					'order_sub_no'=>$each_data['order_sub_no'],
					'split_rec_type'=>$each_data['rec_type_id'],
					'split_description'=>$each_data['description'],
					'split_rick_payments'=>$rick_payments,
					'split_eNoah_payments'=>$eNoah_payments,
					'payment_id'=>$each_data['payment_id'],
					'user_id'=>$each_data['user_id'],
					'payment_status_id'=>$each_data['payment_status_id'],
					'payment_type'=>$each_data['payment_type'],
					'completion_date'=>$each_data['completion_date'],
					'created_on'=>$each_data['created_on'],
					'first_name'=>$facFName,
					'last_name'=>$facLName,
					'orderref'=>$each_data['orderref'],
					'lead_title'=>$each_data['lead_title'],
					'lead_title_last'=>$each_data['lead_title_last'],
					'company'=>$facomp,
					'add1_line1'=>$facAdd1,
					'add1_line2'=>$facAdd2,
					'add1_location' => $facLocation,
					'add1_postcode' => $facPstCode,
					'cardnumber'=>$each_data['cardnumber'],
					'card_type'=>$card_type,
					'payment_type_name'=>$payment_type_name,
					'rec_type_name'=>$each_data['rec_type_name'],
					'copy_service_name' => $each_data['copy_service_name'],
					'copy_service_address' => $each_data['copy_service_address'],
					'copy_service_name_new' => $each_data['copy_service_name_new'],
					'copy_service_address_new' => $each_data['copy_service_address_new'],
					'State' => $each_data['State'],
					'state_name' => $each_data['state_name'],
					'c_state_name' => $each_data['c_state_name'],
					'state_new' => $each_data['state_new'],
					'account_number' => $each_data['account_number'],
					'company_name' => $each_data['company_name'],
					'invoice_number' => $each_data['invoice_number'],
					'payment_to' => $paymtTo,
					'approval_code' => $each_data['approval_code'],
					'cancel_reason' => $each_data['cancel_reason'],
					'status' => $each_data['sign_amount_type'],
					'cheque_payable_name' => $each_data['cheque_payable_name'],
					'cheque_number' => $each_data['cheque_number'],
					'user_first_name' => $each_data['user_first_name'],
					'user_last_name' => $each_data['user_last_name'],
					'date_created' => $each_data['date_created'],
					'confirmed_close_date' => $each_data['confirmed_close_date'],
					'close_code' => $each_data['close_code'],
					'description' => $each_data['closecode_description'],
					'is_packet_required' => $each_data['is_packet_required'],
					'invoice_required' => $each_data['invoice_required'],
					'lead_stage_name' => $each_data['lead_stage_name'],
					'is_retention_done' => $each_data['is_retention_done'],
					'archive_done_at'=>$each_data['archive_done_at']
				);
			}else{
					$order_number[][$each_data['lead_id']] = array(
					'lead_id' => $each_data['lead_id'],
					'order_no'=>$each_data['order_no'],
					'aps_order_no'=>$each_data['aps_order_no'],
					'order_sub_no'=>$each_data['order_sub_no'],
					'split_rec_type'=>$each_data['rec_type_id'],
					'split_description'=>$each_data['description'],
					'split_rick_payments'=>$rick_payments,
					'split_eNoah_payments'=>$eNoah_payments,
					'payment_id'=>$each_data['payment_id'],
					'user_id'=>$each_data['user_id'],
					'payment_status_id'=>$each_data['payment_status_id'],
					'payment_type'=>$each_data['payment_type'],
					'completion_date'=>$each_data['completion_date'],
					'created_on'=>$each_data['created_on'],
					'first_name'=>$facFName,
					'last_name'=>$facLName,
					'orderref'=>$each_data['orderref'],
					'lead_title'=>$each_data['lead_title'],
					'lead_title_last'=>$each_data['lead_title_last'],
					'company'=>$facomp,
					'add1_line1'=>$facAdd1,
					'add1_line2'=>$facAdd2,
					'cardnumber'=>$each_data['cardnumber'],
					'card_type'=>$card_type,
					'payment_type_name'=>$payment_type_name,
					'rec_type_name'=>$each_data['rec_type_name'],
					'copy_service_name' => $each_data['copy_service_name'],
					'copy_service_address' => $each_data['copy_service_address'],
					'copy_service_name_new' => $each_data['copy_service_name_new'],
					'copy_service_address_new' => $each_data['copy_service_address_new'],
					'State' => $each_data['State'],
					'state_name' => $each_data['state_name'],
					'c_state_name' => $each_data['c_state_name'],
					'state_new' => $each_data['state_new'],
					'account_number' => $each_data['account_number'],
					'company_name' => $each_data['company_name'],
					'invoice_number' => $each_data['invoice_number'],
					'payment_to' => $paymtTo,
					'approval_code' => $each_data['approval_code'],
					'cancel_reason' => $each_data['cancel_reason'],
					'status' => $each_data['sign_amount_type'],
					'cheque_payable_name' => $each_data['cheque_payable_name'],
					'cheque_number' => $each_data['cheque_number'],
					'user_first_name' => $each_data['user_first_name'],
					'user_last_name' => $each_data['user_last_name'],
					'date_created' => $each_data['date_created'],
					'confirmed_close_date' => $each_data['confirmed_close_date'],
					'description' => $each_data['closecode_description'],
					'lead_stage_name' => $each_data['lead_stage_name'],
					'is_retention_done' => $each_data['is_retention_done'],
					'archive_done_at'=>$each_data['archive_done_at']
				);

			}
		} 
		// echo'<pre>'; print_r($order_number); exit;
	    return $order_number;
	}

	public function add_topup($data)
	{
		$this->db->insert($this->cfg['dbpref'].'topup', $data);
    	return $this->db->insert_id();
	}
	
	public function update_topup($data)
	{
		$this->db->where("topup_id", $data['topup_id']);
		return $this->db->update($this->cfg['dbpref'] . 'topup', $data);
	}

	public function get_total_topup_balance()
	{
		$this->db->select('amt');
	    $this->db->from('v_payment_trans');
		$this->db->where("trans_state", 1);
		// $this->db->order_by('id', 'desc');
	    $result = $this->db->get();//echo $this->db->last_query();
	    $credit_data = $result->result();//echo'<pre>';print_r($data);
		$credit_sum = 0;
		foreach($credit_data as $key=>$value){
		  if(isset($value->amt))
			 $credit_sum += $value->amt;
		}	
	    return $credit_sum;
	}
	
	public function get_total_debit_amount()
	{
		$this->db->select('amt');
	    $this->db->from('v_payment_trans');
		$this->db->where("trans_state", 0);
		$this->db->where("payment_type", 1);
		// $this->db->order_by('id', 'desc');
	    $result = $this->db->get();
	    $debit_data = $result->result();
		$debit_sum = 0;
		foreach($debit_data as $key=>$value){
		  if(isset($value->amt))
			 $debit_sum += $value->amt;
		}	
	    return $debit_sum;
	}
	
	public function get_topup_records_list()
	{
		$this->db->select('*');
	    $this->db->from('v_payment_trans paymt');
		$where_query = "(paymt.payment_type =  1 OR `paymt`.`payment_type` =  '')";
		$this->db->where($where_query);
	    $result = $this->db->get();
		// $ress = $this->db->last_query();
		// echo'<pre>';print_r($ress);exit;
	    $data = $result->result_array();
	    return $data;
	}
	
	public function get_topup_records()
	{
		$this->db->select('topup_id, payment_date, topup_amount, pay_description');
	    $this->db->from($this->cfg['dbpref'].'topup');
		// $this->db->order_by('topup_id', 'desc');
		// $this->db->limit(1);
	    $result = $this->db->get();
	    $data = $result->result_array();//echo'<pre>';print_r($result);exit;
	    return $data;
	}
	
	public function edit_topup_record($topup_id)
	{
		$this->db->select('topup_id, payment_date, topup_amount, pay_description');
	    $this->db->from($this->cfg['dbpref'].'topup');
		$this->db->where("topup_id",$topup_id);
	    $result = $this->db->get();
	    $data = $result->result_array();//echo'<pre>';print_r($result);exit;
	    return $data;
	}
	
	public function get_topup_results($filter=false)
	{
		// echo'<pre>';print_r($filter);exit;
		$this->db->select('*');
	    $this->db->from('v_payment_trans pay');
		// $this->db->join($this->cfg['dbpref'] . 'record_type as rt', 'rt.rec_type_id = pay.rec_type_id', 'LEFT');
		// $this->db->join($this->cfg['dbpref'] . 'leads as ld', 'ld.lead_id = pay.lead_id', 'LEFT');
		if(isset($filter['start_date']) && !empty($filter['start_date']) && empty($filter['end_date'])) {
			$dt_query =  'DATE(pay.trans_date) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'"';
			$this->db->where($dt_query);
		} else if(isset($filter['end_date']) && !empty($filter['end_date']) && empty($filter['start_date'])) {	
			$dt_query = 'DATE(pay.trans_date) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'"';
			$this->db->where($dt_query);
		} else if(isset($filter['start_date']) && !empty($filter['start_date']) && isset($filter['end_date']) && !empty($filter['end_date'])) {
			$dt_query = '(DATE(pay.trans_date) >= "'.date('Y-m-d', strtotime($filter['start_date'])).'" AND DATE(pay.trans_date) <= "'.date('Y-m-d', strtotime($filter['end_date'])).'")';
			$this->db->where($dt_query);
		}
		$where_query = "(pay.payment_type =  1 OR `pay`.`payment_type` =  '')";
		$this->db->where($where_query);
		// $this->db->order_by('payment_id', 'desc');
	    $result = $this->db->get();
		// $ress = $this->db->last_query();
		// echo'<pre>';print_r($ress);exit;
	    $data = $result->result_array();
	    return $data;
	}
	
	
	public function get_payment_all()
	{
		$this->db->select('pay.*, rt.rec_type_name');
	    $this->db->from($this->cfg['dbpref'].'payment_records pay');
		$this->db->join($this->cfg['dbpref'] . 'record_type as rt', 'rt.rec_type_id = pay.rec_type_id', 'LEFT');
		 
		 
		/* $this->db->select('rt.rec_type_name,ps.payment_status_name,pay.payment_type,pay.completion_date,pay.payment_id, pay.paid_amount');
	    $this->db->from($this->cfg['dbpref'].'payment_records pay');
	    $this->db->join($this->cfg['dbpref'] . 'payment_status as ps', 'ps.payment_status_id = pay.payment_status_id', 'LEFT'); */
	   
		
		
		// $this->db->where("payment_id", $payment_id);
	    $result = $this->db->get();
	    $data = $result->result_array();
	    return $data;
	}
	// Fetch all copy services
	public function get_all_copy_servies()
	{
		$this->db->select('*');
		$this->db->from('crm_copy_service');		
		$query = $this->db->get(); 
		return $query->result_array();
		
	}
	// Fetch all copy services
	public function get_copy_servies($id)
	{
		$this->db->select('*');
		$this->db->from('crm_copy_service');		
		$this->db->where("id",$id);		
		$query = $this->db->get(); 
		return $query->row_array();
		
	}
	// Fetch all copy services
	public function get_copy_service_records($lead_id)
	{
		$query = $this->db->query('SELECT lead_id,copy_service_id as copy_service_id,csr.Name,csr.Address,csr.City,csr.State FROM (`crm_copy_service_record` as cr) JOIN `crm_copy_service` csr ON `cr`.`copy_service_id` = `csr`.`id` WHERE lead_id = "'.$lead_id.'" ORDER BY `cr`.`id` DESC limit 1'); 
		
		$result_data =  $query->row_array();

		return $result_data;
	}

	// Fetch all copy services
	public function get_copy_service_records_multiple($lead_id="")
	{
		if($lead_id != '')
		{
			$query = $this->db->query('SELECT lead_id,GROUP_CONCAT(copy_service_id) as copy_service_id FROM (`crm_copy_service_record` as cr) JOIN `crm_copy_service` csr ON `cr`.`copy_service_id` = `csr`.`id` WHERE lead_id ='.$lead_id.' GROUP BY `cr`.`lead_id` having COUNT(1) > 1'); 
		}else{
			$query = $this->db->query('SELECT lead_id,GROUP_CONCAT(copy_service_id) as copy_service_id FROM (`crm_copy_service_record` as cr) JOIN `crm_copy_service` csr ON `cr`.`copy_service_id` = `csr`.`id` GROUP BY `cr`.`lead_id` having COUNT(1) > 1'); 
		}
		
		$result_data =  $query->result_array();
		$returnArr = array();
		
			foreach($result_data as $record)
			{
				$returnArr[$record['lead_id']] = $record['copy_service_id'];
			}
		return $returnArr;
	}

	// Fetch attachmnet types
	public function get_attachment_type()
	{
		$this->db->select('*');
		$this->db->from('crm_attachments_master as cr');
		$this->db->where('cr.status', '1');		
		$query = $this->db->get(); 
		return $query->result_array();
	}
	function get_records_lists($id) 
	{
		$this->db->select('r.*,ar.*');
		$this->db->from($this->cfg['dbpref'] . 'records as r');	
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as ar', 'ar.id = r.record_title','LEFT');	
		$this->db->where('r.jobid', $id);
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_payments($lead_id)
	{
		
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'payment_records as pay');
		$this->db->join($this->cfg['dbpref'] . 'record_type as rec', 'pay.rec_type_id = rec.rec_type_id', 'LEFT');
		$this->db->where("pay.lead_id", $lead_id);
		$this->db->order_by("pay.payment_id", "DESC");
	    $result = $this->db->get();
	    $data = $result->row_array();
	    return $data;
	}

	function coversheet_record_delete($condn) 
	{
		$this->db->where($condn);
		$this->db->delete($this->cfg['dbpref'] . 'coversheet_log');
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}	
	function coversheet_records($post_data) 
	{		
	
		$this->db->insert($this->cfg['dbpref'].'coversheet_log' , $post_data);
		
	}	
	
	public function get_coversheet_log_detail($lead_id)
	{
		$this->db->select('*');
		 $this->db->from($this->cfg['dbpref'].'coversheet_log as cl');		
		$this->db->where("cl.jobid",$lead_id);	
		$this->db->order_by("cl.id", "desc");	
		$query = $this->db->get(); 
		return $query->row_array();
		
	}
	// public function get_request_type()
	// {
		// $this->db->select('*');
		// $this->db->from('crm_prepare_request_type as cr');
		// $this->db->where('cr.status', '1');		
		// $query = $this->db->get(); 
		// return $query->result_array();

	// }
	
	
	function get_user_lead_stage($user_id)
	{
    	$this->db->select('lead_stage');
		$this->db->where('inactive', 0);
		$this->db->where('userid', $user_id);
    	$this->db->order_by('first_name', "asc");
		$q = $this->db->get($this->cfg['dbpref'] . 'users');
		return $q->result_array();
	}

	public function get_already_assigned_log($today,$current_time,$leadId){
		$hour = substr($current_time,0,2);
		$userdata = $this->session->userdata('logged_in_user');
		$this->db->select('*');
		$this->db->from($this->cfg['dbpref'] . 'logs as lg');
		$this->db->where('lg.jobid_fk', $leadId);
		$this->db->where('lg.userid_fk', $userdata['userid']);
		$this->db->where('DATE(lg.date_created)',$today);
		$this->db->where("log_content LIKE '%Order Assigned on Queue%'");
		$sql = $this->db->get();
		// echo $this->db->last_query(); echo '<br><br>';
		$res =  $sql->row_array();
		return $res;
	}
	
	//** Request Letter QR-Code Success Or Failure Log Start
	function requestLetter_QRcode_log($post_data) 
	{		
	
		$this->db->insert($this->cfg['dbpref'].'relqrcode_log' , $post_data);
		
	}
	//** Request Letter QR-Code Success Or Failure Log End

	public function get_user_leads($user_lead_stage,$today,$current_time,$company_type,$company_list,$orderhandle)
	{
		$hour = substr($current_time,0,2);
		$userdata = $this->session->userdata('logged_in_user'); 
		$lead_indicators = array('URGENT','NORMAL');
		if($userdata['co_workers'])
		{
			$lead_assign = array($userdata['userid'],$userdata['co_workers'],0);
		}else{
			$lead_assign = array($userdata['userid'],0);
		}
		
		// $check='DATEDIFF(j.proposal_expected_date,j.date_quoted)';
		// $this->db->select('j.lead_id,@order_age:=DATEDIFF(j.proposal_expected_date,j.date_quoted)');
		// $this->db->_protect_identifiers=FALSE;
		foreach($lead_indicators as $lead_indicator){
		foreach($lead_assign as $key =>$value){
		$this->db->select('j.lead_id');
		// $this->db->select('j.lead_id, j.invoice_no, j.lead_title, j.lead_title_last, j.orderref, j.lead_service, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to, j.created_by, j.expect_worth_amount, j.actual_worth_amount, j.expect_worth_id, j.division, j.lead_indicator, j.lead_status, j.lead_assign, j.proposal_expected_date, j.log_view_status, j.lead_hold_reason, j.assigned_to, j.internal_notes, j.patient_dob, j.ss_no, j.patient_adr, j.p_city, j.p_state, j.order_no, j.confirmed_close_date, j.time_frame, c.*, c.first_name AS cfn, c.last_name AS cln, c.add1_region, c.add1_country, c.add1_state, c.add1_location, rg.region_name, coun.country_name, st.state_name, c.add1_location as location_name, ass.first_name as assfname, ass.last_name as asslname, us.first_name as usfname, us.last_name as usslname, own.first_name as ownfname, own.last_name as ownlname, ls.lead_stage_name,ew.expect_worth_name, lsrc.lead_source_name, jbcat.services as lead_service, sadiv.division_name,(SELECT GROUP_CONCAT(lsh.dateofchange) FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id) as dateofchange,j.order_disposition_id,j.aps_order_no');
		// $this->db->select("CONCAT(COALESCE(j.order_no, ''), ' ', COALESCE(j.order_sub_no, '')) AS orderno", FALSE);
		$this->db->from($this->cfg['dbpref'] . 'leads as j',FALSE);
		$this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk', 'LEFT');	
		$this->db->join($this->cfg['dbpref'] . 'custodian as cs', 'cs.custid = j.custid_fk', 'LEFT');	
		// $this->db->join($this->cfg['dbpref'] . 'users as ass', 'ass.userid = j.lead_assign');
		// $this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by');
		// $this->db->join($this->cfg['dbpref'] . 'users as own', 'own.userid = j.belong_to');
		// $this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region');
		// $this->db->join($this->cfg['dbpref'] . 'country as coun', 'coun.countryid = c.add1_country');
		// $this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state');
		// $this->db->join($this->cfg['dbpref'] . 'location as loc', 'loc.locationid = c.add1_location');
		$this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage', 'LEFT');
		// $this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id');
		// $this->db->join($this->cfg['dbpref'] . 'lead_source as lsrc', 'lsrc.lead_source_id = j.lead_source', 'LEFT');
		// $this->db->join($this->cfg['dbpref'] . 'lead_services as jbcat', 'jbcat.sid = j.lead_service', 'LEFT');
		// $this->db->join($this->cfg['dbpref'] . 'sales_divisions as sadiv', 'sadiv.div_id = j.division', 'LEFT');
		$this->db->join($this->cfg['dbpref'] . 'timezone as tz', 'tz.timezone = c.timezone OR tz.timezone = cs.timezone', 'LEFT');
		// $this->db->join($this->cfg['dbpref'] . 'disposition as d', 'd.id = j.lead_stage', 'LEFT');
		if($user_lead_stage!=NULL){
			$this->db->where('j.lead_stage IN ('.$user_lead_stage.')');
		}
		
		// $this->db->where('DATE(j.proposal_expected_date)',$today);
		$this->db->where("(DATE(j.proposal_expected_date) <= '".$today."' AND DATE_FORMAT(j.proposal_expected_date,'%H:%i:%s') <= '".$current_time."')");
		$this->db->where('"'.$current_time.'" BETWEEN tz.start_time and tz.end_time',NULL,FALSE);
		$this->db->where('"'.$current_time.'" NOT BETWEEN tz.break_start_time and tz.break_end_time',NULL,FALSE);
		$this->db->where('j.pjt_status', 0);
		$this->db->where('j.lead_assign', $value);
		$this->db->where('j.assigned_to IS NULL');
		$this->db->where('j.lead_indicator', $lead_indicator);
		// $this->db->where("IF(j.flag_queue_restrict = 1, DATE_FORMAT(j.proposal_expected_date,'%:%i') LIKE '".$current_time."', j.flag_queue_restrict = 0)");
		
		// Condition for company type of respective user
		if(count($company_type)>0){
				$this->db->where_in('j.company_type', $company_type);
		}
                // Condition for company list of respective user
                if(count($company_list)>0){
                        $this->db->where_in('j.company_code', $company_list);
                }
                // Condition for order handle by of respective user
                if(count($orderhandle)>0){
                        $this->db->where_in('j.order_handle_by', $orderhandle);
                }

		// $this->db->_protect_identifiers=FALSE;
		$this->db->order_by('DATEDIFF(j.proposal_expected_date,j.date_quoted)','ASC',FALSE);
		// $this->db->_protect_identifiers=TRUE;
		// $this->db->order_by($check,FALSE);
		// $this->db->order_by('j.order_no','ASC');
		$this->db->order_by('tz.priority','ASC');
		$this->db->order_by('ls.priority','ASC');
		// $this->db->order_by('d.time_service_stop','DESC');
		
		$sql = $this->db->get();
		// echo $this->db->last_query(); echo '<br><br>';
		$res =  $sql->result_array();
		if($res)
		{
			return $res;
		}
	}
	}

		// else{
			// $this->db->select('j.lead_id');
			// $this->db->select('j.lead_id, j.invoice_no, j.lead_title, j.lead_title_last, j.orderref, j.lead_service, j.lead_source, j.lead_stage, j.date_created, j.date_modified, j.belong_to, j.created_by, j.expect_worth_amount, j.actual_worth_amount, j.expect_worth_id, j.division, j.lead_indicator, j.lead_status, j.lead_assign, j.proposal_expected_date, j.log_view_status, j.lead_hold_reason, j.assigned_to, j.internal_notes, j.patient_dob, j.ss_no, j.patient_adr, j.p_city, j.p_state, j.order_no, j.confirmed_close_date, j.time_frame, c.*, c.first_name AS cfn, c.last_name AS cln, c.add1_region, c.add1_country, c.add1_state, c.add1_location, rg.region_name, coun.country_name, st.state_name, c.add1_location as location_name, ass.first_name as assfname, ass.last_name as asslname, us.first_name as usfname, us.last_name as usslname, own.first_name as ownfname, own.last_name as ownlname, ls.lead_stage_name,ew.expect_worth_name, lsrc.lead_source_name, jbcat.services as lead_service, sadiv.division_name,(SELECT GROUP_CONCAT(lsh.dateofchange) FROM crm_lead_stage_history AS lsh WHERE lsh.lead_id=j.lead_id) as dateofchange,j.order_disposition_id,j.aps_order_no');
			// $this->db->select("CONCAT(COALESCE(j.order_no, ''), ' ', COALESCE(j.order_sub_no, '')) AS orderno", FALSE);
			// $this->db->from($this->cfg['dbpref'] . 'leads as j');
			// $this->db->join($this->cfg['dbpref'] . 'customers as c', 'c.custid = j.custid_fk');		
			// $this->db->join($this->cfg['dbpref'] . 'users as ass', 'ass.userid = j.lead_assign');
			// $this->db->join($this->cfg['dbpref'] . 'users as us', 'us.userid = j.modified_by');
			// $this->db->join($this->cfg['dbpref'] . 'users as own', 'own.userid = j.belong_to');
			// $this->db->join($this->cfg['dbpref'] . 'region as rg', 'rg.regionid = c.add1_region');
			// $this->db->join($this->cfg['dbpref'] . 'country as coun', 'coun.countryid = c.add1_country');
			// $this->db->join($this->cfg['dbpref'] . 'state as st', 'st.stateid = c.add1_state');
			// $this->db->join($this->cfg['dbpref'] . 'location as loc', 'loc.locationid = c.add1_location');
			// $this->db->join($this->cfg['dbpref'] . 'lead_stage as ls', 'ls.lead_stage_id = j.lead_stage');
			// $this->db->join($this->cfg['dbpref'] . 'expect_worth as ew', 'ew.expect_worth_id = j.expect_worth_id');
			// $this->db->join($this->cfg['dbpref'] . 'lead_source as lsrc', 'lsrc.lead_source_id = j.lead_source', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'lead_services as jbcat', 'jbcat.sid = j.lead_service', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'sales_divisions as sadiv', 'sadiv.div_id = j.division', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'timezone as tz', 'tz.timezone = j.timezone', 'LEFT');
			// $this->db->join($this->cfg['dbpref'] . 'disposition as d', 'd.id = j.lead_stage', 'LEFT');
			// $this->db->where('j.lead_stage IN ('.$user_lead_stage.')');
			// $this->db->where('DATE(j.proposal_expected_date)',$today);
			// $this->db->where('"'.$current_time.'" BETWEEN tz.start_time and tz.end_time',NULL,FALSE);
			// $this->db->where('"'.$current_time.'" NOT BETWEEN tz.break_start_time and tz.break_end_time',NULL,FALSE);
			// $this->db->where('j.pjt_status', 0);
			// $this->db->where('j.lead_assign = 0 OR j.lead_assign ='.$userdata['userid']);
			// $this->db->where('j.assigned_to IS NULL');
			// $this->db->order_by('d.time_service_stop','DESC');
			// $this->db->_protect_identifiers=FALSE;
			// $this->db->order_by('DATEDIFF(j.proposal_expected_date,j.date_quoted)','ASC');
			// $this->db->_protect_identifiers=TRUE;
			// $this->db->order_by('d.time_service_stop','DESC');
		
			// $sql = $this->db->get();
			// echo $this->db->last_query(); exit;
			// $res =  $sql->result_array();
			// return $res;	
		// }
		
	}	
		
		public function get_calculated_ts($lead_id) {
		$this->db->select('l.confirmed_close_date,l.lead_stage,l.date_modified');
		$this->db->from($this->cfg['dbpref'].'leads as l');	
		$this->db->where("l.lead_id",$lead_id);
		// $this->db->order_by("l.lead_id", "desc");
		// $this->db->limit(1);	
		$query = $this->db->get(); 
		return $query->row_array();
		
	}


	public function get_cards_list_by_cardtype($type)
	{
		$this->db->select('cardnumber, id');

		$this->db->where('type', $type);
	    $this->db->from($this->cfg['dbpref'].'credit_card', $condn);
		// $this->db->order_by('topup_id', 'desc');
		// $this->db->limit(1);
	    $result = $this->db->get();
	    $data = $result->result_array();//echo'<pre>';print_r($result);exit;
	    return $data;
	}

	public function get_list_usercard($uid,$ucard_type)
	{
		$this->db->select('card.cardnumber,card.id');
		$this->db->from($this->cfg['dbpref'].'credit_card as card');
		$this->db->join($this->cfg['dbpref'] . 'user_creditcard_mapping as card_m', 'card.id = card_m.cardid', 'LEFT');
		$this->db->where('userid', $uid);
		$this->db->where('card_type', $ucard_type);
		$this->db->where('status', 1);
		// $this->db->order_by('topup_id', 'desc');
		// $this->db->limit(1);
	    $result = $this->db->get();
	    $data = $result->result_array();//echo'<pre>';print_r($result);exit;
	    return $data;
	}

	public function get_all_credit_cards($card_type="")
	{
		$this->db->select('cardnumber,id');
		$this->db->from($this->cfg['dbpref'].'credit_card');
		$this->db->where('status', 1);
		if($card_type != ''){
			$this->db->where('type', $card_type);
		}
		// $this->db->order_by('topup_id', 'desc');
		// $this->db->limit(1);
	    $result = $this->db->get();
	    $data = $result->result_array();//echo'<pre>';print_r($result);exit;
	    return $data;
	}

    public function get_record_detail($record_id)
	{
		$this->db->from($this->cfg['dbpref'].'records');
		$this->db->where('id',$record_id);
		$query = $this->db->get();
		$query = $query->row_array();
		return $query;
	}

	public function lead_stage_removed() {
	    $this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'lead_stage');
	    // $this->db->limit(13);
		$this->db->where("status",1);
		$this->db->where("is_deleted",0);
		$this->db->where_not_in("lead_stage_id",$this->cfg['aps_summarization_status']);
		$this->db->where_not_in("lead_stage_id",$this->cfg['aps_summarization_completed_status']);
		//$this->db->order_by("sequence", "asc");
		$this->db->order_by("lead_stage_name", "asc");
	    $lead_stage = $this->db->get();
	    $leads = $lead_stage->result_array();
	    return $leads;
	}
	/**
	* Get timezone by state
	* 
	* @params state string required
	* @response timezone
	*/
	public function get_state_timezone($state)
	{
		$this->db->select('timezone');
		$this->db->from($this->cfg['dbpref'].'state');	
		$this->db->where("stateid",$state);
		$query = $this->db->get(); 
		$timezone = "";
		if($query->num_rows() > 0)
		{
			$res = $query->row();
			$timezone = $res->timezone;
		}
		return $timezone;
	}
	
	function get_cust_data_by_id($cid) {
		$this->db->where('custid', $cid);
		$client = $this->db->get($this->cfg['dbpref'] . 'custodian');
		return $client->result_array();
	}
	
	/**
	* Check Customer Is Exist
	* 
	* @params  type string
	* @params  data array
	* @response num rows
	*/
	public function checkIsExistCustomer($type,$customerData = array())
	{
		$first_name = $customerData['first_name'];
		$last_name = $customerData['last_name'];
		$company = $customerData['company'];
		$add1_line1 = $customerData['add1_line1'];
		$add1_location = $customerData['add1_location'];
		$add1_state = $customerData['add1_state'];
		$add1_postcode = $customerData['add1_postcode'];
		$phone_1 = $customerData['phone_1'];
		$phone_4 = $customerData['phone_4'];
		
		$this->db->select('custid');
		$this->db->from($this->cfg['dbpref'].$type);	
		$this->db->where("first_name = '".$first_name."' AND last_name = '".$last_name."' AND company = '".$company."' AND add1_line1 = '".$add1_line1."' AND add1_location = '".$add1_location."' AND add1_state = '".$add1_state."' AND add1_postcode = '".$add1_postcode."' AND phone_1 = '".$phone_1."' AND phone_4 = '".$phone_4."'");
		$query = $this->db->get(); 
		 // echo $this->db->last_query();exit;
		return $query->num_rows();
	}	

	function get_result_pending_record_lists($id,$record_id) 
	{
		$this->db->select('r.*,ar.*,r.id as rec_id');
		$this->db->from($this->cfg['dbpref'] . 'records as r');	
		$this->db->join($this->cfg['dbpref'] . 'attachments_master as ar', 'ar.id = r.record_title');	
		$this->db->where('r.jobid', $id);
		$this->db->where('r.id',$record_id);
		$this->db->where('ar.id',13);//result_pending documents
		$this->db->order_by("r.id", "DESC");
		$query = $this->db->get();
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
                }
		else
		{
			return FALSE;
		}
	}
/**
	* Delete Log Content 
	* @params logid string
	* @return boolean false/true
	*/
	public function delete_log_content($logid)
	{
		$this->db->where('logid',$logid);
		$this->db->delete($this->cfg['dbpref'] . 'logs');
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	/**
	* Get Payment Based Log Content 
	* @params payment string
	* @return boolean false/true
	*/
	public function get_log_content_id($paymentid)
	{
		$this->db->select('logid');
		$this->db->where('payment_id',$paymentid);
		$data = $this->db->get($this->cfg['dbpref'] . 'logs')->result_array();
		
		return ($data[0]['logid']) ? $data[0]['logid'] : 0;
	}
	/**
	* Delete Payment Content 
	* @params payment id string , lead id string
	* @return boolean false/true
	*/
	public function delete_payment_record($payment_id,$lead_id)
	{
		$this->db->where('payment_id',$payment_id);
		$this->db->where('lead_id',$lead_id);
		$this->db->delete($this->cfg['dbpref'] . 'payment_records');
		
		if($this->db->affected_rows() == TRUE)
		{
			$this->db->where('payment_id',$payment_id);
			$this->db->where('jobid_fk',$lead_id);
			$this->db->delete($this->cfg['dbpref'] . 'logs');
			return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		}
		return FALSE;
	}
        
        // Calculate Special Auth TAT Value
	public function get_log_details($id) {
		$this->db->order_by('date_created', 'asc');
		$this->db->order_by('logid', 'asc');
		$query = $this->db->get_where($this->cfg['dbpref'] . 'logs', array('jobid_fk' => $id));
		// echo $this->db->last_query();die;
		return $query->result_array();
	}
		
	public function get_log_sequence($id) {
		$this->db->order_by('logid', 'desc');
		$query = $this->db->get_where($this->cfg['dbpref'] . 'logs', array('jobid_fk' => $id,'sequence'=>1));
		return $query->result_array();
	}

	public function update_row_log($tbl, $updt, $jid) {
		$this->db->where('logid', $jid);
		$this->db->update($this->cfg['dbpref'] . $tbl, $updt);
		// return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
		return true;
	}

	public function update_new_stage_log($tbl,$updt,$dispId){
		// $this->db->select('d.id');
		// $this->db->from($this->cfg['dbpref'].'disposition d');
		// $this->db->where("d.is_special_auth", 1);
		// $disp = $this->db->get();
		// $dispId = $disp->result_array();
		// $arr_disp=array();
		// foreach($dispId as $vl){
		// 	array_push($arr_disp,$vl['id']);
		// }

		$query = $this->db->query("update crm_logs set new_stage = lead_stage where new_stage = 0 and sequence_flag = 0");

		$ldstgid = array(36,37);
		$this->db->where_in('disposition_id', $dispId);
		$this->db->where_not_in('lead_stage', $ldstgid);
		$this->db->where('sequence_flag',0);
		$this->db->update($this->cfg['dbpref'] . $tbl, $updt);
		// echo $this->db->last_query(); die;
		return true;
	}
	
	/* Function to get special auth required */
	public function get_special_auth_case($id) {
		$this->db->select('d.id');
		$this->db->from($this->cfg['dbpref'].'disposition d');
		$this->db->where("d.is_special_auth", 1);
		$disp = $this->db->get();
		$dispId = $disp->result_array();
		$arr_disp=array();
		foreach($dispId as $vl){
			array_push($arr_disp,$vl['id']);
		}
		
		$result=array();
		$this->db->select('l.disposition_id,l.lead_stage,l.new_stage,dp.disposition_name');
		$this->db->from($this->cfg['dbpref'].'logs l');
		$this->db->join($this->cfg['dbpref'] . 'disposition as dp', 'dp.id = l.disposition_id','LEFT');
		$this->db->where("l.jobid_fk", $id);
		if(count($arr_disp)>0){
			$this->db->where_in("l.disposition_id", $arr_disp);	
		}
		$this->db->order_by("l.logid","desc");
		$this->db->limit('1');
		$lead_stg_his = $this->db->get();
		// echo $this->db->last_query();echo '<br>';
		$result['flag'] = $lead_stg_his->num_rows();
		$getrst = $lead_stg_his->result_array();
		if(count($getrst)>0){
			$result['disposition_name'] = $getrst[0]['disposition_name'];
		}else{
			$result['disposition_name'] = NULL;
		}
		
		// $flag=0; $last_diposition=0;$count = 0;$dispId = NULL; 
		// foreach($getrst as $ky => $val){
		// 	if($last_diposition!=$val['disposition_id'] && $count>0){
		// 		if(in_array($val['disposition_id'],$arr_disp)){
		// 			$flag++;
		// 			$dispId = $val['disposition_id'];	
		// 		}			
		// 	}
		// 	$last_diposition = $val['disposition_id'];
		// 	$count++;
		// }
		
		// print_r($result);
		return $result;
	}

	public function get_special_auth_required_service($id){
		$this->db->select('l.lead_stage');
		$this->db->from($this->cfg['dbpref'].'leads l');
		$this->db->where("lead_id", $id);
		$this->db->where("order_disposition_id IN (select id from crm_disposition where is_special_auth = 1)");
		$subQuery =  $this->db->get();

		// $this->db->select('l.lead_stage,l.new_stage');
		// $this->db->from($this->cfg['dbpref'].'logs l');
		// $this->db->join($this->cfg['dbpref'] . 'disposition as dp', 'dp.id = l.disposition_id', 'LEFT');
		// $this->db->where("jobid_fk", $id);
		// $this->db->where("dp.is_special_auth", 1);
		// // $this->db->where("disposition_id IN (select id from crm_disposition where is_special_auth = 1)");
    	// $subQuery =  $this->db->get();
	 	// echo $this->db->last_query(); echo '<br>'; die;
	 	return $subQuery->num_rows();
	}

	public function get_all_leads(){
		$this->db->distinct();
		$this->db->select('jobid_fk');
	    $this->db->from($this->cfg['dbpref'] . 'logs');
		$this->db->where('sequence_flag',0);
	    $lead_history = $this->db->get();
		// echo $this->db->last_query();die;
	    return $leads =  $lead_history->result_array();
	}

	public function get_special_auth_dates($id){
		$ldstg = array(1111);
		$this->db->select('*');
	    $this->db->from('v_special_tat');
		$this->db->where('jobid_fk',$id);
		$this->db->where_in('lead_stage',$ldstg);
		$lead_history = $this->db->get();
		return $leads =  $lead_history->result_array();
	}
	
	public function get_company_list($company_list = array(),$company_type = ''){
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] .'company');
		
		if(is_countable($company_list) && sizeof($company_list) > 0 && $company_list != 'all')
		{
			$company_list = explode(',',$company_list);
			$this->db->where_in('company_code',$company_list);
		}
		if($company_type != '' && $company_type != 'all')
		{
			$this->db->where('company_type',$company_type);
		}
		$company_list = $this->db->get();
		//echo $this->db->last_query();exit;
		return $leads =  $company_list->result_array();
	}

	public function get_leads() 
	{
	    $this->db->select('lead_id,patient_dob,ss_no');
	    $this->db->from($this->cfg['dbpref'] . 'leads');
	    $lead_history = $this->db->get();
	    return $leads =  $lead_history->result_array();
	}
        
	public function get_three_tat($id) 
	{
		$this->db->select('rcs.real_tat as completed_age,rcs.calc_tat as order_age,rcs.stop_tat as stop_time');
		$this->db->from($this->cfg['dbpref'].'real_calc_stop_tat as rcs');	
		$this->db->where('rcs.lead_id', $id);
		$tat_list = $this->db->get();
		// echo $this->db->last_query();exit;
		return $tat_list->result_array();
	}
    	/**
	* Set inactive for all old facility copy service 
	* 
	* @params $customer_id int
	* @return (true/false) boolean 
	*/
	public function update_facility_copy_service_inactive($customer_id,$facility_type = '') 
	{
	    $this->db->where('facility_id', $customer_id);
		$this->db->where('status', 1);
		$this->db->where('facility_type',$facility_type);
		$this->db->update($this->cfg['dbpref'].'facility_copy_service_log', array('status'=>0,'updated_on'=>date('Y-m-d H:i:s'),'updated_by'=>$this->userdata['userid']));
		return true;
	}
	/**
	* Set inactive for all order facility 
	* 
	* @params $order_id int
	* @return (true/false) boolean 
	*/
	public function update_facility_order_inactive($order_id) 
	{
	    $this->db->where('order_id', $order_id);
	    $this->db->where('status', 1);
		$this->db->update($this->cfg['dbpref'].'facility_order_log', array('status'=>0,'updated_on'=>date('Y-m-d H:i:s'),'updated_by'=>$this->userdata['userid']));
		
		return true;
	}
	/**
	* Get facility mapped copy service list for history & active copy service 
	* 
	* @params $order_id int
	* @params $status int
	* @return $list array
	*/
	public function get_facility_copy_service_details($order_id,$status = '',$company_type = '') 
	{
	    $this->db->select('c.custid,c.first_name as first_name_c,c.last_name as last_name_c,c.company,c.add1_line1,c.add1_location,s.state_name,fcl.facility_id,fcl.copy_service_id,cs.Name,cs.State,cs.Address,cs.City,cs.Zip,cs.Phone,fcl.created_on,u.first_name,u.last_name,us.first_name as user_first_name,us.last_name as user_last_name,fcl.updated_on,cs.id as service_record_id,cs.*,ls.lead_id');
	    $this->db->from($this->cfg['dbpref'].'facility_order_log as fol');
		$this->db->join($this->cfg['dbpref'].'facility_copy_service_log as fcl','fcl.facility_id = fol.facility_id','left');
		$this->db->join($this->cfg['dbpref'].'copy_service as cs','cs.id = fcl.copy_service_id');
		$this->db->join($this->cfg['dbpref'].'users as u','u.userid = fcl.user_id','left');
		$this->db->join($this->cfg['dbpref'].'users as us','us.userid = fcl.updated_by','left');
		$this->db->join($this->cfg['dbpref'].'leads as ls','ls.lead_id = fol.order_id','left');
		
		if($company_type == 0)
		{
			$this->db->join($this->cfg['dbpref'].'customers as c','c.custid = fcl.facility_id','LEFT');
			$this->db->where('fcl.facility_type',0);
		}else{
			$this->db->join($this->cfg['dbpref'].'custodian as c','c.custid = fcl.facility_id','LEFT');
			$this->db->where('fcl.facility_type',1);
		}
		$this->db->join($this->cfg['dbpref'].'state as s','c.add1_state = s.stateid','left');
		$this->db->where('fcl.facility_type',$company_type);
		$this->db->where('fol.order_id',$order_id);
		if($status != '')
		{
			$this->db->where('fcl.status',1);
			$this->db->where('fol.status',1);
		}
		$this->db->order_by('fol.created_on,fcl.created_on','DESC');
		
	    $lead_history = $this->db->get();
		
	    return $leads =  $lead_history->result_array();
	}
	/**
	* Get facility mapped copy service list based on customer / custodian
	* 
	* @params $customer_id int
	* @params $type string
	* @return $status int 
	*/
	public function get_mapped_copy_service($customer_id,$type,$status = '')
	{
		$this->db->select('fl.copy_service_id,cs.Name,cs.Address,cs.City,cs.State,cs.Phone,cs.Zip,cs.Fax,fl.created_on,u.first_name,u.last_name,us.first_name as user_first_name,us.last_name as user_last_name,fl.updated_on,fl.facility_id,fl.status');
	    $this->db->from($this->cfg['dbpref'].'facility_copy_service_log as fl');
		$this->db->join($this->cfg['dbpref'].'copy_service as cs','fl.copy_service_id = cs.id');
		if($type == 0)
		{
			$this->db->join($this->cfg['dbpref'].'customers as c','c.custid = fl.facility_id','LEFT');
			$this->db->where('fl.facility_type',0);
		}else{
			$this->db->join($this->cfg['dbpref'].'custodian as c','c.custid = fl.facility_id','LEFT');
			$this->db->where('fl.facility_type',1);
		}
		$this->db->join($this->cfg['dbpref'].'users as u','u.userid = fl.user_id','left');
		$this->db->join($this->cfg['dbpref'].'users as us','us.userid = fl.updated_by','left');
		$this->db->where('fl.facility_id',$customer_id);
		
		if($status != '')
		{
			$this->db->where('fl.status',$status);
		}
		$this->db->order_by('fl.created_on DESC');
	    $history = $this->db->get();
		//echo $this->db->last_query();exit;
	    return $history->result_array();
	}
	/**
	* Get facility mapped copy service list based on customer / custodian
	* 
	* @params $customer_id int
	* @params $type string
	* @return $status int 
	*/
	public function get_log_copy_service($customer_id,$type,$status = '')
	{
		$this->db->select('cs.Name,fl.*,u.first_name,u.last_name');
		$this->db->select('DATE_FORMAT(fl.created_on,"%W, %D %M %y %h:%i %p") as created_on_format',false);
	    $this->db->from($this->cfg['dbpref'].'facility_copy_service_process_log as fl');
		if($type == 0)
		{
			$this->db->join($this->cfg['dbpref'].'customers as c','c.custid = fl.facility_id','LEFT');
			$this->db->where('fl.facility_type',0);
		}else{
			$this->db->join($this->cfg['dbpref'].'custodian as c','c.custid = fl.facility_id','LEFT');
			$this->db->where('fl.facility_type',1);
		}
		$this->db->join($this->cfg['dbpref'].'copy_service as cs','fl.copy_service_id = cs.id');
		$this->db->join($this->cfg['dbpref'].'users as u','u.userid = fl.user_id','left');
		$this->db->where('fl.facility_id',$customer_id);
		
		if($status != '')
		{
			$this->db->where('fl.status',$status);
		}
		$this->db->order_by('fl.logid DESC');
		$this->db->order_by('fl.created_on DESC');
	    $history = $this->db->get();
		
	    return $history->result_array();
	}
	/**
	* Get facility list based on order id for history
	* 
	* @params $order_id int
	* @params $company_type int
	* @return $list array 
	*/
	public function get_facility_history($order_id,$company_type,$status = '')
	{
		$this->db->select('c.custid,c.first_name,c.last_name,c.company,c.add1_line1,c.add1_location,s.state_name,c.phone_1,fl.created_on,fl.user_id,u.first_name as user_first_name,u.last_name as user_last_name,fl.updated_on,us.first_name as updated_first_name,us.last_name as updated_last_name,or.requestor_id,or.requestor_name,fl.facility_id,ld.ordered_by');
		$this->db->from($this->cfg['dbpref'].'facility_order_log fl');
		if($company_type == 0)
		{
			$this->db->join($this->cfg['dbpref'].'customers as c','fl.facility_id = c.custid','left');
		}else{
			$this->db->join($this->cfg['dbpref'].'custodian as c','fl.facility_id = c.custid','left');
		}
		$this->db->join($this->cfg['dbpref'].'state as s','c.add1_state = s.stateid','left');
		$this->db->join($this->cfg['dbpref'].'users as u','u.userid = fl.user_id','left');
		$this->db->join($this->cfg['dbpref'].'users as us','us.userid = fl.updated_by','left');
		$this->db->join($this->cfg['dbpref'].'leads as ld','ld.lead_id = fl.order_id','left');
		$this->db->join($this->cfg['dbpref'].'order_requestor as or','or.requestor_id = ld.requestor_id','left');
		
		if($status != '')
		{
			$this->db->where("fl.status", 1); 
		}
		$this->db->where("fl.order_id", $order_id);
		$this->db->order_by('fl.created_on DESC');
	    $history = $this->db->get();
		// echo $this->db->last_query();exit;
	    return $history->result_array();
	}
	public function get_active_facility_copyService($custid,$company_type){
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'].'facility_copy_service_log');
		$this->db->where('facility_id',$custid);
		$this->db->where('facility_type',$company_type);
		$this->db->where('status',1);
		$result = $this->db->get();
	    return $result->num_rows();
	}
	public function cancel_payment($data,$cancelReasonId){
		$dta=array();
		$dta['lead_id'] = $data['lead_id'];
		$dta['user_id'] = $data['user_id'];
		$dta['rec_type_id'] = $data['rec_type_id'];
		$dta['payment_status_id'] = $data['payment_status_id'];
		$dta['payment_type'] = $data['payment_type'];
		$dta['card_type'] = $data['card_type'];
		$dta['user_card_type'] = $data['user_card_type'];
		$dta['card_number'] = $data['card_number'];
		$dta['paid_amount'] = $data['paid_amount'];
		$dta['sign_amount_type'] = 1;
		$dta['ref_payment_id'] = $data['payment_id'];
		$dta['completion_date'] = $data['completion_date'];
		$dta['description'] = $data['description'];
		$dta['created_on'] = date('Y-m-d');
		$dta['cheque_payable_name'] = $data['cheque_payable_name'];
		$dta['cheque_number'] = $data['cheque_number'];
		$dta['approval_code'] = $data['approval_code'];
		$dta['invoice_number'] = $data['invoice_number'];
		$dta['payment_to'] = $data['pay_to'];
		$dta['status'] = $data['status'];
		$dta['facility_id'] = $data['facility_id'];
		$dta['copy_service_id'] = $data['copy_service_id'];
		$dta['is_packet_required'] = $data['is_packet_required'];
		$dta['invoice_required'] = $data['invoice_required'];
		$dta['payment_cancel_id'] = $cancelReasonId;
		$dta['digital_vendor_id'] = $data['digital_vendor_id'];
		// echo '<br>';print_r($dta); die;
		if($this->db->insert($this->cfg['dbpref'] . 'payment_records', $dta)){
			$insert_id = $this->db->insert_id();
			return $insert_id;
		}else{
			return false;
		}
	}
	
	/**
	* Update copy service 
	*/
	public function update_copy_service($customer_id,$copy_service_id)
	{
		$query = $this->db->query('SELECT l.lead_id,csr.copy_service_id as copy_service_id FROM (`crm_leads` as l) JOIN `crm_copy_service_record` csr ON `l`.`lead_id` = `csr`.`lead_id` WHERE l.custid_fk = "'.$customer_id.'" AND DATE(l.date_created) > '.$this->cfg['copy_service_date']); 
		
		if($query->num_rows() > 0)
		{
			return $sql = $this->db->query("UPDATE ".$this->cfg['dbpref']."copy_service_record as csr INNER JOIN ".$this->cfg['dbpref']."leads as ls ON csr.lead_id = ls.lead_id SET copy_service_id=".$copy_service_id);
		}
		else{
			$lead_arr = '';
			foreach($query->result_array() as $record)
			{
				$lead_arr =','.$record['lead_id'];
			}	
			echo $lead_arr = preg_replace('//', ',', $lead_arr, 1);exit;
			
			$data['lead_id'] = $record['lead_id'];
			$data['copy_service_id'] = $record['copy_service_id'];
			
			return $this->db->insert($this->cfg['dbpref'].'copy_service_record',$data);
			
		}
	}
	/**
	* Lead Files Results Pending / Results Count
	* @params $lead_id int
	* @return count
	*/
	public function get_record_count($lead_id)
	{
		$this->db->select('*');
	    $this->db->from($this->cfg['dbpref'] . 'records');
		$this->db->where('jobid',$lead_id);
		$this->db->where_in('record_title',array(13,17));
	    $lead_history = $this->db->get();
	    return $leads =  $lead_history->num_rows();
	}

	public function remove_flag_restrict_queue($lead_id){
		$this->db->where('lead_id', $lead_id);
		$this->db->update($this->cfg['dbpref'].'leads', array('flag_queue_restrict'=>0,'proposal_expected_date'=>date('Y-m-d')));
		return true;
	}
	
	public function get_upload_file_size($lead_id)
	{
		$this->db->select('c.company_code,c.company_type,c.upload_file_size,l.lead_id');
		$this->db->from($this->cfg['dbpref'].'leads as l');
		$this->db->join($this->cfg['dbpref'].'company as c','l.company_type = c.company_type AND l.company_code = c.company_code','left');
		$this->db->where('l.lead_id',$lead_id);
		$company_details = $this->db->get();
		return $company_details->result_array();
	}

	public function check_existLeadID($leadID){
		$this->db->select('*');
		$this->db->from('orderage');
		$this->db->where('lead_id',$leadID);
		$company_details = $this->db->get();
		return $company_details->row_array();
        }

	public function get_carrier_manager($carrmangId){
		$this->db->select('cm.*');
		$this->db->from($this->cfg['dbpref']."carrier_manager as cm");
		$this->db->where('carrier_manager_id',$carrmangId);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	
	public function order_update_data($order_data,$order_id)
	{
		# history table entry
		$this->db->insert($this->cfg['dbpref'].'customer_service_fee',$order_data);
		
		# update order flag
		$this->db->where('lead_id', $order_id);
		$this->db->update($this->cfg['dbpref'] . 'leads', array('customer_service_fee' => $order_data['customer_service_fee']));
		return true;
	}
	
	public function get_customer_service_records_list($order_id)
	{
		$this->db->select('cm.*,ur.first_name,ur.last_name');
		$this->db->from($this->cfg['dbpref']."customer_service_fee as cm");
		$this->db->join($this->cfg['dbpref'].'users as ur','ur.userid = cm.added_by','left');
		$this->db->where('order_id',$order_id);
		$this->db->order_by("cm.created_on", "DESC");
		$query = $this->db->get();
		
		$result = $query->result_array();
		return $result;
	}
        
        public function get_calculate_tat_value($id='')
	{
           $this->db->select('lead_id,real_tat,calc_tat,stop_tat');
		   $this->db->from($this->cfg['dbpref'].'real_calc_stop_tat');
		   $this->db->where('lead_id',$id);
           $query=$this->db->get();
		   $data=$query->result_array();
		   return $data;
        }
        public function get_primerica_ehr_orders()
        {
                $this->db->select("*");
                $this->db->from("v_primerica_reopened_ehr_order");
                $query=$this->db->get();
                if ($query !== FALSE && $query->num_rows() > 0) {
                        $data=$query->result_array();
                } else {		
                        $data = array();
                }
                return $data;
        }
}

?>
