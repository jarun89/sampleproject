<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Riversource_Monthly_Metric_Report_model extends CRM_Model {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('lead_stage_helper');
        $this->stg = getLeadStage();
        $this->stages = @implode('","', $this->stg);
    }

    public function get_overall_order_counts($start_date,$end_date)
    {
        $this->db->select('COUNT(*) as total_count');
	    $this->db->from('v_riversource_report_order_detail');
	    $this->db->where('DATE(confirmed_close_date) between "'.$start_date.'" and "'.$end_date.'"');
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_overall_order_details($start_date,$end_date)
    {
        $company_code='riversource';
        $query = $this->db->select('count(rcs.real_tat) as total_order')
	    ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->where('j.company_code',$company_code)
	    ->where('DATE(j.confirmed_close_date) between "'.$start_date.'" and "'.$end_date.'"');
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row_array();
            $data = $result['total_order'];
        }
        return $data;
    }

    public function get_avg_order_tat($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $company_code='riversource';
        $query = $this->db->select('sum(rcs.real_tat) as total_tat, count(rcs.real_tat) as total_order')
	    ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->where_in('j.lead_stage',$arr_stage)
        ->where('j.company_code',$company_code)
        ->where('DATE(j.date_created) between "'.$start_date.'" and "'.$end_date.'"')
	    ->where('DATE(j.confirmed_close_date) between "'.$start_date.'" and "'.$end_date.'"');
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0;     
        if ($query !== FALSE && $query->num_rows() > 0) {
            $result = $query->row_array();
            $data = $result;
        }
        // print_r($data);
        return $data;
    }

    public function get_avg_order_tat_with_special_authorization($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $company_code='riversource';
        $RiverSource_with_special_authorization=$this->config->item('crm')['RiverSource_with_special_authorization'];
        $query = $this->db->select('sum(rcs.real_tat) as total_tat, count(rcs.real_tat) as total_order')
	    ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->join('crm_logs as cl', 'cl on cl.jobid_fk=j.lead_id', 'left')
        ->where_in('j.lead_stage',$arr_stage)
        ->where('j.company_code',$company_code)
        ->where_in("cl.disposition_id",$RiverSource_with_special_authorization)
        ->where('DATE(j.date_created) between "'.$start_date.'" and "'.$end_date.'"')
	    ->where('DATE(j.confirmed_close_date) between "'.$start_date.'" and "'.$end_date.'"');
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row_array();
            $data = $result;
        }
        return $data;
    }

    public function get_avg_order_tat_without_special_authorization($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $company_code='riversource';
        $RiverSource_with_special_authorization=$this->config->item('crm')['RiverSource_with_special_authorization'];
        $query = $this->db->select('sum(rcs.real_tat) as total_tat, count(rcs.real_tat) as total_order')
	    ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->join('crm_logs as cl', 'cl on cl.jobid_fk=j.lead_id', 'left')
        ->where_in('j.lead_stage',$arr_stage)
        ->where('j.company_code',$company_code)
        ->where_not_in("cl.disposition_id",$RiverSource_with_special_authorization)
        ->where('DATE(j.date_created) between "'.$start_date.'" and "'.$end_date.'"')
	    ->where('DATE(j.confirmed_close_date) between "'.$start_date.'" and "'.$end_date.'"');
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row_array();
            $data = $result;
        }
        return $data;
    }

    public function get_avg_return_special_authorization($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $company_code='riversource';
        $RiverSource_Return_Special_Authorization=$this->config->item('crm')['RiverSource_Return_Special_Authorization'];
        $query = $this->db->select('`j`.`lead_id`, `j`.`company_code`, `j`.`lead_stage`, `l`.`jobid_fk`, `l`.`date_created`,l.callback_date,DATEDIFF(l.callback_date,l.date_created) AS days_difference,l.disposition_id')
        ->from('crm_leads as j')
        ->join('crm_logs as l', 'l.jobid_fk=j.lead_id', 'left')
        ->where('DATE(j.confirmed_close_date) BETWEEN "'. $start_date. '" AND "'. $end_date. '"')
        ->where_in('j.lead_stage', $arr_stage)
        ->where('j.company_code',$company_code)
        ->where_in('l.disposition_id', $RiverSource_Return_Special_Authorization)
        ->get();
   		    // echo $this->db->last_query(); die;
        $data = 0;
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->result_array();
            $data = $result;
        }
        return $data;
    }

    // public function get_avg_return_special_authorization_days($start_date,$end_date)
    // {
    //     $arr_stage=array(36,37);
    //     $company_code='riversource';
    //     $RiverSource_Return_Special_Authorization=$this->config->item('crm')['RiverSource_Return_Special_Authorization'];
    //     $query = $this->db->select('sum(rcs.real_tat) as total_tat, count(rcs.real_tat) as total_order')
    //     ->from('crm_leads as j')
    //     ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
    //     ->where('DATE(j.confirmed_close_date) BETWEEN "'. $start_date. '" AND "'. $end_date. '"')
    //     ->where_in('j.lead_stage', $arr_stage)
    //     ->where('j.company_code',$company_code)
    //     ->where_in('j.order_disposition_id', $RiverSource_Return_Special_Authorization)
    //     ->get();
    //     // echo $this->db->last_query(); die;
    //     $data = 0;
    //     if($query !== FALSE && $query->num_rows() > 0){
    //         $result = $query->row_array();
    //         $data = $result;
    //     }
    //     return $data;
    // }

    // public function get_avg_return_special_authorization_days_cal()
    // {
    //     $company_code='riversource';
    //     $RiverSource_Return_Special_Authorization=$this->config->item('crm')['RiverSource_Return_Special_Authorization'];
    //     $query = $this->db->select('*')
    //     ->from('crm_leads as j')
    //     ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
    //     ->where('j.company_code',$company_code)
    //     ->where('j.lead_stage NOT IN (36,37)')
    //     ->where_in('j.order_disposition_id', $RiverSource_Return_Special_Authorization);
    //     $query = $this->db->get();
    //     // echo $this->db->last_query(); die;
    //     $data = 0;
    //     if ($query !== FALSE && $query->num_rows() > 0) {
    //         $result = $query->result_array();
    //         $data = $result;
    //     }
    //     return $data;
    // }

    public function get_avg_cycle_time($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $company_code='riversource';
        $RiverSource_Avg_cycle_time=$this->config->item('crm')['RiverSource_Avg_cycle_time'];
        $query = $this->db->select('sum(rcs.real_tat) as total_tat, count(rcs.real_tat) as total_order')
	    ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->join('crm_logs as l', 'l.jobid_fk=j.lead_id', 'left')
        ->where('j.company_code',$company_code)
        // ->where('DATE(j.date_created) between "'.$start_date.'" and "'.$end_date.'"')
	    ->where('DATE(j.confirmed_close_date) between "'.$start_date.'" and "'.$end_date.'"')
        ->where_in('l.disposition_id', $RiverSource_Avg_cycle_time)
        ->where_in('j.lead_stage', $arr_stage)
        ->get();
    	// echo $this->db->last_query(); die;
        $data = 0;
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row_array();
            $data = $result;
        }
        return $data;
    }

    public function get_avg_records_fee($start_date,$end_date)
    {
        $query = "SELECT sum(if(pay.sign_amount_type=0,paid_amount,(-1 * ABS(paid_amount)))) as amount FROM crm_payment_records AS pay LEFT JOIN crm_leads AS ld ON ld.lead_id = pay.lead_id WHERE ld.lead_stage IN (36, 37) AND ld.company_code='riversource' AND DATE(pay.created_on) BETWEEN '".$start_date."' AND '".$end_date."'";
        $query = $this->db->query($query);
        // echo $this->db->last_query(); exit;
        $data = 0;
        if ($query !== FALSE && $query->num_rows() > 0) {
            $result = $query->row();
            $data = $result->amount;
        }
        return $data;
    }

    public function get_opted_out_vol($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $RiverSource_opted_out_vol=$this->config->item('crm')['RiverSource_opted_out_vol'];
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->join('crm_logs as l', 'l.jobid_fk=j.lead_id', 'left');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $this->db->where("l.disposition_id",$RiverSource_opted_out_vol);
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }


    public function get_time_out_vol($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $RiverSource_time_out_vol=$this->config->item('crm')['RiverSource_time_out_vol'];
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->join('crm_logs as l', 'l.jobid_fk=j.lead_id', 'left');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $this->db->where("l.disposition_id",$RiverSource_time_out_vol);
        $query = $this->db->get(); 
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_comp_vol($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $RiverSource_comp_vol=$this->config->item('crm')['RiverSource_comp_vol'];
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->join('crm_logs as l', 'l.jobid_fk=j.lead_id', 'left');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $this->db->where("l.disposition_id",$RiverSource_comp_vol);
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_inprogress_vol($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $RiverSource_inprogress_vol_event_type=$this->config->item('crm')['RiverSource_inprogress_vol_event_type'];
        $RiverSource_inprogress_vol_stage=$this->config->item('crm')['RiverSource_inprogress_vol_stage'];
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->join('crm_logs as l', 'l.jobid_fk=j.lead_id', 'left');
        $this->db->where('DATE(j.date_created) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_not_in("j.lead_stage",$arr_stage);
        $this->db->where("l.lead_stage",$RiverSource_inprogress_vol_stage);
        $this->db->where("l.disposition_id",$RiverSource_inprogress_vol_event_type);
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_cases_requiring_special_authorization($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $RiverSource_with_special_authorization=$this->config->item('crm')['RiverSource_with_special_authorization'];
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("lead_stage",$arr_stage);
        $this->db->where_in("id",$RiverSource_with_special_authorization);
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_completed_cancelled_order_details($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    /*dinesh-03-01-24*/

    public function get_completed_order_details($start_date,$end_date)
    {
        $arr_stage=array(36);
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $this->db->where_in("close_code_id",$this->config->item('crm')['RiverSource_volume_completed']);
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_cancelled_order_details($start_date,$end_date)
    {
        $arr_stage=array(37);
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $this->db->where_in("close_code_id",$this->config->item('crm')['RiverSource_volume_cancelled']);
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_pending_order_details($start_date,$end_date)
    {
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->where('DATE(j.date_created) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where('j.lead_stage NOT IN (36,37)');
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_avg_pending_orderage()
    {
        $company_code='riversource';
        $query = $this->db->select('sum(rcs.real_tat) as total_tat, count(rcs.real_tat) as total_order')
	    ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->where('j.company_code',$company_code)
        ->where('j.lead_stage NOT IN (36,37)');
        $query = $this->db->get();
	    // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row_array();
            $data = $result;
        }
        return $data;
    }

    public function get_avg_pending_orderage_days()
    {
        $company_code='riversource';
        $query = $this->db->select('j.lead_id, j.company_code, j.lead_stage, rcs.real_tat')
        ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->where('j.company_code',$company_code)
        ->where('j.lead_stage NOT IN (36,37)');
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0;
        if ($query !== FALSE && $query->num_rows() > 0) {
            $result = $query->result_array();
            $data = $result;
        }
        return $data;
    }

    public function get_all_completed_cancelled_orders($start_date,$end_date)
    {
        $arr_stage=array(36,37);
        $this->db->select('COUNT(*) as total_count');
        $this->db->from('v_riversource_report_order_detail as j');
        $this->db->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"');
        $this->db->where_in("j.lead_stage",$arr_stage);
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0; 
        if($query !== FALSE && $query->num_rows() > 0){
            $result = $query->row();
            $data = $result->total_count;
        }
        return $data;
    }

    public function get_all_completed_cancelled_orders_days($start_date,$end_date)
    {
        $company_code='riversource';
        $query = $this->db->select('j.lead_id, j.company_code, j.lead_stage, j.confirmed_close_date, rcs.real_tat')
        ->from('crm_leads as j')
        ->join('crm_real_calc_stop_tat as rcs', 'rcs on rcs.lead_id=j.lead_id', 'left')
        ->where('j.company_code',$company_code)
        ->where('DATE(j.confirmed_close_date) BETWEEN "'.$start_date.'" AND "'.$end_date.'"')
        ->where('j.lead_stage IN (36,37)');
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0;
        if ($query !== FALSE && $query->num_rows() > 0) {
            $result = $query->result_array();
            $data = $result;
        }
        return $data;
    }

    public function get_avg_days_last_status_update()
    {
        $company_code='riversource';
        $query = $this->db->select('j.lead_id,j.company_code,j.lead_stage,MAX(l.logid) AS max_logid,l.jobid_fk,l.date_created')
        ->from('crm_leads as j')
        ->join('crm_logs as l', 'l on l.jobid_fk=j.lead_id', 'left')
        ->where('j.company_code',$company_code)
        ->where('j.lead_stage NOT IN (36,37)')
        ->group_by('j.lead_id')
        ->order_by('l.date_created');
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        $data = 0;
        if ($query !== FALSE && $query->num_rows() > 0) {
            $result = $query->result_array();
            $data = $result;
        }
        return $data;
    }

}
?>